/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement;

public enum Issuer {
    LEGAL_EXPERT("Legal Expert", "LegalExpert"), POLICY_EXPERT("Policy Expert","PolicyExpert"), USER("User","User");

    String label;
    String role;

    Issuer(String label, String role) {
        this.label = label;
        this.role = role;
    }

    public String getLabel(Issuer issuer) {
        return issuer.getLabel();
    }

    public String getRole(Issuer issuer) {
        return issuer.getRole();
    }

    public String getLabel() {
        return label;

    }
    public String getRole() {
        return role;

    }

}
