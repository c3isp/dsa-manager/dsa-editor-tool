/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.shared;

import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Organization;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Util;


public class Config extends Properties {
	private static final String propertiesFile = "etc/config/config.properties";
	private static Config THE_INSTANCE = null;
	private static final Log log = LogFactory.getLog(Config.class);

	//    public String testProp = null;

	public static final String propNameParties = "parties";
	public static final String propNameContext_providers = "context-providers";
	public static final String propNameEncryptionKeySchema = "encryption-key-schema";
	public static final String propNameUpperVocabularyUri = "upperVocabularyUri";
	public static final String propNameBasePath = "basePath";
	public static final String propNameDsaApi = "dsaApi";
	public static final String propAvailableVocabularyList = "availableVocabularyList";


	private List<Organization> parties = new ArrayList<Organization>();
	private List<Organization> trustedProviders = new ArrayList<Organization>();
	private String encryptionKeySchema = "";
	private String upperVocabularyUri = "";
	private String basePath = "";
	private String dsaApi = "";
	private String availableVocabularyList = "";

	private static final String JVM_ARG_PROFILE = "-Dspring.profiles.active=";

	public static class Props {
		public String name;
		public Map<String, String> source = new HashMap<>();

		public Props() {}
	}

	public static class JConf {
		public String name;
		public List<Props> propertySources = new ArrayList<>();
		public JConf() {}

		public static JConf fromJsonString(String json) {
			Gson g = new GsonBuilder().create();
			JConf jc = g.fromJson(json, JConf.class);
			return jc;
		}
	}


	private String loadFromLocalFile() {
		//read from local file
		log.info("---- Trying to load configuration from local file ----");
		InputStream is = null;
		try {
			is = getClass().getClassLoader().getResourceAsStream(propertiesFile);
			this.load(is);
			return initConf();
		} catch (IOException e) {
			log.error(e);
			return "cannot read " + propertiesFile;
		}finally {
			if(is != null) {
				try {is.close();}catch(Exception e) {}
			}
		}
	}


	private static String parseProfile(String jvmArg) {
		//-Dspring.profiles.active=testbed,rest
		if(jvmArg == null) {
			return null;
		}
		int ind = jvmArg.indexOf("=");
		if(ind <=0 || ind == jvmArg.length() - 1) {
			return null;
		}
		return jvmArg.substring(ind + 1);
	}

	private String loadFromApi(String jvmArg) {
		log.info("---- Trying to load configuration from  API ----");
		//parse profile string
		//-Dspring.profiles.active=testbed,rest
		String profile = parseProfile(jvmArg);
		if(profile == null) {
			return "bad jvm argument, expected soemthing like -Dspring.profiles.active=testbed,rest";
		}

		Bootstrap bs = Bootstrap.getInstance();

//		final static String url = "http://localhost:8888/dsa-editor/testbed,rest";
//		final static String username = "configServer";
//		final static String password = "3fbc39fa-cc61-4f1b-a7a5-2b58f972fee7";
		final String url = String.format("%s/%s/%s",
				bs.spring_cloud_config_uri == null?"":bs.spring_cloud_config_uri,
						bs.spring_application_name == null?"":bs.spring_application_name,
								profile == null?"":profile
				);

		log.debug("config server url created: " + url);
		String jsonConf = Config.getRemoteConf(url, bs.spring_cloud_config_username, bs.spring_cloud_config_password);
		if(jsonConf == null) {
			return "Reading from confServer failed";
		}

		//parse json and convert to properties

    	JConf jc = JConf.fromJsonString(jsonConf);

    	if(jc != null && jc.propertySources != null && jc.propertySources.size() > 0) {
    		Props props = jc.propertySources.get(0);
    		if(props != null && props.source != null && props.source.size() > 0) {
    			Map<String, String> m = props.source;
    			log.debug("Parsed properties");
    			for(String k:m.keySet()) {
    				String v = m.get(k);
    				log.debug(String.format("%s ---> %s",k, v == null?"no value found ":v));
    				this.setProperty(k, v);
    			}
    		}
    	}

		return initConf();
	}

	private String initConf() {
		StringBuilder msg = new StringBuilder();
		String err = "";

		String orgsStr = getProperty(propNameParties);
		err = loadOrganizations(orgsStr,this.parties);
		if(!Util.isEmpty(err)) {
			msg.append(" " + propNameParties + ": " + err + "\n");
		}


		String cprovStr = getProperty(propNameContext_providers);
		err = loadOrganizations(cprovStr, this.trustedProviders);
		if(!Util.isEmpty(err)) {
			msg.append(" " + propNameContext_providers + ": " + err + "\n");
		}


		this.encryptionKeySchema = getProperty(propNameEncryptionKeySchema);
		if(Util.isEmpty(this.encryptionKeySchema )) {
			msg.append(" " + propNameEncryptionKeySchema + " not found\n");
		}else {
			this.encryptionKeySchema = this.encryptionKeySchema.trim();
		}

		this.upperVocabularyUri = getProperty(propNameUpperVocabularyUri);
		if(Util.isEmpty(this.upperVocabularyUri )) {
			msg.append(" " + propNameUpperVocabularyUri + " not found\n");
		}else {
			this.upperVocabularyUri = this.upperVocabularyUri.trim();
		}


		this.basePath = getProperty(propNameBasePath);
		if(Util.isEmpty(this.basePath )) {
			msg.append(" " + propNameBasePath + " not found\n");
		}else {
			this.basePath = this.basePath.trim();
		}


		this.dsaApi = getProperty(propNameDsaApi);
		if(Util.isEmpty(this.dsaApi )) {
			msg.append(" " + propNameDsaApi + " not found\n");
		}else {
			this.dsaApi = this.dsaApi.trim();
		}

		this.availableVocabularyList = getProperty(propAvailableVocabularyList);
		if(Util.isEmpty(this.availableVocabularyList )) {
			msg.append(" " + propAvailableVocabularyList + " not found\n");
		}else {
			this.availableVocabularyList = this.availableVocabularyList.trim();
		}

		if(msg.length() > 0) {
			return msg.toString();
//			log.error("Configuration problems.\n" + msg.toString());
		}else {
			return null;
		}
	}



	private Config() {
		log.info("---- Configuration initialization ----");

		boolean loaded = false;

		RuntimeMXBean runtimeMxBean = ManagementFactory.getRuntimeMXBean();
		List<String> arguments = runtimeMxBean.getInputArguments();

		if(arguments == null || arguments.size() == 0) {
			log.debug(" No JVM arguments found");
		}else {
			for(String s:arguments) {
				if(s.startsWith(JVM_ARG_PROFILE)) {
					log.debug(" Interesting JVM argument found: " + s);
					String err = loadFromApi(s);
					if(err == null) {
						loaded = true;
						log.debug(" Configuration successfully loaded from confServer ");
					}else {
						log.error(" Error while loading configuration from confServer\n" + err);
					}
					break;
				}
			}
		}

		if(!loaded) {
			String err = loadFromLocalFile();
			if(err == null) {
				loaded = true;
				log.debug(" Configuration successfully loaded from localFile ");
			}else {
				log.error(" Error while loading configuration from localFile: " + err);
			}
		}

		if(loaded) {
			StringBuilder msg = new StringBuilder();
			msg.append("Configuration content\n");

			msg.append(" #parties:" + parties.size() + "\n");
			msg.append(" #trustedProviders:" + trustedProviders.size() + "\n");
			msg.append(" encryptionKeySchema:" + encryptionKeySchema + "\n");
			msg.append(" upperVocabularyUri:" + upperVocabularyUri + "\n");
			msg.append(" basePath:" + basePath + "\n");
			msg.append(" dsaApi:" + dsaApi + "\n");
			msg.append(" availableVocabularyList:" + availableVocabularyList + "\n");

			log.info(msg);
		}

//		InputStream is = getClass().getClassLoader().getResourceAsStream(propertiesFile);
//
//		if (is != null) {
//			try {
//				StringBuilder msg = new StringBuilder();
//				this.load(is);
//
//				String err = "";
//
//				String orgsStr = getProperty(propNameParties);
//				err = loadOrganizations(orgsStr,this.parties);
//				if(!Util.isEmpty(err)) {
//					msg.append(" " + propNameParties + ": " + err + "\n");
//				}
//
//
//				orgsStr = getProperty(propNameContext_providers);
//				err = loadOrganizations(orgsStr, this.trustedProviders);
//				if(!Util.isEmpty(err)) {
//					msg.append(" " + propNameContext_providers + ": " + err + "\n");
//				}
//
//
//				this.encryptionKeySchema = getProperty(propNameEncryptionKeySchema);
//				if(Util.isEmpty(this.encryptionKeySchema )) {
//					msg.append(" " + propNameEncryptionKeySchema + " not found\n");
//				}else {
//					this.encryptionKeySchema = this.encryptionKeySchema.trim();
//				}
//
//				this.upperVocabularyUri = getProperty(propNameUpperVocabularyUri);
//				if(Util.isEmpty(this.upperVocabularyUri )) {
//					msg.append(" " + propNameUpperVocabularyUri + " not found\n");
//				}else {
//					this.upperVocabularyUri = this.upperVocabularyUri.trim();
//				}
//
//
//				this.basePath = getProperty(propNameBasePath);
//				if(Util.isEmpty(this.basePath )) {
//					msg.append(" " + propNameBasePath + " not found\n");
//				}else {
//					this.basePath = this.basePath.trim();
//				}
//
//
//				this.dsaApi = getProperty(propNameDsaApi);
//				if(Util.isEmpty(this.dsaApi )) {
//					msg.append(" " + propNameDsaApi + " not found\n");
//				}else {
//					this.dsaApi = this.dsaApi.trim();
//				}
//
//				this.availableVocabularyList = getProperty(propAvailableVocabularyList);
//				if(Util.isEmpty(this.availableVocabularyList )) {
//					msg.append(" " + propAvailableVocabularyList + " not found\n");
//				}else {
//					this.availableVocabularyList = this.availableVocabularyList.trim();
//				}
//
//				if(msg.length() > 0) {
//					log.error("Configuration problems.\n" + msg.toString());
//				}
//
//				//debug
//				msg.setLength(0);
//				msg.append("Configuration loaded\n");
//
//				msg.append(" #parties:" + parties.size() + "\n");
//				msg.append(" #trustedProviders:" + trustedProviders.size() + "\n");
//				msg.append(" encryptionKeySchema:" + encryptionKeySchema + "\n");
//				msg.append(" upperVocabularyUri:" + upperVocabularyUri + "\n");
//				msg.append(" basePath:" + basePath + "\n");
//				msg.append(" dsaApi:" + dsaApi + "\n");
//				msg.append(" availableVocabularyList:" + availableVocabularyList + "\n");
//
//
//
//				log.info(msg.toString());
//
//				//                log.info(" -----------  #Props=" + this.entrySet().size());
//			} catch (IOException e) {
//				log.error("cannot read " + propertiesFile, e);
//			}
//			try {
//				is.close();
//			} catch (IOException e) {
//				log.error("cannot close InputStream attached to "
//						+ propertiesFile);
//			}
//		} else {
//			log.error("cannot find " + propertiesFile);
//		}


	}





	//
//	private Config() {
//		log.info("---- Configuration initialization ----");
//
//		//
//
//		RuntimeMXBean runtimeMxBean = ManagementFactory.getRuntimeMXBean();
//		List<String> arguments = runtimeMxBean.getInputArguments();
//
//		if(arguments == null || arguments.size() == 0) {
//			log.debug(" Cannot read JVM arguments \n");
//		}else {
//			for(String s:arguments) {
//				if(s.startsWith(JVM_ARG_PROFILE)) {
//					log.debug(" Interesting JVM argument found: " + s);
//					String err = readFromApi(s);
//				}
//			}
//
//		}
//
//		InputStream is = getClass().getClassLoader().getResourceAsStream(propertiesFile);
//
//		if (is != null) {
//			try {
//				StringBuilder msg = new StringBuilder();
//				this.load(is);
//
//				String err = "";
//
//				String orgsStr = getProperty(propNameParties);
//				err = loadOrganizations(orgsStr,this.parties);
//				if(!Util.isEmpty(err)) {
//					msg.append(" " + propNameParties + ": " + err + "\n");
//				}
//
//
//				orgsStr = getProperty(propNameContext_providers);
//				err = loadOrganizations(orgsStr, this.trustedProviders);
//				if(!Util.isEmpty(err)) {
//					msg.append(" " + propNameContext_providers + ": " + err + "\n");
//				}
//
//
//				this.encryptionKeySchema = getProperty(propNameEncryptionKeySchema);
//				if(Util.isEmpty(this.encryptionKeySchema )) {
//					msg.append(" " + propNameEncryptionKeySchema + " not found\n");
//				}else {
//					this.encryptionKeySchema = this.encryptionKeySchema.trim();
//				}
//
//				this.upperVocabularyUri = getProperty(propNameUpperVocabularyUri);
//				if(Util.isEmpty(this.upperVocabularyUri )) {
//					msg.append(" " + propNameUpperVocabularyUri + " not found\n");
//				}else {
//					this.upperVocabularyUri = this.upperVocabularyUri.trim();
//				}
//
//
//				this.basePath = getProperty(propNameBasePath);
//				if(Util.isEmpty(this.basePath )) {
//					msg.append(" " + propNameBasePath + " not found\n");
//				}else {
//					this.basePath = this.basePath.trim();
//				}
//
//
//				this.dsaApi = getProperty(propNameDsaApi);
//				if(Util.isEmpty(this.dsaApi )) {
//					msg.append(" " + propNameDsaApi + " not found\n");
//				}else {
//					this.dsaApi = this.dsaApi.trim();
//				}
//
//				this.availableVocabularyList = getProperty(propAvailableVocabularyList);
//				if(Util.isEmpty(this.availableVocabularyList )) {
//					msg.append(" " + propAvailableVocabularyList + " not found\n");
//				}else {
//					this.availableVocabularyList = this.availableVocabularyList.trim();
//				}
//
//				if(msg.length() > 0) {
//					log.error("Configuration problems.\n" + msg.toString());
//				}
//
//				//debug
//				msg.setLength(0);
//				msg.append("Configuration loaded\n");
//
//				msg.append(" #parties:" + parties.size() + "\n");
//				msg.append(" #trustedProviders:" + trustedProviders.size() + "\n");
//				msg.append(" encryptionKeySchema:" + encryptionKeySchema + "\n");
//				msg.append(" upperVocabularyUri:" + upperVocabularyUri + "\n");
//				msg.append(" basePath:" + basePath + "\n");
//				msg.append(" dsaApi:" + dsaApi + "\n");
//				msg.append(" availableVocabularyList:" + availableVocabularyList + "\n");
//
//
//
//				log.info(msg.toString());
//
//				//                log.info(" -----------  #Props=" + this.entrySet().size());
//			} catch (IOException e) {
//				log.error("cannot read " + propertiesFile, e);
//			}
//			try {
//				is.close();
//			} catch (IOException e) {
//				log.error("cannot close InputStream attached to "
//						+ propertiesFile);
//			}
//		} else {
//			log.error("cannot find " + propertiesFile);
//		}
//
//
//	}


	private String loadOrganizations(String orgsStr, List<Organization> list) {
		//    	List<Organization> ret = new ArrayList<>();
		//    	String orgsStr = Config.getInstance().getProperty(key);
		String ret = "";
		if(Util.isEmpty(orgsStr)) {
			return "property not found";//
		}else {
			String[] orgs = orgsStr.trim().split(";");
			if(orgs == null || orgs.length == 0) {
				return "property malformed";
			}else {

				for (String orgStr : orgs) {
					String orgData[] = orgStr.split(",");
					if(orgData == null || orgData.length != 2) {
						if(!Util.isEmpty(ret)) {
							ret = "bad organization data found";
						}
					}else {
						final String uid = orgData[0];
						final String name = orgData[1];
						list.add(new Organization(uid, name, "", ""));
					}
				}
			}
		}
		return ret;
	}

	public static synchronized Config getInstance() {
		if (THE_INSTANCE == null) {
			THE_INSTANCE = new Config();
		}
		return THE_INSTANCE;
	}

	//--------------------------------------------------------------
	/**
	 * Retrieves an integer value.
	 * @param key The key name.
	 * @return The requested value
	 */
	public Integer getInteger(String key, Integer defaultValue){
		if(key == null || !this.containsKey(key)){
			return defaultValue;
		}
		Integer value = null;
		String string = getProperty(key);
		if (string != null){
			value = new Integer(string.trim());
		}
		if( value == null ){
			value = defaultValue;
		}
		return value;
	}



	public static String getRemoteConf(String url, String username, String password) {
		String ret = null;
		String auth = username + ":" + password;

		byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(Charset.forName("US-ASCII")) );
		String authHeader = "Basic " + new String( encodedAuth );

		//       DsaDocument dsaDocument = null;
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add( new ByteArrayHttpMessageConverter());

		HttpHeaders headers = new HttpHeaders();
		//       headers.setAccept(Arrays.asList(MediaType.APPLICATION_XML));
		headers.set( "Authorization", authHeader );

		HttpEntity<String> entity = new HttpEntity<String>(headers);

		try {
			ResponseEntity<byte[]> response = restTemplate.exchange(url, HttpMethod.GET, entity, byte[].class);
			HttpStatus status = response.getStatusCode();
			if (status == HttpStatus.OK){
				ret = IOUtils.toString(response.getBody(), "UTF-8");
				log.debug("Remote configuration json read\n" + ret);
			}else {
				//			ret = "Error occurred while reading from configServer, http response code = " + status;
				log.error("Error occurred while reading from configServer - http response code = " + status);
			}
		} catch (Exception e) {
			//				ret = "Error occurred while reading from configServer";
			log.error("Error occurred while reading from configServer\n", e);
		}
		return ret;
	}



	private void loadList(Properties properties, String key, List<Organization> list) {
		if (properties.containsKey(key)) {
			String orgsStr = properties.getProperty(key);
			String[] orgs = orgsStr.split(";");
			assert orgs != null && orgs.length > 0 : "missing parties";
			for (String orgStr : orgs) {
				String orgData[] = orgStr.split(",");
				assert orgData != null && orgData.length == 2 : "bad organization data";
				list.add(new Organization(orgData[0], orgData[1], "", ""));
			}
		} else {
			log.error("cannot find property " + key);
		}
	}



	public List<Organization> getParties() {
		List<Organization> result = new ArrayList<Organization>(parties.size());
		result.addAll(parties);
		return result;
	}

	public List<Organization> getTrustedProviders() {
		List<Organization> result = new ArrayList<Organization>(
				trustedProviders.size());
		result.addAll(trustedProviders);
		return result;
	}

	public Organization getTrustedProvider(String uid) {
		for (Organization org : trustedProviders)
			if (uid.equals(org.getUid()))
				return org;
		return null;
	}

	public Organization getTrustedProviderFromName(String name) {
		for (Organization org : trustedProviders)
			if (name.equals(org.getName()))
				return org;
		return null;
	}

	public String getEncryptionKeySchema() {
		return encryptionKeySchema;
	}

	public String getUpperVocabularyUri() {
		return upperVocabularyUri;
	}

	public String getBasePath() {
		return basePath;
	}

	public String getDsaApi() {
		return dsaApi;
	}

	public String getAvailableVocabularyList() {
		return availableVocabularyList;
	}




}
