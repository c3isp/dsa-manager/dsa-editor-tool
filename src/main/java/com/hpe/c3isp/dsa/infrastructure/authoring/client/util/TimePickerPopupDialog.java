package com.hpe.c3isp.dsa.infrastructure.authoring.client.util;

import java.util.Date;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.tractionsoftware.gwt.user.client.ui.UTCTimeBox;

public class TimePickerPopupDialog extends DialogBox{
   
    private String timeSelected = "";
    private int row;
    VerticalPanel panel = new VerticalPanel();
    public TimePickerPopupDialog() {
        // Set the dialog box's caption.
       
        setText("Time Picker Dialog");

        // Enable animation.
        setAnimationEnabled(true);

        // Enable glass background.
        setGlassEnabled(true);
        Button ok = new Button("OK");
        ok.addClickHandler(new ClickHandler() {
           public void onClick(ClickEvent event) {
               TimePickerPopupDialog.this.hide();
           }
        });

        Button cancel = new Button("Cancel");
        cancel.addClickHandler(new ClickHandler() {
           public void onClick(ClickEvent event) {
               timeSelected = null;
               TimePickerPopupDialog.this.hide();
           }
        });
       // DateTimeFormat format = DateTimeFormat.getFormat(PredefinedFormat.TIME_FULL);
        DateTimeFormat format = DateTimeFormat.getFormat("HH:mm:ss+02:00");
        
        //format 18:00:00+02:00
        UTCTimeBox time = new UTCTimeBox(format);
        time.setTitle("Select a value time clicking on the text box");
        time.addValueChangeHandler(new ValueChangeHandler<Long>() {
            
            @Override
            public void onValueChange(ValueChangeEvent<Long> event) {
               long value = event.getValue();
             //  timeSelected = DateTimeFormat.getFormat("hh:mm:ss +02:00").format(new Date(event.getValue()));       
               //this fix a bug of the time widget..
               long hour = 60 * 60 * 1000;
               value = value -hour;
               timeSelected= DateTimeFormat.getFormat("HH:mm:ss+02:00").format(new Date(value));
            }
        });      
        time.setValue(UTCTimeBox.getValueForNextHour());
        panel.setSpacing(10);
        panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        panel.add(new Label("Select a value time from the drop-down menu"));   
        panel.add(time);
        HorizontalPanel buttonPanel = new HorizontalPanel();
        buttonPanel.add(ok);
        buttonPanel.add(cancel);
        panel.add(buttonPanel);        
        setWidget(panel);
     }
    
    public String getTimeSelected() {
        return timeSelected;
    }
    public void setDateValueAsString(String timeAsString) {
        this.timeSelected = timeAsString;
    }
    
    public int getRow() {
        return row;
    }


    public void setRow(int row) {
        this.row = row;
    }
    
}
