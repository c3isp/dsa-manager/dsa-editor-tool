/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa;

public class VariableReference extends AbstractSyntacticItem {

    private String variable;
    private String syntacticForm;

    // for serialization
    VariableReference() {
    }

    public VariableReference(String uid, String humanForm, String variable, String value) {
        super(uid, humanForm, value);
        this.variable = variable;
        this.syntacticForm = variable;
    }

    @Override
    public SyntacticItem copyOf() {
        return new VariableReference(uid, humanForm, variable,value);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
                + ((syntacticForm == null) ? 0 : syntacticForm.hashCode());
        result = prime * result
                + ((variable == null) ? 0 : variable.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        VariableReference other = (VariableReference) obj;
        if (syntacticForm == null) {
            if (other.syntacticForm != null)
                return false;
        } else if (!syntacticForm.equals(other.syntacticForm))
            return false;
        if (variable == null) {
            if (other.variable != null)
                return false;
        } else if (!variable.equals(other.variable))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "VariableReference [syntacticForm=" + syntacticForm
                + ", variable=" + variable + ", humanForm=" + getHumanForm()
                + ", uid=" + uid + "]";
    }

    @Override
    public String getSyntacticForm() {
        return syntacticForm;
    }

    public String getVariable() {
        return variable;
    }

    public String getType() {
        return uid;
    }

    public boolean refersTo(VariableDeclaration vd) {
        return variable.equals(vd.getVariable()) && uid.equals(vd.getUid());
    }

    @Override
    public String getHumanForm() {
        if (humanForm.startsWith("an "))
            return "that" + humanForm.substring(2);
        if (humanForm.startsWith("a "))
            return "that" + humanForm.substring(1);
        return "that " + humanForm;
    }

}
