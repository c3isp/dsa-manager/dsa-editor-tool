/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.hpe.c3isp.dsa.infrastructure.authoring.server.DsaApiServiceManager;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa.helper.DsaStorage;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.helper.VocabularyManager;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.helper.VocabularyOntology;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.Config;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Functor;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Term;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Token;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Tokens;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableDeclaration;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableReference;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.dsa.FetchDsaAction;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.dsa.FetchDsaResult;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.dsa.FetchDsaResultImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.ComplexPolicy;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Credentials;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.DsaBean;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.DsaBeanUtils;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Organization;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Validity;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.roles.CredentialExpression;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.roles.Operator;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.roles.Roles;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.Issuer;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.Statement;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.StatementMetadata;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.StatementType;

import eu.cocoCloud.dsa.AuthorisationsCategory;
import eu.cocoCloud.dsa.AuthorizationType;
import eu.cocoCloud.dsa.AuthorizationsType;
import eu.cocoCloud.dsa.ComplexPolicyType;
import eu.cocoCloud.dsa.DataType;
import eu.cocoCloud.dsa.DatumType;
import eu.cocoCloud.dsa.DsaDocument;
import eu.cocoCloud.dsa.DsaType;
import eu.cocoCloud.dsa.EntityType;
import eu.cocoCloud.dsa.ExpressionType;
import eu.cocoCloud.dsa.IssuerType;
import eu.cocoCloud.dsa.LanguageType;
import eu.cocoCloud.dsa.ObligationType;
import eu.cocoCloud.dsa.ObligationsCategory;
import eu.cocoCloud.dsa.ObligationsType;
import eu.cocoCloud.dsa.PartiesType;
import eu.cocoCloud.dsa.PolicyType;
import eu.cocoCloud.dsa.ProhibitionType;
import eu.cocoCloud.dsa.ProhibitionsType;
import eu.cocoCloud.dsa.StatusType;
import eu.cocoCloud.dsa.ValidityType;
import net.customware.gwt.dispatch.server.ActionHandler;
import net.customware.gwt.dispatch.server.ExecutionContext;
import net.customware.gwt.dispatch.shared.ActionException;
import net.customware.gwt.dispatch.shared.DispatchException;

public class FetchDsaActionHandler implements
        ActionHandler<FetchDsaAction, FetchDsaResult> {

    private static Log log = LogFactory.getLog(FetchDsaActionHandler.class);

    @Override
    public FetchDsaResult execute(FetchDsaAction action,
            ExecutionContext context) throws DispatchException {
        return new FetchDsaResultImpl(buildDsaBean(action.getDsaUid(),
                action.getIssuer(), action.isStandalone()));
    }

    @Override
    public Class<FetchDsaAction> getActionType() {
        return FetchDsaAction.class;
    }


    @Override
    public void rollback(FetchDsaAction action, FetchDsaResult result,
            ExecutionContext context) throws DispatchException {
        throw new UnsupportedOperationException();
    }

    private DsaBean buildDsaBean(String dsaUid, String issuer, boolean isStandalone)
            throws ActionException {
        log.info("executing buildDsaBean");
        DsaDocument dsaDocument = null;
        if (isStandalone){
            //get DSA from local repository
            log.info("Get DSA from local repository.");
            dsaDocument = DsaStorage.getInstance().read(dsaUid);
        }else{
            //invoke DSA API to get the DSA
            log.info("Invoke DSA API to get the DSA.");
            DsaApiServiceManager dsaSvcMgr = new DsaApiServiceManager();
            dsaDocument = dsaSvcMgr.loadDSA(dsaUid);
        }

        if (dsaDocument == null)
            throw new ActionException("Cannot read DSA with UID " + dsaUid
                    + " from storage");

        DsaType dsa = dsaDocument.getDsa();
        if (dsa == null)
            throw new ActionException("cannot parse XML content of DSA with "
                    + "UID " + dsaUid);

        if (!dsa.getId().equals(dsaUid))
            throw new ActionException("inconsistent DSA UID: passed UID = "
                    + dsaUid + "; XML file contains attribute id = "
                    + dsa.getId());

        String vocabularyUri = dsa.getVocabularyUrl();
        VocabularyOntology vocont = VocabularyManager.getInstance()
                .getVocabularyOntology(vocabularyUri);
        if (vocont == null)
            throw new ActionException("cannot read vocabulary " + vocabularyUri
                    + " for DSA with UID " + dsaUid);

        DsaBean dsaBean = DsaBeanUtils
                .createEmptyDsaBean(dsaUid, vocabularyUri, vocont.getParamTermList());
        dsaBean.setDescription(dsa.getDescription());
        dsaBean.setUpdatePolicy(buildComplexPolicyType(dsa.getUpdatePolicy()));
        dsaBean.setExpirationPolicy(buildComplexPolicyType(dsa.getExpirationPolicy()));
        dsaBean.setRevocationPolicy(buildComplexPolicyType(dsa.getRevocationPolicy()));
        dsaBean.setKeyEncryptionSchema(dsa.getEncryptionKeySchema());
        dsaBean.setUid(dsaUid);
        dsaBean.setVocabularyUri(dsa.getVocabularyUrl());
        dsaBean.setTitle(dsa.getTitle());
        dsaBean.setVersion(dsa.getVersion());
        dsaBean.setParties(buildParties(dsa.getParties()));
        dsaBean.setValidity(buildValidity(dsa.getValidity()));
        dsaBean.setStatus(retrieveStatus(dsa.getStatus()));

        dsaBean.setPurpose(dsa.getPurpose());

        dsaBean.setApplicationDomain(dsa.getApplicationDomain());



        dsaBean.setDataClassification(dsa.getDataClassification());
        dsaBean.setIndemnities(dsa.getIndemnities());
        dsaBean.setGoverningLaw(dsa.getGoverningLaw());
        dsaBean.setUserConsent(dsa.getUserConsent());
        dsaBean.setRequiredConsent(dsa.getConsentRequired());

        DataType data = dsa.getData();

        if (data != null) {
            Map<String, VariableDeclaration> vds = buildVariableDeclarationsMap(
                    data, vocont);
            dsaBean.setAuthorizations(buildAuthorizations(
                    dsa.getAuthorizations(), vds, vocont, issuer,
                    StatementType.AUTHORIZATION));
            dsaBean.setDataSubjectAuthorizations(buildAuthorizations(
                    dsa.getDataSubjectAuthorizations(), vds, vocont, issuer,
                    StatementType.DATA_SUBJECT_AUTHORIZATION));
            dsaBean.setObligations(buildObligations(dsa.getObligations(), vds,
                    vocont, issuer, StatementType.OBLIGATION));
            dsaBean.setProhibitions(buildProhibitions(dsa.getProhibitions(),
                    vds, vocont, issuer, StatementType.PROHIBITION));
            // Third parties statements
            dsaBean.setDerivedObjectsAuthorizations(buildAuthorizations(
                    dsa.getDerivedObjectsAuthorizations(), vds, vocont, issuer,
                    StatementType.DERIVED_OBJECTS_AUTHORIZATION));
            dsaBean.setDerivedObjectsObligations(buildObligations(
                    dsa.getDerivedObjectsObligations(), vds, vocont, issuer,
                    StatementType.DERIVED_OBJECTS_OBLIGATION));
            dsaBean.setDerivedObjectsProhibitions(buildProhibitions(
                    dsa.getDerivedObjectsProhibitions(), vds, vocont, issuer,
                    StatementType.DERIVED_OBJECTS_PROHIBITION));

        }
        log.debug("buildDsaBean: " + dsaBean);
        return dsaBean;
    }

    private String retrieveStatus(eu.cocoCloud.dsa.StatusType.Enum status) {
        if (status == StatusType.COMPLETED) {
            return "COMPLETED";
        }
        if (status == StatusType.CUSTOMISED) {
            return "CUSTOMISED";
        }
        if (status == StatusType.TEMPLATE) {
            return "TEMPLATE";
        }
        if (status == StatusType.PREPARED) {
            return "PREPARED";
        }
        if (status == StatusType.AVAILABLE) {
            return "AVAILABLE";
        }
        return null;
    }

    private String buildPolicyType(PolicyType.Enum pol) {
        if (pol == PolicyType.DELETE_IN_SPECIFIED_PERIOD)
            return PolicyType.DELETE_IN_SPECIFIED_PERIOD.toString();
        if (pol == PolicyType.DENY_ALL)
            return PolicyType.DENY_ALL.toString();
        if (pol == PolicyType.DENY_ALL_AND_DELETE_NOW)
            return PolicyType.DENY_ALL_AND_DELETE_NOW.toString();
        return null;
    }
    /*
     * Builds a Roles object from a String based on the following pattern:
     *
     * Toxicologist:Authentication.group=12 AND
     * Location.country=Italy;Paramedic:Authentication.id=23
     *
     * If the string is not well formed, it returns a Roles object initialized
     * with Roles form the passed vocabulary, and empty role definitions.
     */
    private Roles buildRoles(String rolesString, VocabularyOntology vocont) {
        // rolesString has the following pattern:
        // Toxicologist:Authentication.group=12 AND
        // Location.country=Italy;Paramedic:Authentication.id=23
        Roles roles = new Roles();
        boolean isValidString = true;
        final String[] rs = rolesString.split(";");
        for (int i = 0; i < rs.length && isValidString; i++)
            isValidString = parseRoleDefinition(roles, rs[i], vocont);
        Set<String> allRoles = vocont.getRoles();
        // if the rolesString is valid we remove from allRoles those for which
        // we set a definition in the Roles object, otherwise we log a warning
        if (isValidString)
            for (Term role : roles.getRoles())
                allRoles.remove(role.getUid());
        else
            log.warn("bad roles string: " + rolesString);
        // now we set an entry in the Roles object for every item in allRoles,
        // assigning an empty definition (i.e. an empty List of
        // CredentialExpressions).
        //
        // if isValidString == false, then allRoles will contain all the roles
        // in the vocabulary, otherwise allRoles will contain only those roles
        // for which we haven't already set a definition while parsing
        // rolesString
        for (String roleUri : allRoles)
            roles.setRoleDefinition(
                    new Term(roleUri, vocont.getHumanFormOf(roleUri), vocont.getHumanFormOf(roleUri)),
                    new ArrayList<CredentialExpression>());
        return roles;
    }

    /*
     * Parses a role definition from roleString and updates the passed Roles
     * object. It returns false if the the roleString is bad, true otherwise.
     */
    private boolean parseRoleDefinition(Roles roles, String roleString,
            VocabularyOntology vocont) {
        // roleStirng has the following pattern:
        // Toxicologist:Authentication.group=12 AND Location.country=Italy
        final String[] rs = roleString.split(":");
        if (rs.length > 2) {
            return false;
        }
        final String roleUri = vocont.getFullUriOfFragment(rs[0]);
        if (roleUri == null) {
            return false;
        }
        final Term role = new Term(roleUri, vocont.getHumanFormOf(roleUri), vocont.getHumanFormOf(roleUri));
        final List<CredentialExpression> credentialExpressions = new ArrayList<CredentialExpression>();
        if (rs.length > 1) {
            // let's work on rs[1] that contains AND/OR combination of
            // attribute expressions
            final String e = rs[1].trim();
            int i = 0;
            while (i < e.length()) {
                int j = e.indexOf(".", i);
                if (j == -1 && i == 0)
                    // if in the entire string there is no '.',
                    // then the string is bad
                    return false;
                String credentialUri = vocont.getFullUriOfFragment(e.substring(
                        i, j));
                if (credentialUri == null)
                    return false;
                final Term credential = new Term(credentialUri,
                        vocont.getHumanFormOf(credentialUri), vocont.getHumanFormOf(credentialUri));
                i = j; // we want the attribute expression to include the '.'
                // let's find where the attribute expression ends
                j = nextOperator(e, i);
                if (j == -1) {
                    // there's only one CredentialExpression
                    final String attributeExpression = e.substring(i);
                    credentialExpressions.add(new CredentialExpression(
                            credential, attributeExpression, Operator.END));
                    i = e.length();
                } else {
                    // there are several CredentialExpression, so we look for an
                    // Operator
                    final String attributeExpression = e.substring(i, j);
                    i = j + 1;
                    // an operator is followed by a space
                    j = e.indexOf(" ", i);
                    if (j == -1)
                        return false;
                    final Operator operator = getOperatorFromString(e
                            .substring(i, j));
                    if (operator == null)
                        return false;
                    credentialExpressions.add(new CredentialExpression(
                            credential, attributeExpression, operator));
                    i = j + 1;
                }
            }
        }
        roles.setRoleDefinition(role, credentialExpressions);
        return true;
    }

    /*
     * Finds the index of the next Operator in the String s starting the search
     * from position i. If no operation is found it returns -1.
     */
    private int nextOperator(String s, int i) {
        final String and = " " + Operator.AND.toString() + " ";
        final String or = " " + Operator.OR.toString() + " ";
        final int ia = s.indexOf(and, i);
        final int io = s.indexOf(or, i);
        if (ia < 0)
            return io;
        else if (io < 0)
            return ia;
        else
            return Math.min(ia, io);
    }

    /*
     * Returnes the Operator corresponding to the passed String, or null.
     */
    private Operator getOperatorFromString(String opstr) {
        if (Operator.AND.toString().equals(opstr))
            return Operator.AND;
        if (Operator.OR.toString().equals(opstr))
            return Operator.OR;
        if (Operator.END.toString().equals(opstr))
            return Operator.END;
        return null;
    }

    /*
     * Builds a Credentials object from a String based on the following pattern:
     *
     * Location:ID1,ID2;Authentication:ID34
     *
     * If the String is not well formated, it returns a Credential object
     * initialized with Credentials from the passed vocabulary, and empty list
     * of trusted providers.
     */
    private Credentials buildCredentials(String credentialsString, VocabularyOntology vocont) {
        // credentialsString has the following pattern:
        // Location:ID1,ID2;Authentication:ID34
        Credentials credentials = new Credentials();
        boolean isValidString = true;
        final String[] creds = credentialsString.split(";");
        for (int i = 0; i < creds.length && isValidString; i++) {
            final String[] ps = creds[i].split(":");
            // cred has the either the pattern Location:ID1,ID2 or the pattern
            // Location:
            if (ps.length == 1 || ps.length == 2) {
                final String termUri = vocont.getFullUriOfFragment(ps[0]);
                if (termUri != null) {
                    final String humanForm = vocont.getHumanFormOf(termUri);
                    final Term credential = new Term(termUri, humanForm, humanForm);
                    // build the list of trusted context providers
                    final List<Organization> trustedProviders = new ArrayList<Organization>();
                    if (ps.length == 2) {
                        final String[] tcps = ps[1].split(",");
                        if (tcps.length > 0) {
                            for (String tcp : tcps) {
                                Organization org = Config
                                        .getInstance().getTrustedProvider(tcp);
                                if (org == null)
                                    log.warn("no Trusted Context Provider "
                                            + "has identifier " + tcp
                                            + ": skipping");
                                else
                                    trustedProviders.add(org);
                            }
                        } else {
                            isValidString = false;
                        }
                    }
                    credentials.setTrustedContextProviders(credential,
                            trustedProviders);
                } else {
                    isValidString = false;
                }
            } else {
                isValidString = false;
            }
        }

        Set<String> allCredentials = vocont.getCredentials();
        // if the credentialsString is valid we remove from allCredentials those
        // for which we set a definition in the Credentials object, otherwise we
        // log a warning
        if (isValidString)
            for (Term credential : credentials.getCredentials())
                allCredentials.remove(credential.getUid());
        else
            log.warn("bad Credentials string: " + credentialsString);
        // now we set an entry in the Credentials object for every item in
        // allCredentials, assigning an empty list of Organization objects.
        //
        // if isValidString == false, then allCredentials will contain all the
        // Credentials in the vocabulary, otherwise allCredentials will contain
        // only those Credentials for which we haven't already set a definition
        // while parsing credentialsString
        for (String credentialUri : allCredentials)
            credentials.setTrustedContextProviders(new Term(credentialUri,
                    vocont.getHumanFormOf(credentialUri),vocont.getHumanFormOf(credentialUri)),
                    new ArrayList<Organization>());
        return credentials;
    }

    private List<Organization> buildParties(PartiesType parties) {
        assert parties != null;
        List<Organization> organizations = new ArrayList<Organization>();
        for (EntityType org : parties.getOrganizationArray())
            organizations.add(new Organization(org.getId(), org.getName(), org
                    .getRole().toString(), org.getResponsibilities()));
        return organizations;
    }

    private Map<String, VariableDeclaration> buildVariableDeclarationsMap(
            DataType data, VocabularyOntology vocont) {
        Map<String, VariableDeclaration> vds = new HashMap<String, VariableDeclaration>();
        for (DatumType datum : data.getDatumArray()) {
            ExpressionType[] expressions = datum.getExpressionArray();
            for (ExpressionType expression : expressions) {
                if (expression.getLanguage().equals(LanguageType.CNL_4_DSA)) {
                    String expr = expression.getStringValue();
                    String[] exprTokens = expr.split(" ");
                    String variable = exprTokens[0];
                    assert variable != null && !"".equals(variable);
                    assert exprTokens[2].startsWith("<")
                            && exprTokens[2].endsWith(">");
                    String uid = exprTokens[2].substring(1,
                            exprTokens[2].length() - 1);
                    assert uid != null && !"".equals(uid);
                    String humanForm = vocont.getHumanFormOf(uid);
                    assert humanForm != null && !"".equals(humanForm);
                    VariableDeclaration vd = new VariableDeclaration(uid,
                            humanForm, variable, datum.getValue());
                    assert !vds.containsKey(vd);
                    vds.put(variable, vd);
                }
            }
        }
        return vds;
    }

    private List<Statement> buildAuthorizations(
            AuthorizationsType authorizations,
            Map<String, VariableDeclaration> vds, VocabularyOntology vocont,
            String issuer, StatementType statementType) {
        List<Statement> statements = new ArrayList<Statement>();

        if (authorizations != null) {
            for (AuthorizationType auth : authorizations
                    .getAuthorizationArray()) {
                auth.setCategory(AuthorisationsCategory.OTHER);
                ExpressionType[] expressions = auth.getExpressionArray();
                for (ExpressionType cnl4dsae : expressions) {
                    if (cnl4dsae.getLanguage().equals(LanguageType.CNL_4_DSA_E)) {
                        StatementMetadata metadata = new StatementMetadata(
                                buildIssuer(issuer, cnl4dsae.xgetIssuer()),
                                statementType, auth.getId(), auth.getIndex(), auth.getIsProtected(), auth.getConflict(), auth.getIsPendingRule(),auth.getStatementInfo(),auth.getInputValue());
                        statements.add(buildStatement(metadata,
                                cnl4dsae.getStringValue(), vds, vocont));
                    }
                }
            }
            Collections.sort(statements, new StatementComparator());
        }
        return statements;
    }

    private Issuer buildIssuer(String issuer, IssuerType issuerType) {
        switch (issuer) {
        case "Legal Expert":
            return Issuer.LEGAL_EXPERT;
        case "Policy Expert":
            return IssuerType.POLICY_EXPERT == issuerType.enumValue() ? Issuer.POLICY_EXPERT
                    : Issuer.LEGAL_EXPERT;
        case "User":
            return IssuerType.USER == issuerType.enumValue() ? Issuer.USER
                    : Issuer.POLICY_EXPERT;
        default:
            return null;
        }

    }

    private List<Statement> buildObligations(ObligationsType obligations,
            Map<String, VariableDeclaration> vds, VocabularyOntology vocont,
            String issuer, StatementType statementType) {
        List<Statement> statements = new ArrayList<Statement>();
        if (obligations != null) {
            for (ObligationType obl : obligations.getObligationArray()) {
                obl.setCategory(ObligationsCategory.OTHER);
                ExpressionType[] expressions = obl.getExpressionArray();
                for (ExpressionType cnl4dsae : expressions) {
                    if (cnl4dsae.getLanguage().equals(LanguageType.CNL_4_DSA_E)) {
                        StatementMetadata metadata = new StatementMetadata(
                                buildIssuer(issuer, cnl4dsae.xgetIssuer()),
                                statementType, obl.getId(), obl.getIndex(), obl.getIsProtected(), obl.getConflict(), obl.getIsPendingRule(),obl.getStatementInfo(),obl.getInputValue());
                        // TODO is it useful for us?
                        // metadata.setReferenceUid(obl.getReference());
                        statements.add(buildStatement(metadata,
                                cnl4dsae.getStringValue(), vds, vocont));
                    }
                }
            }
            Collections.sort(statements, new StatementComparator());
        }
        return statements;
    }

    private List<Statement> buildProhibitions(ProhibitionsType prohibitions,
            Map<String, VariableDeclaration> vds, VocabularyOntology vocont,
            String issuer, StatementType statementType) {
        List<Statement> statements = new ArrayList<Statement>();
        if (prohibitions != null) {
            for (ProhibitionType pro : prohibitions.getProhibitionArray()) {
                ExpressionType[] expressions = pro.getExpressionArray();
                for (ExpressionType cnl4dsae : expressions) {
                    if (cnl4dsae.getLanguage().equals(LanguageType.CNL_4_DSA_E)) {
                        StatementMetadata metadata = new StatementMetadata(
                                buildIssuer(issuer, cnl4dsae.xgetIssuer()),
                                statementType, pro.getId(), pro.getIndex(), pro.getIsProtected(), pro.getConflict(),pro.getIsPendingRule(),pro.getStatementInfo(),pro.getInputValue());
                        statements.add(buildStatement(metadata,
                                cnl4dsae.getStringValue(), vds, vocont));
                    }
                }
            }
            Collections.sort(statements, new StatementComparator());
        }
        return statements;
    }

    private static class StatementComparator implements Comparator<Statement> {

        @Override
        public int compare(Statement s1, Statement s2) {
            int i1 = s1.getIndex();
            int i2 = s2.getIndex();
            return (i1 < i2) ? -1 : (i1 == i2) ? 0 : 1;
        }

        public static <T, U extends Comparable<? super U>> Comparator<T> comparing(
                Function<? super T, ? extends U> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        public static <T, U> Comparator<T> comparing(
                Function<? super T, ? extends U> arg0,
                Comparator<? super U> arg1) {
            // TODO Auto-generated method stub
            return null;
        }

        public static <T> Comparator<T> comparingDouble(
                ToDoubleFunction<? super T> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        public static <T> Comparator<T> comparingInt(
                ToIntFunction<? super T> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        public static <T> Comparator<T> comparingLong(
                ToLongFunction<? super T> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        public static <T extends Comparable<? super T>> Comparator<T> naturalOrder() {
            // TODO Auto-generated method stub
            return null;
        }

        public static <T> Comparator<T> nullsFirst(Comparator<? super T> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        public static <T> Comparator<T> nullsLast(Comparator<? super T> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        public static <T extends Comparable<? super T>> Comparator<T> reverseOrder() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Comparator<Statement> reversed() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Comparator<Statement> thenComparing(
                Comparator<? super Statement> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public <U extends Comparable<? super U>> Comparator<Statement> thenComparing(
                Function<? super Statement, ? extends U> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public <U> Comparator<Statement> thenComparing(
                Function<? super Statement, ? extends U> arg0,
                Comparator<? super U> arg1) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Comparator<Statement> thenComparingDouble(
                ToDoubleFunction<? super Statement> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Comparator<Statement> thenComparingInt(
                ToIntFunction<? super Statement> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Comparator<Statement> thenComparingLong(
                ToLongFunction<? super Statement> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

    }

    private Statement buildStatement(StatementMetadata metadata,
            String cnl4dsa_e_Str, Map<String, VariableDeclaration> vds,
            VocabularyOntology vocont) {
        Statement statement = new Statement(metadata, vocont.getParamTermList());
        String[] strParts = cnl4dsa_e_Str.split(" ");
        for (int i = 0; i < strParts.length; i++) {
           // log.warn("strParts[i]= " + strParts[i]);
            if (strParts[i].startsWith("?")) {
                // it's either a VariableDeclaration or a Variable
                if (strParts[i].indexOf(":") > 0) {
                    // it's a VariableDeclaration
                    String[] vdparts = strParts[i].split(":");
                    String var = vdparts[0];
                    VariableDeclaration vd = vds.get(var);

                    assert vd != null;
                    statement.addSyntacticItem(vd);
                } else {
                    // it's a VariableReference
                    String var = strParts[i];
                    VariableDeclaration vd = vds.get(var);
                    assert vd != null;
                    VariableReference vr = new VariableReference(vd.getUid(),
                            vd.getHumanForm(), vd.getVariable(), vd.getValue());
                    statement.addSyntacticItem(vr);
                }
            } else {
                Token token = Tokens.getTokenHavingSyntacticForm(strParts[i],
                        metadata.getType());
                if (token != null) {
                    // it's a token
                    statement.addSyntacticItem(token);
                } else {
                    // it's a Functor
                  //  log.warn("functor fragment= " + strParts[i]);
                    String uri = vocont.getFullUriOfFragment(strParts[i]);
                    assert uri != null;
                    //log.warn("uri= " + uri);
                    String humanForm = vocont.getHumanFormOf(uri);
                    assert humanForm != null;
                    Functor functor = new Functor(uri, humanForm);
                    statement.addSyntacticItem(functor);
                }
            }
        }
        return statement;
    }

    private Validity buildValidity(ValidityType validity) {
        Date startDate = validity.getStartDate().getTime();
        assert startDate != null;

        Date endDate = validity.getEndDate().getTime();
        assert endDate != null;

        int durationInDays = validity.getOfflineLicencesDuration().getDay();
        assert durationInDays > 0;

        Validity validityBean = new Validity();
        validityBean.setStartDate(startDate);
        validityBean.setEndDate(endDate);
        validityBean.setOfflineLicencesDurationInDays(durationInDays);

        return validityBean;
    }

    private ComplexPolicy buildComplexPolicyType(ComplexPolicyType pol) {
        ComplexPolicy policyBean = new ComplexPolicy();
        policyBean.setPeriodInDays(pol.getPeriodInDays());
        String[] polList ={PolicyType.DENY_ALL.toString(),PolicyType.DENY_ALL_AND_DELETE_NOW.toString(),PolicyType.DELETE_IN_SPECIFIED_PERIOD.toString()};
        int index = pol.getComplexPolicy().intValue();
        policyBean.setPolicy(polList[index-1]);
        return policyBean;
    }

}
