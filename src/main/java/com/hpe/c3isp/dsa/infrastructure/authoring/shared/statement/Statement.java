/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableDeclaration;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableReference;

public class Statement implements IsSerializable {

    private StatementMetadata metadata;
    private List<SyntacticItem> syntacticItems;
    private HashMap<String,String> paramItems;

    // for serialization
    Statement() {
    }

    public Statement(StatementMetadata metadata, HashMap<String,String> paramItems ) {
        this(metadata, new ArrayList<SyntacticItem>(), paramItems);
    }

    public Statement(StatementMetadata metadata,
            List<SyntacticItem> syntacticItems, HashMap<String,String> paramItems) {
        this.metadata = metadata;
        this.syntacticItems = syntacticItems;
        this.paramItems = paramItems;
    }

    public int getIndex() {
        return metadata.getIndex();
    }

    public String getReferenceUid() {
        return metadata.getReferenceUid();
    }

    public void setIndex(int index) {
        metadata.setIndex(index);
    }

    public StatementType getType() {
        return metadata.getType();
    }

    public String getUid() {
        return metadata.getUid();
    }

    public StatementMetadata getStatementMetadata() {
        return metadata;
    }
    
    

    public HashMap<String,String> getParamItems() {
        return paramItems;
    }

    public void setParamItems(HashMap<String,String> paramItems) {
        this.paramItems = paramItems;
    }

    public List<SyntacticItem> getSyntacticItems() {
        return Collections.unmodifiableList(syntacticItems);
    }

    public String getHumanForm() {
        StringBuffer buffer = new StringBuffer();
        for (SyntacticItem syntacticItem : syntacticItems) {
            buffer.append(syntacticItem.getHumanForm()).append(" ");
        }
        return buffer.toString().trim();
    }

    public String getSyntacticForm() {
        StringBuffer buffer = new StringBuffer();
        for (SyntacticItem syntacticItem : syntacticItems) {
            buffer.append(syntacticItem.getSyntacticForm()).append(" ");
        }
        return buffer.toString().trim();
    }

    public void addSyntacticItem(SyntacticItem syntacticItem) {
        syntacticItems.add(syntacticItem);
    }

    public SyntacticItem getSyntacticItem(int index) {
        return syntacticItems.get(index);
    }

    public boolean isEmpty() {
        return syntacticItems.isEmpty();
    }

    /**
     * Updates itself, by removing references to the passed statement, and by
     * turning the first {@link VariableReference referring to a
     * {@link VariableDeclaration} in the list toBeRemoved into a corresponding
     * {@link VariableDeclaration} (this eliminates references to terms declared
     * into the passed statement). Note: if a {@link VariableReference} VR
     * referring to a {@link VariableDeclaration} VD in the list toBeRemoved is
     * turned into a corresponding {@link VariableDeclaration}, the VD is
     * removed from the list toBeRemoved.
     * 
     * @param statement
     *            , a statement being deleted
     * @param toBeRemoved
     *            , a list of {@link VariableDeclaration} to be checked for
     *            {@link VariableReference}; this list is updated.
     */
    public void removeReferenceTo(Statement statement,
            List<VariableDeclaration> toBeRemoved) {
        // if this statement refers to the passed one, then remove the reference
        if (metadata.getReferenceUid().equals(statement.getUid()))
            metadata.setReferenceUid("");
        // we go through our SyntacticItems, and if we find a VariableReference
        // VR that refers to a VariableDeclaration VD in the list toBeRemoved,
        // then we turn VR into a copy of VD, and we remove VD from the list
        // toBeRemoved
        for (int i = 0; i < syntacticItems.size(); i++) {
            if (syntacticItems.get(i) instanceof VariableReference) {
                VariableReference vr = (VariableReference) syntacticItems
                        .get(i);
                int removeIndex = -1;
                for (int j = 0; j < toBeRemoved.size(); j++) {
                    VariableDeclaration vd = toBeRemoved.get(j);
                    if (vr.refersTo(vd)) {
                        syntacticItems.set(i, vd.copyOf());
                        removeIndex = j;
                        break;
                    }
                }
                if (removeIndex >= 0)
                    toBeRemoved.remove(removeIndex);
            }
        }
    }

    @Override
    public String toString() {
        return "Statement [syntactic form = \"" + getSyntacticForm()
                + "\"; metadata=" + metadata + "]";
    }

    // TODO: I'm not 100% sure that this is the right way of doing it!
    @SuppressWarnings("unchecked")
    public <T extends SyntacticItem> List<T> getSyntacticItems(Class<T> clazz) {
        List<T> result = new ArrayList<T>();
        for (SyntacticItem syntacticItem : syntacticItems) {
            if (syntacticItem.getClass().equals(clazz)) {
                result.add((T) syntacticItem);
            }
        }
        return result;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((metadata == null) ? 0 : metadata.hashCode());
        result = prime * result
                + ((syntacticItems == null) ? 0 : syntacticItems.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Statement other = (Statement) obj;
        if (metadata == null) {
            if (other.metadata != null)
                return false;
        } else if (!metadata.equals(other.metadata))
            return false;
        if (syntacticItems == null) {
            if (other.syntacticItems != null)
                return false;
        } else if (!syntacticItems.equals(other.syntacticItems))
            return false;
        return true;
    }

}
