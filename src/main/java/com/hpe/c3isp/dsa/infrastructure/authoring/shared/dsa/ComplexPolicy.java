package com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa;

import com.google.gwt.user.client.rpc.IsSerializable;

public class ComplexPolicy implements IsSerializable {
    
    String policy;
    int periodInDays;
    
    public ComplexPolicy(){
    }

    public String getPolicy() {
        return policy;
    }

    public void setPolicy(String policy) {
        this.policy = policy;
    }

    public int getPeriodInDays() {
        return periodInDays;
    }

    public void setPeriodInDays(int periodInDays) {
        this.periodInDays = periodInDays;
    }

    @Override
    public String toString() {
        return "RevocationPolicy [policy=" + policy + ", periodInDays="
                + periodInDays + "]";
    }

    
    
}
