/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.hpe.c3isp.dsa.infrastructure.authoring.server.DsaApiServiceManager;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa.helper.DsaStorage;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa.helper.XmlUtil;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.helper.VocabularyManager;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.helper.VocabularyOntology;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.Config;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Term;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.dsa.CreateNewDsaAction;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.dsa.CreateNewDsaResult;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.dsa.CreateNewDsaResultImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.DsaBean;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.DsaBeanUtils;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Organization;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.roles.CredentialExpression;

import eu.cocoCloud.dsa.DsaDocument;
import net.customware.gwt.dispatch.server.ActionHandler;
import net.customware.gwt.dispatch.server.ExecutionContext;
import net.customware.gwt.dispatch.shared.ActionException;
import net.customware.gwt.dispatch.shared.DispatchException;

public class CreateNewDsaActionHandler implements
        ActionHandler<CreateNewDsaAction, CreateNewDsaResult> {

    private static Log log = LogFactory.getLog(CreateNewDsaActionHandler.class);

    @Override
    public CreateNewDsaResult execute(CreateNewDsaAction action,
            ExecutionContext context) throws DispatchException {
        String vocabularyUri = action.getVocabularyUri();
        if (vocabularyUri == null || "".equals(vocabularyUri))
            throw new ActionException("Empty or missing Vocabulary URI");
        // read the vocabulary
        VocabularyOntology vocont = VocabularyManager.getInstance()
                .getVocabularyOntology(vocabularyUri);
        if (vocont == null)
            throw new ActionException("cannot load vocabulary from "
                    + vocabularyUri);
        boolean saved = false;

        // generate a DSA UID
        String dsaUid = DsaStorage.getInstance().createNew();
        // create an empty DsaBean
        DsaBean dsaBean = DsaBeanUtils
                .createEmptyDsaBean(dsaUid, vocabularyUri, vocont.getParamTermList());
//        dsaBean.setKeyEncryptionSchema(SimpleTrustManager.getInstance().getEncryptionKeySchema());
        dsaBean.setKeyEncryptionSchema(Config.getInstance().getEncryptionKeySchema());
        // set up credentials with empty lists of trusted providers
        for (String credentialUri : vocont.getCredentials())
            dsaBean.getCredentials().setTrustedContextProviders(
                    new Term(credentialUri,
                            vocont.getHumanFormOf(credentialUri),vocont.getHumanFormOf(credentialUri)),
                    new ArrayList<Organization>());

        // set up roles with empty definitions
        for (String roleUri : vocont.getRoles()) {
            dsaBean.getRoles().setRoleDefinition(
                    new Term(roleUri, vocont.getHumanFormOf(roleUri), vocont.getHumanFormOf(roleUri)),
                    new ArrayList<CredentialExpression>());
        }
        // build the DsaDocument
        DsaDocument dsaDocument = null;

        DsaBeanToDsaDocumentConverter converter = new DsaBeanToDsaDocumentConverter(
                dsaBean);
        dsaDocument = converter.convert();
        String dsaContent = dsaDocument.xmlText(XmlUtil.getXmlOptions());
        if (!action.isStandalone()) {
            // the DSAAT is invoked as a service:call dsa api
            DsaApiServiceManager dsaSrvMgr = new DsaApiServiceManager();
            String dsaID = dsaUid
                    .substring(0, dsaBean.getUid().indexOf(".xml"));
            log.info("invoking DSAAT as a service: call DSA api using dsaID="
                    + dsaID + "and vocabulary uri=" + vocabularyUri
                    + " for userID=" + action.getUserID());
            String createdDsaId = dsaSrvMgr.createDSA(dsaID,
                    action.getUserID(), dsaContent);
            if (createdDsaId == null) {
                saved = false;
                log.error("Error creating new dsa");
            } else {
                DsaApiServiceManager dsaSvcMgr = new DsaApiServiceManager();
                dsaDocument = dsaSvcMgr.loadDSA(dsaUid);
                if (dsaDocument != null) {
                    saved = true;
                    log.info("New dsa successfully retrieved from the remote repository.");
                } else {
                    saved = false;
                    log.error("Error occurred retrieving new dsa");
                }
            }

        }
//        else {
//            DsaBeanToDsaDocumentConverter converter = new DsaBeanToDsaDocumentConverter(
//                    dsaBean);
//            dsaDocument = converter.convert();
//        }
        log.info("DSA Document" + dsaContent);
        // validate and save
        if (!dsaDocument.validate()) {
            log.error("invalid DSA:"
                    + dsaContent);
        } else {
            log.info("the DSA is valid.");
            saved = DsaStorage.getInstance().write(dsaBean.getUid(),
                    dsaDocument);
        }
        // if saved successfully, then returned the newly created DsaBean
        // otherwise return a null DsaBean
        return new CreateNewDsaResultImpl((saved) ? dsaBean : null);
    }

    @Override
    public Class<CreateNewDsaAction> getActionType() {
        return CreateNewDsaAction.class;
    }

    @Override
    public void rollback(CreateNewDsaAction action, CreateNewDsaResult result,
            ExecutionContext context) throws DispatchException {
        throw new UnsupportedOperationException();
    }

}
