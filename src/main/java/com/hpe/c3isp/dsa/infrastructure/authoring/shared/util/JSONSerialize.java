package com.hpe.c3isp.dsa.infrastructure.authoring.shared.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JSONSerialize {


	public static String objectToJson(Gson gson, Object object) throws Exception {
		Gson g = gson;
		if(g == null){
			g = new GsonBuilder().create();
		}
		return g.toJson(object);
	}



	public static Object jsonToObejct(Gson gson, String json, Class returnClass) throws Exception {
		Gson g = gson;
		if(g == null){
			g = new GsonBuilder().create();
		}

		Object ret = g.fromJson(json, returnClass);
		return ret;
	}




	public static Object jsonFromFile(Gson gson, String path, Class returnClass, boolean compress) throws Exception {
		File file = new File(path);
		if(!file.exists() || file.isDirectory()){
//			logger.error("jsonFromFile: il file non esiste o e' una directory");
			throw new Exception("Invalid file path: " + path);
		}
		Gson g = gson;
		if(g == null){
			g = new GsonBuilder().create();
		}
		Object ret = null;
		BufferedReader in = null;
		try {
			if(compress) {
				in = new BufferedReader(
						   new InputStreamReader(new GZIPInputStream(new FileInputStream(path)), "UTF8"));
			}else {
				in = new BufferedReader(
						   new InputStreamReader(new FileInputStream(path), StandardCharsets.UTF_8));
			}
			ret = g.fromJson(in, returnClass);
//			logger.debug("jsonFromFile: effettuata la deserializzazione dell'oggetto da " + path);
		} catch (Exception e) {
//			logger.error("XStreamSerializer.deserializeFromFile: eccezione",e);
			throw new Exception(
					"Error while reading file " + path , e);
		} finally{
			if(in != null){
				try {in.close();} catch (IOException e) {}
			}
		}
		return ret;
	}


	public static void jsonToFile(Gson gson, Object object, String path, boolean compress) throws Exception {
		//creo eventaalil drectory intermedie
		final File saveFile = new File(path);
		final File saveFolder = saveFile.getParentFile();
		if(saveFolder != null){
			//non vedo come potrebbe essere altrimenti
			if(!saveFolder.exists()){
				saveFolder.mkdirs();
			}
		}
		Gson g = gson;
		if(g == null){
			g = new GsonBuilder().setPrettyPrinting().create();
		}

		OutputStreamWriter writer = null;
		try {
			if(compress) {
				writer = new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(path)), StandardCharsets.UTF_8);
			}else {
				writer = new OutputStreamWriter(new FileOutputStream(path), StandardCharsets.UTF_8);
			}
			g.toJson(object, writer);
//			logger.debug("jsonToFile: effettuata la serializzazione dell'oggetto in " + path);
		} catch (Exception e) {
//			logger.error("XStreamSerializer.serializeToFile: eccezione",e);
			throw new Exception("Error while saving file " + path , e);
		} finally{
			try {
				if(writer != null){writer.flush(); writer.close();}
			}catch (IOException e) {}
		}
	}


}
