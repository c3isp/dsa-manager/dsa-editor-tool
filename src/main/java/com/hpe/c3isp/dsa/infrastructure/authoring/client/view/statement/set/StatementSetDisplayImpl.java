/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextInputCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLTable.Cell;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.Mode;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.delete.DeleteRowEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.delete.DeleteRowHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.delete.DisableRowMovementEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.delete.DisableRowMovementHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.move.EnableRowMovementEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.move.EnableRowMovementHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.move.MoveRowEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.move.MoveRowEvent.Direction;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.move.MoveRowHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.StatementPresenter.StatementDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.util.OptionsPopupDialog;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.util.WidgetUtil;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Util;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.Statement;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.StatementMetadata;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.util.InputValueHandler;


public abstract class StatementSetDisplayImpl extends Composite implements
        StatementSetDisplay{

    protected final FlexTable theFlexTable = new FlexTable();
    private final Button addButton = new Button("&#x2B;");  // &#x2B; is html code for '+'

    private final DialogBox moveTools = new DialogBox(false, true);
    // we reuse the same ClickHandler for the delete-row button of every
    // row, thus saving some memory
    private final ClickHandler deleteRowButtonClickHandler;
    // we reuse the same ClickHandler for the enable-row-movement button of
    // every row, thus saving some memory
    private final ClickHandler enableRowMovementButtonClickHandler;


    @Inject
    public StatementSetDisplayImpl() {
        super();
        // addButton.setStyleName("addButton");
        VerticalPanel vp = new VerticalPanel();
        vp.add(theFlexTable);
        addButton.setStyleName("addButton");
        vp.add(addButton);
        // vp.addStyleName("statementSection");
        initWidget(vp);
        initMoveTools();
        deleteRowButtonClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                final Cell clickedCell = theFlexTable.getCellForEvent(event);
                if (clickedCell != null) {
                    final int selected = clickedCell.getRowIndex();
                    DeleteRowEvent.fire(StatementSetDisplayImpl.this, selected);
                }
            }
        };
        enableRowMovementButtonClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                final Cell clickedCell = theFlexTable.getCellForEvent(event);
                if (clickedCell != null) {
                    final int selected = clickedCell.getRowIndex();
                    EnableRowMovementEvent.fire(StatementSetDisplayImpl.this,
                            selected);
                }
            }
        };

    }

    @Override
    public void addStatementDisplay(StatementDisplay statementDisplay) {
        theFlexTable.setWidget(theFlexTable.getRowCount(), 0,
                statementDisplay.asWidget());
    }


    @Override
    public void addStatementControlWidgets(int row, boolean isEnabled,
            Statement stmt, boolean isEndUser, final HashMap<String,String> params) {


    	GWT.log("_______ addStatementControlWidgets:" + Util.MapToString(params, "NULL"));

        GWT.log("addStatementControlWidgets: statementMetadata: " + stmt.getStatementMetadata());

    	String title = "";
        final Statement updatedStatement = stmt;
        boolean buttonEnabled = !isEndUser && isEnabled;
        Button deleteRowButton = new Button("&otimes;");
        if (!buttonEnabled) {
            title = "Not available";
        }else {
            title = "Delete statement";
        }

        deleteRowButton.setTitle(title);
        theFlexTable.setWidget(row, 1, deleteRowButton);
        deleteRowButton.addClickHandler(deleteRowButtonClickHandler);

        Button enableRowMovementButton = new Button("&uArr;&dArr;");
        if (buttonEnabled) {
            title = "Not available";
        }
        else {
            title = "Move statement";
        }

        enableRowMovementButton.setTitle(title);
        theFlexTable.setWidget(row, 2, enableRowMovementButton);
        enableRowMovementButton
                .addClickHandler(enableRowMovementButtonClickHandler);
        enableRowMovementButton.setEnabled(buttonEnabled);
        addLinkingButtons(row);

        final StatementMetadata statementMetadata = updatedStatement.getStatementMetadata();
        //final CheckBox protectedCheckBox = new CheckBox("Protected");
        final CheckBox protectedCheckBox = new CheckBox("Apply to Other Data"); // TODO: put in I18N file
        protectedCheckBox.setValue(statementMetadata.isProtected());
        protectedCheckBox.setEnabled(buttonEnabled);
        protectedCheckBox
                .addValueChangeHandler(new ValueChangeHandler<Boolean>() {
                    @Override
                    public void onValueChange(ValueChangeEvent<Boolean> event) {
                    	statementMetadata.setProtected(protectedCheckBox.getValue());
                    }
                });

        final CheckBox isPendingRuleCheckBox = new CheckBox("Pending Rule");
        isPendingRuleCheckBox.setValue(statementMetadata.isPendingRule());
        isPendingRuleCheckBox.setEnabled(buttonEnabled);
        isPendingRuleCheckBox.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                updatedStatement.getStatementMetadata().setPendingRule(isPendingRuleCheckBox.getValue());
            }
        });

        final List<String> freeTextValues = new ArrayList<String>();

        for (SyntacticItem si : stmt.getSyntacticItems()) {
            String element = si.getHumanForm();

            GWT.log("addStatementControlWidgets: found element: " + element );
            for (String p:params.keySet()){
                String pattern= ".\\s"+p+".*";
                RegExp regExp = RegExp.compile(pattern);
                MatchResult res = regExp.exec(element);

                //if element is a term and it is a parametric field
                if ( element.startsWith(p) || res != null) {
                    statementMetadata.setPendingRule(true);
                    isPendingRuleCheckBox.setValue(true);
                    updatedStatement.getStatementMetadata().setPendingRule(isPendingRuleCheckBox.getValue());
                    freeTextValues.add(si.getSyntacticForm());
                }
            }
        }

        final StringBuffer sbFreeTextValues = new StringBuffer();
        if(freeTextValues != null) {
        	for(String s:freeTextValues) {
        		sbFreeTextValues.append(s + "-");
        	}
        }
        GWT.log("addStatementControlWidgets: sbFreeTextValues: " + sbFreeTextValues.toString() );

        theFlexTable.setWidget(row, 4, protectedCheckBox);
        addButton.setEnabled(!isEndUser);
        final CellTable<String> freeValueTable = new CellTable<String>();
        freeValueTable.setRowData(freeTextValues);
        freeValueTable.setVisible(isPendingRuleCheckBox.getValue());

        final StringBuffer cachedValue = new StringBuffer("\r\nPlease enter a value");

        TextColumn<String> explainColumn = new TextColumn<String>() {
        	@Override
        	public String getValue(String s) {
        		String parameterName = s.substring(s.indexOf(":") + 1);
        		// if some parameter exist, they will be in the format FieldName[p1,p2,..pm] and FieldName is not User (means that the value of the field will be specified by the user using a text field)
        		String paramValue = params.get(parameterName);


       		 	GWT.log("addStatementControlWidgets: explainColumn, parameName/paramValue: " + parameterName + ", " + paramValue);

        		if (paramValue.contains("[")){
        			paramValue = params.get(parameterName).substring(0, params.get(parameterName).indexOf('['));
        		}
        		if (!paramValue.equals("User")){
        			//this is a DMO
        			return   "Add additional info for "+ parameterName
        					+ "#" + Integer.toString(freeTextValues.indexOf(s) + 1)
//        					+ cachedValue.toString()
        					;
        		}else{
        			return   "Specify a value for "+ parameterName
        					+ "#" + Integer.toString(freeTextValues.indexOf(s) + 1)
//        					+ cachedValue.toString()
        					;
        		}
        	}
        };
        freeValueTable.addColumn(explainColumn,"Complete the policy");

        if (buttonEnabled){
                //add additional info
//        	 	TextColumn<String> valueColumn = new TextColumn<String>() {
//        	 		@Override
//        	 		public String getValue(String s) {
//                     return updatedStatement.getStatementMetadata().getStatementInfo().substring(updatedStatement.getStatementMetadata().getStatementInfo().indexOf("{"));
//        	 		}
//        	 	};
//        	 	freeValueTable.addColumn(valueColumn);
//
                ButtonCell buttonCell = new ButtonCell();
                Column<String, String> buttonColumn2 = new Column<String, String>(buttonCell) {
                    @Override
                    public String getValue(String s) {
                    	return "Add Info";
                    }
                };
                freeValueTable.addColumn(buttonColumn2);
////                if (updatedStatement.getStatementMetadata().getStatementInfo().isEmpty() && updatedStatement.getStatementMetadata().isPendingRule()){
////
////                }

//                buttonColumn2.setFieldUpdater(new FieldUpdater<String, String>() {
//                    @Override
//                    public void update(int index, final String object, String value) {
//                    	StatementMetadata sdata = updatedStatement.getStatementMetadata();
//                    	String inputValue = sdata.getInputValue();
//
//
//                    	//String existingValue = inputValue.substring(sdata.getInputValue().indexOf("=")+1);
//
//                    	String existingValue = inputValue.substring(sdata.getInputValue().indexOf("=")+1);
//
//
//                    	String existingStatementInfo = sdata.getStatementInfo();
//                        String parameterName = object.substring(object.indexOf(":") + 1);
//                    	final OptionsPopupDialog optionDialog = new OptionsPopupDialog(params, parameterName, existingValue, existingStatementInfo);
//                    	optionDialog.center();
//                        optionDialog.setRow(index);
//
//                    	String debug = "ind="
//                    			+ index
//                    			+ ",obj="
//                    			+ ((object==null)?"N":object)
//                    			+ ",inputValue="
//                    			+ ((inputValue==null)?"N":inputValue)
//                    			+ ",va="
//                    			+ ((value==null)?"N":value)
//                    			+ ",existingValue="
//                    			+ ((existingValue==null)?"N":existingValue)
//                    			+ ",existingStatementInfo="
//                    			+ ((existingStatementInfo==null)?"N":existingStatementInfo)
//                    			+ ",parameterName="
//                    			+ ((parameterName==null)?"N":parameterName)
//                    			+ ", statementMetadata="
//                    			+ sdata
////                    			+ ", params (map)="
////                    			+ Util.MapToString(params, "N")
//                    			;
//
//                    	GWT.log("_______ " + debug);
//
//                        optionDialog.setText("Add additional info");
//                        optionDialog.show();
//                        optionDialog.addCloseHandler(new CloseHandler<PopupPanel>() {
//                            @Override
//                            public void onClose(CloseEvent<PopupPanel> event) {
//                            	StringBuffer sdebug = new StringBuffer();
//                            	sdebug.append("\r\n onclose>>>>: event " + event.toString());
//
//
//                            	//TODO lvorare qui con GWT.log() per
//                            	//capire come mai se non seleziono nulla nel combo (lascio quindi il valore di default) anche cliccando OK
//                            	//non viene selezionato alcun valore
//
//                            	String dialogValue = optionDialog.getUpdatedValue();
//                            	int row = optionDialog.getRow();
//                                String inputValue = updatedStatement.getStatementMetadata().getInputValue();
//                            	sdebug.append("\r\n dialogValue " + dialogValue);
//                            	sdebug.append("\r\n row " + row + " ---- \r\n");
////                            	sdebug.append("\r\n inputValue " + inputValue == null?"NULL":inputValue);
//                            	sdebug.append("\r\n inputValue " + ((inputValue == null || inputValue.length() == 0)?"EMPTY":inputValue));
//
//
//                            	if (!dialogValue.isEmpty()){
//
//                               		String updatedValue = freeTextValues.get(row)+"="+dialogValue;
//                               		sdebug.append("\r\n updatedValue " + updatedValue);
//                            		String newInputValue = WidgetUtil.updateValue(inputValue, updatedValue, freeTextValues.indexOf(object));
//                            		sdebug.append("\r\n newInputValue " + newInputValue);
//                            		updatedStatement.getStatementMetadata().setInputValue(newInputValue);
//
//
//
////                            		String updatedValue = freeTextValues.get(optionDialog.getRow())+"="+optionDialog.getUpdatedValue();
////                            		String newInputValue = WidgetUtil.updateValue(updatedStatement.getStatementMetadata().getInputValue(), updatedValue,freeTextValues.indexOf(object));
////                            		updatedStatement.getStatementMetadata().setInputValue(newInputValue);
//
//                            	}
//
//
//                            	String optionSelected = optionDialog.getSelectedOption();
//                            	String paramSelected = optionDialog.getSelectedParam() ;
//                            	if (!optionSelected.isEmpty() || !paramSelected.isEmpty()){
//                            		updatedStatement.getStatementMetadata().setStatementInfo(freeTextValues.get(optionDialog.getRow())+"{param="+ paramSelected+" option="+optionSelected+"}");
////                                    String pValue =
////                                    		"\r\nValue: "+ paramSelected
////                                    		+ (optionSelected.isEmpty()?"": " option="+ optionSelected);
////                            		cachedValue.setLength(0);
////                                    cachedValue.append(pValue);
////                                    GWT.log("_______*** " + cachedValue);
//                            	}
//                          		freeValueTable.redrawRow(optionDialog.getRow());
//
//                          		GWT.log("_______ " + sdebug);
//                            }
//                        });
//                    }
//                });














                buttonColumn2.setFieldUpdater(new FieldUpdater<String, String>() {
                    @Override
                    public void update(final int index, final String object, String value) {
                    	StatementMetadata sdata = updatedStatement.getStatementMetadata();
                    	//para1=val1|param2=val2|....
                    	final String inputValue = sdata.getInputValue();
                    	final String paramName = freeTextValues.get(index);
                    	final String existingValue = InputValueHandler.gatValue(inputValue, paramName);
                    	String existingStatementInfo = sdata.getStatementInfo();
                        String parameterName = object.substring(object.indexOf(":") + 1);
                    	final OptionsPopupDialog optionDialog
                    		= new OptionsPopupDialog(params, parameterName, existingValue, existingStatementInfo);
                    	optionDialog.center();
                        optionDialog.setRow(index);

                    	String debug = "ind="
                    			+ index
                    			+ ",obj="
                    			+ ((object==null)?"N":object)
                    			+ ",inputValue="
                    			+ ((inputValue==null)?"N":inputValue)
                    			+ ",va="
                    			+ ((value==null)?"N":value)
                    			+ ",existingValue="
                    			+ ((existingValue==null)?"N":existingValue)
                    			+ ",existingStatementInfo="
                    			+ ((existingStatementInfo==null)?"N":existingStatementInfo)
                    			+ ",parameterName="
                    			+ ((parameterName==null)?"N":parameterName)
                    			+ ", statementMetadata="
                    			+ sdata
//                    			+ ", params (map)="
//                    			+ Util.MapToString(params, "N")
                    			;

                    	GWT.log("_______ " + debug);

                        optionDialog.setText("Add additional info");
                        optionDialog.setCanceled(true);
                        optionDialog.addCloseHandler(new CloseHandler<PopupPanel>() {
                            @Override
                            public void onClose(CloseEvent<PopupPanel> event) {
                            	if(optionDialog.isCanceled()) {
                            		return;
                            	}
                            	StringBuffer sdebug = new StringBuffer();
                            	sdebug.append("\r\n onclose>>>>: event " + event.toString());

                            	String dialogValue = optionDialog.getUpdatedValue();
//                            	int row = optionDialog.getRow();
                                String inputValue = updatedStatement.getStatementMetadata().getInputValue();
                            	sdebug.append("\r\n dialogValue " + ((dialogValue == null || dialogValue.length() == 0)?"EMPTY":dialogValue));
                            	sdebug.append("\r\n index " + index + " ---- \r\n");
//                            	sdebug.append("\r\n inputValue " + inputValue == null?"NULL":inputValue);
                            	sdebug.append("\r\n inputValue " + ((inputValue == null || inputValue.length() == 0)?"EMPTY":inputValue));


                            	if (dialogValue != null && dialogValue.length()>0){
                            		String updatedValue = InputValueHandler.updateValue(inputValue, paramName, dialogValue);
                               		sdebug.append("\r\n updatedValue " + ((updatedValue == null || updatedValue.length() == 0)?"EMPTY":updatedValue));
                            		updatedStatement.getStatementMetadata().setInputValue(updatedValue);
                            	}

                            	String paramSelected = optionDialog.getSelectedParam() ;
                            	String optionSelected = optionDialog.getSelectedOption();
                            	if(paramSelected == null) {
                            		paramSelected = "";
                            	}
                            	if(optionSelected == null) {
                            		optionSelected = "";
                            	}
                            	if(paramSelected.length() > 0 || optionSelected.length() > 0) {
                            		final String key = freeTextValues.get(optionDialog.getRow());
                            		updatedStatement.getStatementMetadata().setStatementInfo(key+"{param="+ paramSelected+" option="+optionSelected+"}");

                            	}
//
//                            	if (optionSelected != null && optionSelected.length() > 0 && paramSelected != null && paramSelected.length() > 0 ){
//                            		updatedStatement.getStatementMetadata().setStatementInfo(freeTextValues.get(optionDialog.getRow())+"{param="+ paramSelected+" option="+optionSelected+"}");
//                            	}
                          		freeValueTable.redrawRow(optionDialog.getRow());

                          		GWT.log("_______ " + sdebug);
                            }
                        });
                        optionDialog.show();
                    }
                });

























        }else{
            //user cannot edit this column except for the empty fields

            if (updatedStatement.getStatementMetadata().getStatementInfo().isEmpty()){

                final TextInputCell namePartyCell = new TextInputCell();
                final Column<String, String> valueColumn = new Column<String, String>(
                        namePartyCell) {
                    @Override
                    public String getValue(String object) {

                        return updatedStatement.getStatementMetadata().getStatementInfo().substring(updatedStatement.getStatementMetadata()
                                .getStatementInfo().indexOf("=")+1);
                        }
                };
                freeValueTable.addColumn(valueColumn);

                valueColumn.setFieldUpdater(new FieldUpdater<String, String>() {

                    @Override
                    public void update(int index, String object, String value) {
                    	updatedStatement.getStatementMetadata().setStatementInfo(freeTextValues.get(index)+"="+value);
//                    	final String val = value == null?"":value;
//                        String pValue = "\r\nValue: "+ val;
//                		cachedValue.setLength(0);
//                        cachedValue.append(pValue);
//                        GWT.log("_______*** " + cachedValue);
                    }
                });


            }else{
                TextColumn<String> valueColumn = new TextColumn<String>() {
                    @Override
                    public String getValue(String s) {
                        return updatedStatement.getStatementMetadata().getStatementInfo().substring(updatedStatement.getStatementMetadata().getStatementInfo().indexOf("{"));
                    }
                };
                freeValueTable.addColumn(valueColumn);
            }
        }

       theFlexTable.setWidget(row, 5, freeValueTable);
    }

    protected abstract void addLinkingButtons(int row);

    @Override
    public Widget asWidget() {
        return this;
    }

    @Override
    public HandlerRegistration addDeleteRowHandler(DeleteRowHandler handler) {
        return addHandler(handler, DeleteRowEvent.TYPE);
    }

    @Override
    public void deleteRow(int rowIndex) {
        theFlexTable.removeRow(rowIndex);
    }

    @Override
    public HandlerRegistration addMoveRowUpHandler(MoveRowHandler handler) {
        return addHandler(handler, MoveRowEvent.TYPE);
    }

    @Override
    public HandlerRegistration addEnableRowMovementHandler(
            EnableRowMovementHandler handler) {
        return addHandler(handler, EnableRowMovementEvent.TYPE);
    }

    @Override
    public HandlerRegistration addDisableRowMovementHandler(
            DisableRowMovementHandler handler) {
        return addHandler(handler, DisableRowMovementEvent.TYPE);
    }

    @Override
    public void insertRow(StatementDisplay statementDisplay, int beforeRow,
            Statement statement) {
        int j = theFlexTable.insertRow(beforeRow);
        theFlexTable.setWidget(j, 0, statementDisplay.asWidget());
        addStatementControlWidgets(j, true, statement, false, statement.getParamItems());
    }

    @Override
    public void hideStatementMoveTools() {
        moveTools.hide();
    }

    @Override
    public void showStatementMoveTools() {
        moveTools.show();
    }

    @Override
    public void highlightStatement(int rowIndex, Mode mode) {
        assert mode.equals(Mode.ON) || mode.equals(Mode.OFF) : "unsupported mode "
                + mode;
        if (mode.equals(Mode.ON))
            theFlexTable.getCellFormatter().setStyleName(rowIndex, 0,
                    "statementMoving");
        else
            theFlexTable.getCellFormatter().removeStyleName(rowIndex, 0,
                    "statementMoving");
    }

    private void initMoveTools() {
        moveTools.setText("Move Tools");
        moveTools.setAnimationEnabled(true);
        moveTools.center();
        moveTools.hide();
        HorizontalPanel hp = new HorizontalPanel();
        moveTools.setWidget(hp);

        Button moveUpButton = new Button("&uArr;");
        hp.add(moveUpButton);
        moveUpButton.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                MoveRowEvent.fire(StatementSetDisplayImpl.this, Direction.UP);
            }
        });

        Button moveDownButton = new Button("&dArr;");
        hp.add(moveDownButton);
        moveDownButton.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                MoveRowEvent.fire(StatementSetDisplayImpl.this, Direction.DOWN);
            }
        });

        Button moveDoneButton = new Button("Done");
        hp.add(moveDoneButton);
        moveDoneButton.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                DisableRowMovementEvent.fire(StatementSetDisplayImpl.this);
            }
        });
    }

    @Override
    public HasClickHandlers getAddButton() {
        return addButton;
    }

    @Override
    public void deactivate() {
//    	GWT.log("StatementSetDisplayImpl deactivate");
    	addButton.setEnabled(false);
        setStatementControlWidgetsEnabled(false);
        // our subclasses my have special widgets that must be disabled
        setSpecializedWidgetsEnabled(false);
    }

    @Override
    public void activate() {
//    	GWT.log("StatementSetDisplayImpl activate");
        addButton.setEnabled(true);
        setStatementControlWidgetsEnabled(true);
        // our subclasses my have special widgets that must be enabled
        setSpecializedWidgetsEnabled(true);
    }

    protected abstract void setSpecializedWidgetsEnabled(boolean enabled);

    private void setStatementControlWidgetsEnabled(boolean enabled) {
        for (int i = 0; i < theFlexTable.getRowCount(); i++) {
            Widget w;
            w = theFlexTable.getWidget(i, 1);
            assert w instanceof Button;
            ((Button) w).setEnabled(enabled);
            w = theFlexTable.getWidget(i, 2);
            assert w instanceof Button;
            ((Button) w).setEnabled(enabled);
        }
    }

    @Override
    public void clear() {
        theFlexTable.clear();
    }

}
