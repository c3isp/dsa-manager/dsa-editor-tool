/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.view.validity;

import java.util.Date;

import com.google.gwt.event.logical.shared.HasOpenHandlers;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment.VerticalAlignmentConstant;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DatePicker;
import com.google.inject.Inject;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.validity.ValidityPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.validity.ValidityPresenter.ValidityDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.widget.MessagePanel;

public class ValidityDisplayImpl extends Composite implements ValidityDisplay {

//    private Label labelStartDate = new Label();
//    private DisclosurePanel dpStartDate = new DisclosurePanel();
//    private final DatePicker startDatePicker = new DatePicker();
//
//    private Label labelEndDate = new Label();
//    private DisclosurePanel dpEndDate = new DisclosurePanel();
//    private DatePicker endDatePicker = new DatePicker();

    private DateBox dateBoxStart = new DateBox();
    private DateBox dateBoxEnd= new DateBox();


    @Inject
    public ValidityDisplayImpl() {
      final VerticalPanel vp = new VerticalPanel();
      initWidget(vp);


//		labelStartDate = new Label("");
//        dpStartDate = new DisclosurePanel("");
//        labelEndDate = new Label("");
//        dpEndDate = new DisclosurePanel("");
//

//
//        dpStartDate.setAnimationEnabled(true);
//        dpStartDate.setContent(startDatePicker);
//        final HorizontalPanel hpStartDate = new HorizontalPanel();
//        hpStartDate.add(labelStartDate);
//        hpStartDate.add(dpStartDate);
//
//        dpEndDate.setAnimationEnabled(true);
//        dpEndDate.setContent(endDatePicker);
//        final HorizontalPanel hpEndDate = new HorizontalPanel();
//        hpEndDate.add(labelEndDate);
//        hpEndDate.add(dpEndDate);



        final HorizontalPanel hpDates = new HorizontalPanel();
        hpDates.setSpacing(5);
        hpDates.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);

        dateBoxStart.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat(ValidityPresenter.DATEFORMAT)));
        dateBoxEnd.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat(ValidityPresenter.DATEFORMAT)));


        hpDates.add(new Label("From"));
        hpDates.add(dateBoxStart);
        hpDates.add(new Label("To"));
        hpDates.add(dateBoxEnd);
        vp.add(hpDates);

//        vp.add(hpStartDate);
//        vp.add(hpEndDate);
    }

    @Override
    public void activate() {
//        dpStartDate.setVisible(true);
//        dpEndDate.setVisible(true);
    }

    @Override
    public void deactivate() {
//        dpStartDate.setVisible(false);
//        dpEndDate.setVisible(false);

    }

    @Override
    public Widget asWidget() {
        return this;
    }

//    @Override
//    public HasOpenHandlers<DisclosurePanel> getDsaEndDatePanel() {
//        return dpEndDate;
//    }
//
//    @Override
//    public HasOpenHandlers<DisclosurePanel> getDsaStartDatePanel() {
//        return dpStartDate;
//    }



    @Override
    public HasValueChangeHandlers<Date> getDsaStartDateBox() {
        return dateBoxStart;
    }

    @Override
    public HasValueChangeHandlers<Date> getDsaEndDateBox() {
        return dateBoxEnd;
    }

//    @Override
//    public HasValueChangeHandlers<Date> getDsaEndDatePicker() {
//        return endDatePicker;
//    }
//
//    @Override
//    public HasValueChangeHandlers<Date> getDsaStartDatePicker() {
//        return startDatePicker;
//    }


    @Override
    public void setEndDate(Date value, String label) {
//        labelEndDate.setText(label+ dateToSting(value));
//        endDatePicker.setValue(value);
//        dpEndDate.setOpen(false);

        dateBoxEnd.setValue(value);

    }

    @Override
    public void setStartDate(Date value, String label) {
//        labelStartDate.setText(label + dateToSting(value));
//        startDatePicker.setValue(value);
//        dpStartDate.setOpen(false);

        dateBoxStart.setValue(value);

    }

    private String dateToSting(Date value) {
        return DateTimeFormat.getMediumDateFormat().format(value);
    }


     @Override
    public void badDurationValue(String value) {

        MessagePanel messagePanel = new MessagePanel("The value " + value
                + " is not valid");
        messagePanel.setAutoHideEnabled(true);
        messagePanel.show();
    }

}




























//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
///**
// *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
// *
// *  Licensed under the Apache License, Version 2.0 (the "License");
// *  you may not use this file except in compliance with the License.
// *  You may obtain a copy of the License at
// *
// *      http://www.apache.org/licenses/LICENSE-2.0
// *
// *  Unless required by applicable law or agreed to in writing, software
// *  distributed under the License is distributed on an "AS IS" BASIS,
// *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// *  See the License for the specific language governing permissions and
// *  limitations under the License.
// */
//package com.hpe.c3isp.dsa.infrastructure.authoring.client.view.validity;
//
//import java.util.Date;
//
//import com.google.gwt.event.logical.shared.HasOpenHandlers;
//import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
//import com.google.gwt.i18n.client.DateTimeFormat;
//import com.google.gwt.user.client.ui.Composite;
//import com.google.gwt.user.client.ui.DisclosurePanel;
//import com.google.gwt.user.client.ui.HorizontalPanel;
//import com.google.gwt.user.client.ui.Label;
//import com.google.gwt.user.client.ui.VerticalPanel;
//import com.google.gwt.user.client.ui.Widget;
//import com.google.gwt.user.datepicker.client.DateBox;
//import com.google.gwt.user.datepicker.client.DatePicker;
//import com.google.inject.Inject;
//import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.validity.ValidityPresenter.ValidityDisplay;
//import com.hpe.c3isp.dsa.infrastructure.authoring.client.widget.MessagePanel;
//
//public class ValidityDisplayImpl extends Composite implements ValidityDisplay {
//
//    private Label labelStartDate = new Label();
//    private DisclosurePanel dpStartDate = new DisclosurePanel();
//    private final DatePicker startDatePicker = new DatePicker();
//
//    private Label labelEndDate = new Label();
//    private DisclosurePanel dpEndDate = new DisclosurePanel();
//    private DatePicker endDatePicker = new DatePicker();
//
//    @Inject
//    public ValidityDisplayImpl() {
//        labelStartDate = new Label("");
//        dpStartDate = new DisclosurePanel("");
//        labelEndDate = new Label("");
//        dpEndDate = new DisclosurePanel("");
//
//        final VerticalPanel vp = new VerticalPanel();
//        initWidget(vp);
//
//        dpStartDate.setAnimationEnabled(true);
//        dpStartDate.setContent(startDatePicker);
//        final HorizontalPanel hpStartDate = new HorizontalPanel();
//        hpStartDate.add(labelStartDate);
//        hpStartDate.add(dpStartDate);
//
//        dpEndDate.setAnimationEnabled(true);
//        dpEndDate.setContent(endDatePicker);
//        final HorizontalPanel hpEndDate = new HorizontalPanel();
//        hpEndDate.add(labelEndDate);
//        hpEndDate.add(dpEndDate);
//
//
//        vp.add(hpStartDate);
//        vp.add(hpEndDate);
//    }
//
//    @Override
//    public void activate() {
//        dpStartDate.setVisible(true);
//        dpEndDate.setVisible(true);
//
//    }
//
//    @Override
//    public void deactivate() {
//        dpStartDate.setVisible(false);
//        dpEndDate.setVisible(false);
//
//    }
//
//    @Override
//    public Widget asWidget() {
//        return this;
//    }
//
//    @Override
//    public HasOpenHandlers<DisclosurePanel> getDsaEndDatePanel() {
//        return dpEndDate;
//    }
//
//    @Override
//    public HasOpenHandlers<DisclosurePanel> getDsaStartDatePanel() {
//        return dpStartDate;
//    }
//
//    @Override
//    public HasValueChangeHandlers<Date> getDsaEndDatePicker() {
//        return endDatePicker;
//    }
//
//    @Override
//    public HasValueChangeHandlers<Date> getDsaStartDatePicker() {
//        return startDatePicker;
//    }
//
//
//    @Override
//    public void setEndDate(Date value, String label) {
//        labelEndDate.setText(label+ dateToSting(value));
//        endDatePicker.setValue(value);
//        dpEndDate.setOpen(false);
//    }
//
//    @Override
//    public void setStartDate(Date value, String label) {
//        labelStartDate.setText(label + dateToSting(value));
//        startDatePicker.setValue(value);
//        dpStartDate.setOpen(false);
//    }
//
//    private String dateToSting(Date value) {
//        return DateTimeFormat.getMediumDateFormat().format(value);
//    }
//
//     @Override
//    public void badDurationValue(String value) {
//
//        MessagePanel messagePanel = new MessagePanel("The value " + value
//                + " is not valid");
//        messagePanel.setAutoHideEnabled(true);
//        messagePanel.show();
//    }
//
//}