/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.util;

public class UIDGenerator {

    private static UIDGenerator THE_INSTANCE = null;

    private long index = 0;

    private UIDGenerator() {
    }

    public static UIDGenerator getInstance() {
        if (THE_INSTANCE == null)
            THE_INSTANCE = new UIDGenerator();
        return THE_INSTANCE;
    }

    public synchronized void setIndex(long value) {
        index = value;
    }

    public long getNextUid() {
        if (index + 1 == Long.MAX_VALUE) {
            System.err.println("UID overflow!");
            index = 0;
        }
        index++;
        return index;
    }

    public synchronized String getNextUid(String prefix) {
        return prefix + String.valueOf(getNextUid());
    }

}
