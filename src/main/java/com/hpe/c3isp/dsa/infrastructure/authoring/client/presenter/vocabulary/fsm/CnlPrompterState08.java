/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.fsm;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.model.VocabularyChoicesUpdater;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Functor;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Tokens;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.vocabulary.GetAllTermsInRangeForPredicate;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.vocabulary.GetTermsInRangeForPredicate;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.vocabulary.VocabularyGetter;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.vocabulary.VocabularyGetResult;

public class CnlPrompterState08 extends
        AbstractCnlPrompterStateWithTermsAndTokenReference {

    private final Functor predicate;
    private final VocabularyChoicesUpdater vocabularyChoicesUpdater;
    private final VocabularyGetter<VocabularyGetResult> actionConstrainingReferenceToken;

    public CnlPrompterState08(SyntacticItem previous) {
        assert previous != null;
        assert previous instanceof Functor : "was expecting a Functor, got a "
                + previous.toString();
        predicate = (Functor) previous;
        actionConstrainingReferenceToken = new GetAllTermsInRangeForPredicate(
                predicate.getUid());
        vocabularyChoicesUpdater = new VocabularyChoicesUpdater(tokens,
                new GetTermsInRangeForPredicate(predicate.getUid()),
                actionConstrainingReferenceToken);
    }

    @Override
    public void handleSelection(CnlPrompterContext context,
            SyntacticItem selectedItem) {
        if (selectedItem.equals(Tokens.FAKE_TOKEN_REFERENCE)) {
            context.updateAndEnableReferenceSelection(new CnlPrompterState09(),
                    actionConstrainingReferenceToken);
        } else {
            context.update(new CnlPrompterState10(), selectedItem);
        }

    }

    @Override
    public VocabularyChoicesUpdater getVocabularyChoicesUpdater() {
        return vocabularyChoicesUpdater;
    }

}
