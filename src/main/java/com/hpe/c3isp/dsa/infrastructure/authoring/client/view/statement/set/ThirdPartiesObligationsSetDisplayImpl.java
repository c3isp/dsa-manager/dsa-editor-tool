/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.HTMLTable.Cell;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.click.SelectedRowEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.click.SelectedRowHandler;

public class ThirdPartiesObligationsSetDisplayImpl extends
        StatementSetDisplayImpl implements ThirdPartiesObligationsSetDisplay {

    // we reuse the same ClickHandler for the link-source button of every
    // row, thus saving some memory
    private final ClickHandler linkSourceButtonClickHandler;

    public ThirdPartiesObligationsSetDisplayImpl() {
        super();
        linkSourceButtonClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                final Cell clickedCell = theFlexTable.getCellForEvent(event);
                if (clickedCell != null) {
                    final int selected = clickedCell.getRowIndex();
                    SelectedRowEvent.fire(
                            ThirdPartiesObligationsSetDisplayImpl.this,
                            selected);
                }
            }
        };
    }

    @Override
    protected void addLinkingButtons(int row) {
        final Button linkSourceButton = new Button("&rArr;");
        linkSourceButton.setTitle("Define a reference to another statement");
        linkSourceButton.addClickHandler(linkSourceButtonClickHandler);
        theFlexTable.setWidget(row, 3, linkSourceButton);
    }

    @Override
    public HandlerRegistration addSelectedRowHandler(SelectedRowHandler handler) {
        return addHandler(handler, SelectedRowEvent.TYPE);
    }

    @Override
    protected void setSpecializedWidgetsEnabled(boolean enabled) {
        for (int i = 0; i < theFlexTable.getRowCount(); i++) {
            Widget w = theFlexTable.getWidget(i, 3);
            assert w instanceof Button;
            ((Button) w).setEnabled(enabled);
        }
    }

}
