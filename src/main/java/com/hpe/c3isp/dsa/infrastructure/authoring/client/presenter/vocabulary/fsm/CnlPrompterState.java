/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.fsm;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.model.VocabularyChoicesUpdater;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableDeclaration;

public interface CnlPrompterState {

    /**
     * This method handles the vocabulary item that the user has selected by
     * clicking on an item in the list/tree of available choices.
     * 
     * @param context
     * @param selectedItem
     */
    void handleSelection(CnlPrompterContext context, SyntacticItem selectedItem);

    /**
     * This method handles the reference that the user has selected by clicking
     * on a {@link VariableDeclaration} in some statement.
     * 
     * @param context
     * @param syntacticItem
     */
    void handleReference(CnlPrompterContext context, SyntacticItem syntacticItem);

    /**
     * This method returns a {@link VocabularyChoicesUpdater} for this state.
     * 
     * @return a {@link VocabularyChoicesUpdater}
     */
    VocabularyChoicesUpdater getVocabularyChoicesUpdater();

}
