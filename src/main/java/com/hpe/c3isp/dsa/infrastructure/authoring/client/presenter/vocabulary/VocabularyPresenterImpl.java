/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary;

import java.util.ArrayList;
import java.util.List;

import net.customware.gwt.dispatch.client.DispatchAsync;

import org.enunes.gwt.mvp.client.EventBus;
import org.enunes.gwt.mvp.client.presenter.BasePresenter;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.EditingCompletedEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.VocabularySelectionEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.syntactic.HighlightReferenceableEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.syntactic.SyntacticItemClickedEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.syntactic.SyntacticItemClickedHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.model.VocabularyChoicesUpdater;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.VocabularyPresenter.VocabularyDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.fsm.CnlPrompterContext;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.fsm.CnlPrompterState;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.fsm.CnlPrompterState01;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.helper.ReferenceableTermsKeeper;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Functor;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItemTreeNode;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Term;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Token;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Tokens;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.vocabulary.GetNothing;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.vocabulary.GetSubTerms;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.vocabulary.VocabularyGetter;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.vocabulary.VocabularyGetResult;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.StatementType;

public class VocabularyPresenterImpl extends BasePresenter<VocabularyDisplay>
implements VocabularyPresenter, CnlPrompterContext {

	private CnlPrompterState state;
	private StatementType statementType;
	private final DispatchAsync dispatcher;

	@Inject
	public VocabularyPresenterImpl(EventBus eventBus,
			VocabularyDisplay display, final DispatchAsync dispatcher) {

		super(eventBus, display);
		this.dispatcher = dispatcher;
		// we immediately call bind so that every new instance of
		// VocabularyPresenterImpl is ready to use, i.e. we can do
		//
		// (new VocabularyPresenterImpl()).go(...)
		//
		// or, with GIN,
		//
		// private final Provider<VocabularyPresenter>
		// providerOfVocabularyPresenter;
		// providerOfVocabularyPresenter.get().go(...)
		bind();
	}

	@Override
	public void bind() {
		super.bind();

		registerHandler(display.addSelectionHandler(new SelectionHandler<SyntacticItem>() {
			@Override
			public void onSelection(SelectionEvent<SyntacticItem> event) {
				state.handleSelection(VocabularyPresenterImpl.this,
						event.getSelectedItem());
			}
		}));
		registerHandler(display.addOpenHandler(new OpenHandler<SyntacticItem>() {

			@Override
			public void onOpen(OpenEvent<SyntacticItem> event) {
				handleOpenEventOn(event.getTarget());
			}
		}));

		registerHandler(eventBus.addHandler(SyntacticItemClickedEvent.TYPE,
				new SyntacticItemClickedHandler() {

			@Override
			public void onSyntacticItemClicked(
					SyntacticItem syntacticItem) {
				state.handleReference(VocabularyPresenterImpl.this,
						syntacticItem);
			}
		}));
	}

	@Override
	public void go(StatementType statementType) {
		this.statementType = statementType;
		state = new CnlPrompterState01();
		updateDisplay();
	}

	@Override
	public void updateAndEnableReferenceSelection(CnlPrompterState nextState) {
		setState(nextState);
		eventBus.fireEvent(new HighlightReferenceableEvent());
		display.hide();
	}

	@Override
	public void updateAndEnableReferenceSelection(
			CnlPrompterState nextState,
			VocabularyGetter<VocabularyGetResult> actionConstrainingReferenceToken) {
		setState(nextState);
		dispatcher.execute(actionConstrainingReferenceToken,
				new AsyncCallback<VocabularyGetResult>() {

			@Override
			public void onFailure(Throwable caught) {
				displayFailureMessage(caught);
				finish(false);
			}

			@Override
			public void onSuccess(VocabularyGetResult result) {
				List<SyntacticItemTreeNode> itemNodes = result
						.getSyntacticItemTreeNodes();
				for (SyntacticItemTreeNode itemNode : itemNodes)
					eventBus.fireEvent(new HighlightReferenceableEvent(
							itemNode.getSyntacticItem()));
				display.hide();
			}
		});
	}

	@Override
	public void update(CnlPrompterState state, SyntacticItem selectedItem) {
		ReferenceableTermsKeeper.getInstance().add(selectedItem);
		eventBus.fireEvent(new VocabularySelectionEvent(selectedItem));
		update(state);
	}

	@Override
	public void update(CnlPrompterState state) {
		setState(state);
		updateDisplay();
	}

	@Override
	public void finish(SyntacticItem selectedItem, boolean successful) {
		eventBus.fireEvent(new VocabularySelectionEvent(selectedItem));
		finish(successful);
	}

	private void setState(CnlPrompterState state) {
		this.state = state;
	}

	private void finish(boolean successful) {
		eventBus.fireEvent(new EditingCompletedEvent(successful));
		display.hide();
		unbind();
	}

	@Override
	public StatementType getStatementType() {
		return statementType;
	}

	private void updateDisplay() {
		final VocabularyChoicesUpdater vcu = state
				.getVocabularyChoicesUpdater();
		if (vcu.getAction() instanceof GetNothing) {
			// we have only tokens
			updateDisplay(vcu.getTokens());
		} else {
			// we must fetch data from server-side
			display.startFetchingData();
			if (vcu.hasActionConstrainingReferenceToken())
				// we must decide whether to include the Token REFERENCE
				updateDisplay(vcu.getAction(),
						vcu.getActionConstrainingReferenceToken());
			else
				// we don't care about the Token REFERENCE
				updateDisplay(vcu.getAction(), vcu.getTokens());
		}
	}

	private void updateDisplay(final List<Token> tokens) {
		List<SyntacticItemTreeNode> itemNodes = new ArrayList<SyntacticItemTreeNode>();
		for (Token token : tokens)
			itemNodes.add(new SyntacticItemTreeNode(token, false));
		display.show(itemNodes);
	}

	private void updateDisplay(
			final VocabularyGetter<VocabularyGetResult> action,
			final List<Token> tokens) {
		dispatcher.execute(action, new AsyncCallback<VocabularyGetResult>() {

			@Override
			public void onFailure(Throwable caught) {
				displayFailureMessage(caught);
				finish(false);
			}

			@Override
			public void onSuccess(VocabularyGetResult result) {
				List<SyntacticItemTreeNode> resultNodes = result
						.getSyntacticItemTreeNodes();
				if (resultNodes.isEmpty()) {
					Window.alert("No further choices are available in the "
							+ "current vocabulary! It looks like the "
							+ "statement being composed has no well defined "
							+ "meaning, and so it will be discarded.");
					finish(false);
				} else {
					List<SyntacticItemTreeNode> itemNodes = new ArrayList<SyntacticItemTreeNode>();
					for (Token token : tokens)
						itemNodes.add(new SyntacticItemTreeNode(token, false));
					itemNodes.addAll(resultNodes);
					display.show(itemNodes);
				}
			}
		});
	}

	private void updateDisplay(
			final VocabularyGetter<VocabularyGetResult> action,
			final VocabularyGetter<VocabularyGetResult> constraint) {
		dispatcher.execute(constraint,
				new AsyncCallback<VocabularyGetResult>() {

			@Override
			public void onFailure(Throwable caught) {
				displayFailureMessage(caught);
				finish(false);
			}

			@Override
			public void onSuccess(VocabularyGetResult result) {
				List<Token> tokens = new ArrayList<Token>();
				List<SyntacticItemTreeNode> sitns = result
						.getSyntacticItemTreeNodes();
				boolean canMakeReference = false;
				for (SyntacticItemTreeNode sitn : sitns) {
					if (ReferenceableTermsKeeper.getInstance()
							.canReferTo(
									sitn.getSyntacticItem().getUid())) {
						canMakeReference = true;
						break;
					}
				}
				if (canMakeReference)
					tokens.add(Tokens.FAKE_TOKEN_REFERENCE);
				updateDisplay(action, tokens);
			}
		});
	}

	private void handleOpenEventOn(final SyntacticItem target) {
		//mr: unico modo per imulare la richiesta di chiusura del dialog con conseguente riattivazione
		//di tutti gli item disattivati nella interfaccia
		if(target == null) {
			GWT.log("VocabularyPresenterImpl.handleOpenEventOn, passing null, simulating a close resquest");
			finish(false);
			return;
		}

		assert target != null;
		assert target instanceof Term || target instanceof Functor : "was expecting a Term or a Functor, got a "
		+ target.toString();
		display.startFetchingData(target);
		dispatcher.execute(new GetSubTerms(target.getUid()),
				new AsyncCallback<VocabularyGetResult>() {

			@Override
			public void onFailure(Throwable caught) {
				displayFailureMessage(caught,
						" for " + target.getHumanForm());
				finish(false);
			}

			@Override
			public void onSuccess(VocabularyGetResult result) {
				display.showSubTerms(target,
						result.getSyntacticItemTreeNodes());
			}
		});
	}

	private void displayFailureMessage(Throwable caught) {
		displayFailureMessage(caught, null);
	}

	private void displayFailureMessage(Throwable caught, String message) {
		String msg = "error while fetching data";
		if (message != null)
			msg += message;
		if (!"".equals(caught.getMessage()))
			msg += ": " + caught.getMessage();
		Window.alert(message);
	}

}
