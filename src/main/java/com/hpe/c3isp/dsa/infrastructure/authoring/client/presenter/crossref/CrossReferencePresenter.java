/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.crossref;

import com.google.gwt.event.logical.shared.HasCloseHandlers;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ToggleButton;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.ControllablePresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.ControllableDisplay;

public interface CrossReferencePresenter extends
        ControllablePresenter<CrossReferencePresenter.CrossReferenceDisplay> {

    public interface CrossReferenceDisplay extends ControllableDisplay {
        ToggleButton getToggleReferencesButton();

        HasCloseHandlers<PopupPanel> showMessage(String message);
    }

}
