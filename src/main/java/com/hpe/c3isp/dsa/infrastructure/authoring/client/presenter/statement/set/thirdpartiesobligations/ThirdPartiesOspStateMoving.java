/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.thirdpartiesobligations;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.AbstractSspStateMoving;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.ThirdPartiesObligationsSetDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.StatementMetadata;

public class ThirdPartiesOspStateMoving
        extends
        AbstractSspStateMoving<ThirdPartiesOspContext, ThirdPartiesObligationsSetDisplay, ThirdPartiesOspState>
        implements ThirdPartiesOspState {

    public ThirdPartiesOspStateMoving(int rowBeingMoved) {
        super(rowBeingMoved);
    }

    @Override
    protected void makeTransitionToStateNormal(ThirdPartiesOspContext context) {
        context.setState(new ThirdPartiesOspStateNormal());
    }

    @Override
    public void handleSelectedRowEvent(ThirdPartiesOspContext context, int row) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleLinkToTargetStatementEvent(
            ThirdPartiesOspContext context, StatementMetadata source,
            StatementMetadata target) {
        throw new UnsupportedOperationException();
    }

}
