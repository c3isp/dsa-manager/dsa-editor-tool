/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.roles;

import java.util.ArrayList;
import java.util.List;

import net.customware.gwt.dispatch.client.DispatchAsync;

import org.enunes.gwt.mvp.client.EventBus;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.inject.Inject;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.presenter.BusyDisplayEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.presenter.IdleDisplayEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.AbstractControllablePresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Term;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.roles.CredentialExpression;

public class RolePresenterImpl extends
        AbstractControllablePresenter<RolePresenter.RoleDisplay> implements
        RolePresenter {

    private Term role;
    private List<Term> allCredentials = new ArrayList<Term>();

    @Inject
    public RolePresenterImpl(EventBus eventBus, RoleDisplay display,
            final DispatchAsync dispatcher) {
        super(eventBus, display);
    }

    @Override
    public void bind() {
        super.bind();
        registerHandler(display.getChangeDefinitionPanel().addOpenHandler(
                new OpenHandler<DisclosurePanel>() {

                    @Override
                    public void onOpen(OpenEvent<DisclosurePanel> event) {
                        fireEvent(new BusyDisplayEvent(
                                RolePresenterImpl.this.display));
                    }
                }));
        registerHandler(display.getOkButton().addClickHandler(
                new ClickHandler() {

                    @Override
                    public void onClick(ClickEvent event) {
                        fireEvent(new IdleDisplayEvent(
                                RolePresenterImpl.this.display));
                    }
                }));
    }

    @Override
    public Term getRole() {
        return role;
    }

    @Override
    public List<CredentialExpression> getRoleDefinition() {
        final int count = display.getCredentialExpressionCount();
        List<CredentialExpression> credentialExpressions = new ArrayList<CredentialExpression>(
                count);
        for (int i = 0; i < count; i++)
            credentialExpressions.add(new CredentialExpression(allCredentials
                    .get(display.getCredentialIndexInExpression(i)), display
                    .getAttributeInExpression(i), display
                    .getOperatorInExpression(i)));
        return credentialExpressions;
    }

    @Override
    public void set(Term role, List<Term> allCredentials) {
        this.role = role;
        display.setRole(role.getHumanForm());
        this.allCredentials.clear();
        this.allCredentials.addAll(allCredentials);
        display.setCredentials(getCredentialsHumanForms());
    }

    @Override
    public void set(Term role, List<Term> allCredentials,
            List<CredentialExpression> definition) {
        set(role, allCredentials);
        for (CredentialExpression ce : definition)
            display.addCredentialExpression(
                    allCredentials.indexOf(ce.getCredential()),
                    ce.getAttributeExpression(), ce.getOperator());
    }

    private List<String> getCredentialsHumanForms() {
        List<String> humanForms = new ArrayList<String>(allCredentials.size());
        for (Term credential : allCredentials)
            humanForms.add(credential.getHumanForm());
        return humanForms;
    }
}
