/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa;

import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.Statement;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.StatementType;

public class Tokens {

    public static final String CNL4DSA_URI = "http://www.coco-cloud.eu/vocabularies/cnl4dsa#";

    public static Token TOKEN_IF = new Token(CNL4DSA_URI + "if", "IF");

    public static Token TOKEN_AFTER = new Token(CNL4DSA_URI + "after", "AFTER");

    public static Token TOKEN_THEN = new Token(CNL4DSA_URI + "then", "THEN");

    public static Token TOKEN_CAN = new Token(CNL4DSA_URI + "can", "CAN");

    public static Token TOKEN_MUST = new Token(CNL4DSA_URI + "must", "MUST");

    /**
     * CANNOT is kind of syntactic sugar: it's used only in Prohibitions, which
     * are implicitly negated, and so we do not insert a real NOT in the
     * statement, but we simply use "can". The uid is the same as that of Token
     * CAN: technically, this is a mistake for two reasons: 1. two tokens cannot
     * have the same UID 2. since TOKEN_CAN and TOKEN_CANNOT have the same UID,
     * they also have the same syntactic-form "can"
     * 
     * ... but, for the time being, we are going to live with this mistake,
     * otherwise we must change the CNL4DSA grammar.
     * 
     * TODO: use a different UID, so that CANNOT and CAN are different!
     */
    public static final Token TOKEN_CANNOT = new Token(CNL4DSA_URI + "can",
            "CANNOT");

    public static Token TOKEN_AND = new Token(CNL4DSA_URI + "and", "AND");
    
    public static Token TOKEN_OR = new Token(CNL4DSA_URI + "or", "OR");

    public static Token TOKEN_NOT = new Token(CNL4DSA_URI + "not", "NOT");

    public static Token[] CNL4DSA_TOKENS = new Token[] { TOKEN_IF, TOKEN_AFTER,
            TOKEN_THEN, TOKEN_CAN, TOKEN_CANNOT, TOKEN_MUST, TOKEN_AND,TOKEN_OR,
            TOKEN_NOT };

    /**
     * Returns the {@link Token} corresponding to the passed String, or null if
     * there is no such a {@link Token}.
     * 
     * @param syntacticForm
     *            of the {@link Token}
     * @param statementType
     *            of the {@link Statement} that the String syntacticForm comes
     *            from: this is required to distinguish between TOKEN_CAN and
     *            TOKEN_CANNOT.
     * 
     * @return the {@link Token} corresponding to the passed String, or null if
     *         there is no such a {@link Token}.
     */
    public static Token getTokenHavingSyntacticForm(String syntacticForm,
            StatementType statementType) {
        Token result = null;
        for (Token token : CNL4DSA_TOKENS) {
            if (syntacticForm.equals(token.getSyntacticForm()))
                // we found a match between syntactic-forms, so we have a Token
                if (token.equals(TOKEN_CAN) || token.equals(TOKEN_CANNOT))
                    // is it CAN or CANNOT?
                    if (statementType.equals(StatementType.AUTHORIZATION)
                            || statementType
                                    .equals(StatementType.DATA_SUBJECT_AUTHORIZATION)
                            || statementType
                                    .equals(StatementType.DERIVED_OBJECTS_AUTHORIZATION))
                        result = TOKEN_CAN;
                    else if (statementType.equals(StatementType.PROHIBITION)
                            || statementType
                                    .equals(StatementType.DERIVED_OBJECTS_PROHIBITION))
                        result = TOKEN_CANNOT;
                    else
                        assert false : "token" + token + " in unexpected "
                                + "StatementType " + statementType;
                else
                    // it is neither CAN nor CANNOT, so we can just return it
                    result = token;
        }
        return result;
    }

    public static Token FAKE_TOKEN_REFERENCE = new Token(CNL4DSA_URI
            + "reference", "REFERENCE");

    // public static Token FAKE_TOKEN_END_OF_STATEMENT =
    // new Token(CNL4DSA_URI + "end-of-statement", "end-of-statement");

    public static Token FAKE_TOKEN_HAS_SUBTERMS = new Token(CNL4DSA_URI
            + "has-subterms", "...");

}
