/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.fsm;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.model.VocabularyChoicesUpdater;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.helper.ReferenceableTermsKeeper;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Tokens;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.vocabulary.GetLeafTerms;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.vocabulary.VocabularyGetter;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.vocabulary.VocabularyGetResult;

public class CnlPrompterState16 extends
        AbstractCnlPrompterStateWithTermsAndTokenReference {

    private final VocabularyChoicesUpdater vocabularyChoicesUpdater;
    private final VocabularyGetter<VocabularyGetResult> actionConstrainingReferenceToken;

    public CnlPrompterState16() {
        super();
        // Note: we're building a CNL AFTER-fragment here, and we're at the
        // point where we have to show possible subjects to the user. We pick
        // only leaf-terms, because we don't want to have sub-classes
        // expansion when saving the DSA. If the user would select a
        // non-leaf-term, then, when saving the DSA, we would have to expand
        // such term using all its subclasses, which, in turn, would yield a set
        // of CNL atomic-fragment linked with the PARALLEL operator. This is
        // impossible, because CNL grammar allows only an atomic fragment
        // after the token AFTER. Therefore we use here only leaf-terms, both
        // for constraining the token REFERENCE and for presenting choosable
        // terms.
        actionConstrainingReferenceToken = new GetLeafTerms(
                ReferenceableTermsKeeper.getInstance().getVocabularyUri());
        VocabularyGetter<VocabularyGetResult> action = actionConstrainingReferenceToken;
        vocabularyChoicesUpdater = new VocabularyChoicesUpdater(tokens, action,
                actionConstrainingReferenceToken);
    }

    @Override
    public VocabularyChoicesUpdater getVocabularyChoicesUpdater() {
        return vocabularyChoicesUpdater;
    }

    @Override
    public void handleSelection(CnlPrompterContext context,
            SyntacticItem selectedItem) {
        if (selectedItem.equals(Tokens.FAKE_TOKEN_REFERENCE)) {
            context.updateAndEnableReferenceSelection(new CnlPrompterState17(),
                    actionConstrainingReferenceToken);
        } else {
            context.update(new CnlPrompterState18(selectedItem), selectedItem);
        }
    }

}
