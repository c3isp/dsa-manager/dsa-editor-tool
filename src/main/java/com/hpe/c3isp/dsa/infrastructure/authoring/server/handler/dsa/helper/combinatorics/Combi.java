/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa.helper.combinatorics;

import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.function.Consumer;

/**
 * It does the following.
 *
 * Given
 *
 * <pre>
 * String[][] data = new String[][] { { &quot;a&quot;, &quot;b&quot; }, { &quot;x&quot;, &quot;y&quot;, &quot;z&quot; },
 *         { &quot;c&quot;, &quot;d&quot; } };
 * </pre>
 *
 * it returns: [a, x, c] [a, x, d] [a, y, c] [a, y, d] [a, z, c] [a, z, d] [b,
 * x, c] [b, x, d] [b, y, c] [b, y, d] [b, z, c] [b, z, d]
 *
 * @author Marco Luca Sbodio (mailto:marco.sbodio@hp.com)
 *
 * @param <T>
 */
public class Combi<T> implements Iterator<T[]> {

    private final T[][] data;
    private final int[] curIndexes;
    private final int n;

    private boolean finished = false;

    public Combi(T[][] data) {
        assert data != null : "cannot work on null data";
        this.data = data;
        // initialize the number of elements n
        n = data.length;
        // sanity check
        for (int i = 0; i < n; i++)
            assert this.data[i].length > 0 : "cannot work with empty set at position "
                    + i;
        // initialize curIndexes
        curIndexes = new int[n];
        for (int i = 0; i < n; i++)
            curIndexes[i] = 0;
    }

    @Override
    public boolean hasNext() {
        return !finished;
    }

    @SuppressWarnings("unchecked")
    @Override
    public T[] next() {
        T[] result = (T[]) Array.newInstance(data[0].getClass()
                .getComponentType(), n);
        // compute the result to be returned
        for (int i = 0; i < n; i++)
            result[i] = data[i][curIndexes[i]];
        // update the indexes
        for (int i = n - 1; i >= 0; i--) {
            if (curIndexes[i] + 1 < data[i].length) {
                // if we can increment curIndex[i], the do it and
                // leave other indexes unchanged
                curIndexes[i]++;
                break;
            } else {
                // otherwise reset curIndex[i] to 0
                curIndexes[i] = 0;
                // if i == 0 then we have finished
                finished = i == 0;
            }
        }
        return result;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void forEachRemaining(Consumer<? super T[]> arg0) {
        // TODO Auto-generated method stub

    }

}
