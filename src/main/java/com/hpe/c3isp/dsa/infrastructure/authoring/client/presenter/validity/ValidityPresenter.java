/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.validity;

import java.util.Date;

import com.google.gwt.event.logical.shared.HasOpenHandlers;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.ControllablePresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.ControllableDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Validity;

public interface ValidityPresenter extends ControllablePresenter<ValidityPresenter.ValidityDisplay> {
	public static final String DATEFORMAT = "MMMM dd, yyyy";
    public interface ValidityDisplay extends ControllableDisplay {
//        HasOpenHandlers<DisclosurePanel> getDsaStartDatePanel();
//
//        HasOpenHandlers<DisclosurePanel> getDsaEndDatePanel();

//        HasValueChangeHandlers<Date> getDsaStartDatePicker();
//
//        HasValueChangeHandlers<Date> getDsaEndDatePicker();

        HasValueChangeHandlers<Date> getDsaStartDateBox();

        HasValueChangeHandlers<Date> getDsaEndDateBox();

        void setStartDate(Date value, String label);
        void setEndDate(Date value, String label);

        void badDurationValue(String value);
    }

    Validity getValidity();

    void setValidity(Validity validityBean);

    void setValidityLabels(String startDate, String endDate, String unit);

}
