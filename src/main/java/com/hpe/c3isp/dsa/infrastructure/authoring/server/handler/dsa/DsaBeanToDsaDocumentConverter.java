/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xmlbeans.GDuration;

import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa.helper.SimpleOntologyAwareCNL4DSABuilder;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa.helper.XmlUtil;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Term;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.ComplexPolicy;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Credentials;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.DsaBean;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Organization;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Validity;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.roles.CredentialExpression;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.roles.Operator;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.roles.Roles;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.Issuer;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.Statement;

import eu.cocoCloud.dsa.AuthorizationType;
import eu.cocoCloud.dsa.AuthorizationsType;
import eu.cocoCloud.dsa.ComplexPolicyType;
import eu.cocoCloud.dsa.DataType;
import eu.cocoCloud.dsa.DatumType;
import eu.cocoCloud.dsa.DsaDocument;
import eu.cocoCloud.dsa.DsaType;
import eu.cocoCloud.dsa.EntityType;
import eu.cocoCloud.dsa.ExpressionType;
import eu.cocoCloud.dsa.IssuerType;
import eu.cocoCloud.dsa.IssuerType.Enum;
import eu.cocoCloud.dsa.LanguageType;
import eu.cocoCloud.dsa.ObligationType;
import eu.cocoCloud.dsa.ObligationsType;
import eu.cocoCloud.dsa.PartiesType;
import eu.cocoCloud.dsa.PolicyType;
import eu.cocoCloud.dsa.ProhibitionType;
import eu.cocoCloud.dsa.ProhibitionsType;
import eu.cocoCloud.dsa.Role;
import eu.cocoCloud.dsa.StatusType;
import eu.cocoCloud.dsa.ValidityType;

public class DsaBeanToDsaDocumentConverter {

    private final DsaBean dsaBean;
    private static Log log = LogFactory.getLog(DsaBeanToDsaDocumentConverter.class);
    public DsaBeanToDsaDocumentConverter(DsaBean dsaBean) {
        this.dsaBean = dsaBean;
    }

    public DsaDocument convert() {
        // build the DsaDocuemtn
        DsaDocument dsaDocument = DsaDocument.Factory.newInstance(XmlUtil
                .getXmlOptions());
        DsaType dsa = dsaDocument.addNewDsa();

        // set DSA metadata
        dsa.setDescription(dsaBean.getDescription());
        dsa.setExpirationPolicy(complexPolicyAsXml(dsaBean.getExpirationPolicy()));
        dsa.setUpdatePolicy(complexPolicyAsXml(dsaBean.getUpdatePolicy()));
        dsa.setRevocationPolicy(complexPolicyAsXml(dsaBean.getRevocationPolicy()));
        dsa.setId(dsaBean.getUid());
        dsa.setStatus(retrieveStatus(dsaBean.getStatus()));
        dsa.setVocabularyUrl(dsaBean.getVocabularyUri());
        dsa.setTitle(dsaBean.getTitle());
        dsa.setVersion(dsaBean.getVersion());

        // set Parties section
        PartiesType partiesXml = partiesAsXml(dsaBean.getParties());
        assert partiesXml != null;
        dsa.setParties(partiesXml);

        // set Validity section
        ValidityType validityXml = validityAsXml(dsaBean.getValidity());
        assert validityXml != null;
        dsa.setValidity(validityXml);
        dsa.setDataClassification(dsaBean.getDataClassification());

        dsa.setPurpose(dsaBean.getPurpose());
        dsa.setApplicationDomain(dsaBean.getApplicationDomain());


        dsa.setGoverningLaw(dsaBean.getGoverningLaw());
        dsa.setIndemnities(dsaBean.getIndemnities());
        dsa.setEncryptionKeySchema(dsaBean.getKeyEncryptionSchema());
        //user preferences
        dsa.setUserConsent(dsaBean.isUserConsent());
        dsa.setConsentRequired(dsaBean.isRequiredConsent());
        // instantiate a CNL4DSA builder to translate CNL4DSA-E to CNL4DSA
        SimpleOntologyAwareCNL4DSABuilder cnl4dsaBuilder = new SimpleOntologyAwareCNL4DSABuilder(
                dsaBean);
        // set Authorizations section
        AuthorizationsType authorizationsXml = authorizationsAsXml(
                dsaBean.getAuthorizations(), cnl4dsaBuilder);
        if (authorizationsXml != null)
            dsa.setAuthorizations(authorizationsXml);

        AuthorizationsType dataSubjectAuthorizationsXml = authorizationsAsXml(
                dsaBean.getDataSubjectAuthorizations(), cnl4dsaBuilder);
        if (dataSubjectAuthorizationsXml != null)
            dsa.setDataSubjectAuthorizations(dataSubjectAuthorizationsXml);

        // set Obligations section
        ObligationsType obligationsXml = obligationsAsXml(
                dsaBean.getObligations(), cnl4dsaBuilder);
        if (obligationsXml != null)
            dsa.setObligations(obligationsXml);

        // set Prohibitions section
        ProhibitionsType prohibitionsXml = prohibitionsAsXml(
                dsaBean.getProhibitions(), cnl4dsaBuilder);
        if (prohibitionsXml != null)
            dsa.setProhibitions(prohibitionsXml);

        // third parties policies
        AuthorizationsType thirdPartiesAuthorizationsXml = authorizationsAsXml(
                dsaBean.getDerivedObjectsAuthorizations(), cnl4dsaBuilder);
        if (thirdPartiesAuthorizationsXml != null)
            dsa.setDerivedObjectsAuthorizations(thirdPartiesAuthorizationsXml);

        // set Obligations section
        ObligationsType thirdPartiesObligationsXml = obligationsAsXml(
                dsaBean.getDerivedObjectsObligations(), cnl4dsaBuilder);
        if (thirdPartiesObligationsXml != null)
            dsa.setDerivedObjectsObligations(thirdPartiesObligationsXml);

        // set Prohibitions section
        ProhibitionsType thirdPartiesProhibitionsXml = prohibitionsAsXml(
                dsaBean.getDerivedObjectsProhibitions(), cnl4dsaBuilder);
        if (thirdPartiesProhibitionsXml != null)
            dsa.setDerivedObjectsProhibitions(thirdPartiesProhibitionsXml);


        // set Data section
        DataType dataXml = dataAsXml(cnl4dsaBuilder);
        if (dataXml != null)
            dsa.setData(dataXml);

        dsaDocument.setDsa(dsa);
        return dsaDocument;
    }

    private eu.cocoCloud.dsa.StatusType.Enum retrieveStatus(String status) {
        if (status.compareTo("TEMPLATE") == 0)
            return StatusType.TEMPLATE;
        if (status.compareTo("COMPLETED") == 0)
            return StatusType.COMPLETED;
        if (status.compareTo("CUSTOMISED") == 0)
            return StatusType.CUSTOMISED;
        if (status.compareTo("PREPARED") == 0)
            return StatusType.PREPARED;
        if (status.compareTo("AVAILABLE") == 0)
            return StatusType.AVAILABLE;
        return StatusType.TEMPLATE;
    }


    private eu.cocoCloud.dsa.PolicyType.Enum retrievePolicy(String pol) {
        if (pol.compareTo(PolicyType.DELETE_IN_SPECIFIED_PERIOD.toString()) == 0)
            return PolicyType.DELETE_IN_SPECIFIED_PERIOD;
        if (pol.compareTo(PolicyType.DENY_ALL.toString()) == 0)
            return PolicyType.DENY_ALL;
        if (pol.compareTo(PolicyType.DENY_ALL_AND_DELETE_NOW.toString()) == 0)
            return PolicyType.DENY_ALL_AND_DELETE_NOW;
        return PolicyType.DENY_ALL;
    }

    private PartiesType partiesAsXml(List<Organization> organizations) {
        PartiesType parties = PartiesType.Factory.newInstance();
        for (Organization organization : organizations) {
            EntityType org = parties.addNewOrganization();
            org.setId(organization.getUid());
            org.setName(organization.getName());
            org.setResponsibilities(organization.getResponsibilities());
            org.setRole(retrieveRole(organization.getRole()));
        }
        return parties;
    }

    private eu.cocoCloud.dsa.Role.Enum retrieveRole(String role) {
        if (role.compareTo("data-controller") == 0) {
            return Role.DATA_CONTROLLER;
        }
        if (role.compareTo("data-subject") == 0) {
            return Role.DATA_SUBJECT;
        }
        if (role.compareTo("data-processor") == 0) {
            return Role.DATA_PROCESSOR;
        }
        return Role.DATA_CONTROLLER;
    }

    private DataType dataAsXml(SimpleOntologyAwareCNL4DSABuilder cnl4dsaBuilder) {
    	log.debug("------------------- DsaBeanToDsaDocumentConverter: dataAsXml called");
        DataType data = null;
        Map<String,String> vdStatements = cnl4dsaBuilder
                .buildAllVariableDeclarationStatements();
        if (vdStatements.size() > 0) {
            data = DataType.Factory.newInstance();
            for (String vdStatement : vdStatements.keySet()) {

            	System.out.println("---- vdStatement="+vdStatement);
                String[] ps = vdStatement.split(" ");
                DatumType datum = data.addNewDatum();
                datum.setValue(vdStatements.get(vdStatement));
                datum.setId("DATUM_" + ps[0].substring(1));
                ExpressionType cnl4dsa = datum.addNewExpression();
                cnl4dsa.setLanguage(LanguageType.CNL_4_DSA);
                cnl4dsa.setStringValue(vdStatement);

            }
        }
        return data;
    }

    private AuthorizationsType authorizationsAsXml(List<Statement> statements,
            SimpleOntologyAwareCNL4DSABuilder cnl4dsaBuilder) {
        AuthorizationsType auths = null;
        if (statements.size() > 0) {
            auths = AuthorizationsType.Factory.newInstance();
            assert auths != null;
            for (Statement statement : statements) {
                Issuer issuer = statement.getStatementMetadata().getIssuer();
                Enum issuerType = retrieveIssuerType(issuer);
                String cnl4dsaeStr = statement.getSyntacticForm();
                String cnl4dsaStr = cnl4dsaBuilder.buildStatement(
                        statement.getType(), cnl4dsaeStr);

                AuthorizationType auth = auths.addNewAuthorization();
                auth.setId(statement.getUid());
                auth.setIndex(statement.getIndex());
                auth.setIsProtected(statement.getStatementMetadata()
                        .isProtected());
                auth.setIsPendingRule(statement.getStatementMetadata().isPendingRule());
                auth.setStatementInfo(statement.getStatementMetadata().getStatementInfo());
                auth.setInputValue(statement.getStatementMetadata().getInputValue());
                auth.setConflict(statement.getStatementMetadata().isConflict());

                ExpressionType cnl4dsae = auth.addNewExpression();
                cnl4dsae.setLanguage(LanguageType.CNL_4_DSA_E);
                cnl4dsae.setStringValue(cnl4dsaeStr);
                cnl4dsae.setIssuer(issuerType);

                ExpressionType upol = auth.addNewExpression();
                upol.setLanguage(LanguageType.UPOL);
               //upol.setStringValue(XmlUtil.getCDataValue());
                upol.setIssuer(issuerType);

                ExpressionType userText = auth.addNewExpression();
                userText.setLanguage(LanguageType.USER_TEXT);
                userText.setStringValue(statement.getHumanForm());
                userText.setIssuer(issuerType);

                ExpressionType cnl4dsa = auth.addNewExpression();
                cnl4dsa.setLanguage(LanguageType.CNL_4_DSA);
                cnl4dsa.setStringValue(cnl4dsaStr);
                cnl4dsa.setIssuer(issuerType);
            }
        }
        // add required attributes: category and conflicts_resolution_algorithm

        return auths;
    }

    private Enum retrieveIssuerType(Issuer issuer) {
        if (issuer == Issuer.LEGAL_EXPERT)
            return IssuerType.LEGAL_EXPERT;
        if (issuer == Issuer.POLICY_EXPERT)
            return IssuerType.POLICY_EXPERT;
        if (issuer == Issuer.USER)
            return IssuerType.USER;
        return null;
    }

    private ObligationsType obligationsAsXml(List<Statement> statements,
            SimpleOntologyAwareCNL4DSABuilder cnl4dsaBuilder) {
        ObligationsType obls = null;
        if (statements.size() > 0) {
            obls = ObligationsType.Factory.newInstance();
            assert obls != null;
            for (Statement statement : statements) {
                Issuer issuer = statement.getStatementMetadata().getIssuer();
                Enum issuerType = retrieveIssuerType(issuer);
                String cnl4dsaeStr = statement.getSyntacticForm();
                String cnl4dsaStr = cnl4dsaBuilder.buildStatement(
                        statement.getType(), cnl4dsaeStr);

                ObligationType obl = obls.addNewObligation();
                obl.setId(statement.getUid());
                obl.setIndex(statement.getIndex());
                obl.setIsProtected(statement.getStatementMetadata()
                        .isProtected());
                obl.setIsPendingRule(statement.getStatementMetadata().isPendingRule());
                obl.setStatementInfo(statement.getStatementMetadata().getStatementInfo());
                obl.setInputValue(statement.getStatementMetadata().getInputValue());
                obl.setConflict(statement.getStatementMetadata().isConflict());
                // TODO
                // String referenceUid = statement.getReferenceUid();
                // if (referenceUid != null && ! "".equals(referenceUid))
                // obl.setReference(referenceUid);

                ExpressionType cnl4dsae = obl.addNewExpression();
                cnl4dsae.setLanguage(LanguageType.CNL_4_DSA_E);
                cnl4dsae.setStringValue(cnl4dsaeStr);
                cnl4dsae.setIssuer(issuerType);

                ExpressionType upol = obl.addNewExpression();
                upol.setLanguage(LanguageType.UPOL);
                //upol.setStringValue(XmlUtil.getCDataValue());
                upol.setIssuer(issuerType);

                ExpressionType userText = obl.addNewExpression();
                userText.setLanguage(LanguageType.USER_TEXT);
                userText.setStringValue(statement.getHumanForm());
                userText.setIssuer(issuerType);

                ExpressionType cnl4dsa = obl.addNewExpression();
                cnl4dsa.setLanguage(LanguageType.CNL_4_DSA);
                cnl4dsa.setStringValue(cnl4dsaStr);
                cnl4dsa.setIssuer(issuerType);

            }
        }
        return obls;
    }

    private ProhibitionsType prohibitionsAsXml(List<Statement> statements,
            SimpleOntologyAwareCNL4DSABuilder cnl4dsaBuilder) {
        ProhibitionsType pros = null;
        if (statements.size() > 0) {
            pros = ProhibitionsType.Factory.newInstance();
            assert pros != null;
            for (Statement statement : statements) {
                Issuer issuer = statement.getStatementMetadata().getIssuer();
                Enum issuerType = retrieveIssuerType(issuer);
                String cnl4dsaeStr = statement.getSyntacticForm();
                String cnl4dsaStr = cnl4dsaBuilder.buildStatement(
                        statement.getType(), cnl4dsaeStr);

                ProhibitionType pro = pros.addNewProhibition();
                pro.setId(statement.getUid());
                pro.setIndex(statement.getIndex());
                pro.setIsProtected(statement.getStatementMetadata()
                        .isProtected());
                pro.setIsPendingRule(statement.getStatementMetadata().isPendingRule());
                pro.setStatementInfo(statement.getStatementMetadata().getStatementInfo());
                pro.setInputValue(statement.getStatementMetadata().getInputValue());
                pro.setConflict(statement.getStatementMetadata().isConflict());
                ExpressionType cnl4dsae = pro.addNewExpression();
                cnl4dsae.setLanguage(LanguageType.CNL_4_DSA_E);
                cnl4dsae.setStringValue(cnl4dsaeStr);
                cnl4dsae.setIssuer(issuerType);

                ExpressionType userText = pro.addNewExpression();
                userText.setLanguage(LanguageType.USER_TEXT);
                userText.setStringValue(statement.getHumanForm());
                userText.setIssuer(issuerType);

                ExpressionType upol = pro.addNewExpression();
                upol.setLanguage(LanguageType.UPOL);
               // upol.setStringValue(XmlUtil.getCDataValue());
                upol.setIssuer(issuerType);

                ExpressionType cnl4dsa = pro.addNewExpression();
                cnl4dsa.setLanguage(LanguageType.CNL_4_DSA);
                cnl4dsa.setStringValue(cnl4dsaStr);
                cnl4dsa.setIssuer(issuerType);
            }
        }
        return pros;
    }

    private ValidityType validityAsXml(Validity validityBean) {
        assert validityBean.getStartDate() != null
                && validityBean.getEndDate() != null;

        ValidityType validity = ValidityType.Factory.newInstance();

        Calendar startDateCalendar = Calendar.getInstance();
        startDateCalendar.setTime(validityBean.getStartDate());
        validity.setStartDate(startDateCalendar);

        Calendar endDateCalendar = Calendar.getInstance();
        endDateCalendar.setTime(validityBean.getEndDate());
        validity.setEndDate(endDateCalendar);

        int durationInDays = validityBean.getOfflineLicencesDurationInDays();
        validity.setOfflineLicencesDuration(new GDuration(1, 0, 0,
                durationInDays, 0, 0, 0, null));

        return validity;
    }

    private ComplexPolicyType complexPolicyAsXml(ComplexPolicy complexPolicyBean) {
        ComplexPolicyType complexPolicyType = ComplexPolicyType.Factory.newInstance();
        complexPolicyType.setPeriodInDays(complexPolicyBean.getPeriodInDays());
        String policy = complexPolicyBean.getPolicy();
        eu.cocoCloud.dsa.PolicyType.Enum complezyPolicy= PolicyType.Enum.forString(policy);
        complexPolicyType.setComplexPolicy(complezyPolicy);
        return complexPolicyType;
    }

    private String buildCredentialsString(Credentials credentials) {
        // credentialsString has the following pattern:
        // Location:ID1,ID2;Authentication:ID34
        StringBuffer buffer = new StringBuffer();
        Iterator<Term> credentialsIterator = credentials.getCredentials()
                .iterator();
        while (credentialsIterator.hasNext()) {
            Term credential = credentialsIterator.next();
            buffer.append(credential.getSyntacticForm());
            buffer.append(":");
            Iterator<Organization> organizationsIterator = credentials
                    .getTrustedContextProviders(credential).iterator();
            while (organizationsIterator.hasNext()) {
                buffer.append(organizationsIterator.next().getUid());
                if (organizationsIterator.hasNext())
                    buffer.append(",");
            }
            if (credentialsIterator.hasNext())
                buffer.append(";");
        }
        return buffer.toString();
    }

    private String buildRolesString(Roles roles) {
        // rolesString has the following pattern:
        // Toxicologist:Authentication.group=12 AND
        // Location.country=Italy;Paramedic:Authentication.id=23
        StringBuffer buffer = new StringBuffer();
        Iterator<Term> rolesIterator = roles.getRoles().iterator();
        while (rolesIterator.hasNext()) {
            Term role = rolesIterator.next();
            buffer.append(role.getSyntacticForm());
            buffer.append(":");
            for (CredentialExpression ce : roles.getRoleDefinition(role)) {
                buffer.append(ce.getCredential().getSyntacticForm());
                buffer.append(ce.getAttributeExpression());
                final Operator op = ce.getOperator();
                if (!op.equals(Operator.END))
                    buffer.append(" ").append(op.toString()).append(" ");
            }
            if (rolesIterator.hasNext())
                buffer.append(";");
        }
        return buffer.toString();
    }

}
