/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FilenameUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.DsaManagerService;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa.helper.DsaStorage;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.RepositoryContent;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.VersionManager;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.Issuer;

/**
 * The server side implementation of the RPC service.
 */

@SuppressWarnings("serial")
public class DsaManagerServiceImpl extends RemoteServiceServlet implements DsaManagerService{

	public String viewRawData(String dsaUuid, boolean isStandalone) throws IllegalArgumentException,
	IOException {
		//bugged (PATH_TRAVERSAL_IN)
		//      String filePath = DsaStorage.getInstance().getTempFilePath(dsaContent,dsaID);

		//FIXED
		String fixedDsaID = FilenameUtils.getName(dsaUuid); //fix

		if (isStandalone){
//			String pathContext = DsaStorage.getInstance().getBasePath() + dsaUuid;
			String pathContext = DsaStorage.getInstance().getBasePath() + fixedDsaID;

			log("opening.." + pathContext);
			return readFile(escapeHtml(pathContext));
		}else{
			String document = null;
			//the DSAAT is invoked as a service
			DsaApiServiceManager dsaSrvMgr = new DsaApiServiceManager();
			if (dsaSrvMgr.getDSA(dsaUuid.substring(0, dsaUuid.indexOf(".xml")))){
				document = dsaSrvMgr.getDocumentContent().getDocumentContent();
			}
			//           String content = escapeHtml(document);
			//           log("Content of the document=" + content);
			return document;
		}
	}

	public String viewDSA(String dsaUuid, boolean isStandalone) throws IllegalArgumentException,
	IOException {
		//bugged (PATH_TRAVERSAL_IN)
				//      String filePath = DsaStorage.getInstance().getTempFilePath(dsaContent,dsaID);

		//FIXED
		String fixedDsaID = FilenameUtils.getName(dsaUuid); //fix


		if (isStandalone){
//			String pathContext = DsaStorage.getInstance().getBasePath() + dsaUuid;
			String pathContext = DsaStorage.getInstance().getBasePath() + fixedDsaID;


			log("opening.." + pathContext);
			return readFile(escapeHtml(pathContext));
		}else{
			String document = null;
			//the DSAAT is invoked as a service
			DsaApiServiceManager dsaSrvMgr = new DsaApiServiceManager();
			if (dsaSrvMgr.getDSA(dsaUuid.substring(0, dsaUuid.indexOf(".xml")))){
				document = dsaSrvMgr.getDocumentContent().getDocumentContent();
			}
			//   String content = escapeHtml(document);
			//   log("Content of the document=" + content);
			return document;
		}
	}

	/**
	 * Escape an html string. Escaping data received from the client helps to
	 * prevent cross-site script vulnerabilities.
	 *
	 * @param html
	 *            the html string to escape
	 * @return the escaped string
	 */
	private String escapeHtml(String html) {
		if (html == null) {
			return null;
		}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;")
				.replaceAll(">", "&gt;");
	}

	private String readFile(String file) throws IOException {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(file));
			String line = null;
			StringBuilder stringBuilder = new StringBuilder();
			String ls = System.getProperty("line.separator");

			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
				stringBuilder.append(ls);
			}

			return stringBuilder.toString();

		} finally {
			if (reader != null) {reader.close();}
		}
	}

	@Override
	public ArrayList<RepositoryContent> loadRepositoryContent(String userRole) {

		ArrayList<RepositoryContent> repoContent = loadTmpContent(userRole); // TODO
		// implement
		// with
		// database
		return repoContent;
	}

	private ArrayList<RepositoryContent> loadTmpContent(String userRole) {

		String pathContext = DsaStorage.getInstance().getBasePath();
		log("opening.." + pathContext);
		return listFiles(pathContext, userRole);
	}

	/**
	 * Retrieve files into the repository according to the user role
	 *
	 * @param directoryName
	 * @param userRole
	 * @return
	 */
	private ArrayList<RepositoryContent> listFiles(String directoryName,
			String userRole) {

		File directory = new File(directoryName);
		log("User is logged as " + userRole);
		List<File> fileList = new ArrayList<File>();
		ArrayList<RepositoryContent> resultList = new ArrayList<RepositoryContent>();

		// get all the files from a directory
		File[] fList = directory.listFiles();
		fileList.addAll(Arrays.asList(fList));
		ArrayList<String> dsaStatusList = new ArrayList<String>();

		if (userRole.compareTo(Issuer.LEGAL_EXPERT.getLabel()) == 0) {
			dsaStatusList.add("TEMPLATE");
			dsaStatusList.add("COMPLETED");
		} else if (userRole.compareTo(Issuer.POLICY_EXPERT.getLabel()) == 0) {
			dsaStatusList.add("TEMPLATE");
			dsaStatusList.add("CUSTOMISED");
			dsaStatusList.add("COMPLETED");
			dsaStatusList.add("AVAILABLE");
		} else {
			dsaStatusList.add("PREPARED");
			dsaStatusList.add("COMPLETED");
		}
		//        int i = 0;
		for (File file : fList) {
			//            i++;
			double bytes = file.length();
			if(bytes <= 0) {
				continue;
			}

			String dsaStatus = getAttributeFromXMLFile(file, "dsa", "status");
			if (file.isFile() && (file.getName().startsWith("DSA") && (dsaStatusList.contains(dsaStatus)))) {
				log(file.getAbsolutePath());
				RepositoryContent repoContent = new RepositoryContent();
				repoContent.setTitle(getAttributeFromXMLFile(file, "dsa",
						"title"));
				repoContent.setSize(String.valueOf(file.length()));
				repoContent.setStatus(dsaStatus);
				repoContent.setUuid(file.getName());
				resultList.add(repoContent);
			}

		}
		return resultList;
	}

	private static String getAttributeFromXMLFile(File file, String itemName, String attribute) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		try {
			//https://find-sec-bugs.github.io/bugs.htm

//			By disabling DTD, almost all XXE attacks will be prevented.
//
//			DocumentBuilderFactory df = DocumentBuilderFactory.newInstance();
//			spf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
//			DocumentBuilder builder = df.newDocumentBuilder();


			factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
			System.out.println("getAttributeFromXMLFile: disabling DTD" );

		} catch (ParserConfigurationException e2) {
			e2.printStackTrace();
		}

		DocumentBuilder builder = null;
		String value = null;
		try {
			builder = factory.newDocumentBuilder();
			Document doc = builder.parse(file);

			NodeList nodeList = doc.getChildNodes();

			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);

				if (node.hasAttributes()) {
					Attr attr = (Attr) node.getAttributes().getNamedItem(
							itemName);
					if (attr != null) {
						value = attr.getValue();
					} else {
						attr = (Attr) node.getAttributes().getNamedItem(
								attribute);
						value = attr.getValue();
					}
					return value;
				}
			}

		} catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return value;
	}

	// protected String getString(String tagName, Element element) {
	//
	//
	// NodeList nodeList = element.getElementsByTagName(tagName);
	//
	// for (int i = 0; i < nodeList.getLength(); i++) {
	// Node node = nodeList.item(i);
	//
	// if (node.hasAttributes()) {
	// Attr attr = (Attr) node.getAttributes().getNamedItem("title");
	// if (attr != null) {
	// return attr.getValue();
	// }else{
	// attr = (Attr) node.getAttributes().getNamedItem("coco-cloud:title");
	// return attr.getValue();
	// }
	//
	// }
	//
	// }
	// return null;
	// }

	@Override
	public String retrieveUser(String name) {
		return name;
	}

	@Override
	public String getVersion(String version) {
		return VersionManager.getVersion();
	}

	@Override
	public void deleteFileFromRepository(String fileUuid) {
		//bugged (PATH_TRAVERSAL_IN)
		//        String filePath = DsaStorage.getInstance().getTempFilePath(dsaContent,dsaID);

		//FIXED
		String fixedfileUuid = FilenameUtils.getName(fileUuid); //fix

		String path = DsaStorage.getInstance().getBasePath().concat("/").concat(fixedfileUuid);
		File file = new File(path);
		if (file.delete()) {
			log(file.getName() + " is deleted!");
		} else {
			log("Delete operation is failed.");
		}

	}

}
