/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.helper;

import com.hp.hpl.jena.ontology.*;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.util.iterator.Filter;
import com.hp.hpl.jena.vocabulary.*;

import java.io.StringWriter;
import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

// Referenced classes of package com.hp.cococloud.dsa.infrastructure.authoring.server.handler.vocabulary.helper:
//            VocabularyUtilities, QuerySolutionProcessor

public class VocabularyOntology {
    
    private static final Log log = LogFactory.getLog(VocabularyOntology.class);
    private final String vocUri;
    private final OntModel vocOntModel;
    private final String theUpperVocabularyUri;
    private final HashMap<String,String> paramTermList;

    public VocabularyOntology(String vocabularyUri,
            OntModel vocabularyOntModel, String upperVocabularyUri, HashMap<String,String> paramInputTermList) {
        vocUri = vocabularyUri;
        vocOntModel = vocabularyOntModel;
        theUpperVocabularyUri = upperVocabularyUri;
        paramTermList = paramInputTermList;
        
    }

    public String getUri() {
        return vocUri;
    }
    
    

    public HashMap<String,String> getParamTermList() {
        return paramTermList;
    }

    public Set<String> getActionsWithTermAsSubject(String termUri) {
        Set<String> result = new HashSet<String>();
        Set<String> possibleSubjects = getAllSuperClassesOf(termUri);
        possibleSubjects.add(termUri);
        Set<?> actionUris = getPresentableActions();
        for (Iterator<?> iterator = actionUris.iterator(); iterator.hasNext();) {
            String actionUri = (String) iterator.next();
            Set<String> declaredSubjects = getSubjectsForAction(actionUri);
            declaredSubjects.retainAll(possibleSubjects);
            if (!declaredSubjects.isEmpty())
                result.add(actionUri);
        }

        return dropSubClasses(result);
    }

    public Set<String> getLeafActionsWithTermAsSubject(String termUri) {
        Set<String> result = new HashSet<String>();
        Set<String> possibleSubjects = getAllSuperClassesOf(termUri);
        possibleSubjects.add(termUri);
        Set<String> actionUris = dropSuperClasses(getPresentableActions());
        for (Iterator<String> iterator = actionUris.iterator(); iterator
                .hasNext();) {
            String actionUri = (String) iterator.next();
            Set<String> declaredSubjects = getSubjectsForAction(actionUri);
            declaredSubjects.retainAll(possibleSubjects);
            if (!declaredSubjects.isEmpty())
                result.add(actionUri);
        }

        return result;
    }

    public Set<String> getRootTerms() {
        return dropSubClasses(getAllPresentableSubClassesOf((new StringBuilder(
                String.valueOf(theUpperVocabularyUri))).append("Term")
                .toString()));
    }

    public Set<String> getLeafTerms() {
        return dropSuperClasses(getAllPresentableSubClassesOf((new StringBuilder(
                String.valueOf(theUpperVocabularyUri))).append("Term")
                .toString()));
    }

    public Set<String> getSubTerms(String termUri) {
        return getDirectPresentableSubClassesOf(termUri);
    }

    public Set<String> getPredicatesWithTermInDomain(String termUri) {
        Set<String> result = new HashSet<String>();
        Set<String> possibleDomain = getAllSuperClassesOf(termUri);
        possibleDomain.add(termUri);
        Set<String> properties = getPresentableProperties();
        for (Iterator<String> iterator = properties.iterator(); iterator
                .hasNext();) {
            String property = (String) iterator.next();
            Set<String> declaredDomain = getDomainOfProperty(property);
            declaredDomain.retainAll(possibleDomain);
            if (!declaredDomain.isEmpty())
                result.add(property);
        }

        return dropSubClasses(result);
    }

    public Set<String> getTermsInDomainForPredicate(String predicateUri) {
        return dropSubClasses(keepPresentable(getDomainOfProperty(predicateUri)));
    }

    public Set<String> getAllTermsInDomainForPredicate(String predicateUri) {
        Set<String> result = getTermsInDomainForPredicate(predicateUri);
        Set<String> subclasses = new HashSet<String>();
        String uri;
        for (Iterator<String> iterator = result.iterator(); iterator.hasNext(); subclasses
                .addAll(getAllPresentableSubClassesOf(uri)))
            uri = (String) iterator.next();

        result.addAll(subclasses);
        return result;
    }

    public Set<String> getTermsInRangeForPredicate(String predicateUri) {
        return dropSubClasses(keepPresentable(getRangeOfProperty(predicateUri)));
    }

    public Set<String> getAllTermsInRangeForPredicate(String predicateUri) {
        Set<String> result = getTermsInRangeForPredicate(predicateUri);
        Set<String> subclasses = new HashSet<String>();
        String uri;
        for (Iterator<String> iterator = result.iterator(); iterator.hasNext(); subclasses
                .addAll(getAllPresentableSubClassesOf(uri)))
            uri = (String) iterator.next();

        result.addAll(subclasses);
        return result;
    }

    public String toString() {
        StringWriter sw = new StringWriter();
        vocOntModel.write(sw, "TURTLE");
        return (new StringBuilder("Vocabulary [\n")).append(sw.toString())
                .append("\n]").toString();
    }

    public Set<String> getObjectsForAction(String actionUri) {
        return getRestrictedItemsForAction(actionUri,
                (new StringBuilder(String.valueOf(theUpperVocabularyUri)))
                        .append("hasObject").toString());
    }

    public Set<String> getAllObjectsForAction(String actionUri) {
        Set<String> result = getObjectsForAction(actionUri);
        Set<String> subclasses = new HashSet<String>();
        String uri;
        for (Iterator<String> iterator = result.iterator(); iterator.hasNext(); subclasses
                .addAll(getAllPresentableSubClassesOf(uri)))
            uri = (String) iterator.next();

        result.addAll(subclasses);
        return result;
    }

    public Set<String> getLeafObjectsForAction(String actionUri) {
        Set<String> leafObjectUris = new HashSet<String>();
        Set<String> objectUris = getObjectsForAction(actionUri);
        Set<?> presentableItems = getAllSubClassesOf((new StringBuilder(
                String.valueOf(theUpperVocabularyUri))).append(
                "PresentableVocabularyItem").toString());
        for (Iterator<String> iterator = objectUris.iterator(); iterator
                .hasNext();) {
            String objectUri = (String) iterator.next();
            Set<String> subClasses = dropSuperClasses(getAllSubClassesOf(objectUri));
            subClasses.retainAll(presentableItems);
            if (subClasses.isEmpty())
                leafObjectUris.add(objectUri);
            else
                leafObjectUris.addAll(subClasses);
        }

        return leafObjectUris;
    }

    public String getHumanFormOf(String uri) {
        String humanForm = null;
        
        OntResource or = vocOntModel.getOntResource(uri);
        VocabularyOntology.log.debug("uri="+uri);
        humanForm = or.getLabel("EN");
        if (humanForm == null) {
            Set<?> terms = getAllSubClassesOf((new StringBuilder(
                    String.valueOf(theUpperVocabularyUri))).append("Term")
                    .toString());
            if (terms.contains(uri))
                humanForm = (new StringBuilder("a ")).append(or.getLocalName())
                        .toString();
            else
                humanForm = or.getLocalName();
        }
        return humanForm;
    }

    public boolean hasSubClasses(String uri) {
        return !getDirectPresentableSubClassesOf(uri).isEmpty();
    }

    public boolean hasSuperClass(String uri) {
        assert ((uri != null) && (!"".equals(uri)));
        OntClass oc = this.vocOntModel.getOntClass(uri);
        assert (oc != null);
        return !oc.getSuperClass().equals(OWL.Thing);
    }

    public boolean isTerm(String uri) {
        return getAllSubClassesOf(
                (new StringBuilder(String.valueOf(theUpperVocabularyUri)))
                        .append("Term").toString()).contains(uri);
    }

    public boolean isAction(String uri) {
        return getAllSubClassesOf(
                (new StringBuilder(String.valueOf(theUpperVocabularyUri)))
                        .append("Action").toString()).contains(uri);
    }

    public boolean isPredicate(String uri) {
        return vocOntModel.getOntResource(uri).isProperty();
    }

    public String getFullUriOfFragment(String fragment) {
        OntResource or = vocOntModel.getOntResource((new StringBuilder(String
                .valueOf(vocUri))).append(fragment).toString());
        return or != null ? or.getURI() : null;
    }

    public Set<String> getCredentials() {
        return getAllSubClassesOf((new StringBuilder(String.valueOf(vocUri)))
                .append("Credential").toString());
    }

    public Set<String> getRoles() {
        return getAllSubClassesOf((new StringBuilder(String.valueOf(vocUri)))
                .append("Role").toString());
    }

    private Set<String> getPresentableProperties() {
        return getDirectSubPropertiesOf((new StringBuilder(
                String.valueOf(theUpperVocabularyUri))).append(
                "presentableObjectProperty").toString());
    }

    private Set<String> getDomainOfProperty(final String propertyUri) {
        final Set<String> result = new HashSet<String>();
        String queryString = (new StringBuilder("PREFIX upvoc: <"))
                .append(theUpperVocabularyUri)
                .append("> ")
                .append("PREFIX rdf: <")
                .append(RDF.getURI())
                .append("> ")
                .append("PREFIX rdfs: <")
                .append(RDFS.getURI())
                .append("> ")
                .append("PREFIX owl: <")
                .append("http://www.w3.org/2002/07/owl#")
                .append("> ")
                .append("SELECT ?d ")
                .append("WHERE { <")
                .append(propertyUri)
                .append("> ")
                .append("\ta owl:ObjectProperty ; ")
                .append("\trdfs:subPropertyOf upvoc:presentableObjectProperty ; ")
                .append("\trdfs:domain  ?d .").append("}").toString();
        VocabularyUtilities.executeSelect(vocOntModel, queryString,
                new QuerySolutionProcessor() {

                    public void process(QuerySolution qs) {
                        RDFNode d = qs.get("d");
                        if (!d.isAnon())
                            if (d.isResource())
                                result.add(d.asResource().getURI());
                            else
                                VocabularyOntology.log
                                        .error((new StringBuilder(
                                                "Unexpected Literal while searching for domain of property <"))
                                                .append(propertyUri)
                                                .append(">").toString());
                    }
                });
        if (result.isEmpty()) {
            queryString = (new StringBuilder("PREFIX upvoc: <"))
                    .append(theUpperVocabularyUri)
                    .append("> ")
                    .append("PREFIX rdf: <")
                    .append(RDF.getURI())
                    .append("> ")
                    .append("PREFIX rdfs: <")
                    .append(RDFS.getURI())
                    .append("> ")
                    .append("PREFIX owl: <")
                    .append("http://www.w3.org/2002/07/owl#")
                    .append("> ")
                    .append("PREFIX list: <http://jena.hpl.hp.com/ARQ/list#> ")
                    .append("SELECT ?m ")
                    .append("WHERE { <")
                    .append(propertyUri)
                    .append("> ")
                    .append("\ta owl:ObjectProperty ; ")
                    .append("\trdfs:subPropertyOf upvoc:presentableObjectProperty ; ")
                    .append("\trdfs:domain ").append("\t[\ta owl:Class ; ")
                    .append("\t\towl:unionOf [ list:member ?m ] ")
                    .append("\t] . ").append("}").toString();
            VocabularyUtilities.executeSelect(vocOntModel, queryString,
                    new QuerySolutionProcessor() {

                        public void process(QuerySolution qs) {
                            RDFNode m = qs.get("m");
                            if (m.isAnon())
                                VocabularyOntology.log
                                        .error((new StringBuilder(
                                                "Unexpected bnode while searching for domain of property <"))
                                                .append(propertyUri)
                                                .append(">").toString());
                            else if (m.isResource())
                                result.add(m.asResource().getURI());
                            else
                                VocabularyOntology.log
                                        .error((new StringBuilder(
                                                "Unexpected Literal while searching for domain of property <"))
                                                .append(propertyUri)
                                                .append(">").toString());
                        }
                    });
        }
        return result;
    }

    private Set<String> getRangeOfProperty(final String propertyUri) {
        final Set<String> result = new HashSet<String>();
        String queryString = (new StringBuilder("PREFIX upvoc: <"))
                .append(theUpperVocabularyUri)
                .append("> ")
                .append("PREFIX rdf: <")
                .append(RDF.getURI())
                .append("> ")
                .append("PREFIX rdfs: <")
                .append(RDFS.getURI())
                .append("> ")
                .append("PREFIX owl: <")
                .append("http://www.w3.org/2002/07/owl#")
                .append("> ")
                .append("SELECT ?r ")
                .append("WHERE { <")
                .append(propertyUri)
                .append("> ")
                .append("\ta owl:ObjectProperty ; ")
                .append("\trdfs:subPropertyOf upvoc:presentableObjectProperty ; ")
                .append("\trdfs:range  ?r .").append("}").toString();
        VocabularyUtilities.executeSelect(vocOntModel, queryString,
                new QuerySolutionProcessor() {

                    public void process(QuerySolution qs) {
                        RDFNode r = qs.get("r");
                        if (!r.isAnon())
                            if (r.isResource())
                                result.add(r.asResource().getURI());
                            else
                                VocabularyOntology.log
                                        .error((new StringBuilder(
                                                "Unexpected Literal while searching for range of property <"))
                                                .append(propertyUri)
                                                .append(">").toString());
                    }
                });
        if (result.isEmpty()) {
            queryString = (new StringBuilder("PREFIX upvoc: <"))
                    .append(theUpperVocabularyUri)
                    .append("> ")
                    .append("PREFIX rdf: <")
                    .append(RDF.getURI())
                    .append("> ")
                    .append("PREFIX rdfs: <")
                    .append(RDFS.getURI())
                    .append("> ")
                    .append("PREFIX owl: <")
                    .append("http://www.w3.org/2002/07/owl#")
                    .append("> ")
                    .append("PREFIX list: <http://jena.hpl.hp.com/ARQ/list#> ")
                    .append("SELECT ?m ")
                    .append("WHERE { <")
                    .append(propertyUri)
                    .append("> ")
                    .append("\ta owl:ObjectProperty ; ")
                    .append("\trdfs:subPropertyOf upvoc:presentableObjectProperty ; ")
                    .append("\trdfs:range ").append("\t[\ta owl:Class ; ")
                    .append("\t\towl:unionOf [ list:member ?m ] ")
                    .append("\t] . ").append("}").toString();
            VocabularyUtilities.executeSelect(vocOntModel, queryString,
                    new QuerySolutionProcessor() {

                        public void process(QuerySolution qs) {
                            RDFNode m = qs.get("m");
                            if (m.isAnon())
                                VocabularyOntology.log
                                        .error((new StringBuilder(
                                                "Unexpected bnode while searching for range of property <"))
                                                .append(propertyUri)
                                                .append(">").toString());
                            else if (m.isResource())
                                result.add(m.asResource().getURI());
                            else
                                VocabularyOntology.log
                                        .error((new StringBuilder(
                                                "Unexpected Literal while searching for range of property <"))
                                                .append(propertyUri)
                                                .append(">").toString());
                        }
                    });
        }
        return result;
    }

    private Set<String> getPresentableActions() {
        Set<String> actions = getAllSubClassesOf((new StringBuilder(
                String.valueOf(theUpperVocabularyUri))).append("Action")
                .toString());
        Set<?> presentableItems = getAllSubClassesOf((new StringBuilder(
                String.valueOf(theUpperVocabularyUri))).append(
                "PresentableVocabularyItem").toString());
        actions.retainAll(presentableItems);
        return actions;
    }

    private Set<String> getRestrictionForAction(final String actionUri,
            final String propertyUri) {
        final Set<String> result = new HashSet<String>();
        String queryString = (new StringBuilder("PREFIX upvoc: <"))
                .append(theUpperVocabularyUri).append("> ")
                .append("PREFIX rdf: <").append(RDF.getURI()).append("> ")
                .append("PREFIX rdfs: <").append(RDFS.getURI()).append("> ")
                .append("PREFIX owl: <")
                .append("http://www.w3.org/2002/07/owl#").append("> ")
                .append("SELECT ?o ").append("WHERE { <").append(actionUri)
                .append("> ").append("\trdfs:subClassOf upvoc:Action ; ")
                .append("\trdfs:subClassOf ")
                .append("\t[ \ta owl:Restriction ; ")
                .append("\t\towl:onProperty <").append(propertyUri)
                .append("> ; ").append("\t\towl:allValuesFrom ?o ")
                .append("\t] . ").append("}").toString();
        VocabularyUtilities.executeSelect(vocOntModel, queryString,
                new QuerySolutionProcessor() {

                    public void process(QuerySolution qs) {
                        RDFNode o = qs.get("o");
                        if (!o.isAnon())
                            if (o.isResource())
                                result.add(o.asResource().getURI());
                            else
                                VocabularyOntology.log
                                        .error((new StringBuilder(
                                                "Unexpected Literal while searching for owl:Restriction on property <"))
                                                .append(propertyUri)
                                                .append("> for class ")
                                                .append(actionUri).toString());
                    }
                });
        if (result.isEmpty()) {
            queryString = (new StringBuilder("PREFIX upvoc: <"))
                    .append(theUpperVocabularyUri).append("> ")
                    .append("PREFIX rdf: <").append(RDF.getURI()).append("> ")
                    .append("PREFIX rdfs: <").append(RDFS.getURI())
                    .append("> ").append("PREFIX owl: <")
                    .append("http://www.w3.org/2002/07/owl#").append("> ")
                    .append("PREFIX list: <http://jena.hpl.hp.com/ARQ/list#> ")
                    .append("SELECT ?m ").append("WHERE { <").append(actionUri)
                    .append("> ").append("\trdfs:subClassOf upvoc:Action ; ")
                    .append("\trdfs:subClassOf ")
                    .append("\t[ \ta owl:Restriction ; ")
                    .append("\t\towl:onProperty <").append(propertyUri)
                    .append("> ; ").append("\t\towl:allValuesFrom ")
                    .append("\t\t[\ta owl:Class ; ")
                    .append("\t\t\towl:unionOf [ list:member ?m ] ")
                    .append("\t\t] ").append("\t] . ").append("}").toString();
            VocabularyUtilities.executeSelect(vocOntModel, queryString,
                    new QuerySolutionProcessor() {

                        public void process(QuerySolution qs) {
                            RDFNode m = qs.get("m");
                            if (m.isAnon())
                                VocabularyOntology.log
                                        .error((new StringBuilder(
                                                "Unexpected bnode while searching for owl:Restriction on property <"))
                                                .append(propertyUri)
                                                .append("> for class ")
                                                .append(actionUri).toString());
                            else if (m.isResource())
                                result.add(m.asResource().getURI());
                            else
                                VocabularyOntology.log
                                        .error((new StringBuilder(
                                                "Unexpected Literal while searching for owl:Restriction on property <"))
                                                .append(propertyUri)
                                                .append("> for class ")
                                                .append(actionUri)
                                                .append("with owl:unionOf")
                                                .toString());
                        }
                    });
        }
        return result;
    }

    private Set<String> getRestrictedItemsForAction(String actionUri,
            String propertyUri) {
        Set<String> result = new HashSet<String>();
        result.addAll(getRestrictionForAction(actionUri, propertyUri));
        if (result.isEmpty()) {
            Queue<String> queue = new LinkedList<String>();
            Set<String> visited = new HashSet<String>();
            queue.add(actionUri);
            visited.add(actionUri);
            while (result.isEmpty() && !queue.isEmpty()) {
                String uri = (String) queue.remove();
                for (Iterator<String> supercs = getDirectSuperClassesOf(uri)
                        .iterator(); supercs.hasNext();) {
                    String superc = (String) supercs.next();
                    if (!visited.contains(superc)) {
                        result.addAll(getRestrictionForAction(superc,
                                propertyUri));
                        visited.add(superc);
                        queue.add(superc);
                    }
                }

            }
        }
        return result;
    }

    private Set<String> getSubjectsForAction(String actionUri) {
        return getRestrictedItemsForAction(actionUri,
                (new StringBuilder(String.valueOf(theUpperVocabularyUri)))
                        .append("hasSubject").toString());
    }

    private Set<String> getSuperClassesOf(String uri, boolean direct) {
        Set<String> result = new HashSet<String>();
        OntClass oc = vocOntModel.getOntClass(uri);
        if (oc != null) {
            ExtendedIterator<?> supercs;
            for (supercs = oc.listSuperClasses(direct).filterDrop(new Filter() {

                public boolean accept(OntClass oc) {
                    return oc.equals(OWL.Thing);
                }

                public boolean accept(Object obj) {
                    return accept((OntClass) obj);
                }
            }); supercs.hasNext(); result.add(((OntClass) supercs.next())
                    .getURI()))
                ;
            supercs.close();
        }
        return result;
    }

    private Set<String> getDirectSuperClassesOf(String uri) {
        return getSuperClassesOf(uri, true);
    }

    private Set<String> getAllSuperClassesOf(String uri) {
        return getSuperClassesOf(uri, false);
    }

    private Set<String> getSubPropertiesOf(String uri, boolean direct) {
        Set<String> result = new HashSet<String>();
        OntProperty op = vocOntModel.getOntProperty(uri);
        if (op != null) {
            for (ExtendedIterator<?> subps = op.listSubProperties(direct); subps
                    .hasNext();) {
                OntProperty subp = (OntProperty) subps.next();
                if (!subp.equals(OWL2.bottomObjectProperty))
                    result.add(subp.getURI());
            }

        }
        return result;
    }

    private Set<String> getDirectSubPropertiesOf(String uri) {
        return getSubPropertiesOf(uri, false);
    }

    private Set<String> getSubclassesOf(String uri, boolean direct) {
        Set<String> result = new HashSet<String>();
        OntClass oc = vocOntModel.getOntClass(uri);
        if (oc != null) {
            ExtendedIterator<?> subcs;
            for (subcs = oc.listSubClasses(direct).filterDrop(new Filter() {

                public boolean accept(OntClass oc) {
                    return oc.equals(OWL.Nothing);
                }

                public boolean accept(Object obj) {
                    return accept((OntClass) obj);
                }
            }); subcs.hasNext(); result.add(((OntClass) subcs.next()).getURI()))
                ;
            subcs.close();
        }
        return result;
    }

    private Set<String> getDirectSubClassesOf(String uri) {
        return getSubclassesOf(uri, true);
    }

    private Set<String> getDirectPresentableSubClassesOf(String uri) {
        Set<String> result = getDirectSubClassesOf(uri);
        Set<String> presentableItems = getAllSubClassesOf((new StringBuilder(
                String.valueOf(theUpperVocabularyUri))).append(
                "PresentableVocabularyItem").toString());
        result.retainAll(presentableItems);
        return result;
    }

    public Set<String> getAllSubClassesOf(String uri) {
        return getSubclassesOf(uri, false);
    }

    private Set<String> getAllPresentableSubClassesOf(String uri) {
        Set<String> result = getAllSubClassesOf(uri);
        Set<?> presentableItems = getAllSubClassesOf((new StringBuilder(
                String.valueOf(theUpperVocabularyUri))).append(
                "PresentableVocabularyItem").toString());
        result.retainAll(presentableItems);
        return result;
    }

    private Set<String> keepPresentable(Set<String> set) {
        Set<String> result = new HashSet<String>(set);
        Set<?> presentables = getAllSubClassesOf((new StringBuilder(
                String.valueOf(theUpperVocabularyUri))).append(
                "PresentableVocabularyItem").toString());
        result.retainAll(presentables);
        return result;
    }

    public Set<String> dropSubClasses(Set<String> uris) {
        Set<String> result = new HashSet<String>(uris);
        String uri;
        for (Iterator<String> iterator = uris.iterator(); iterator.hasNext(); result
                .removeAll(getAllSubClassesOf(uri)))
            uri = (String) iterator.next();

        return result;
    }

    public Set<String> dropSuperClasses(Set<String> uris) {
        Set<String> result = new HashSet<String>(uris);
        String uri;
        for (Iterator<String> iterator = uris.iterator(); iterator.hasNext(); result
                .removeAll(getAllSuperClassesOf(uri)))
            uri = (String) iterator.next();

        return result;
    }

  

}
