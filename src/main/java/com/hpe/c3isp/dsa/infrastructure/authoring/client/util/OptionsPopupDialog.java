package com.hpe.c3isp.dsa.infrastructure.authoring.client.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Util;

public class OptionsPopupDialog extends DialogBox {
	private static final String SPECIAL_DMO_OPTION = "__USEREDITABLE__";
	private String selectedParam = "";
	private String selectedOption = "";
	private String updatedValue = "";
	private int row;
	private VerticalPanel panel = new VerticalPanel();
	private final String paramLabel= "param=";
	private final String optionLabel= "option=";
	private boolean canceled = true;
	private TextBox textFieldUserInput = null;
	private ListBox listBoxParam = null;
	private ListBox listBoxOption = null;

	private TextBox textFieldUserInputOptionValue = null;


	public boolean isCanceled() {
		return canceled;
	}


	public void setCanceled(boolean canceled) {
		this.canceled = canceled;
	}


	public OptionsPopupDialog(final HashMap<String, String> params, String parameterName, String inputValue, String statementInfo) {
		// Set the dialog box's caption.
		//Window.alert("paramValue="+params.get(parameterName) + "paramName="+parameterName+"row="+ row);
		updatedValue = inputValue;
		// Enable animation.
		setAnimationEnabled(true);

		// Enable glass background.
		setGlassEnabled(true);
		Button ok = new Button("OK");
		ok.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				canceled = false;

				updatedValue = null;
				if(textFieldUserInput != null) {
					updatedValue = textFieldUserInput.getText();
				}

				selectedParam = null;

				if(listBoxParam != null) {
					selectedParam = listBoxParam.getSelectedValue();
				}

				selectedOption = null;

				if(listBoxOption != null) {
					selectedOption = listBoxOption.getSelectedValue();
				}
				else if(textFieldUserInputOptionValue != null) {
					selectedOption = textFieldUserInputOptionValue.getText();
				}

				OptionsPopupDialog.this.hide();
			}
		});

		Button cancel = new Button("Cancel");
		cancel.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				canceled = true;
				selectedOption = null;
				selectedParam = null;
				listBoxParam = null;
				listBoxOption = null;
				textFieldUserInputOptionValue = null;
				OptionsPopupDialog.this.hide();
			}
		});
		panel.setSpacing(10);
		panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		if (params!= null && !params.isEmpty()){

			String paramValue = params.get(parameterName);
			if (paramValue.contains("[")){
				paramValue = params.get(parameterName).substring(0, params.get(parameterName).indexOf('['));
			}
			if (paramValue.equals("User")){

				GWT.log("OptionsPopupDialog: User: parameterName:"
						+ parameterName
						+ " inputValue=" + inputValue
				);
				//defined by user
				Label valaueLabel = new Label("Value for "+parameterName);
				this.textFieldUserInput = new TextBox();
				textFieldUserInput.setValue(inputValue);
//				valueBox.addValueChangeHandler(new ValueChangeHandler<String>() {
//					@Override
//					public void onValueChange(ValueChangeEvent<String> event) {
//						updatedValue = ((TextBox) event.getSource()).getText();
//					}
//				});
				HorizontalPanel valuePanel = new HorizontalPanel();
				valuePanel.add(valaueLabel);
				valuePanel.add(textFieldUserInput);
				panel.add(valuePanel);
			}else{
				//this is a DMO

				String inputParam = statementInfo.substring(statementInfo.indexOf(paramLabel)+ paramLabel.length(), statementInfo.indexOf(" "));
				List<String> paramItems = WidgetUtil.getOptionsList(paramValue, ",");
				GWT.log("OptionsPopupDialog: DMO: statementInfo:"
						+ statementInfo
						+ " inputParam=" + inputParam
						+ " paramItems=" + Util.listToString(paramItems, "NULL")
				);

				if (paramItems != null){
					Label paramLabel = new Label("Parameters");
					listBoxParam = new ListBox();
					int i=0;
					for (Iterator iterator = paramItems.iterator(); iterator.hasNext();) {
						String item = (String) iterator.next();
						listBoxParam.addItem(item,item);
						if (item.equals(inputParam)){
							listBoxParam.setSelectedIndex(i);
						}
						i++;
					}
//					paramBox.addClickHandler(new ClickHandler() {
//						@Override
//						public void onClick(ClickEvent event) {
//							ListBox source = (ListBox) event.getSource();
//							selectedParam = source.getSelectedValue();
//						}});
					HorizontalPanel paramPanel = new HorizontalPanel();
					paramPanel.add(paramLabel);
					paramPanel.add(listBoxParam);
					panel.add(paramPanel);
				}
				String paramOption = params.get(parameterName).substring(params.get(parameterName).indexOf('[') + 1,
						params.get(parameterName).indexOf(']'));
				String inputOption = statementInfo.substring(statementInfo.indexOf(optionLabel)+ optionLabel.length(), statementInfo.indexOf("}"));

				List<String> optionItems = WidgetUtil.getOptionsList(paramOption, ",");

				GWT.log("OptionsPopupDialog: DMO: statementInfo:" + statementInfo
						+ " inputOption=" + inputOption
						+ " optionItems=" + Util.listToString(optionItems, "NULL")
						);


				if (optionItems != null){
					Label optionLabel = new Label("Options");
					HorizontalPanel optionPanel = new HorizontalPanel();
					optionPanel.add(optionLabel);

					if(containsUserEditableSpecialOptionValue(optionItems)) {
						GWT.log("Option list contains special value, show a TextField instead of a combo");
						//mettere un text field invece che un combo
						this.textFieldUserInputOptionValue = new TextBox();
						textFieldUserInputOptionValue.setValue(inputOption);
						optionPanel.add(textFieldUserInputOptionValue);
					}else {
						// normal combo for options
						listBoxOption = new ListBox();
//						List<String> optionItems = WidgetUtil.getOptionsList(paramOption, ",");
						int i= 0;
						for (Iterator iterator = optionItems.iterator(); iterator.hasNext();) {
							String item = (String) iterator.next();
							listBoxOption.addItem(item,item);
							if (item.equals(inputOption)){
								listBoxOption.setSelectedIndex(i);
							}
							i++;
						}
//						listBoxOption.addClickHandler(new ClickHandler() {
//							@Override
//							public void onClick(ClickEvent event) {
//								ListBox source = (ListBox) event.getSource();
//								selectedOption = source.getSelectedValue();
//							}
//						});
						optionPanel.add(listBoxOption);
					}


					panel.add(optionPanel);
				}
			}
		}

		HorizontalPanel buttonPanel = new HorizontalPanel();
		buttonPanel.add(ok);
		buttonPanel.add(cancel);
		panel.add(buttonPanel);
		setWidget(panel);
	}


	private boolean containsUserEditableSpecialOptionValue(List<String> optionItems) {
		if(optionItems != null && optionItems.size() == 1) {
			String o = optionItems.get(0);
			if(o.equals(SPECIAL_DMO_OPTION)) {
				return true;
			}
		}
		return false;
	}


	public String getSelectedParam() {
		return selectedParam;
	}


	public void setSelectedParam(String selectedParam) {
		this.selectedParam = selectedParam;
	}



	public String getSelectedOption() {
		return selectedOption;
	}



	public void setSelectedOption(String selectedOption) {
		this.selectedOption = selectedOption;
	}



	public String getUpdatedValue() {
		return updatedValue;
	}



	public void setUpdatedValue(String updatedValue) {
		this.updatedValue = updatedValue;
	}



	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}
}




















//package com.hpe.c3isp.dsa.infrastructure.authoring.client.util;
//
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.List;
//
//import com.google.gwt.core.client.GWT;
//import com.google.gwt.event.dom.client.ClickEvent;
//import com.google.gwt.event.dom.client.ClickHandler;
//import com.google.gwt.event.logical.shared.ValueChangeEvent;
//import com.google.gwt.event.logical.shared.ValueChangeHandler;
//import com.google.gwt.user.client.ui.Button;
//import com.google.gwt.user.client.ui.DialogBox;
//import com.google.gwt.user.client.ui.HasHorizontalAlignment;
//import com.google.gwt.user.client.ui.HorizontalPanel;
//import com.google.gwt.user.client.ui.Label;
//import com.google.gwt.user.client.ui.ListBox;
//import com.google.gwt.user.client.ui.TextBox;
//import com.google.gwt.user.client.ui.VerticalPanel;
//import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Util;
//
//public class OptionsPopupDialog extends DialogBox {
//
//	private String selectedParam = "";
//	private String selectedOption = "";
//	private String updatedValue = "";
//	private int row;
//	private VerticalPanel panel = new VerticalPanel();
//	private final String paramLabel= "param=";
//	private final String optionLabel= "option=";
//	private boolean canceled = true;
//	private TextBox textFieldUserInput = null;
//	private ListBox listBoxParam = null;
//	private ListBox listBoxOption = null;
//
//
//	public boolean isCanceled() {
//		return canceled;
//	}
//
//
//	public void setCanceled(boolean canceled) {
//		this.canceled = canceled;
//	}
//
//
//	public OptionsPopupDialog(final HashMap<String, String> params, String parameterName, String inputValue, String statementInfo) {
//		// Set the dialog box's caption.
//		//Window.alert("paramValue="+params.get(parameterName) + "paramName="+parameterName+"row="+ row);
//		updatedValue = inputValue;
//		// Enable animation.
//		setAnimationEnabled(true);
//
//		// Enable glass background.
//		setGlassEnabled(true);
//		Button ok = new Button("OK");
//		ok.addClickHandler(new ClickHandler() {
//			public void onClick(ClickEvent event) {
//				canceled = false;
//
//				updatedValue = null;
//				if(textFieldUserInput != null) {
//					updatedValue = textFieldUserInput.getText();
//				}
//
//				selectedParam = null;
//				if(listBoxParam != null) {
//					selectedParam = listBoxParam.getSelectedValue();
//				}
//
//				selectedOption = null;
//				if(listBoxOption != null) {
//					selectedOption = listBoxOption.getSelectedValue();
//				}
//				OptionsPopupDialog.this.hide();
//			}
//		});
//
//		Button cancel = new Button("Cancel");
//		cancel.addClickHandler(new ClickHandler() {
//			public void onClick(ClickEvent event) {
//				canceled = true;
//				selectedOption = null;
//				selectedParam = null;
//				listBoxParam = null;
//				listBoxOption = null;
//				OptionsPopupDialog.this.hide();
//			}
//		});
//		panel.setSpacing(10);
//		panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
//		if (params!= null && !params.isEmpty()){
//
//			String paramValue = params.get(parameterName);
//			if (paramValue.contains("[")){
//				paramValue = params.get(parameterName).substring(0, params.get(parameterName).indexOf('['));
//			}
//			if (paramValue.equals("User")){
//
//				GWT.log("OptionsPopupDialog: User: parameterName:"
//						+ parameterName
//						+ " inputValue=" + inputValue
//				);
//				//defined by user
//				Label valaueLabel = new Label("Value for "+parameterName);
//				this.textFieldUserInput = new TextBox();
//				textFieldUserInput.setValue(inputValue);
////				valueBox.addValueChangeHandler(new ValueChangeHandler<String>() {
////					@Override
////					public void onValueChange(ValueChangeEvent<String> event) {
////						updatedValue = ((TextBox) event.getSource()).getText();
////					}
////				});
//				HorizontalPanel valuePanel = new HorizontalPanel();
//				valuePanel.add(valaueLabel);
//				valuePanel.add(textFieldUserInput);
//				panel.add(valuePanel);
//			}else{
//				//this is a DMO
//
//				String inputParam = statementInfo.substring(statementInfo.indexOf(paramLabel)+ paramLabel.length(), statementInfo.indexOf(" "));
//				List<String> paramItems = WidgetUtil.getOptionsList(paramValue, ",");
//				GWT.log("OptionsPopupDialog: DMO: statementInfo:"
//						+ statementInfo
//						+ " inputParam=" + inputParam
//						+ " paramItems=" + Util.listToString(paramItems, "NULL")
//				);
//
//				if (paramItems != null){
//					Label paramLabel = new Label("Parameters");
//					listBoxParam = new ListBox();
//					int i=0;
//					for (Iterator iterator = paramItems.iterator(); iterator.hasNext();) {
//						String item = (String) iterator.next();
//						listBoxParam.addItem(item,item);
//						if (item.equals(inputParam)){
//							listBoxParam.setSelectedIndex(i);
//						}
//						i++;
//					}
////					paramBox.addClickHandler(new ClickHandler() {
////						@Override
////						public void onClick(ClickEvent event) {
////							ListBox source = (ListBox) event.getSource();
////							selectedParam = source.getSelectedValue();
////						}});
//					HorizontalPanel paramPanel = new HorizontalPanel();
//					paramPanel.add(paramLabel);
//					paramPanel.add(listBoxParam);
//					panel.add(paramPanel);
//				}
//				String paramOption = params.get(parameterName).substring(params.get(parameterName).indexOf('[') + 1,
//						params.get(parameterName).indexOf(']'));
//				String inputOption = statementInfo.substring(statementInfo.indexOf(optionLabel)+ optionLabel.length(), statementInfo.indexOf("}"));
//
//				List<String> optionItems = WidgetUtil.getOptionsList(paramOption, ",");
//
//				GWT.log("OptionsPopupDialog: DMO: statementInfo:" + statementInfo
//						+ " inputOption=" + inputOption
//						+ " optionItems=" + Util.listToString(optionItems, "NULL")
//						);
//
//				if (optionItems != null){
//					Label optionLabel = new Label("Options");
//					listBoxOption = new ListBox();
////					List<String> optionItems = WidgetUtil.getOptionsList(paramOption, ",");
//					int i= 0;
//					for (Iterator iterator = optionItems.iterator(); iterator.hasNext();) {
//						String item = (String) iterator.next();
//						listBoxOption.addItem(item,item);
//						if (item.equals(inputOption)){
//							listBoxOption.setSelectedIndex(i);
//						}
//						i++;
//					}
//					listBoxOption.addClickHandler(new ClickHandler() {
//						@Override
//						public void onClick(ClickEvent event) {
//							ListBox source = (ListBox) event.getSource();
//							selectedOption = source.getSelectedValue();
//						}});
//					HorizontalPanel optionPanel = new HorizontalPanel();
//					optionPanel.add(optionLabel);
//					optionPanel.add(listBoxOption);
//					panel.add(optionPanel);
//				}
//			}
//		}
//
//		HorizontalPanel buttonPanel = new HorizontalPanel();
//		buttonPanel.add(ok);
//		buttonPanel.add(cancel);
//		panel.add(buttonPanel);
//		setWidget(panel);
//	}
//
//
//
//	public String getSelectedParam() {
//		return selectedParam;
//	}
//
//
//	public void setSelectedParam(String selectedParam) {
//		this.selectedParam = selectedParam;
//	}
//
//
//
//	public String getSelectedOption() {
//		return selectedOption;
//	}
//
//
//
//	public void setSelectedOption(String selectedOption) {
//		this.selectedOption = selectedOption;
//	}
//
//
//
//	public String getUpdatedValue() {
//		return updatedValue;
//	}
//
//
//
//	public void setUpdatedValue(String updatedValue) {
//		this.updatedValue = updatedValue;
//	}
//
//
//
//	public int getRow() {
//		return row;
//	}
//
//	public void setRow(int row) {
//		this.row = row;
//	}
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
////package com.hpe.c3isp.dsa.infrastructure.authoring.client.util;
////
////import java.util.HashMap;
////import java.util.Iterator;
////import java.util.List;
////
////import com.google.gwt.core.client.GWT;
////import com.google.gwt.event.dom.client.ClickEvent;
////import com.google.gwt.event.dom.client.ClickHandler;
////import com.google.gwt.event.logical.shared.ValueChangeEvent;
////import com.google.gwt.event.logical.shared.ValueChangeHandler;
////import com.google.gwt.user.client.ui.Button;
////import com.google.gwt.user.client.ui.DialogBox;
////import com.google.gwt.user.client.ui.HasHorizontalAlignment;
////import com.google.gwt.user.client.ui.HorizontalPanel;
////import com.google.gwt.user.client.ui.Label;
////import com.google.gwt.user.client.ui.ListBox;
////import com.google.gwt.user.client.ui.TextBox;
////import com.google.gwt.user.client.ui.VerticalPanel;
////import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Util;
////
////public class OptionsPopupDialog extends DialogBox {
////
////	private String selectedParam = "";
////	private String selectedOption = "";
////	private String updatedValue;
////	private int row;
////	VerticalPanel panel = new VerticalPanel();
////	private final String paramLabel= "param=";
////	private final String optionLabel= "option=";
////	private boolean canceled = true;
////
////	public boolean isCanceled() {
////		return canceled;
////	}
////
////
////	public void setCanceled(boolean canceled) {
////		this.canceled = canceled;
////	}
////
////
////	public OptionsPopupDialog(final HashMap<String, String> params, String parameterName, String inputValue, String statementInfo) {
////		// Set the dialog box's caption.
////		//Window.alert("paramValue="+params.get(parameterName) + "paramName="+parameterName+"row="+ row);
////		updatedValue = inputValue;
////		// Enable animation.
////		setAnimationEnabled(true);
////
////		// Enable glass background.
////		setGlassEnabled(true);
////		Button ok = new Button("OK");
////		ok.addClickHandler(new ClickHandler() {
////			public void onClick(ClickEvent event) {
////				canceled = false;
////				OptionsPopupDialog.this.hide();
////			}
////		});
////
////		Button cancel = new Button("Cancel");
////		cancel.addClickHandler(new ClickHandler() {
////			public void onClick(ClickEvent event) {
////				canceled = true;
////				selectedOption = null;
////				selectedParam = null;
////				OptionsPopupDialog.this.hide();
////			}
////		});
////		panel.setSpacing(10);
////		panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
////		if (params!= null && !params.isEmpty()){
////
////			String paramValue = params.get(parameterName);
////			if (paramValue.contains("[")){
////				paramValue = params.get(parameterName).substring(0, params.get(parameterName).indexOf('['));
////			}
////			if (paramValue.equals("User")){
////				//defined by user
////				Label valaueLabel = new Label("Value for "+parameterName);
////				TextBox valueBox = new TextBox();
////				valueBox.setValue(inputValue);
////				valueBox.addValueChangeHandler(new ValueChangeHandler<String>() {
////					@Override
////					public void onValueChange(ValueChangeEvent<String> event) {
////						updatedValue = ((TextBox) event.getSource()).getText();
////					}
////				});
////				HorizontalPanel valuePanel = new HorizontalPanel();
////				valuePanel.add(valaueLabel);
////				valuePanel.add(valueBox);
////				panel.add(valuePanel);
////			}else{
////				//this is a DMO
////
////				String inputParam = statementInfo.substring(statementInfo.indexOf(paramLabel)+ paramLabel.length(), statementInfo.indexOf(" "));
////				List<String> paramItems = WidgetUtil.getOptionsList(paramValue, ",");
////				GWT.log("OptionsPopupDialog: DMO: statementInfo:"
////						+ statementInfo
////						+ " inputParam=" + inputParam
////						+ " paramItems=" + Util.listToString(paramItems, "NULL")
////				);
////				if(inputParam == null || inputParam.length() == 0) {
////					if(paramItems != null && paramItems.size() > 0) {
////						//defualt = first element
////						inputParam = paramItems.get(0);
////					}
////				}
////
////
////				if (paramItems != null){
////					Label paramLabel = new Label("Parameters");
////					ListBox paramBox = new ListBox();
////					int i=0;
////					for (Iterator iterator = paramItems.iterator(); iterator.hasNext();) {
////						String item = (String) iterator.next();
////						paramBox.addItem(item,item);
////						if (item.equals(inputParam)){
////							paramBox.setSelectedIndex(i);
////						}
////						i++;
////					}
////					paramBox.addClickHandler(new ClickHandler() {
////						@Override
////						public void onClick(ClickEvent event) {
////							ListBox source = (ListBox) event.getSource();
////							selectedParam = source.getSelectedValue();
////						}});
////					HorizontalPanel paramPanel = new HorizontalPanel();
////					paramPanel.add(paramLabel);
////					paramPanel.add(paramBox);
////					panel.add(paramPanel);
////				}
////				String paramOption = params.get(parameterName).substring(params.get(parameterName).indexOf('[') + 1,
////						params.get(parameterName).indexOf(']'));
////				String inputOption = statementInfo.substring(statementInfo.indexOf(optionLabel)+ optionLabel.length(), statementInfo.indexOf("}"));
////
////				List<String> optionItems = WidgetUtil.getOptionsList(paramOption, ",");
////
////				GWT.log("OptionsPopupDialog: DMO: statementInfo:" + statementInfo
////						+ " inputOption=" + inputOption
////						+ " optionItems=" + Util.listToString(optionItems, "NULL")
////						);
////				if(paramOption == null || paramOption.length() == 0) {
////					if(optionItems != null && optionItems.size() > 0) {
////						//defualt = first element
////						paramOption = optionItems.get(0);
////					}
////				}
////
////
////				if (optionItems != null){
////					Label optionLabel = new Label("Options");
////					ListBox optionBox = new ListBox();
//////					List<String> optionItems = WidgetUtil.getOptionsList(paramOption, ",");
////					int i= 0;
////					for (Iterator iterator = optionItems.iterator(); iterator.hasNext();) {
////						String item = (String) iterator.next();
////						optionBox.addItem(item,item);
////						if (item.equals(inputOption)){
////							optionBox.setSelectedIndex(i);
////						}
////						i++;
////					}
////					optionBox.addClickHandler(new ClickHandler() {
////						@Override
////						public void onClick(ClickEvent event) {
////							ListBox source = (ListBox) event.getSource();
////							selectedOption = source.getSelectedValue();
////						}});
////					HorizontalPanel optionPanel = new HorizontalPanel();
////					optionPanel.add(optionLabel);
////					optionPanel.add(optionBox);
////					panel.add(optionPanel);
////				}
////			}
////		}
////
////		HorizontalPanel buttonPanel = new HorizontalPanel();
////		buttonPanel.add(ok);
////		buttonPanel.add(cancel);
////		panel.add(buttonPanel);
////		setWidget(panel);
////	}
////
////
////
////	public String getSelectedParam() {
////		return selectedParam;
////	}
////
////
////	public void setSelectedParam(String selectedParam) {
////		this.selectedParam = selectedParam;
////	}
////
////
////
////	public String getSelectedOption() {
////		return selectedOption;
////	}
////
////
////
////	public void setSelectedOption(String selectedOption) {
////		this.selectedOption = selectedOption;
////	}
////
////
////
////	public String getUpdatedValue() {
////		return updatedValue;
////	}
////
////
////
////	public void setUpdatedValue(String updatedValue) {
////		this.updatedValue = updatedValue;
////	}
////
////
////
////	public int getRow() {
////		return row;
////	}
////
////	public void setRow(int row) {
////		this.row = row;
////	}
////}
