/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.view.crossref;

import com.google.gwt.event.logical.shared.HasCloseHandlers;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ToggleButton;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.crossref.CrossReferencePresenter.CrossReferenceDisplay;

public class CrossReferenceDisplayImpl extends Composite implements
        CrossReferenceDisplay {

    private final ToggleButton toggleReferencesButton = new ToggleButton(
            "Show references", "Hide references");

    @Inject
    public CrossReferenceDisplayImpl() {
        initWidget(toggleReferencesButton);
    }

    @Override
    public ToggleButton getToggleReferencesButton() {
        return toggleReferencesButton;
    }

    @Override
    public HasCloseHandlers<PopupPanel> showMessage(String message) {
        final PopupPanel pp = new PopupPanel(true, true);
        pp.setWidget(new Label(message));
        pp.setAnimationEnabled(true);
        pp.center();
        return pp;
    }

    @Override
    public Widget asWidget() {
        return this;
    }

    @Override
    public void deactivate() {
        toggleReferencesButton.setEnabled(false);
    }

    @Override
    public void activate() {
        toggleReferencesButton.setEnabled(true);
    }

}
