/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa.helper;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xmlbeans.XmlException;

import com.hpe.c3isp.dsa.infrastructure.authoring.shared.Config;

import eu.cocoCloud.dsa.DsaDocument;

public class DsaStorage implements Serializable {
    private static final long serialVersionUID = 1L;

    private static Log log = LogFactory.getLog(DsaStorage.class);

//    private final String propertiesFile = "etc/config/dsa-storage.properties";
    private String basePath = "";
    private String dsaApi = "";

   private DsaStorage() {
//        Properties properties = new Properties();
//        InputStream is = getClass().getClassLoader().getResourceAsStream(
//                propertiesFile);
//        if (is != null) {
//            try {
//                properties.load(is);
//                if (properties.containsKey("basePath")) {
//                    basePath = properties.getProperty("basePath");
//                    log.info("using base path: " + basePath);
//                } else {
//                    log.error("cannot find property basePath");
//                }
//                if (properties.containsKey("dsaApi")) {
//                    dsaApi = properties.getProperty("dsaApi");
//                    log.info("dsa api url: " + dsaApi);
//                } else {
//                    log.error("cannot find property dsa api");
//                }
//            } catch (IOException e) {
//                log.error("cannot read " + propertiesFile, e);
//            }
//            try {
//                is.close();
//            } catch (IOException e) {
//                log.error("cannot close InputStream attached to "
//                        + propertiesFile);
//            }
//        } else {
//            log.error("cannot find " + propertiesFile);
//        }

        basePath = Config.getInstance().getBasePath();
        dsaApi = Config.getInstance().getDsaApi();

    }

    private static class DSAStorageSingletonHolder {
        private static final DsaStorage theInstance = new DsaStorage();
    }

    public static DsaStorage getInstance() {
        return DSAStorageSingletonHolder.theInstance;
    }

    public DsaDocument read(String filename) {
        DsaDocument dsaDocument = null;
        if (filename == null || "".equals(filename)) {
            log.error("missing filename");
        } else {
            String path = basePath + filename;
            File file = new File(path);
            if (!file.exists()) {
                log.error(path + " does not exist");
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("reading " + path + " ...");
                }
                try {
                    dsaDocument = DsaDocument.Factory.parse(file);
                } catch (XmlException e) {
                    log.error("error parsing " + path, e);
                } catch (IOException e) {
                    log.error("error while reading from " + path, e);
                }
            }
        }
        return dsaDocument;
    }

    public boolean write(String filename, DsaDocument dsaDocument) {
        boolean result = false;
        String path = basePath + filename;
        if (log.isDebugEnabled()) {
            log.debug("writing " + path + " ...");

        }
        File file = new File(path);
        if (!file.exists()) {
            log.error(path + " does not exist");
        } else {
            log.debug(dsaDocument);
            try {
                dsaDocument.save(file, XmlUtil.getXmlOptions());
                result = true;
            } catch (IOException e) {
                log.error("error while saving to " + path, e);
            }
        }
        return result;
    }

    //meeting pisa 10/2018. testare cosa succede a AP se si rimuove .xml dal UUID
//    public String createNew() {
//        String uid = "DSA-" + UUID.randomUUID().toString();
//    	String filename = uid + ".xml";
//        String path = basePath + filename;
//        File file = new File(path);
//        try {
//            if (!file.createNewFile()) {
//                log.error("cannot create new file " + path);
//                filename = null;
//            }
//        } catch (IOException e) {
//            log.error("error while creating new file " + path, e);
//            filename = null;
//        }
//        return uid;
//    }

    public String createNew() {
        String filename = "DSA-" + UUID.randomUUID().toString() + ".xml";
        String path = basePath + filename;
        File file = new File(path);
        try {
            if (!file.createNewFile()) {
                log.error("cannot create new file " + path);
                filename = null;
            }
        } catch (IOException e) {
            log.error("error while creating new file " + path, e);
            filename = null;
        }
        return filename;
    }

    public String getDsaApi() {
        return dsaApi;
    }

    public String getBasePath() {
        return basePath;
    }


    public String getTempFilePath(String dsaContent, String dsaId){
        File dsaFile = null;
        try {
            dsaFile = File.createTempFile(dsaId,".xml");
            FileWriter fileWriter = new FileWriter(dsaFile);
            fileWriter.write(dsaContent);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return dsaFile.getAbsolutePath();
    }

}
