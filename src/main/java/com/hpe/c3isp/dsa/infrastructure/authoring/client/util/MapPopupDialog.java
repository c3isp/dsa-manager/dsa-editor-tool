package com.hpe.c3isp.dsa.infrastructure.authoring.client.util;

import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.MapOptions;
import org.gwtopenmaps.openlayers.client.MapWidget;
import org.gwtopenmaps.openlayers.client.Style;
import org.gwtopenmaps.openlayers.client.control.DrawFeature;
import org.gwtopenmaps.openlayers.client.control.DrawFeatureOptions;
import org.gwtopenmaps.openlayers.client.control.LayerSwitcher;
import org.gwtopenmaps.openlayers.client.event.MapClickListener;
import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.geometry.Point;
import org.gwtopenmaps.openlayers.client.geometry.Polygon;
import org.gwtopenmaps.openlayers.client.handler.RegularPolygonHandler;
import org.gwtopenmaps.openlayers.client.handler.RegularPolygonHandlerOptions;
import org.gwtopenmaps.openlayers.client.layer.OSM;
import org.gwtopenmaps.openlayers.client.layer.Vector;
import org.gwtopenmaps.openlayers.client.util.JSObject;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class MapPopupDialog extends DialogBox{
   
    private String coordinates;
    VerticalPanel panel = new VerticalPanel();
    int row;
    public MapPopupDialog() {
        // Set the dialog box's caption.
       // loadMapApi();
        
        coordinates = null;
        setText("Map Dialog");
    
        // Enable animation.
        setAnimationEnabled(true);

        // Enable glass background.
        setGlassEnabled(true);

        // DialogBox is a SimplePanel, so you have to set its widget 
        // property to whatever you want its contents to be.
        Button ok = new Button("OK");
        ok.addClickHandler(new ClickHandler() {
           public void onClick(ClickEvent event) {
               MapPopupDialog.this.hide();
           }
        });
        Button cancel = new Button("Cancel");
        cancel.addClickHandler(new ClickHandler() {
           public void onClick(ClickEvent event) {
               coordinates = null;
               MapPopupDialog.this.hide();
           }
        });
        VerticalPanel vPanel = new VerticalPanel();
        MapOptions defaultMapOptions = new MapOptions();
        MapWidget mapWidget = new MapWidget("300px", "300px", defaultMapOptions);

        OSM osmMapnik = OSM.Mapnik("Mapnik");
        OSM osmCycle = OSM.CycleMap("CycleMap");

        osmMapnik.setIsBaseLayer(true);
        osmCycle.setIsBaseLayer(true);

        mapWidget.getMap().addLayer(osmMapnik);
        mapWidget.getMap().addLayer(osmCycle);
        double defaultLong = 6.95;
        double defaultLat = 29.94;
        coordinates = defaultLong + ", " + defaultLat + ", "+15;

        LonLat lonLat = new LonLat(defaultLong, defaultLat);
        //convert in WSG84 format (used by Google Map)
        lonLat.transform("WSG84", mapWidget.getMap().getProjection()); 
        
      
        mapWidget.getMap().setCenter(lonLat, 6);

        final Vector vectorLayer = new Vector("Vectorlayer");

        final Point point = new Point(lonLat.lon(), lonLat.lat());
    
        final Style stopPointStyle = new Style();
        stopPointStyle.setPointRadius(15);
        stopPointStyle.setFillColor("red");
        stopPointStyle.setStrokeColor("blue");
        stopPointStyle.setStrokeWidth(1);
        stopPointStyle.setFillOpacity(0.5);
        
        VectorFeature pointFeature = new VectorFeature(point, stopPointStyle);       
        vectorLayer.addFeature(pointFeature);
        
        mapWidget.getMap().addLayer(vectorLayer);
        mapWidget.getMap().addControl(new LayerSwitcher());
        
        mapWidget.getMap().addMapClickListener(new MapClickListener(){
            public void onClick(MapClickEvent eventObject)
            {
                LonLat selectedCoordinates = eventObject.getLonLat();  
                //convert in WSG84 format (used by Google Map)
                selectedCoordinates.transform("WSG84", eventObject.getSource().getProjection()); 
                coordinates =  selectedCoordinates.lon() + ", " + selectedCoordinates.lat()+ ", "+15;
                Point newPoint = new Point(selectedCoordinates.lon(), selectedCoordinates.lat());
                
                VectorFeature updatedPointFeature = new VectorFeature(newPoint, stopPointStyle); 
                vectorLayer.destroyFeatures();
                vectorLayer.addFeature(updatedPointFeature);
               // Window.alert("Coordinates: " + coordinates);
                
                JSObject polygonJSObject = jsCreateRegularPolygon(point, 100f, 30, 45f); // with 30 sides for a circle
                Polygon circle = Polygon.narrowToPolygon(polygonJSObject);
              
            }

        });
        /**
         * add radius feature
         */
     // Handler option for circle
        DrawFeatureOptions drawFeatureOptions = new DrawFeatureOptions();
        RegularPolygonHandlerOptions handlerOptions = new RegularPolygonHandlerOptions();
        
        // 30 side is ok for a circle
        handlerOptions.setSides(30);
        handlerOptions.setSnapAngle(0);
        handlerOptions.setIrregular(false);

        // Add the handler option to the DrawFeatureOptions
        drawFeatureOptions.setHandlerOptions(handlerOptions);

        // create the draw feature control
       
        DrawFeature drawCircleFeatureControl = new DrawFeature(vectorLayer, new RegularPolygonHandler(),
                drawFeatureOptions);

     // Add the control to the map
        mapWidget.getMap().addControl(drawCircleFeatureControl);
        vPanel.add(mapWidget);
        panel.setHeight("250");
        panel.setWidth("200");
        panel.setSpacing(10);
        panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        panel.add(new Label("Select an area in the map and then click OK.")); 
        panel.add(vPanel);
        HorizontalPanel buttonPanel = new HorizontalPanel();
        buttonPanel.add(ok);
        buttonPanel.add(cancel);
        panel.add(buttonPanel);
        
        setWidget(panel);
     }
    
    
    public String getCoordinates() {
        return coordinates;
    }


    public int getRow() {
        return row;
    }


    public void setRow(int row) {
        this.row = row;
    }
    
    /**
     * Create a regular polygon around a radius. Useful for creating circles and the like.
     * 
     * @param point {OpenLayers.Geometry.Point} center of polygon.
     * @param radius {Float} distance to vertex, in map units.
     * @param sides {Integer} Number of sides. 20 approximates a circle.
     * @param rotation {Float} original angle of rotation, in degrees.
     * @return Polygon
     */
    public static native JSObject jsCreateRegularPolygon(Point point, Float radius, Integer sides, Float rotation)
    /*-{
        return $wnd.OpenLayers.Geometry.Polygon.createRegularPolygon(origin, radius, sides, rotation);
    }-*/;
 
}
