/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;

public interface Resources extends ClientBundle {

    final String DEFAULT_CSS = "DSAAuthoringTool.css";
    final String HEALTHCARE_CUSTOM_CSS = "DSAAuthoringTool_healthcare.css";
    final String MOBILE_CUSTOM_CSS = "DSAAuthoringTool_mobile.css";
    final String PA_CUSTOM_CSS = "DSAAuthoringTool_pa.css";

    @Source("question-mark-16.png")
    ImageResource questionMark16();

    @Source("c3isplogo.png")
    ImageResource c3ispLogo();

    @Source("delete.PNG")
    ImageResource delete();

    @Source("usermanual.png")
    ImageResource userManualDownload();

    @Source("closedarrow.png")
    ImageResource closedArrow();

    @Source("hpelogo.png")
    ImageResource hpeLogo();

    @Source(DEFAULT_CSS)
    @CssResource.NotStrict
    CssResource defaultCss();

    @Source(HEALTHCARE_CUSTOM_CSS)
    @CssResource.NotStrict
    CssResource customHealthcareCss();

    @Source(HEALTHCARE_CUSTOM_CSS)
    @CssResource.NotStrict
    CssResource customMobileCss();

    @Source(HEALTHCARE_CUSTOM_CSS)
    @CssResource.NotStrict
    CssResource customPACss();

}