/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.view.definitions.roles;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.roles.RolePresenter.RoleDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.roles.RolesSetPresenter.RolesSetDisplay;

public class RolesSetDisplayImpl extends Composite implements RolesSetDisplay {

    private final VerticalPanel rolesPanel = new VerticalPanel();
    private final Label noRolesLabel = new Label("There are no roles");

    @Inject
    public RolesSetDisplayImpl() {
        final VerticalPanel vp = new VerticalPanel();
        initWidget(vp);

        vp.add(rolesPanel);
        vp.add(noRolesLabel);
    }

    @Override
    public void addRoleDisplay(RoleDisplay display) {
        rolesPanel.add(display.asWidget());
        noRolesLabel.setVisible(false);
    }

    @Override
    public void clear() {
        rolesPanel.clear();
        noRolesLabel.setVisible(true);
    }

    @Override
    public void activate() {
        // nothing to do here
    }

    @Override
    public void deactivate() {
        // nothing to do here
    }

    @Override
    public Widget asWidget() {
        return this;
    }

}
