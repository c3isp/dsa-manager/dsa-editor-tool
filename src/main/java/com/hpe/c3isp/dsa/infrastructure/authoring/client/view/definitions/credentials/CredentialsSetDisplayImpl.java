/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.view.definitions.credentials;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.credentials.CredentialPresenter.CredentialDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.credentials.CredentialsSetPresenter.CredentialsSetDisplay;

public class CredentialsSetDisplayImpl extends Composite implements
        CredentialsSetDisplay {

    private final VerticalPanel credentialsPanel = new VerticalPanel();
    private final Label noCredentialsLabel = new Label(
            "There are no Credentials");

    @Inject
    public CredentialsSetDisplayImpl() {
        final VerticalPanel vp = new VerticalPanel();
        initWidget(vp);

        vp.add(noCredentialsLabel);
        vp.add(credentialsPanel);
    }

    @Override
    public void addCredentialDisplay(CredentialDisplay display) {
        credentialsPanel.add(display.asWidget());
        noCredentialsLabel.setVisible(false);
    }

    @Override
    public void clear() {
        credentialsPanel.clear();
        noCredentialsLabel.setVisible(true);
    }

    @Override
    public void activate() {
        // nothing to do here
    }

    @Override
    public void deactivate() {
        // nothing to do here
    }

    @Override
    public Widget asWidget() {
        return this;
    }

}
