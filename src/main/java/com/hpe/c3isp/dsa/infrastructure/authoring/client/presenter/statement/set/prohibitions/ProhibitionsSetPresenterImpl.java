/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.prohibitions;

import org.enunes.gwt.mvp.client.EventBus;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.presenter.BusyDisplayEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.presenter.IdleDisplayEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.move.MoveRowEvent.Direction;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.StatementPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.AbstractStatementSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.VocabularyPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.ProhibitionsSetDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.Issuer;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.StatementMetadata;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.StatementType;

public class ProhibitionsSetPresenterImpl extends
        AbstractStatementSetPresenter<ProhibitionsSetDisplay> implements
        PspContext, ProhibitionsSetPresenter {

    private final StatementType myStatementType = StatementType.PROHIBITION;
    private PspState state;

    @Inject
    public ProhibitionsSetPresenterImpl(EventBus eventBus,
            ProhibitionsSetDisplay display, Provider<StatementPresenter> psp,
            Provider<VocabularyPresenter> pvp) {
        super(eventBus, display, psp, pvp);
        state = new PspStateNormal();
    }

    @Override
    protected void addStatement() {
        // tell everyone that our display is now in the active mode
        fireEvent(new BusyDisplayEvent(display));
        // we want to deactivate our display to avoid users adding, or deleting,
        // or moving other statement while adding a new one
        display.deactivate();
        // fire up the VocabularyPresenter
        providerOfVocabularyPresenter.get().go(myStatementType);
        // delegate to Finite-State-Machine state
        state.addStatement(this);
    }

    @Override
    protected void handleDeleteRowEvent(int rowIndex) {
        state.handleDeleteRowEvent(this, rowIndex);
    }

    @Override
    protected void handleDisableRowMovementEvent() {
        state.handleDisableRowMovementEvent(this);
    }

    @Override
    protected void handleEnableRowMovementEvent(int rowIndex) {
        state.handleEnableRowMovementEvent(this, rowIndex);
    }

    @Override
    protected void handleMoveRowEvent(Direction direction){//, CheckBox protectedCheckBox, CheckBox isPendingRuleCheckBox, Label statementInfo, TextArea inputValue) {
        state.handleMoveRowEvent(this, direction);//, protectedCheckBox, isPendingRuleCheckBox, statementInfo, inputValue);
    }

    @Override
    protected void handleStatementBrokenEvent(StatementMetadata metadata) {
        state.handleStatementBrokenEvent(this, metadata.getIndex());
        fireEvent(new IdleDisplayEvent(display));
    }

    @Override
    protected void handleStatementCompletedEvent(StatementMetadata metadata){//, CheckBox protectedCheckBox, CheckBox isPendingRuleCheckBox, Label statementInfo, TextArea inputValue) {
        boolean isEnabled = false;

        if (userRole.compareTo(Issuer.LEGAL_EXPERT.getLabel()) == 0
                || metadata.getIssuer() == null
                || (metadata.getIssuer().getLabel().compareTo(userRole) == 0)) {
            isEnabled = true;
        }
        boolean isEndUser = false;
        if (userRole.compareTo(Issuer.USER.getLabel())==0){
            isEndUser = true;
        }
        state.handleStatementCompletedEvent(this, metadata.getIndex(),
                isEnabled, isEndUser, params);
        fireEvent(new IdleDisplayEvent(display));
    }

    @Override
    public void setState(PspState state) {
        this.state = state;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * marco.client.presenter.statement.set.AbstractSspContext#getStatementType
     * ()
     */
    @Override
    public StatementType getStatementType() {
        return myStatementType;
    }


    @Override
    protected void handleCompleteMetadataEvent(StatementMetadata metadata) {
        boolean isEnabled = false;

        if (userRole.compareTo(Issuer.LEGAL_EXPERT.getLabel()) == 0
                || metadata.getIssuer() == null
                || (metadata.getIssuer().getLabel().compareTo(userRole) == 0)) {
            isEnabled = true;
        }
        boolean isEndUser = false;
        if (userRole.compareTo(Issuer.USER.getLabel())==0){
            isEndUser = true;
        }
        state.handleCompleteMetadataEvent(this, metadata.getIndex(),
                isEnabled, isEndUser,null);
        fireEvent(new IdleDisplayEvent(display));

    }
}
