/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.syntactic.vr;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.Mode;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.syntactic.SyntacticItemClickedEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableDeclaration;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableReference;

public class SvrpStateShowable implements SvrpState {

    @Override
    public void handleClickEvent(StatefulVRPresenterImpl context) {
        // for some reason that I don't know the MouseOut event gets lost when
        // there is a ClickEvent, and so the clicked SyntacticItem will keep
        // the CSS style applied when getting the MouseOver event. The following
        // two lines essentially forces a reset of the style. It is not very
        // elegant, but it works
        context.getDisplay().highlightShowable(Mode.OFF);
        context.getDisplay().highlightShowable(Mode.ON);
        // we just fire a SyntacticItemClickedEvent to tell anyone interested
        // that this VariableReference has been clicked; this event will be
        // received also by our context (StatefulVRPresenterImpl), which will
        // delegate its handling to the method handleSyntacticItemClickedEvent
        context.fireEvent(new SyntacticItemClickedEvent(context
                .getVariableReference()));
    }

    @Override
    public void handleHighlightShowableEvent(StatefulVRPresenterImpl context) {
        // if this VariableReference was clicked, then we have to remove style
        context.getDisplay().highlightClickedReference(Mode.OFF);
        // if this VariableReference was highlighted (because corresponding to
        // a clicked SyntacticItem), then we have to remove style
        context.getDisplay().highlightReference(Mode.OFF);
        // finally, we have to make sure that this VairableReference has the
        // show-style
        context.getDisplay().highlightShowable(Mode.ON);
    }

    @Override
    public void handleUnhighlightShowableEvent(StatefulVRPresenterImpl context) {
        context.getDisplay().highlightShowable(Mode.OFF);
        context.setState(new SvrpStateNormal());
    }

    @Override
    public void handleSyntacticItemClickedEvent(
            StatefulVRPresenterImpl context, SyntacticItem syntacticItem) {
        // here we handle the SyntacticItemClickedEvent, which has been fired
        // either by ourselves (i.e. the clicked SyntactiItem is the same as our
        // VariableReference), or by some other SyntactiItem
        VariableReference variableReference = context.getVariableReference();
        if (syntacticItem instanceof VariableReference) {
            VariableReference vr = (VariableReference) syntacticItem;
            if (variableReference.equals(vr))
                context.getDisplay().highlightClickedReference(Mode.ON);
        } else if (syntacticItem instanceof VariableDeclaration) {
            VariableDeclaration vd = (VariableDeclaration) syntacticItem;
            if (variableReference.getVariable().equals(vd.getVariable()))
                context.getDisplay().highlightReference(Mode.ON);
        }
    }

}
