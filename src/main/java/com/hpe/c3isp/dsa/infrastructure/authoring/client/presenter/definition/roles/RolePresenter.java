/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.roles;

import java.util.List;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.HasOpenHandlers;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.ControllablePresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.ControllableDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Term;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.roles.CredentialExpression;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.roles.Operator;

public interface RolePresenter extends
        ControllablePresenter<RolePresenter.RoleDisplay> {

    public interface RoleDisplay extends ControllableDisplay {

        void setRole(String humanForm);

        void setCredentials(List<String> credentials);

        void addCredentialExpression(int credentialIndex,
                String attributeExpression, Operator operator);

        int getCredentialExpressionCount();

        int getCredentialIndexInExpression(int index);

        String getAttributeInExpression(int index);

        Operator getOperatorInExpression(int index);

        HasOpenHandlers<DisclosurePanel> getChangeDefinitionPanel();

        HasClickHandlers getOkButton();
    }

    Term getRole();

    List<CredentialExpression> getRoleDefinition();

    void set(Term role, List<Term> allCredentials);

    void set(Term role, List<Term> allCredentials,
            List<CredentialExpression> definition);

}
