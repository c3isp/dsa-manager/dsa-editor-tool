/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary;

import net.customware.gwt.dispatch.server.ActionHandler;
import net.customware.gwt.dispatch.server.ExecutionContext;
import net.customware.gwt.dispatch.shared.DispatchException;

import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.helper.VocabularyOntology;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.vocabulary.GetLeafTerms;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.vocabulary.VocabularyGetResult;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.vocabulary.VocabularyGetResultImpl;

public class GetLeafTermsHandler extends AbstractVocabularyGetterHandler
        implements ActionHandler<GetLeafTerms, VocabularyGetResult> {

    @Override
    public VocabularyGetResult execute(GetLeafTerms action,
            ExecutionContext context) throws DispatchException {
        VocabularyOntology vocont = getVocabularyOntology(action
                .getVocabularyUri());
        return new VocabularyGetResultImpl(buildTermTreeNodes(
                vocont.getLeafTerms(), vocont));
    }

    @Override
    public Class<GetLeafTerms> getActionType() {
        return GetLeafTerms.class;
    }

    @Override
    public void rollback(GetLeafTerms action, VocabularyGetResult result,
            ExecutionContext context) throws DispatchException {
        throw new UnsupportedOperationException();
    }

}
