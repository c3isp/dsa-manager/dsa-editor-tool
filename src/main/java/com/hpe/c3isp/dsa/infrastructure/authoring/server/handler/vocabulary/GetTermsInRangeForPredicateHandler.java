/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary;

import net.customware.gwt.dispatch.server.ActionHandler;
import net.customware.gwt.dispatch.server.ExecutionContext;
import net.customware.gwt.dispatch.shared.DispatchException;

import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.helper.VocabularyOntology;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.vocabulary.GetTermsInRangeForPredicate;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.vocabulary.VocabularyGetResult;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.vocabulary.VocabularyGetResultImpl;

public class GetTermsInRangeForPredicateHandler extends
        AbstractVocabularyGetterHandler implements
        ActionHandler<GetTermsInRangeForPredicate, VocabularyGetResult> {

    @Override
    public VocabularyGetResult execute(GetTermsInRangeForPredicate action,
            ExecutionContext context) throws DispatchException {
        String predicateUid = action.getSyntacticItemUid();
        VocabularyOntology vocont = getVocabularyOntology(action
                .getSyntacticItemVocabularyUri());
        return new VocabularyGetResultImpl(buildTermTreeNodes(
                vocont.getTermsInRangeForPredicate(predicateUid), vocont));
    }

    @Override
    public Class<GetTermsInRangeForPredicate> getActionType() {
        return GetTermsInRangeForPredicate.class;
    }

    @Override
    public void rollback(GetTermsInRangeForPredicate action,
            VocabularyGetResult result, ExecutionContext context)
            throws DispatchException {
        // nothing to do: can't rollback a get-action
    }

}
