/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.roles;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.enunes.gwt.mvp.client.EventBus;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.presenter.BusyDisplayHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.presenter.IdleDisplayHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.AbstractControllablePresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.roles.RolesSetPresenter.RolesSetDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.ControllableDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Term;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.roles.Roles;

public class RolesSetPresenterImpl extends
        AbstractControllablePresenter<RolesSetDisplay> implements
        RolesSetPresenter, BusyDisplayHandler, IdleDisplayHandler {

    private final List<RolePresenter> rolePresenters = new ArrayList<RolePresenter>();

    private final Provider<RolePresenter> providerOfRolePresenter;

    @Inject
    public RolesSetPresenterImpl(EventBus eventBus, RolesSetDisplay display,
            Provider<RolePresenter> prp) {
        super(eventBus, display);
        providerOfRolePresenter = prp;
    }

    @Override
    public Roles getRoles() {
        Roles roles = new Roles();
        for (RolePresenter rp : rolePresenters)
            roles.setRoleDefinition(rp.getRole(), rp.getRoleDefinition());
        return roles;
    }

    @Override
    public void setRoles(Roles roles, Collection<Term> allCredentials) {
        // a RolePresenter needs a ordered list of credential Terms (because its
        // display will refer to indexes in this list), so here we build a List
        // from the Collection
        final List<Term> allCredentialsList = new ArrayList<Term>(
                allCredentials.size());
        allCredentialsList.addAll(allCredentials);
        rolePresenters.clear();
        display.clear();
        for (Term role : roles.getRoles()) {
            final RolePresenter rp = providerOfRolePresenter.get();
            rp.set(role, allCredentialsList, roles.getRoleDefinition(role));
            rp.addBusyDisplayHandler(this);
            rp.addIdleDisplayHandler(this);
            rolePresenters.add(rp);
            rp.bind();
            display.addRoleDisplay(rp.getDisplay());
        }
    }

    @Override
    public void onBusyDisplay(ControllableDisplay display) {
        // deactivate the displays of all my RolePresenters except the one
        // that possibly sent the BusyDisplayEvent
        for (RolePresenter rp : rolePresenters) {
            ControllableDisplay cd = rp.getDisplay();
            if (!cd.equals(display))
                cd.deactivate();
        }
    }

    @Override
    public void onIdleDisplay(ControllableDisplay display) {
        // activate all the displays of my RolePresenters
        for (RolePresenter rp : rolePresenters)
            rp.getDisplay().activate();
    }
}
