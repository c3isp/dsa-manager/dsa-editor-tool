/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa.helper;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptionCharEscapeMap;
import org.apache.xmlbeans.XmlOptions;

public class XmlUtil {

    public static XmlOptions getXmlOptions() {
        XmlOptions xmlOptions = new XmlOptions();
        xmlOptions.setSavePrettyPrint();
        xmlOptions.setUseDefaultNamespace();
        xmlOptions.setCharacterEncoding("UTF-8");
        
        XmlOptionCharEscapeMap escapes = new XmlOptionCharEscapeMap();
        try {
            escapes.addMapping('<', XmlOptionCharEscapeMap.PREDEF_ENTITY);
            escapes.addMapping('>', XmlOptionCharEscapeMap.PREDEF_ENTITY);
        } catch (XmlException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        

        xmlOptions.setSaveSubstituteCharacters(escapes);
   
        return xmlOptions;
    }
    
    public static String getCDataValue() {
        String xmlText = "";
//                "<a>\n" +
//                "<a><![CDATA[cdata text]]></a>\n" +
//                "<c>text <![CDATA[cdata text]]></c>\n" +
//                "</a>";
        XmlOptions opts = new XmlOptions();
        opts.setUseCDataBookmarks();
        String out = "";
        XmlObject xo;
        try {
            xo = XmlObject.Factory.parse( xmlText , opts);
            opts.setSavePrettyPrint();
            out = xo.xmlText(opts);
        } catch (XmlException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return out;
    }
}
