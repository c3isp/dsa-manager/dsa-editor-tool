/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.event.statement;

import com.google.gwt.event.shared.GwtEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.StatementMetadata;

public class StatementBrokenEvent extends GwtEvent<StatementBrokenHandler> {

    private final StatementMetadata metadata;

    public static Type<StatementBrokenHandler> TYPE = new Type<StatementBrokenHandler>();

    public StatementBrokenEvent(StatementMetadata metadata) {
        super();
        this.metadata = metadata;
    }

    @Override
    protected void dispatch(StatementBrokenHandler handler) {
        handler.onStatementBroken(metadata);
    }

    @Override
    public Type<StatementBrokenHandler> getAssociatedType() {
        return TYPE;
    }

}
