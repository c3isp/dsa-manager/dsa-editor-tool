/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.fsm;

import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableReference;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.StatementType;

public class CnlPrompterState12 extends
        AbstractCnlPrompterStateWithTheSelectedReference {

    @Override
    public void handleReference(CnlPrompterContext context,
            SyntacticItem syntacticItem) {
        StatementType statementType = context.getStatementType();
        assert statementType != null;
        VariableReference vr = buildVariableReference(syntacticItem);
        if (statementType.equals(StatementType.AUTHORIZATION)
                || statementType
                        .equals(StatementType.DATA_SUBJECT_AUTHORIZATION)
                || statementType
                        .equals(StatementType.DERIVED_OBJECTS_AUTHORIZATION))
            context.update(new CnlPrompterState02Auth(vr), vr);
        else if (statementType.equals(StatementType.OBLIGATION)
                || statementType.equals(StatementType.DERIVED_OBJECTS_OBLIGATION))
            context.update(new CnlPrompterState02Obl(vr), vr);
        else if (statementType.equals(StatementType.PROHIBITION)
                || statementType
                        .equals(StatementType.DERIVED_OBJECTS_PROHIBITION))
            context.update(new CnlPrompterState02Pro(vr), vr);
        else
            assert false : "unknonw StatementType " + statementType;
    }

}
