/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa;

import com.google.gwt.core.shared.GWT;

public abstract class AbstractSyntacticItem implements SyntacticItem {

    protected String uid;
    protected String humanForm;
    protected String value;

    // for serialization
    AbstractSyntacticItem() {
    }

    public AbstractSyntacticItem(String uid, String humanForm, String value) {
        this.uid = uid;
        this.humanForm = humanForm;
        this.value = value;
    }

    @Override
    public String getHumanForm() {
        return humanForm;
    }

    @Override
    public String getUid() {
        return uid;
    }

    @Override
    public String getValue() {
    	GWT.log("AbstractSyntacticItem getValue called, value is:" + value == null?"NULL":value);

        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


	//marco: why there is no SetHumanForm? I want to be able to modify it, let us add the setter
    public void setHumanForm(String humanForm) {
        this.humanForm = humanForm;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((humanForm == null) ? 0 : humanForm.hashCode());
        result = prime * result + ((uid == null) ? 0 : uid.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AbstractSyntacticItem other = (AbstractSyntacticItem) obj;
        if (humanForm == null) {
            if (other.humanForm != null)
                return false;
        } else if (!humanForm.equals(other.humanForm))
            return false;
        if (uid == null) {
            if (other.uid != null)
                return false;
        } else if (!uid.equals(other.uid))
            return false;
        return true;
    }

    @Override
    public String toString() {
          return "AbstractSyntacticItem [humanForm=" + humanForm + ", uid=" + uid
                + "]";
    }

    protected String getFragmentOfUri(String uri) {
        return uri.substring(uri.indexOf("#") + 1, uri.length());
    }

}
