/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.crossref;

import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.user.client.ui.PopupPanel;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.presenter.IdleDisplayEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.syntactic.HighlightShowableEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.syntactic.UnhighlightShowableEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;

public class CrpStateCrossRefsOn implements CrpState {

    @Override
    public void doToggleReferences(CrossReferencePresenterImpl context) {
        // we expect that now the ToggleButton is not down ...
        assert !context.getDisplay().getToggleReferencesButton().isDown();
        // ... so we switch to the off-state
        context.setState(new CrpStateCrossRefsOff());
        // we tell everyone to un-highlight its own showable syntactic items
        context.fireEvent(new UnhighlightShowableEvent());
        // we tell everyone that our display is now back to the inactive mode
        context.fireEvent(new IdleDisplayEvent(context.getDisplay()));
    }

    @Override
    public void handleClickOnSyntacticItem(
            final CrossReferencePresenterImpl context,
            SyntacticItem syntacticItem) {
        context.getDisplay()
                .showMessage(
                        "showing cross-references for \""
                                + syntacticItem.getHumanForm() + "\"")
                .addCloseHandler(new CloseHandler<PopupPanel>() {

                    @Override
                    public void onClose(CloseEvent<PopupPanel> event) {
                        context.fireEvent(new HighlightShowableEvent());
                    }
                });
    }

}
