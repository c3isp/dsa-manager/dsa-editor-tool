/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.view.syntactic;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.Mode;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.syntactic.vr.StatefulVRPresenter.VariableReferenceDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.helper.SyntacticItemDisplayDecorator;

public class VariableReferenceDisplayImpl extends SyntacticItemDisplayImpl
        implements VariableReferenceDisplay {

    private final SyntacticItemDisplayDecorator<VariableReferenceDisplay> decoratorForShow;
    private final SyntacticItemDisplayDecorator<VariableReferenceDisplay> decoratorForHighlight;
    private final SyntacticItemDisplayDecorator<VariableReferenceDisplay> decoratorForHighlightClicked;

    public VariableReferenceDisplayImpl() {
        decoratorForShow = new SyntacticItemDisplayDecorator<VariableReferenceDisplay>(
                this, "VariableReferenceBorder",
                "VariableReferenceBorderAndMouseOver");
        decoratorForHighlight = new SyntacticItemDisplayDecorator<VariableReferenceDisplay>(
                this, "VariableReferenceHighlighted", "");
        decoratorForHighlightClicked = new SyntacticItemDisplayDecorator<VariableReferenceDisplay>(
                this, "VariableReferenceHighlightedClicked", "");
    }

    @Override
    public void highlightShowable(Mode mode) {
        decoratorForShow.decorate(mode);
    }

    @Override
    public void highlightReference(Mode mode) {
        decoratorForHighlight.decorate(mode);
    }

    @Override
    public void highlightClickedReference(Mode mode) {
        decoratorForHighlightClicked.decorate(mode);
    }

}
