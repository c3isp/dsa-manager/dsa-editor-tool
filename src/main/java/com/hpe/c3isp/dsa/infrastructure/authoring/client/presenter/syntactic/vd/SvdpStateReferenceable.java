/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.syntactic.vd;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.Mode;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.syntactic.SyntacticItemClickedEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.syntactic.UnhighlightReferenceableEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;

public class SvdpStateReferenceable implements SvdpState {

    @Override
    public void handleClickEvent(StatefulVDPresenterImpl context) {
        context.fireEvent(new SyntacticItemClickedEvent(context
                .getVariableDeclaration()));
        context.fireEvent(new UnhighlightReferenceableEvent());
    }

    @Override
    public void handleHighlightReferenceable(StatefulVDPresenterImpl context) {
    }

    @Override
    public void handleHighlightShowableEvent(StatefulVDPresenterImpl context) {
    }

    @Override
    public void handleSyntacticItemClickedEvent(
            StatefulVDPresenterImpl context, SyntacticItem syntacticItem) {
    }

    @Override
    public void handleUnhighlightReferenceable(StatefulVDPresenterImpl context) {
        context.getDisplay().highlightReferenceable(Mode.OFF);
        context.setState(new SvdpStateNormal());
    }

    @Override
    public void handleUnhighlightShowableEvent(StatefulVDPresenterImpl context) {
    }

}
