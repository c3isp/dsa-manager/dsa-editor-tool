/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.util;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class MyDialog extends DialogBox {


    public MyDialog(String title, String content) {
        // Set the dialog box's caption.
        setText(title);

        // Enable animation.
        setAnimationEnabled(false);

        // Enable glass background.
        setGlassEnabled(true);

        Button close = new Button("Close");
        close.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                MyDialog.this.hide();
            }
        });

        Label label = new Label();
        label.setText(content);
        label.setStyleName("dialogContent");

        VerticalPanel panel = new VerticalPanel();
        panel.setHeight("100");
        panel.setWidth("700");
        panel.setSpacing(10);
        panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        panel.add(label);
        panel.add(close);

        setWidget(panel);
    }
}