/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.TextInputCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.DsaBean;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Organization;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.Issuer;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.Statement;

public class WidgetUtil {
	//ma perche' usare valori cablati???

    public static String getPolicyTypeString(int index) {
        String[] policyType = { "DenyAll", "DenyAllAndDeleteNow", "DeleteInSpecifiedPeriod" };
        if (index <= policyType.length - 1) {
            return policyType[index];
        }
        return policyType[0];
    }

    public static int getPolicyTypeIndex(String policy) {
        if ("DenyAll".equals(policy))
            return 0;
        if ("DenyAllAndDeleteNow".equals(policy))
            return 1;
        if ("DeleteInSpecifiedPeriod".endsWith(policy))
            return 2;
        return 0;

    }

    public static String getRoles(int index) {
        String[] roleList = { "data-controller", "data-processor", "data-subject" };
        if (index > 0 && index <= roleList.length)
            return roleList[index - 1];
        return null;
    }

    public static Widget addField(String label, String styleName, String value) {
        Label lblName = new Label(label);

        HorizontalPanel hPanel = new HorizontalPanel();
        hPanel.add(lblName);
        hPanel.setStyleName(styleName);

        TextBox textBox = new TextBox();
        textBox.setText(value);
        textBox.setStyleName("text");
        hPanel.add(textBox);
        VerticalPanel vPanel = new VerticalPanel();
        vPanel.setSpacing(10);
        vPanel.add(hPanel);
        vPanel.add(textBox);
        vPanel.setSpacing(5);

        return vPanel;
    }

    public static String setCustomCreateButtonPanel(String userRole,
            String createTemplateButtonLabel) {
        String createButtonLabel = null;
        if (userRole.compareTo(Issuer.LEGAL_EXPERT.getLabel()) == 0) {
            createButtonLabel = createTemplateButtonLabel;
        }
        return createButtonLabel;
    }

    public static String setCustomCompleteButtonPanel(String userRole,
            String completeTemplateButtonLabel) {
        String createButtonLabel = null;
        if (userRole.compareTo(Issuer.POLICY_EXPERT.getLabel()) == 0) {
            createButtonLabel = completeTemplateButtonLabel;
        }
        return createButtonLabel;
    }

    public static String setCustomViewButtonPanel(String userRole,
            String viewButtonLabel, String viewTemplateButtonLabel) {
        String createButtonLabel = null;
        if ((userRole.compareTo(Issuer.USER.getLabel()) == 0)
                || (userRole.compareTo(Issuer.POLICY_EXPERT.getLabel()) == 0)) {
            createButtonLabel = viewButtonLabel;
        } else if (userRole.compareTo(Issuer.LEGAL_EXPERT.getLabel()) == 0) {
            createButtonLabel = viewTemplateButtonLabel;
        }
        return createButtonLabel;
    }

    public static String setCustomViewRawDataButtonPanel(String userRole,
            String buttonRawLabel, String buttonRawTemplateLabel) {
        String createButtonLabel = null;
        if ((userRole.compareTo(Issuer.USER.getLabel()) == 0)
                || (userRole.compareTo(Issuer.POLICY_EXPERT.getLabel()) == 0)) {
            createButtonLabel = buttonRawLabel;
        } else if (userRole.compareTo(Issuer.LEGAL_EXPERT.getLabel()) == 0) {
            createButtonLabel = buttonRawTemplateLabel;
        }
        return createButtonLabel;
    }

    public static String setCustomEditButtonPanel(String userRole,
            String buttonEditDSALabel, String buttonEditDSATemplateLabel) {
        String createButtonLabel = null;
        if ((userRole.compareTo(Issuer.USER.getLabel()) == 0)
                || (userRole.compareTo(Issuer.POLICY_EXPERT.getLabel())) == 0) {
            createButtonLabel = buttonEditDSALabel;
        } else if (userRole.compareTo(Issuer.LEGAL_EXPERT.getLabel()) == 0) {
            createButtonLabel = buttonEditDSATemplateLabel;
        }
        return createButtonLabel;
    }

    public static Widget getPartiesTable(
            final List<Organization> organizations, String userRole,
            String name, String role, String responsibilities) {

        final CellTable<Organization> table = new CellTable<Organization>();
        table.setStyleName("myCellTable");

        if (userRole.compareTo(Issuer.POLICY_EXPERT.getLabel()) == 0) {
            // Add a text input column to edit the name.
            final TextInputCell namePartyCell = new TextInputCell();
            Column<Organization, String> nameCol = new Column<Organization, String>(
                    namePartyCell) {
                @Override
                public String getValue(Organization object) {
                    return object.getName();
                }
            };

            table.addColumn(nameCol, name);

            // Add a field updater to be notified when the user enters a new
            // name.
            nameCol.setFieldUpdater(new FieldUpdater<Organization, String>() {

                @Override
                public void update(int index, Organization object, String value) {
                    // TODO Auto-generated method stub
                    object.setName(value);
                    table.redraw();
                }
            });
        } else {
            // Create name column.
            TextColumn<Organization> nameColumn = new TextColumn<Organization>() {
                @Override
                public String getValue(Organization org) {
                    return org.getName();
                }
            };

            // Add the columns.
            table.addColumn(nameColumn, name);
        }

        // Create role column.
        TextColumn<Organization> roleColumn = new TextColumn<Organization>() {
            @Override
            public String getValue(Organization org) {
                return org.getRole();
            }

        };

        // Add the columns.
        table.addColumn(roleColumn, role);

        // Create role column.
        TextColumn<Organization> respoColumn = new TextColumn<Organization>() {
            @Override
            public String getValue(Organization org) {
                return org.getResponsibilities();
            }
        };

        // Add the columns.
        table.addColumn(respoColumn, responsibilities);

        if (userRole.compareTo(Issuer.LEGAL_EXPERT.getLabel()) == 0) {
            List<String> roleList = new ArrayList<String>();
            roleList.add("data-controller");
            roleList.add("data-processor");
            roleList.add("data-subject");
            SelectionCell roleCell = new SelectionCell(roleList);
            Column<Organization, String> roleCol = new Column<Organization, String>(
                    roleCell) {
                @Override
                public String getValue(Organization object) {
                    return object.getRole();
                }
            };
            table.addColumn(roleCol, "Update Role");
            roleCol.setFieldUpdater(new FieldUpdater<Organization, String>() {

                @Override
                public void update(int index, Organization object, String value) {
                    object.setRole(value);
                    table.redraw();

                }
            });

        }
        // Add a text input column to edit the responsibilities.
        final TextInputCell nameCell = new TextInputCell();
        Column<Organization, String> testCol = new Column<Organization, String>(
                nameCell) {
            @Override
            public String getValue(Organization object) {
                return object.getResponsibilities();
            }
        };

        table.addColumn(testCol, responsibilities);

        // Add a field updater to be notified when the user enters a new name.
        testCol.setFieldUpdater(new FieldUpdater<Organization, String>() {

            @Override
            public void update(int index, Organization object, String value) {
                // TODO Auto-generated method stub
                object.setResponsibilities(value);
                table.redraw();
            }
        });
        return table;
    }


    public static Widget getPartiesTableForName(
            final List<Organization> organizations, String userRole,
            String name) {

        final CellTable<Organization> table = new CellTable<Organization>();
        table.setStyleName("myCellTable");

        if (userRole.compareTo(Issuer.USER.getLabel()) != 0) {
            // Add a text input column to edit the name.
            final TextInputCell namePartyCell = new TextInputCell();
            Column<Organization, String> nameCol = new Column<Organization, String>(
                    namePartyCell) {
                @Override
                public String getValue(Organization object) {
                    return object.getName();
                }
            };

            table.addColumn(nameCol, name);

            // Add a field updater to be notified when the user enters a new
            // name.
            nameCol.setFieldUpdater(new FieldUpdater<Organization, String>() {

                @Override
                public void update(int index, Organization object, String value) {
                    // TODO Auto-generated method stub
                    object.setName(value);
                    table.redraw();
                }
            });
        } else {
            // Create name column.
            TextColumn<Organization> nameColumn = new TextColumn<Organization>() {
                @Override
                public String getValue(Organization org) {
                    return org.getName();
                }
            };

            // Add the columns.
            table.addColumn(nameColumn, name);
        }
        return table;
    }

    public static boolean containsPendingRules(DsaBean obj) {
        List<Statement> statements = obj.getAuthorizations();
        statements.addAll(obj.getDataSubjectAuthorizations());
        statements.addAll(obj.getObligations());
        statements.addAll(obj.getProhibitions());
        statements.addAll(obj.getDerivedObjectsAuthorizations());
        statements.addAll(obj.getDerivedObjectsObligations());
        statements.addAll(obj.getDerivedObjectsProhibitions());

        for (Iterator<Statement> iterator = statements.iterator(); iterator
                .hasNext();) {
            Statement statement = (Statement) iterator.next();
            if (statement.getStatementMetadata().isPendingRule()
                    && statement.getStatementMetadata().getStatementInfo()
                            .isEmpty())
                return true;
        }
        return false;
    }

	public static ListBox getVocabularyListBox(String vocabularyValues) {
		ListBox vocabularyListBox = new ListBox();
		//TODO retrieve values from db
		List<String> couples = Arrays.asList(vocabularyValues.split(","));
		for (Iterator iterator = couples.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			String[] items=string.split("-");
			vocabularyListBox.addItem(items[0],items[1]);
		}
		vocabularyListBox.setVisibleItemCount(1);
		return vocabularyListBox;
	}

	public static List<String> getOptionsList(String commaSeparatedOptions, String separator){
		if (commaSeparatedOptions==null || commaSeparatedOptions.isEmpty())
			return null;
		else{
			String[] elements = commaSeparatedOptions.split(separator);
			if (elements.length!=0)
				return Arrays.asList(elements);
			else
				return null;
		}
	}

	/**
	 * Update the value in the input string.
	 *
	 * Format example:
	 * 	 inputValue = ?X_16:Identifier=oldValue
	 * 	 return ?X_16:Identifier=updatedValue
	 * @param inputValue
	 * @param updatedValue
	 * @return
	 */
	public static String updateValue(String inputValue, String updatedValue, int index){
		String[] elements = inputValue.split(";");
		for (int i = 0; i < elements.length; i++) {
			//String id = elements[i].substring(elements[i].indexOf("?")+1,elements[i].indexOf(":") );
			if (i ==  index){
//				String value =  elements[i].substring(elements[i].lastIndexOf("=")+1);
//				elements[i] = elements[i].replaceAll(value, updatedValue);
				elements[i] = updatedValue;
			}
		}
		String out = null;
		for (int i = 0; i < elements.length; i++) {
			if (out == null)
				out = elements[i];
			else
				out = out.concat(";"+elements[i]);
		}
		return out;
	}











	public static void listBoxSelectValue(ListBox listBox, String selectedValue) {
		if(listBox == null) {
			GWT.log("))))))))))))))listBoxSelectValue: lb is null");
			return;
		}
		GWT.log("))))))))))))))listBoxSelectValue:"
		+ (listBox == null?"lb NULL":"lb OK")
		+ "LB enabled:" + (listBox.isEnabled()?"enabled":"NOT enabled")
		+ " slect value:" + selectedValue
		);
		int count = listBox.getItemCount();
		int selIndex = -1;
		if(selectedValue != null && count > 0) {
			for(int i=0;i<count;i++) {
				String v = listBox.getItemText(i);
				if(v.equals(selectedValue)) {
					selIndex = i;
					break;
				}
			}
		}
		if(selIndex == -1) {
			selIndex = 0;
		}
		boolean disabled = !listBox.isEnabled();
		if(disabled) {
			listBox.setEnabled(true);
		}
		listBox.setSelectedIndex(selIndex);
		if(disabled) {
			listBox.setEnabled(false);
		}
	}

	public static ListBox createListBox(List<String> values, String selectedValue, String styleName, int visibleItemCount) {
		ListBox ret = new ListBox();
		ret.addStyleName(styleName);
		int selIndex = 0;
		for (int i=0;i<values.size();i++) {
			String v = values.get(i);
			ret.addItem(v);
			if(selectedValue != null && v.equals(selectedValue)) {
				selIndex = i;
			}
		}
		ret.setSelectedIndex(selIndex);
		ret.setVisibleItemCount(visibleItemCount);
		return ret;
	}






}
