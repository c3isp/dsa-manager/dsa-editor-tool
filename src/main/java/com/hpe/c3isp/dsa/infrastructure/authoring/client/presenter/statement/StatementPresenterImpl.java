/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement;

import java.util.ArrayList;
import java.util.List;

import org.enunes.gwt.mvp.client.EventBus;
import org.enunes.gwt.mvp.client.presenter.BasePresenter;
import org.enunes.gwt.mvp.client.presenter.Presenter;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.EditingCompletedEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.EditingCompletedHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.EditingCompletedMetadataEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.EditingCompletedMetadataHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.VocabularySelectionEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.VocabularySelectionHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.statement.CompleteMetadataEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.statement.StatementBrokenEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.statement.StatementCompletedEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.StatementPresenter.StatementDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.syntactic.FunctorPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.syntactic.TokenPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.syntactic.vd.StatefulVDPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.syntactic.vr.StatefulVRPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.util.UIDGenerator;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.syntactic.SyntacticItemDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Functor;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Term;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Token;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableDeclaration;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableReference;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.Statement;

public class StatementPresenterImpl extends BasePresenter<StatementDisplay>
        implements StatementPresenter {

    private Statement statement = null;
    private final List<Presenter<?>> presenters = new ArrayList<Presenter<?>>();
    private final Provider<StatefulVDPresenter> providerOfStatefulVDPresenter;
    private final Provider<StatefulVRPresenter> providerOfStatefulVRPresenter;
    private final Provider<FunctorPresenter> providerOfFunctorPresenter;
    private final Provider<TokenPresenter> providerOfTokenPresenter;

    @Inject
    public StatementPresenterImpl(EventBus eventBus, StatementDisplay display,
            Provider<FunctorPresenter> pfp, Provider<TokenPresenter> ptp,
            Provider<StatefulVDPresenter> psvdp,
            Provider<StatefulVRPresenter> psvrp) {
        super(eventBus, display);
        providerOfFunctorPresenter = pfp;
        providerOfTokenPresenter = ptp;
        providerOfStatefulVDPresenter = psvdp;
        providerOfStatefulVRPresenter = psvrp;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.enunes.gwt.mvp.client.presenter.BasePresenter#bind()
     */
    @Override
    public void bind() {
        super.bind();
        registerHandler(eventBus.addHandler(VocabularySelectionEvent.TYPE,
                new VocabularySelectionHandler() {

                    @Override
                    public void onVocabularySelection(
                            SyntacticItem syntacticItem) {
                        addSyntacticItem(syntacticItem);
                    }
                }));
        registerHandler(eventBus.addHandler(EditingCompletedEvent.TYPE,
                new EditingCompletedHandler() {

                    @Override
                    public void onEditingCompleted(boolean successful) {
                        handleEditingCompletedEvent(successful);
                    }
                }));
        
        registerHandler(eventBus.addHandler(EditingCompletedMetadataEvent.TYPE,
                new EditingCompletedMetadataHandler() {

                    @Override
                    public void onEditingCompletedMetadata(boolean successful) {
                        handleEditingCompletedMetadataEvent(successful);
                    }
                }));

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.enunes.gwt.mvp.client.presenter.BasePresenter#unbind()
     */
    @Override
    public void unbind() {
        super.unbind();
        for (Presenter<?> presenter : presenters) {
            presenter.unbind();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see marco.client.presenter.statement.StatementPresenter#getStatement()
     */
    @Override
    public Statement getStatement() {
        return statement;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * marco.client.presenter.statement.StatementPresenter#setStatement(marco
     * .client.model.Statement)
     */
    @Override
    public void setStatement(Statement statement) {
        this.statement = statement;
        updateDisplay();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * marco.client.presenter.statement.StatementPresenter#removeReferencesTo
     * (marco.client.model.Statement, java.util.List)
     */
    @Override
    public void removeReferencesTo(Statement statement,
            List<VariableDeclaration> toBeRemoved) {
        this.statement.removeReferenceTo(statement, toBeRemoved);
        updateDisplay();
    }

    protected void handleEditingCompletedEvent(boolean successful) {
        // we don't want to listen to editing-related event any longer, so
        // we call unbind()
        unbind();
        if (successful) {
            // we tell anyone who's interested that our statement is completed
//            CheckBox protectedCheckBox = new CheckBox("Protected");
//            protectedCheckBox.setValue(statement.getStatementMetadata().isProtected());            
//            CheckBox isPendingRuleCheckBox = new CheckBox("PendingRule");
//            isPendingRuleCheckBox.setValue(statement.getStatementMetadata().isPendingRule());
//           
//            Label statementInfo = new Label(statement.getStatementMetadata().getStatementInfo());;
//            statementInfo.setVisible(statement.getStatementMetadata().isPendingRule());
//            TextArea inputValue = new TextArea();
//            inputValue.setValue(statement.getStatementMetadata().getInputValue());
//            inputValue.setVisible(statement.getStatementMetadata().isPendingRule());
//            
//            statement.getStatementMetadata().setInputValue(inputValue.getValue());
//            statement.getStatementMetadata().setPendingRule(isPendingRuleCheckBox.getValue());
//            statement.getStatementMetadata().setProtected(protectedCheckBox.getValue());
//            statement.getStatementMetadata().setStatementInfo(statementInfo.getText());
            eventBus.fireEvent(new StatementCompletedEvent(statement
                    .getStatementMetadata()));//,protectedCheckBox, isPendingRuleCheckBox, statementInfo, inputValue));
            // we log the statement
            GWT.log(statement.toString());
        } else {
            // we tell anyone who's interested that our statement is meaningless
            eventBus.fireEvent(new StatementBrokenEvent(statement
                    .getStatementMetadata()));
            // we log the statement
            GWT.log("broken statement: " + statement.toString());
        }
    }
    
    protected void  handleEditingCompletedMetadataEvent(boolean successful) {
        // we don't want to listen to editing-related event any longer, so
        // we call unbind()
        unbind();
        if (successful) {           
            eventBus.fireEvent(new CompleteMetadataEvent(statement
                    .getStatementMetadata()));
            GWT.log(statement.toString());
        } else {
            // we tell anyone who's interested that our statement is meaningless
            eventBus.fireEvent(new StatementBrokenEvent(statement
                    .getStatementMetadata()));
            // we log the statement
            GWT.log("broken statement: " + statement.toString());
        }
    }

    protected void addSyntacticItem(SyntacticItem si) {
        if (si instanceof Term) {
            // we turn a vocabulary Term into a VariableDeclaration
            final Term t = (Term) si;
            final VariableDeclaration vd = new VariableDeclaration(t.getUid(),
                    t.getHumanForm(), UIDGenerator.getInstance().getNextUid(
                            "?X_"), t.getValue());
            statement.addSyntacticItem(vd);
            addPresenterFor(vd);
        } else {
            assert si instanceof VariableDeclaration
                    || si instanceof VariableReference || si instanceof Functor
                    || si instanceof Token : "unknown SyntacticItem: "
                    + si.toString();
            // we handle all other SyntacticItems
            statement.addSyntacticItem(si);
            addPresenterFor(si);
        }
    }

    private void updateDisplay() {
        display.clear();
        for (SyntacticItem si : statement.getSyntacticItems())
            addPresenterFor(si);
    }

    private void addPresenterFor(SyntacticItem si) {
        if (si instanceof VariableDeclaration) {
            final StatefulVDPresenter svdp = providerOfStatefulVDPresenter
                    .get();
            svdp.show((VariableDeclaration) si);
            addPresenter(svdp);
        } else if (si instanceof VariableReference) {
            ;
            final StatefulVRPresenter svrp = providerOfStatefulVRPresenter
                    .get();
            svrp.show((VariableReference) si);
            addPresenter(svrp);
        } else if (si instanceof Functor) {
            final FunctorPresenter fp = providerOfFunctorPresenter.get();
            fp.show((Functor) si);
            addPresenter(fp);
        } else if (si instanceof Token) {
            final TokenPresenter tp = providerOfTokenPresenter.get();
            tp.show((Token) si);
            addPresenter(tp);
        } else {
            assert false : "unknown SyntacticItem: " + si.toString();
        }
    }

    private void addPresenter(
            Presenter<? extends SyntacticItemDisplay> presenter) {
        if (presenter != null) {
            presenters.add(presenter);
            display.addSyntacticItemDisplay(presenter.getDisplay());
            presenter.bind();
        }
    }

    @Override
    public void finish(boolean successful) {
        eventBus.fireEvent(new EditingCompletedMetadataEvent(successful));
        GWT.log("Statement is completed: " + statement.toString());
        
    }

}
