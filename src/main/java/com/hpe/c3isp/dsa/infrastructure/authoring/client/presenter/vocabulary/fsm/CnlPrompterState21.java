/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.fsm;

import java.util.ArrayList;
import java.util.List;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.model.VocabularyChoicesUpdater;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Token;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Tokens;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.vocabulary.GetNothing;

public class CnlPrompterState21 implements CnlPrompterState {

    private final List<Token> tokens;
    private final VocabularyChoicesUpdater vocabularyChoicesUpdater;

    public CnlPrompterState21() {
        tokens = new ArrayList<Token>();
        tokens.add(Tokens.TOKEN_THEN);
        vocabularyChoicesUpdater = new VocabularyChoicesUpdater(tokens,
                new GetNothing());
    }

    @Override
    public VocabularyChoicesUpdater getVocabularyChoicesUpdater() {
        return vocabularyChoicesUpdater;
    }

    @Override
    public void handleReference(CnlPrompterContext context,
            SyntacticItem syntacticItem) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleSelection(CnlPrompterContext context,
            SyntacticItem selectedItem) {
        context.update(new CnlPrompterState11(), selectedItem);
    }

}
