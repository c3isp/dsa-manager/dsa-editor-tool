/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.view.helper;

import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Label;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.Mode;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.syntactic.SyntacticItemDisplay;

/**
 * Helper class for decorating the {@link Label} of a
 * {@link SyntacticItemDisplay}. It works like this
 * <ul>
 * <li>when switching on the decoration (by calling decorate with {@link Mode}
 * ON) it adds decorationStyle to the label</li>
 * <li>when the decoration is on and the mouse is over the label, it adds
 * decorationStyleWithMouseOver to the label</li>
 * <li>when switching off the decoration (by calling decorate with {@link Mode}
 * OFF) it removes both decorationStyleWithMouseOver and decorationStyle from
 * the label</li>
 * </ul>
 * Note: it uses add/removeStyleDependentName, so CSS styles must be declared in
 * the proper way (dependent styles!)
 * 
 * @author Marco Luca Sbodio (mailto:marco.sbodio@hp.com)
 * 
 * @param <D>
 */
public class SyntacticItemDisplayDecorator<D extends SyntacticItemDisplay> {

    private final D display;
    private final String decorationStyle;
    private final String decorationStyleWithMouseOver;
    @SuppressWarnings("unused")
    private final HandlerRegistration mouseOverHandler;
    @SuppressWarnings("unused")
    private final HandlerRegistration mouseOutHandler;
    private boolean isDecorationOn = false;

    public SyntacticItemDisplayDecorator(final D display,
            final String decorationStyle,
            final String decorationStyleWithMouseOver) {
        this.display = display;
        this.decorationStyle = decorationStyle;
        this.decorationStyleWithMouseOver = decorationStyleWithMouseOver;
        mouseOverHandler = display.getLabel().addMouseOverHandler(
                new MouseOverHandler() {

                    @Override
                    public void onMouseOver(MouseOverEvent event) {
                        if (isDecorationOn)
                            display.getLabel().addStyleDependentName(
                                    decorationStyleWithMouseOver);
                    }
                });
        mouseOutHandler = display.getLabel().addMouseOutHandler(
                new MouseOutHandler() {

                    @Override
                    public void onMouseOut(MouseOutEvent event) {
                        if (isDecorationOn)
                            display.getLabel().removeStyleDependentName(
                                    decorationStyleWithMouseOver);
                    }
                });
    }

    public void decorate(Mode mode) {
        assert mode == Mode.ON || mode == Mode.OFF : "unsupported mode " + mode;
        if (mode == Mode.ON)
            decorateOn();
        else
            decorateOff();
    }

    private void decorateOn() {
        isDecorationOn = true;
        display.getLabel().addStyleDependentName(decorationStyle);
    }

    private void decorateOff() {
        isDecorationOn = false;
        display.getLabel().removeStyleDependentName(
                decorationStyleWithMouseOver);
        display.getLabel().removeStyleDependentName(decorationStyle);
    }
}
