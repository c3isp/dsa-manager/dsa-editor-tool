package com.hpe.c3isp.dsa.infrastructure.authoring.client;

import com.google.gwt.i18n.client.Messages;

public interface DsaAuthoringToolMessages extends Messages {

    @DefaultMessage("Update DSA")
    String buttonUpdateDSA();

    @DefaultMessage("Complete DSA")
    String buttonCompleteDSA();

    @DefaultMessage("Enter")
    String buttonEnter();

    @DefaultMessage("Save")
    String buttonSave();

    @DefaultMessage("Save DSA Template")
    String buttonSaveDSATemplate();

    @DefaultMessage("Back")
    String buttonBack();

    @DefaultMessage("OK")
    String buttonOK();

    @DefaultMessage("Cancel")
    String buttonCancel();

    @DefaultMessage("")
    String buttonLogout();

    @DefaultMessage("")
    String buttonCreateDSATemplate();

    @DefaultMessage("")
    String buttonViewDSA();

    @DefaultMessage("")
    String buttonViewDSATemplate();

    @DefaultMessage("")
    String buttonRawDSA();

    @DefaultMessage("")
    String buttonRawDSATemplate();

    @DefaultMessage("")
    String buttonEditDSA();

    @DefaultMessage("")
    String buttonEditDSATemplate();

    @DefaultMessage("")
    String buttonClose();

    @DefaultMessage("UID*")
    String labelUID();

    @DefaultMessage("Vocabulary URI")
    String labelVocabularyURI();

    @DefaultMessage("Data Classification")
    String labelDataClassification();

    @DefaultMessage("Title*")
    String labelTitle();

    @DefaultMessage("Parties*")
    String labelParties();

    @DefaultMessage("Validity*")
    String labelValidity();

    @DefaultMessage("Policies")
    String labelPolicies();

    @DefaultMessage("Type")
    String labelType();

    @DefaultMessage("User Preferences")
    String labelUserPreferences();

    @DefaultMessage("Parties Policies")
    String labelPartiesPolicies();

    @DefaultMessage("Authorisations")
    String labelAuthorisations();

    @DefaultMessage("Data Subject Authorisations")
    String labelDataSubjectAuthorisations();

    @DefaultMessage("Obligations")
    String labelObligations();

    @DefaultMessage("Prohibitions")
    String labelProhibitions();

    @DefaultMessage("Indemnities")
    String labelIndemnities();

    @DefaultMessage("Analytics Result Policies")
    String labelAnalyticsResultPolicies();

    @DefaultMessage("")
    String labelCategoryNonPersonalData();

    @DefaultMessage("")
    String labelCategoryBusinessData();

    @DefaultMessage("")
    String labelCategoryPersonalData();

    @DefaultMessage("")
    String labelCategorySpecialCategory();

    @DefaultMessage("")
    String labelCategorySensitiveData();

    @DefaultMessage("")
    String labelCategoryBusinessDataHighlyConfidential();

    @DefaultMessage("")
    String labelCategoryBusinessDataConfidential();

    @DefaultMessage("")
    String labelCategoryBusinessDataPublic();

    @DefaultMessage("")
    String labelCategorySpecialCategoryAdministrativeData();

    @DefaultMessage("")
    String labelCategorySpecialCategoryIdentificationDetails();

    @DefaultMessage("")
    String labelCategorySpecialCategoryContactDetails();

    @DefaultMessage("")
    String labelCategorySpecialCategoryJuridicalData();

    @DefaultMessage("")
    String labelCategorySensitiveDataMedicalData();

    @DefaultMessage("")
    String labelCategorySensitiveDataSexualData();

    @DefaultMessage("")
    String labelCategorySensitiveRacialEthnicData();

    @DefaultMessage("")
    String labelCategoryReligionOpinionData();

    @DefaultMessage("")
    String labelCategoryPoliticalOpinionOrOtherBeliefsData();

    @DefaultMessage("")
    String labelSize();

    @DefaultMessage("")
    String labelStatus();

    @DefaultMessage("")
    String labelPartyName();

    @DefaultMessage("")
    String labelPartyRole();

    @DefaultMessage("")
    String labelPartyResponsibilities();

    @DefaultMessage("")
    String labelPartyUpdateRole();

    @DefaultMessage("")
    String labelPartyUpdateResponsibilities();

    @DefaultMessage("")
    String labelGlobalPoliciesParties();

    String labelMetadata();

    String labelStartDate();

    String labelEndDate();

    String labelDuration();

    String labelUnit();

    String labelPeriodInDays();

    @DefaultMessage("")
    String messageRoleSelection();

    @DefaultMessage("Error loading version of application.")
    String messageLoadingApplicationVersion();

    @DefaultMessage("DSA Editor")
    String header();

    @DefaultMessage("")
    String messageActionError();

    @DefaultMessage("\n Close this window to continue.")
    String messageClose();

    @DefaultMessage("Purpose*")
    String labelPurpose();

    @DefaultMessage("Application Domain*")
    String labelApplicationDomain();


    @DefaultMessage("Governing Law")
    String labelGoverningLaw();

    @DefaultMessage("N/A")
    String messageNotAvailable();

    @DefaultMessage("")
    String messageBack();

    @DefaultMessage("")
    String messageVocabularyURI();

    @DefaultMessage("")
    String messageVocabularyURIShort();

    @DefaultMessage("")
    String messageVocabularyLoading();

    @DefaultMessage("")
    String messageErrorGettingDSA();

    @DefaultMessage("")
    String messageErrorEditingDSA();

    @DefaultMessage("")
    String messageErrorGettingNewDSA();

    @DefaultMessage("")
    String messageEditDenied();

    @DefaultMessage("")
    String messageCategorySelection();

    @DefaultMessage("")
    String messageFileRemoved();

    @DefaultMessage("")
    String messageDeleteOperationFailed();

    @DefaultMessage("")
    String messageDSASelection();

    @DefaultMessage("")
    String messageDSALoading();

    @DefaultMessage("")
    String messageDSAErrorLoading();

    @DefaultMessage("")
    String messageDSAErrorOpening();

    @DefaultMessage("")
    String messageGenericError();

    @DefaultMessage("")
    String messageOperationCompleted();

    @DefaultMessage("")
    String messageConnectionError();

    @DefaultMessage("")
    String messageLoggedAs();

    @DefaultMessage("")
    String messageSaving();

    @DefaultMessage("")
    String messageErrorSaving();

    @DefaultMessage("")
    String messageContinue();

    @DefaultMessage("")
    String messageDoneAndContinue();

    @DefaultMessage("")
    String messageDSASavedWithWarning();

    @DefaultMessage("")
    String messageDSASavedWithError();

    @DefaultMessage("")
    String messageUpdating();

    @DefaultMessage("")
    String messafeErrorUpdatingDSA();

    @DefaultMessage("")
    String messageInfoMissingConsent();

    @DefaultMessage("")
    String messageInfoMissingUserPreferences();

    @DefaultMessage("")
    String messageFinalizing();

    @DefaultMessage("")
    String messageDSACompletingWithError();

    @DefaultMessage("")
    String messageDSACompleteWithWarning();

    @DefaultMessage("")
    String messageLoadingFromRemoteRepository();

    @DefaultMessage("")
    String messagesLoadingAndRetry();

    @DefaultMessage("")
    String messageErrorEditDSA();

    @DefaultMessage("")
    String CONSENT_STATEMENT();

    @DefaultMessage("")
    String CONSENT_STATEMENT_OBTAINED();

    @DefaultMessage("")
    String CONSENT_STATEMENT_NOT_OBTAINED();

    @DefaultMessage("")
    String ASK_FOR_CONSENT_STATEMENT();

    @DefaultMessage("")
    String CONSENT_WARNING_MESSAGE();

    @DefaultMessage("")
    String CONSENT_INFO_MESSAGE();

    @DefaultMessage("")
    String labelSelectRole();

    @DefaultMessage("")
    String labelDC();

    @DefaultMessage("")
    String labelDP();

    @DefaultMessage("")
    String labelDS();

    @DefaultMessage("")
    String labelPurpose1();

    @DefaultMessage("")
    String labelPurpose2();

    @DefaultMessage("")
    String labelPurpose3();

    @DefaultMessage("")
    String labelPurpose4();

    @DefaultMessage("")
    String labelPurpose5();

    @DefaultMessage("")
    String labelPurpose6();

    @DefaultMessage("")
    String labelPurpose7();

    @DefaultMessage("")
    String labelPurpose8();

    @DefaultMessage("")
    String labelPurpose9();

    String tooltipHelpDownload();

    String labelDescription();

    @DefaultMessage("")
    String labelPolicy1();

    @DefaultMessage("")
    String labelPolicy2();

    @DefaultMessage("")
    String labelPolicy3();

    @DefaultMessage("")
    String labelExpirationPolicy();

    @DefaultMessage("")
    String labelUpdatePolicy();

    @DefaultMessage("")
    String labelRevocationPolicy();

    @DefaultMessage("")
    String tooltipDescription();

    String tooltipTitle();

    String tooltipMetadata();

    String tooltipUUID();

    String tooltipVocabulary();

    String tooltipPurpose();

    String tooltipApplicationDomain();

    String tooltipDataClassification();

    String tooltipGoverningLaw();

    String tooltipIndemnities();

    String tooltipParties();

    String tooltipGlobalPoliciesParties();

    String tooltipExpirationPolicy();

    String tooltipUpdatePolicy();

    String tooltipRevocationPolicy();

    String tooltipValidity();

    String tooltipUpdateButton();

    String tooltipSaveTemplateButton();

    String tooltipCompleteButton();

    String tooltipBackButton();

    String tooltipAuthorizations();

    String tooltipObligations();

    String tooltipProhibitions();

    String tooltipThirdPartiesPolicies();

    String tooltipPeriodInDays();

    String purposeValuesAsString();

    String applicationDomainValuesAsString();

    String dataClassificationLValuesAsString();

//    String availableVocabularyList();
}
