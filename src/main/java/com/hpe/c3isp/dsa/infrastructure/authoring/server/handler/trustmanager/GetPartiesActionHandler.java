/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.trustmanager;

import com.hpe.c3isp.dsa.infrastructure.authoring.shared.Config;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.trustmanager.GetPartiesAction;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.trustmanager.GetOrganizationsResult;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.trustmanager.GetOrganizationsResultImpl;

import net.customware.gwt.dispatch.server.ActionHandler;
import net.customware.gwt.dispatch.server.ExecutionContext;
import net.customware.gwt.dispatch.shared.DispatchException;

public class GetPartiesActionHandler implements
        ActionHandler<GetPartiesAction, GetOrganizationsResult> {

    @Override
    public GetOrganizationsResult execute(GetPartiesAction action,
            ExecutionContext context) throws DispatchException {

        return new GetOrganizationsResultImpl(Config.getInstance().getParties());
    }

    @Override
    public Class<GetPartiesAction> getActionType() {
        return GetPartiesAction.class;
    }

    @Override
    public void rollback(GetPartiesAction action, GetOrganizationsResult result, ExecutionContext context) throws DispatchException {
        throw new UnsupportedOperationException();
    }

}
