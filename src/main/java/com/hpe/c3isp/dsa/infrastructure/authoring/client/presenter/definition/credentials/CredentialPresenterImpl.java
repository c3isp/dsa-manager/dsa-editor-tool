/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.credentials;

import java.util.List;

import net.customware.gwt.dispatch.client.DispatchAsync;
import net.customware.gwt.dispatch.shared.Action;

import org.enunes.gwt.mvp.client.EventBus;

import com.google.inject.Inject;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.credentials.CredentialPresenter.CredentialDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.parties.AbstractOrganizationsPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Term;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.trustmanager.GetContextProvidersAction;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.trustmanager.GetOrganizationsResult;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Organization;

public class CredentialPresenterImpl extends
        AbstractOrganizationsPresenter<CredentialDisplay> implements
        CredentialPresenter {

    private Term credentialTerm;

    @Inject
    public CredentialPresenterImpl(EventBus eventBus,
            CredentialDisplay display, DispatchAsync dispatcher) {
        super(eventBus, display, dispatcher);
    }

    @Override
    public Term getCredential() {
        return credentialTerm;
    }

    @Override
    public List<Organization> getTrustedContextProviders() {
        return chosenOrganizations;
    }

    @Override
    public void set(Term credential) {
        this.credentialTerm = credential;
        display.setTerm(credential.getHumanForm());
    }

    @Override
    public void set(Term credential, List<Organization> trustedContextProviders) {
        set(credential);
        setChosenOrganizations(trustedContextProviders, null, null, null);
    }

    @Override
    protected Action<GetOrganizationsResult> getLoadOrganizationsAction() {
        return new GetContextProvidersAction();
    }

}
