/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa;

import java.util.Date;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Validity implements IsSerializable {

    private Date startDate;
    private Date endDate;
    private int offlineLicencesDurationInDays;

    // for serialization
    public Validity() {
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getOfflineLicencesDurationInDays() {
        return offlineLicencesDurationInDays;
    }

    @Override
    public String toString() {
        return "Start Date=" + startDate + ", End Date=" + endDate
                + ", Offline Licences Duration In Days="
                + offlineLicencesDurationInDays;
    }

    public void setOfflineLicencesDurationInDays(
            int offlineLicencesDurationInDays) {
        this.offlineLicencesDurationInDays = offlineLicencesDurationInDays;
    }


}
