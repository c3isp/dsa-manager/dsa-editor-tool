/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.thirdpartiesauthorizations;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.Mode;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.AbstractSspStateNormal;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.ThirdPartiesAuthorizationsSetDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.StatementMetadata;

public class ThirdPartiesAspStateNormal
        extends
        AbstractSspStateNormal<ThirdPartiesAspContext, ThirdPartiesAuthorizationsSetDisplay, ThirdPartiesAspState>
        implements ThirdPartiesAspState {

    @Override
    protected void makeTransitionToStateMoving(ThirdPartiesAspContext context,
            int rowIndex) {
        context.setState(new ThirdPartiesAspStateMoving(rowIndex));
    }

    @Override
    public void handleLinkFromSourceStatementEvent(
            ThirdPartiesAspContext context, StatementMetadata source) {
        // if the source is already linked to a statement, then we highlight the
        // linked statement
        if (!"".equals(source.getReferenceUid())) {
            int targetIndex = context.getIndexOfStatement(source
                    .getReferenceUid());
            assert targetIndex != -1;
            context.getDisplay().highlightStatement(targetIndex, Mode.ON);
        }
        context.getDisplay().showStatementLinkTools();
        context.setState(new ThirdPartiesAspStateLinking(source));
    }

    @Override
    public void handleSelectedRowEvent(ThirdPartiesAspContext context, int row) {
        throw new UnsupportedOperationException();
    }

}
