/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa.helper.combinatorics.Combi;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Functor;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Tokens;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableDeclaration;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.DsaBean;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.DsaBeanUtils;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Util;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.Statement;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.util.InputValueHandler;

public class SimpleOntologyAwareCNL4DSABuilder extends SimpleCNL4DSABuilder {

	/**
	 * inferredVariableDeclarations:
	 *
	 * <li>keys are the variable (?X_n) used in the statements of a
	 * {@link DsaBean}</li>
	 *
	 * <li>the value corresponding to a key is a list of
	 * {@link VariableDeclaration} generated from vocabulary Terms that are
	 * sub-classes of the one represented by the key</li>
	 *
	 */
	private final Map<String, List<VariableDeclaration>> inferredVariableDeclarations = new HashMap<String, List<VariableDeclaration>>();

	/**
	 * AbstractOrganizationsPresenter list of explicit
	 * {@link VariableDeclaration}, i.e. those that are explicitly declared in a
	 * {@link DsaBean}
	 */
	private final List<VariableDeclaration> explicitVariableDeclarations = new ArrayList<VariableDeclaration>();

	/**
	 * inferredActions:
	 *
	 * <li>keys are URIs of vocabulary Actions</li>
	 *
	 * <li>the value corresponding to a key is a list of {@link Functor}
	 * generated from vocabulary Actions that are sub-classes of the one
	 * represented by the key</li>
	 */
	private final Map<String, List<Functor>> inferredActions = new HashMap<String, List<Functor>>();

	public SimpleOntologyAwareCNL4DSABuilder(DsaBean dsaBean) {
		super(dsaBean);
		computeSubsumptions(dsaBean);
	}




//marco
	/*
	 * computes inferredVariableDeclarations and inferredActions
	 */
	private void computeSubsumptions(DsaBean dsaBean) {
		long index = DsaBeanUtils.getMaxUidValue(dsaBean);
		int statmentCount = 0;
		for (Statement stmt : DsaBeanUtils.getAllStatements(dsaBean)) {
			statmentCount++;
			//  String statementInfo = stmt.getStatementMetadata().getStatementInfo();
			String inputValue = stmt.getStatementMetadata().getInputValue();
			System.out.println("-----> statmentCount = "+ statmentCount + "; inputValue="+inputValue);

			//mr: inputValue may actually contain more than one value, in the form
			//"?X_29:Identifier=iddd|?X_30:Identifier=iddd2|?X_31:Identifier=iddd3"
			//following tranform in a map
			//?X_29:Identifier---->iddd , ?X_30:Identifier---->iddd2 , ?X_31:Identifier---->iddd3
			Map<String, String> mapInputValue = InputValueHandler.stringToMap(inputValue);


			int syntacticItemCount = 0;
			for (SyntacticItem si : stmt.getSyntacticItems()) {
				syntacticItemCount++;

				System.out.println("    -----> syntacticItemCount = "+ syntacticItemCount);

				if (si instanceof VariableDeclaration) {
					VariableDeclaration vd = (VariableDeclaration) si;
					System.out.println("        -----> VariableDeclaration found: "+ vd);
					//VariableDeclaration [syntacticForm=?X_3:Identifier, variable=?X_3, humanForm=a Identifier, uid=http://localhost:7080/vocabularies/c3isp_vocabulary_3.3.owl#Identifier, value=id1|?X_4:Identifier=id2]


//					String value ="";
//					if (inputValue != null && !inputValue.isEmpty()){
////						System.out.println("inputValue="+inputValue + " vd="+vd);
//						if (inputValue.indexOf("?")!=-1){
//							//example X_3:Area=1234
//							String id = inputValue.substring(0,inputValue.indexOf(":"));
//							if (vd.getVariable().contains(id)){
//								value = inputValue.substring(inputValue.indexOf("=")+1);
//							}
//						}
//						vd.setValue(value);
//					}

					String value = InputValueHandler.findRightInputValue(vd, mapInputValue);
					vd.setValue(value);






					//questo fa si che la parte del XML "UserText" contanga gia' al proprio interno il replace delle
					//variabili con l'ID selezionato. Purtroppo la stringa in "UserText" non viene mai utilizzat in lettura
					//quindi e' pressoche' inutile. In particolare, per costruire la stringa visualizzata a video
					//in "View DSA" non si utlizza "UserText" m si cstruisce una nuova stringa da zero a partire dalle varibili dello
					//statement
					String newHumanForm = null;
					//modify human form adding the found value
					if(!Util.isEmpty(value)) {
						newHumanForm = vd.getHumanForm();
						if(!Util.isEmpty(newHumanForm)) {
							vd.setHumanForm(newHumanForm+"("+value+")");
						}
					}
					//////////////////////////////




					explicitVariableDeclarations.add(vd);
					String var = vd.getVariable();
					if (!inferredVariableDeclarations.containsKey(var)) {
						// compute all sub-classes dropping intermediate classes
						Set<String> subClassUris = vocont
								.dropSuperClasses(vocont.getAllSubClassesOf(vd
										.getUid()));
						List<VariableDeclaration> ivds = new ArrayList<VariableDeclaration>();
						for (String subClassUri : subClassUris) {
							String humanForm = vocont
									.getHumanFormOf(subClassUri);
							String variable = "?Y_" + index;
							index++;
							ivds.add(new VariableDeclaration(subClassUri,
									humanForm, variable, ""));
						}
						inferredVariableDeclarations.put(var, ivds);
					}
				} else if (si instanceof Functor) {
					System.out.println("        -----> Functor found: ");

					String uid = si.getUid();
					if (vocont.isAction(uid)
							&& !inferredActions.containsKey(uid)) {
						// compute all sub-classes dropping intermediate classes
						Set<String> subClassesUri = vocont
								.dropSuperClasses(vocont
										.getAllSubClassesOf(uid));
						List<Functor> actions = new ArrayList<Functor>();
						for (String subClassUri : subClassesUri) {
							String humanForm = vocont
									.getHumanFormOf(subClassUri);
							actions.add(new Functor(subClassUri, humanForm));
						}
						inferredActions.put(uid, actions);
					}
				}
			}
		}
	}












//	/*
//	 * computes inferredVariableDeclarations and inferredActions
//	 */
//	private void computeSubsumptions(DsaBean dsaBean) {
//		long index = DsaBeanUtils.getMaxUidValue(dsaBean);
//		for (Statement stmt : DsaBeanUtils.getAllStatements(dsaBean)) {
//			for (SyntacticItem si : stmt.getSyntacticItems()) {
//				//  String statementInfo = stmt.getStatementMetadata().getStatementInfo();
//				String inputValue = stmt.getStatementMetadata().getInputValue();
//				if (si instanceof VariableDeclaration) {
//					VariableDeclaration vd = (VariableDeclaration) si;
//
//					String value ="";
//					if (inputValue != null && !inputValue.isEmpty()){
//						System.out.println("inputValue="+inputValue + " vd="+vd);
//						if (inputValue.indexOf("?")!=-1){
//							//example X_3:Area=1234
//							String id = inputValue.substring(0,inputValue.indexOf(":"));
//							if (vd.getVariable().contains(id)){
//								value = inputValue.substring(inputValue.indexOf("=")+1);
//							}
//						}
//						vd.setValue(value);
//					}
//					explicitVariableDeclarations.add(vd);
//					String var = vd.getVariable();
//					if (!inferredVariableDeclarations.containsKey(var)) {
//						// compute all sub-classes dropping intermediate classes
//						Set<String> subClassUris = vocont
//								.dropSuperClasses(vocont.getAllSubClassesOf(vd
//										.getUid()));
//						List<VariableDeclaration> ivds = new ArrayList<VariableDeclaration>();
//						for (String subClassUri : subClassUris) {
//							String humanForm = vocont
//									.getHumanFormOf(subClassUri);
//							String variable = "?Y_" + index;
//							index++;
//							ivds.add(new VariableDeclaration(subClassUri,
//									humanForm, variable, ""));
//						}
//						inferredVariableDeclarations.put(var, ivds);
//					}
//				} else if (si instanceof Functor) {
//					String uid = si.getUid();
//					if (vocont.isAction(uid)
//							&& !inferredActions.containsKey(uid)) {
//						// compute all sub-classes dropping intermediate classes
//						Set<String> subClassesUri = vocont
//								.dropSuperClasses(vocont
//										.getAllSubClassesOf(uid));
//						List<Functor> actions = new ArrayList<Functor>();
//						for (String subClassUri : subClassesUri) {
//							String humanForm = vocont
//									.getHumanFormOf(subClassUri);
//							actions.add(new Functor(subClassUri, humanForm));
//						}
//						inferredActions.put(uid, actions);
//					}
//				}
//			}
//		}
//	}

	@Override
	protected String consumePredicate(String predicate, String arg1, String arg2) {
		StringBuffer buffer = new StringBuffer();
		String[][] data = new String[][] { getInferredPredicates(predicate),
			getInferredVariables(arg1), getInferredVariables(arg2) };
			Combi<String> combi = new Combi<String>(data);
			assert combi.hasNext() : "cannot infer anything from "
			+ toPredicate(predicate, arg1, arg2);
			buffer.append(toPredicate(combi.next()));
			boolean many = combi.hasNext();
			while (combi.hasNext())
				buffer.append(" OR ").append(toPredicate(combi.next()));
			return (many) ? " ( " + buffer.toString() + " ) " : buffer.toString()
					+ " ";
	}

	private String toPredicate(String[] ps) {
		assert ps.length == 3 : "cannot build a CNL4DSA predicate from "
				+ "these elements: " + Arrays.toString(ps);
		return toPredicate(ps[0], ps[1], ps[2]);
	}

	private String toPredicate(String predicate, String arg1, String arg2) {
		return " " + predicate + "(" + arg1 + "," + arg2 + ") ";
	}

	private String[] getInferredVariables(String var) {
		assert inferredVariableDeclarations.containsKey(var) : "no inference on var "
				+ var;
		String[] ivs = null;
		List<VariableDeclaration> ivds = inferredVariableDeclarations.get(var);
		if (ivds.size() > 0) {
			ivs = new String[ivds.size()];
			for (int i = 0; i < ivds.size(); i++)
				ivs[i] = ivds.get(i).getVariable();
		} else {
			// if var has no sub-classes then we return var alone
			ivs = new String[] { var };
		}
		return ivs;
	}

	private String[] getInferredPredicates(String predicate) {
		// Predicates are sub-properties of the OWL ObjectProperty
		// "presentableObjectProperty", and they have no sub-properties,
		// so we don't look for a hierarchy here. This method is kind of a
		// placeholder for future development, where we might have
		// hierarchies of predicates.
		return new String[] { predicate };
	}

	@Override
	protected String consumeAtomicFragment(String token, String subject,
			String action, String object) {
		StringBuffer buffer = new StringBuffer();
		boolean many = false;
		if (token.equals(Tokens.TOKEN_AFTER.getSyntacticForm())) {
			// we do not expand an atomic fragment following an AFTER token,
			// because we should use the PARALLEL operator that yields a
			// composite fragment, which is not allowed after an AFTER
			buffer.append(token).append(
					toAtomicFragment(subject, action, object));
		} else {
			assert token.equals(Tokens.TOKEN_CAN.getSyntacticForm())
			|| token.equals(Tokens.TOKEN_MUST.getSyntacticForm())
			|| token.equals(Tokens.TOKEN_CANNOT.getSyntacticForm()) : "unexpected token"
			+ token
			+ " for atomic fragment "
			+ toAtomicFragment(subject, action, object);
			// here we transform the atomic fragment following CAN or MUST or
			// CANNOT into a composite fragment using the PARALLEL operator
			String[][] data = new String[][] { getInferredVariables(subject),
				getInferredActions(action), getInferredVariables(object) };
				Combi<String> combi = new Combi<String>(data);
				assert combi.hasNext() : "cannot infer anything from "
				+ toAtomicFragment(subject, action, object);
				buffer.append(token).append(toAtomicFragment(combi.next()));
				many = combi.hasNext();
				while (combi.hasNext())
					buffer.append(" ; ").append(token)
					.append(toAtomicFragment(combi.next()));

		}
		return (many) ? " { " + buffer.toString() + " } " : " "
		+ buffer.toString() + " ";
	}

	private String[] getInferredActions(String action) {
		String actionUri = vocont.getFullUriOfFragment(action);
		assert inferredActions.containsKey(actionUri) : "no inference on action "
		+ action;
		String[] ias = null;
		List<Functor> actions = inferredActions.get(actionUri);
		if (actions.size() > 0) {
			ias = new String[actions.size()];
			for (int i = 0; i < actions.size(); i++)
				ias[i] = actions.get(i).getSyntacticForm();
		} else {
			// if action has no sub-classes that we return action alone
			ias = new String[] { action };
		}
		return ias;
	}

	private String toAtomicFragment(String[] ps) {
		assert ps.length == 3 : "cannot build an atomic fragment from "
				+ Arrays.toString(ps);
		return toAtomicFragment(ps[0], ps[1], ps[2]);
	}

	private String toAtomicFragment(String subject, String action, String object) {
		return " [" + subject + ", " + action + ", " + object + "] ";
	}

	private List<VariableDeclaration> getAllVariableDeclarations() {
		List<VariableDeclaration> vds = new ArrayList<VariableDeclaration>();
		vds.addAll(explicitVariableDeclarations);
		for (String var : inferredVariableDeclarations.keySet())
			vds.addAll(inferredVariableDeclarations.get(var));
		return Collections.unmodifiableList(vds);
	}

	public Map<String,String> buildAllVariableDeclarationStatements() {
		Map<String,String> vdStatements = new HashMap<>();
		for (VariableDeclaration vd : getAllVariableDeclarations()){
			vdStatements.put(vd.getVariable() + " is-a <" + vd.getUid() + ">", vd.getValue());
		}

		return vdStatements;
	}
}
