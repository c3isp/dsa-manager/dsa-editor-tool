/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.fsm;

import java.util.ArrayList;
import java.util.List;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.model.VocabularyChoicesUpdater;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Term;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Token;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Tokens;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableReference;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.vocabulary.GetPredicatesWithTermInDomain;

public class CnlPrompterState07 implements CnlPrompterState {

    private final List<Token> tokens;
    private final Term term;
    private final VocabularyChoicesUpdater vocabularyChoicesUpdater;

    public CnlPrompterState07(SyntacticItem previous) {
        assert previous != null;
        assert previous instanceof Term
                || previous instanceof VariableReference : "was expecting a Term or a VariableReference, got a "
                + previous.toString();
        term = new Term(previous.getUid(), previous.getHumanForm(), previous.getValue());
        tokens = new ArrayList<Token>();
        tokens.add(Tokens.TOKEN_NOT);
        vocabularyChoicesUpdater = new VocabularyChoicesUpdater(tokens,
                new GetPredicatesWithTermInDomain(term.getUid()));
    }

    @Override
    public void handleReference(CnlPrompterContext context,
            SyntacticItem syntacticItem) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleSelection(CnlPrompterContext context,
            SyntacticItem selectedItem) {
        if (selectedItem.equals(Tokens.TOKEN_NOT))
            context.update(new CnlPrompterState14(term), selectedItem);
        else
            context.update(new CnlPrompterState08(selectedItem), selectedItem);
    }

    @Override
    public VocabularyChoicesUpdater getVocabularyChoicesUpdater() {
        return vocabularyChoicesUpdater;
    }

}
