/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.view.syntactic;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.Mode;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.syntactic.vd.StatefulVDPresenter.VariableDeclarationDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.helper.SyntacticItemDisplayDecorator;

public class VariableDeclarationDisplayImpl extends SyntacticItemDisplayImpl
        implements VariableDeclarationDisplay {

    private final SyntacticItemDisplayDecorator<VariableDeclarationDisplay> decoratorForShow;
    private final SyntacticItemDisplayDecorator<VariableDeclarationDisplay> decoratorForHighlight;
    private final SyntacticItemDisplayDecorator<VariableDeclarationDisplay> decoratorForHighlightClicked;
    private final SyntacticItemDisplayDecorator<VariableDeclarationDisplay> decoratorForHighlightForSelection;

    public VariableDeclarationDisplayImpl() {
        decoratorForShow = new SyntacticItemDisplayDecorator<VariableDeclarationDisplay>(
                this, "VariableDeclarationBorder",
                "VariableDeclarationBorderAndMouseOver");
        decoratorForHighlight = new SyntacticItemDisplayDecorator<VariableDeclarationDisplay>(
                this, "VariableDeclarationHighlighted", "");
        decoratorForHighlightClicked = new SyntacticItemDisplayDecorator<VariableDeclarationDisplay>(
                this, "VariableDeclarationHighlightedClicked", "");
        decoratorForHighlightForSelection = new SyntacticItemDisplayDecorator<VariableDeclarationDisplay>(
                this, "VariableDeclarationOn",
                "VariableDeclarationOnAndMouseOver");
    }

    @Override
    public void highlightReferenceable(Mode mode) {
        decoratorForHighlightForSelection.decorate(mode);
    }

    @Override
    public void highlightClickedReference(Mode mode) {
        decoratorForHighlightClicked.decorate(mode);
    }

    @Override
    public void highlightReference(Mode mode) {
        decoratorForHighlight.decorate(mode);
    }

    @Override
    public void highlightShowable(Mode mode) {
        decoratorForShow.decorate(mode);
    }

}
