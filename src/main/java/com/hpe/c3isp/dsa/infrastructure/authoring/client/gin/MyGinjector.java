/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.gin;

import net.customware.gwt.dispatch.client.DispatchAsync;
import net.customware.gwt.dispatch.client.gin.StandardDispatchModule;

import org.enunes.gwt.mvp.client.EventBus;

import com.google.gwt.inject.client.GinModules;
import com.google.gwt.inject.client.Ginjector;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.crossref.CrossReferencePresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.credentials.CredentialsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.roles.RolesSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.parties.PartiesPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.authorizations.AuthorizationsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.datasubjectauthorizations.DataSubjectAuthorizationsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.obligations.ObligationsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.prohibitions.ProhibitionsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.thirdpartiesauthorizations.ThirdPartiesAuthorizationsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.thirdpartiesobligations.ThirdPartiesObligationsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.thirdpartiesprohibitions.ThirdPartiesProhibitionsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.validity.ValidityPresenter;

/**
 * Ginjector definition. It includes:
 * 
 * <ul>
 * <li>the Gin module for this application components</li>
 * <li>the {@link StandardDispatchModule} of gwt-dispatch for provinding
 * instances of {@link DispatchAsync}</li>
 * </ul>
 * 
 * @author Marco Luca Sbodio (mailto:marco.sbodio@hp.com)
 * 
 */
@GinModules({ MyGinModule.class, StandardDispatchModule.class })
public interface MyGinjector extends Ginjector {

    EventBus getEventBus();

    DispatchAsync getDispatchAsync();

    AuthorizationsSetPresenter getAuthorizationsPresenter();

    DataSubjectAuthorizationsSetPresenter getDataSubjectAuthorizationsPresenter();

    ObligationsSetPresenter getObligationsPresenter();

    ProhibitionsSetPresenter getProhibitionsSetPresenter();

    ThirdPartiesAuthorizationsSetPresenter getThirdPartiesAuthorizationsPresenter();

    ThirdPartiesObligationsSetPresenter getThirdPartiesObligationsPresenter();

    ThirdPartiesProhibitionsSetPresenter getThirdPartiesProhibitionsPresenter();

    CrossReferencePresenter getCrossReferencePresenter();

    ValidityPresenter getValidityPresenter();

    PartiesPresenter getPartiesPresenter();

    CredentialsSetPresenter getCredentialsSetPresenter();

    RolesSetPresenter getRolesSetPresenter();
}
