/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.gin;

import com.google.gwt.inject.client.AbstractGinModule;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.crossref.CrossReferencePresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.crossref.CrossReferencePresenterImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.crossref.CrossReferencePresenter.CrossReferenceDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.credentials.CredentialPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.credentials.CredentialPresenterImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.credentials.CredentialsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.credentials.CredentialsSetPresenterImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.credentials.CredentialPresenter.CredentialDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.credentials.CredentialsSetPresenter.CredentialsSetDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.roles.RolePresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.roles.RolePresenterImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.roles.RolesSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.roles.RolesSetPresenterImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.roles.RolePresenter.RoleDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.roles.RolesSetPresenter.RolesSetDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.parties.PartiesPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.parties.PartiesPresenterImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.parties.PartiesPresenter.PartiesDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.StatementPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.StatementPresenterImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.StatementPresenter.StatementDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.authorizations.AuthorizationsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.authorizations.AuthorizationsSetPresenterImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.datasubjectauthorizations.DataSubjectAuthorizationsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.datasubjectauthorizations.DataSubjectAuthorizationsSetPresenterImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.obligations.ObligationsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.obligations.ObligationsSetPresenterImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.prohibitions.ProhibitionsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.prohibitions.ProhibitionsSetPresenterImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.thirdpartiesauthorizations.ThirdPartiesAuthorizationsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.thirdpartiesauthorizations.ThirdPartiesAuthorizationsSetPresenterImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.thirdpartiesobligations.ThirdPartiesObligationsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.thirdpartiesobligations.ThirdPartiesObligationsSetPresenterImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.thirdpartiesprohibitions.ThirdPartiesProhibitionsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.thirdpartiesprohibitions.ThirdPartiesProhibitionsSetPresenterImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.syntactic.FunctorPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.syntactic.FunctorPresenterImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.syntactic.TokenPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.syntactic.TokenPresenterImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.syntactic.vd.StatefulVDPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.syntactic.vd.StatefulVDPresenterImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.syntactic.vd.StatefulVDPresenter.VariableDeclarationDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.syntactic.vr.StatefulVRPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.syntactic.vr.StatefulVRPresenterImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.syntactic.vr.StatefulVRPresenter.VariableReferenceDisplay;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.validity.ValidityPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.validity.ValidityPresenterImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.validity.ValidityPresenter.ValidityDisplay;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.VocabularyPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.VocabularyPresenterImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.VocabularyPresenter.VocabularyDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.crossref.CrossReferenceDisplayImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.definitions.credentials.CredentialDisplayImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.definitions.credentials.CredentialsSetDisplayImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.definitions.roles.RoleDisplayImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.definitions.roles.RolesSetDisplayImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.parties.PartiesDisplayImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.StatementDisplayImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.AuthorizationsSetDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.AuthorizationsSetDisplayImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.DataSubjectAuthorizationsSetDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.DataSubjectAuthorizationsSetDisplayImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.ObligationsSetDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.ObligationsSetDisplayImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.ProhibitionsSetDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.ProhibitionsSetDisplayImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.ThirdPartiesAuthorizationsSetDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.ThirdPartiesAuthorizationsSetDisplayImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.ThirdPartiesObligationsSetDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.ThirdPartiesObligationsSetDisplayImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.ThirdPartiesProhibitionsSetDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.ThirdPartiesProhibitionsSetDisplayImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.syntactic.SyntacticItemDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.syntactic.SyntacticItemDisplayImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.syntactic.VariableDeclarationDisplayImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.syntactic.VariableReferenceDisplayImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.validity.ValidityDisplayImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.vocabulary.VocabularyDisplayImpl;

public class MyGinModule extends AbstractGinModule {

    @Override
    protected void configure() {
        install(new org.enunes.gwt.mvp.client.gin.Module());

        bind(StatefulVDPresenter.class).to(StatefulVDPresenterImpl.class);
        bind(VariableDeclarationDisplay.class).to(VariableDeclarationDisplayImpl.class);

        bind(StatefulVRPresenter.class).to(StatefulVRPresenterImpl.class);
        bind(VariableReferenceDisplay.class).to( VariableReferenceDisplayImpl.class);

        bind(FunctorPresenter.class).to(FunctorPresenterImpl.class);

        bind(TokenPresenter.class).to(TokenPresenterImpl.class);

        bind(SyntacticItemDisplay.class).to(SyntacticItemDisplayImpl.class);

        bind(StatementPresenter.class).to(StatementPresenterImpl.class);
        bind(StatementDisplay.class).to(StatementDisplayImpl.class);

        bind(AuthorizationsSetPresenter.class).to(
                AuthorizationsSetPresenterImpl.class);
        bind(AuthorizationsSetDisplay.class).to(
                AuthorizationsSetDisplayImpl.class);

        bind(DataSubjectAuthorizationsSetPresenter.class).to(
                DataSubjectAuthorizationsSetPresenterImpl.class);
        bind(DataSubjectAuthorizationsSetDisplay.class).to(
                DataSubjectAuthorizationsSetDisplayImpl.class);

        bind(ObligationsSetPresenter.class).to(
                ObligationsSetPresenterImpl.class);
        bind(ObligationsSetDisplay.class).to(ObligationsSetDisplayImpl.class);

        bind(ProhibitionsSetPresenter.class).to(
                ProhibitionsSetPresenterImpl.class);
        bind(ProhibitionsSetDisplay.class).to(ProhibitionsSetDisplayImpl.class);

        bind(ThirdPartiesAuthorizationsSetPresenter.class).to(
                ThirdPartiesAuthorizationsSetPresenterImpl.class);
        bind(ThirdPartiesAuthorizationsSetDisplay.class).to(
                ThirdPartiesAuthorizationsSetDisplayImpl.class);

        bind(ThirdPartiesObligationsSetPresenter.class).to(
                ThirdPartiesObligationsSetPresenterImpl.class);
        bind(ThirdPartiesObligationsSetDisplay.class).to(
                ThirdPartiesObligationsSetDisplayImpl.class);

        bind(ThirdPartiesProhibitionsSetPresenter.class).to(
                ThirdPartiesProhibitionsSetPresenterImpl.class);
        bind(ThirdPartiesProhibitionsSetDisplay.class).to(
                ThirdPartiesProhibitionsSetDisplayImpl.class);

        bind(VocabularyPresenter.class).to(VocabularyPresenterImpl.class);
        bind(VocabularyDisplay.class).to(VocabularyDisplayImpl.class);

        bind(CrossReferencePresenter.class).to(
                CrossReferencePresenterImpl.class);
        bind(CrossReferenceDisplay.class).to(CrossReferenceDisplayImpl.class);

        bind(ValidityPresenter.class).to(ValidityPresenterImpl.class);
        bind(ValidityDisplay.class).to(ValidityDisplayImpl.class);

//        bind(ValidityPresenter2.class).to(ValidityPresenterImpl2.class);
//        bind(ValidityDisplay2.class).to(ValidityDisplayImpl2.class);

        bind(PartiesPresenter.class).to(PartiesPresenterImpl.class);
        bind(PartiesDisplay.class).to(PartiesDisplayImpl.class);

        bind(CredentialPresenter.class).to(CredentialPresenterImpl.class);
        bind(CredentialDisplay.class).to(CredentialDisplayImpl.class);

        bind(CredentialsSetPresenter.class).to(
                CredentialsSetPresenterImpl.class);
        bind(CredentialsSetDisplay.class).to(CredentialsSetDisplayImpl.class);

        bind(RolePresenter.class).to(RolePresenterImpl.class);
        bind(RoleDisplay.class).to(RoleDisplayImpl.class);

        bind(RolesSetPresenter.class).to(RolesSetPresenterImpl.class);
        bind(RolesSetDisplay.class).to(RolesSetDisplayImpl.class);
    }

}
