/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.roles;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Term;

public class Roles implements IsSerializable {

    private Map<Term, List<CredentialExpression>> rolesMap = new HashMap<Term, List<CredentialExpression>>();

    // for serialization
    public Roles() {
    }

    public void setRoleDefinition(Term role,
            List<CredentialExpression> definition) {
        rolesMap.put(role, definition);
    }

    public Set<Term> getRoles() {
        return rolesMap.keySet();
    }

    public List<CredentialExpression> getRoleDefinition(Term role) {
        return rolesMap.get(role);
    }

}
