/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.parties;

import java.util.ArrayList;
import java.util.List;

import net.customware.gwt.dispatch.client.DispatchAsync;
import net.customware.gwt.dispatch.shared.Action;

import org.enunes.gwt.mvp.client.EventBus;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.presenter.BusyDisplayEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.presenter.IdleDisplayEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.AbstractControllablePresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.trustmanager.GetOrganizationsResult;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Organization;

public abstract class AbstractOrganizationsPresenter<D extends OrganizationsDisplay>
		extends AbstractControllablePresenter<D> {

	protected final List<Organization> chosenOrganizations = new ArrayList<Organization>();

	protected List<Organization> allOrganizations = new ArrayList<Organization>();

	protected final DispatchAsync dispatcher;

	protected String userRole;

	public AbstractOrganizationsPresenter(EventBus eventBus, D display, final DispatchAsync dispatcher) {
		super(eventBus, display);
		this.dispatcher = dispatcher;
		loadOrganizationsList();
	}

	@Override
	public void bind() {
		super.bind();
		registerHandler(display.getChangeOrganizationsPanel().addOpenHandler(new OpenHandler<DisclosurePanel>() {

			@Override
			public void onOpen(OpenEvent<DisclosurePanel> event) {
				loadOrganizationsList();
			}
		}));
		registerHandler(display.getOkButton().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				updateChosenOrganizations(null, null, null);
			}
		}));
	}

	protected void updateChosenOrganizations(String name, String role, String responsibilities) {
		int[] selected = display.getSelectedOrganizations();
		chosenOrganizations.clear();
		for (int i = 0; i < selected.length; i++)
			chosenOrganizations.add(allOrganizations.get(selected[i]));
		display.hideAvailableOrganizationNames();
		display.setChosenOrganization(chosenOrganizations, userRole, getAllOrganizations(), name, role,
				responsibilities);
		eventBus.fireEvent(new IdleDisplayEvent(display));
	}

	protected void loadOrganizationsList() {
		eventBus.fireEvent(new BusyDisplayEvent(display));
		display.clearChosenOrganizationNames();
		display.loadingOrganizations();
		dispatcher.execute(getLoadOrganizationsAction(), new AsyncCallback<GetOrganizationsResult>() {

			@Override
			public void onFailure(Throwable caught) {
				display.failure(caught.getMessage());
			}

			@Override
			public void onSuccess(GetOrganizationsResult result) {
				allOrganizations = result.getOrganizations();
				display.showAvailableOrganizationNames(getOrganizationsNames(allOrganizations));
			}
		});
	}

	protected abstract Action<GetOrganizationsResult> getLoadOrganizationsAction();

	protected void setChosenOrganizations(List<Organization> organizations, String name, String role,
			String responsibilities) {
		this.chosenOrganizations.clear();
		this.chosenOrganizations.addAll(organizations);
			//display.setChosenOrganizationName(getOrganizationsNames(organizations));
		display.setChosenOrganization(organizations, userRole, getAllOrganizations(), name, role, responsibilities);
	}

	protected List<String> getOrganizationsNames(List<Organization> orgs) {
		List<String> names = new ArrayList<String>(orgs.size());
		for (Organization organization : orgs)
			names.add(organization.getName());
		return names;
	}

	protected List<Organization> getAllOrganizations() {
		dispatcher.execute(getLoadOrganizationsAction(), new AsyncCallback<GetOrganizationsResult>() {

			@Override
			public void onFailure(Throwable caught) {
				display.failure(caught.getMessage());
			}

			@Override
			public void onSuccess(GetOrganizationsResult result) {
				allOrganizations = result.getOrganizations();

			}
		});
		return allOrganizations;
	}

	protected void setUserRole(String userRole) {
		this.userRole = userRole;

	}

}
