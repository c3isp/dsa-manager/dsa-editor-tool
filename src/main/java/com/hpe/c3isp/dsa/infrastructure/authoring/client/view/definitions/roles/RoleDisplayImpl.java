/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.view.definitions.roles;

import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.HasOpenHandlers;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.roles.RolePresenter.RoleDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.widget.RoleDefinitionEditor;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.roles.Operator;

public class RoleDisplayImpl extends Composite implements RoleDisplay {

    private final InlineLabel roleLabel = new InlineLabel("");
    private final InlineLabel sentenceLabel = new InlineLabel(
            "has no definition");
    private final InlineLabel definitionLabel = new InlineLabel("");
    private final DisclosurePanel dpChangeDefinition = new DisclosurePanel(
            "Change");
    private final RoleDefinitionEditor roleDefinitionEditor = new RoleDefinitionEditor();
    private final Button okButton = new Button("OK");

    @Inject
    public RoleDisplayImpl() {
        roleLabel.setStylePrimaryName("syntacticItemLabel");
        sentenceLabel.setStylePrimaryName("syntacticItemLabel");

        final HorizontalPanel hp = new HorizontalPanel();
        initWidget(hp);

        final FlowPanel fp = new FlowPanel();
        fp.add(roleLabel);
        fp.add(sentenceLabel);
        fp.add(definitionLabel);

        hp.add(fp);
        hp.add(dpChangeDefinition);

        final VerticalPanel dpvp = new VerticalPanel();
        dpvp.add(roleDefinitionEditor);
        dpvp.add(okButton);
        dpvp.add(new Label("WARNING: attribute expressions are evaluated at "
                + "enforcement level."));
        dpChangeDefinition.setAnimationEnabled(true);
        dpChangeDefinition.setContent(dpvp);

        dpChangeDefinition.addOpenHandler(new OpenHandler<DisclosurePanel>() {

            @Override
            public void onOpen(OpenEvent<DisclosurePanel> event) {
                definitionLabel.setText("");
                if (roleDefinitionEditor.isEmpty())
                    roleDefinitionEditor.addEmptyCredentialExpression();
            }
        });

        okButton.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                updateDefinitionLabel();
                dpChangeDefinition.setOpen(false);
            }
        });
    }

    @Override
    public void activate() {
        dpChangeDefinition.setVisible(true);
    }

    @Override
    public void deactivate() {
        dpChangeDefinition.setVisible(false);
    }

    @Override
    public Widget asWidget() {
        return this;
    }

    @Override
    public void setRole(String humanForm) {
        roleLabel.setText(humanForm);
    }

    @Override
    public void setCredentials(List<String> allCredentials) {
        roleDefinitionEditor.setCredentials(allCredentials);
    }

    @Override
    public void addCredentialExpression(int credentialIndex,
            String attributeExpression, Operator operator) {
        roleDefinitionEditor.addCredentialExpression(credentialIndex,
                attributeExpression, operator);
        updateDefinitionLabel();
    }

    @Override
    public int getCredentialExpressionCount() {
        return roleDefinitionEditor.getCredentialExpressionCount();
    }

    @Override
    public int getCredentialIndexInExpression(int index) {
        return roleDefinitionEditor.getCredentialIndexInExpression(index);
    }

    @Override
    public String getAttributeInExpression(int index) {
        return roleDefinitionEditor.getAttributeInExpression(index);
    }

    @Override
    public Operator getOperatorInExpression(int index) {
        return roleDefinitionEditor.getOperatorInExpression(index);
    }

    @Override
    public HasOpenHandlers<DisclosurePanel> getChangeDefinitionPanel() {
        return dpChangeDefinition;
    }

    @Override
    public HasClickHandlers getOkButton() {
        return okButton;
    }

    private void updateDefinitionLabel() {
        if (!roleDefinitionEditor.isEmpty()) {
            sentenceLabel.setText("is defined as");
            definitionLabel.setText(roleDefinitionEditor.getRoleDefinition());
        }
    }
}
