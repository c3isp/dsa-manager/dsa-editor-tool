/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xmlbeans.XmlError;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.util.WidgetUtil;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.DsaApiServiceManager;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa.helper.DsaStorage;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa.helper.XmlUtil;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.Config;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.dsa.StoreDsaAction;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.dsa.StoreDsaResult;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.dsa.StoreDsaResultImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.DsaBean;

import eu.cocoCloud.dsa.DsaDocument;
import net.customware.gwt.dispatch.server.ActionHandler;
import net.customware.gwt.dispatch.server.ExecutionContext;
import net.customware.gwt.dispatch.shared.DispatchException;

@Component
public class StoreDsaActionHandler implements
        ActionHandler<StoreDsaAction, StoreDsaResult> {

    private static Log log = LogFactory.getLog(StoreDsaActionHandler.class);
    private static final Logger AUDIT = LoggerFactory.getLogger("AuditLog");

    /**
     * Used for logging //
     */

    @Value("${info.build.version}")
    private String DEVICE_VERSION; // = "1.0.0";

    @Value("${info.app.project}")
    private String DEVICE_VENDOR; // = "Coco Cloud";

    @Value("${info.build.name}")
    private String DEVICE_PRODUCT; // = "TMPL_SWAGGER"; // component name, as
                                   // get from Maven (e.g. STOR, ENF, etc.)
    private static final int CEF_SEVERITY_INFO = 5;
    private static final int CEF_SEVERITY_WARN = 8;
    private static final int CEF_SEVERITY_ERROR = 9;
    private static final int CEF_SEVERITY_FATAL = 10;

    @Override
    public StoreDsaResult execute(StoreDsaAction action,
            ExecutionContext context) throws DispatchException {


        // get the DsaBean
        final DsaBean dsaBean = action.getDsaBean();
        // build the DsaDocuemtn
        log.debug("StoreDsaResult execute: " + dsaBean);
        dsaBean.setKeyEncryptionSchema(Config.getInstance()
                .getEncryptionKeySchema());
        DsaBeanToDsaDocumentConverter converter = new DsaBeanToDsaDocumentConverter(
                dsaBean);

        DsaDocument dsaDocument = converter.convert();

        // generate Polpa expressions
        // boolean translated = false;
        try {
            DsaDocument copyDsaDocument = DsaDocument.Factory.parse(dsaDocument
                    .getDomNode());
            // Polpa is not used
            // Cnl4dsaToPolpaTranslator polpaTranslator =
            // new Cnl4dsaToPolpaTranslator();
            // polpaTranslator.translate(copyDsaDocument);
            dsaDocument = copyDsaDocument;
            // translated = true;
            log.debug("DSA Document"
                    + dsaDocument.xmlText(XmlUtil.getXmlOptions()));
        } catch (XmlException e) {
            log.error("cannot make a copy of DsaDocument: "
                    + dsaDocument.xmlText(XmlUtil.getXmlOptions()));
        }
        // catch (Cnl4dsaToPolpaTranslationException e) {
        // log.error("Error while translating CNL4DSA to Polpa: " +
        // dsaDocument.xmlText(XmlUtil.getXmlOptions()));
        // }
        //

        // validate and save
        boolean saved = false;
        // if (! dsaDocument.validate()) {

        XmlOptions validateOptions = new XmlOptions();
        ArrayList<XmlError> errorList = new ArrayList<>();
        validateOptions.setErrorListener(errorList);

        // Validate the XML.
        boolean isValid = dsaDocument.validate(validateOptions);

        // If the XML isn't valid, loop through the listener's contents,
        // printing contained messages.
        if (!isValid) {
            for (int i = 0; i < errorList.size(); i++) {
                XmlError error = (XmlError) errorList.get(i);
                log.info("Message: " + error.getMessage() + "\n");
                log.info("Invalid dsa: " + error.getCursorLocation().xmlText()
                        + "\n");
            }
        }
        // log.error("invalid DSA:" + dsaDocument.xmlText(
        // XmlUtil.getXmlOptions()));
        // }
        else {
            log.info("The dsa is valid");
            if (action.isStandalone()) {
                log.info("Standalone mode: store the document on the local repository");
                saved = DsaStorage.getInstance().write(dsaBean.getUid(),
                        dsaDocument);
            } else {
                // the DSAAT is invoked as a service:call dsa api
                String dsaID = dsaBean.getUid().substring(0,
                        dsaBean.getUid().indexOf(".xml"));
                DsaApiServiceManager dsaSrvMgr = new DsaApiServiceManager();
                log.info("invoking DSAAT as a service: call DSA api - update DSA - for dsaID="
                        + dsaID);
                saved = dsaSrvMgr.updateDSA(
                        dsaDocument.xmlText(XmlUtil.getXmlOptions()), dsaID);
                if (saved) {
                //    try {
                        final String messageForUserPreferences;
                        if (WidgetUtil.containsPendingRules(dsaBean)
                                && dsaBean.getStatus().compareTo("COMPLETED") == 0)
                            messageForUserPreferences = "User preferences has been acquired.";
                        else
                            messageForUserPreferences = "It doesn't contain user preferences.";

//                        AUDIT.info(new CEF(DEVICE_VENDOR, DEVICE_PRODUCT,
//                                DEVICE_VERSION, "CORR_ID",
//                                "Save a DSA in the object storage",
//                                CEF_SEVERITY_INFO, new Extension(
//                                        new HashMap<String, String>() {
//                                            {
//                                                put("deviceDirection", "0"); // "0"
//                                                                             // for
//                                                                             // inbound
//                                                                             // or
//                                                                             // "1"
//                                                                             // for
//                                                                             // outbound
//                                                put("msg",
//                                                        "A DSA with UUID= "
//                                                                + dsaBean
//                                                                        .getUid()
//                                                                + " has been stored. "
//                                                                + messageForUserPreferences);
//
//                                            }
//                                        })).toString());
//
//                    } catch (InvalidField | InvalidExtensionKey e) {
//                        log.error(e.getLocalizedMessage());
//                    }

                }

            }

        }
       log.info("Dsa storage saved=" + saved);

        return new StoreDsaResultImpl(saved);
    }

    @Override
    public Class<StoreDsaAction> getActionType() {
        return StoreDsaAction.class;
    }

    @Override
    public void rollback(StoreDsaAction action, StoreDsaResult result,
            ExecutionContext context) throws DispatchException {
        throw new UnsupportedOperationException();
    }

}
