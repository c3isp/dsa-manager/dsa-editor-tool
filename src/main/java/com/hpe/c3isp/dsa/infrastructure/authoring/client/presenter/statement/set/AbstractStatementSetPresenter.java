/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.enunes.gwt.mvp.client.EventBus;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.statement.CompleteMetadataEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.statement.CompleteMetadataHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.statement.StatementBrokenEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.statement.StatementBrokenHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.statement.StatementCompletedEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.statement.StatementCompletedHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.statement.StatementDeletedEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.delete.DeleteRowHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.delete.DisableRowMovementHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.move.EnableRowMovementHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.move.MoveRowHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.move.MoveRowEvent.Direction;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.AbstractControllablePresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.StatementPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.VocabularyPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.StatementSetDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableDeclaration;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.Issuer;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.Statement;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.StatementMetadata;

public abstract class AbstractStatementSetPresenter<D extends StatementSetDisplay>
        extends AbstractControllablePresenter<D> implements
        StatementSetPresenter, AbstractSspContext<D> {

    protected final List<StatementPresenter> statementPresenters = new ArrayList<StatementPresenter>();

    protected final Provider<StatementPresenter> providerOfStatementPresenter;
    protected final Provider<VocabularyPresenter> providerOfVocabularyPresenter;
    protected String userRole;
    
    protected HashMap<String,String> params;

    @Inject
    public AbstractStatementSetPresenter(EventBus eventBus, D display,
            Provider<StatementPresenter> psp, Provider<VocabularyPresenter> pvp) {
        super(eventBus, display);
        providerOfStatementPresenter = psp;
        providerOfVocabularyPresenter = pvp;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * marco.client.presenter.statement.set.StatementSetPresenter#getStatements
     * ()
     */
    @Override
    public List<Statement> getStatements() {
        List<Statement> statements = new ArrayList<Statement>();
        for (StatementPresenter statementPresenter : statementPresenters)
            statements.add(statementPresenter.getStatement());
        return statements;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * marco.client.presenter.statement.set.StatementSetPresenter#setStatements
     * (java.util.List)
     */
    @Override
    public void setStatements(List<Statement> statements, HashMap<String,String> param) {
        statementPresenters.clear();

        display.clear();
        for (int i = 0; i < statements.size(); i++) {
            // we create an idle StatementPresenter sp (i.e. not listening to
            // events), because we're passing a fully edited Statement to sp
            Statement statement = statements.get(i);
            statement.setParamItems(this.params);
            createIdleStatementPresenterFor(statement);
            boolean isEnabled = false;
            if (userRole.compareTo(Issuer.LEGAL_EXPERT.getLabel()) == 0
                    || statement.getStatementMetadata().getIssuer() == null
                    || (statement.getStatementMetadata().getIssuer().getLabel()
                            .compareTo(userRole) == 0)) {
                isEnabled = true;
            }
            boolean isEndUser=false;
            if (userRole.compareTo(Issuer.USER.getLabel())==0){
                isEndUser = true;
            }
                
            display.addStatementControlWidgets(i, isEnabled, statement, isEndUser,this.params);   
          
        }
     
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * marco.client.presenter.statement.set.StatementSetPresenter#removeReferencesTo
     * (marco.client.model.Statement)
     */
    @Override
    public void removeReferencesTo(Statement statement,
            List<VariableDeclaration> toBeRemoved) {
        for (int i = 0; i < statementPresenters.size()
                && !toBeRemoved.isEmpty(); i++)
            statementPresenters.get(i).removeReferencesTo(statement,
                    toBeRemoved);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.enunes.gwt.mvp.client.presenter.BasePresenter#bind()
     */
    @Override
    public void bind() {
        super.bind();
        registerHandler(eventBus.addHandler(StatementCompletedEvent.TYPE,
                new StatementCompletedHandler() {

                    @Override
                    public void onStatementCompleted(StatementMetadata metadata){//, CheckBox protectedCheckBox, CheckBox isPendingRuleCheckBox, Label statementInfo, TextArea inputValue) {
                        // we care only of StatementCompletedEvents for
                        // statements having the same StatementType as
                        // this StatementSetPresenter. For example, if this
                        // StatementSetPresenter works for AUTHORIZATION, than
                        // we handle only StatementCompletedEvents for
                        // statements of type AUTHORIZATION
                        if (getStatementType().equals(metadata.getType()))
                            handleStatementCompletedEvent(metadata);
                    }
                }));
        registerHandler(eventBus.addHandler(CompleteMetadataEvent.TYPE,
                new CompleteMetadataHandler() {
                    
                    @Override
                    public void onCompleteMetadataEvent(StatementMetadata metadata) {
                        if (getStatementType().equals(metadata.getType()))
                            handleCompleteMetadataEvent(metadata);//, protectedCheckBox,isPendingRuleCheckBox, statementInfo, inputValue);
                                    
                    }
                }));
        registerHandler(eventBus.addHandler(StatementBrokenEvent.TYPE,
                new StatementBrokenHandler() {

                    @Override
                    public void onStatementBroken(StatementMetadata metadata) {
                        // we care only of StatementBrokenEvent for
                        // statements having the same StatementType as
                        // this StatementSetPresenter. For example, if this
                        // StatementSetPresenter works for AUTHORIZATION, than
                        // we handle only StatementBrokenEvent for
                        // statements of type AUTHORIZATION
                        if (getStatementType().equals(metadata.getType()))
                            handleStatementBrokenEvent(metadata);
                    }
                }));
        registerHandler(display.getAddButton().addClickHandler(
                new ClickHandler() {

                    @Override
                    public void onClick(ClickEvent event) {
                        addStatement();
                    }
                }));
        registerHandler(display.addDeleteRowHandler(new DeleteRowHandler() {

            @Override
            public void onDeleteRow(int rowIndex) {
                handleDeleteRowEvent(rowIndex);
            }
        }));
        registerHandler(display.addMoveRowUpHandler(new MoveRowHandler() {

            @Override
            public void onMoveRow(Direction direction){// CheckBox protectedCheckBox, CheckBox isPendingRuleCheckBox,Label statementInfo, TextArea inputValue) {
                handleMoveRowEvent(direction);//, protectedCheckBox, isPendingRuleCheckBox, statementInfo, inputValue);
            }
        }));

        registerHandler(display
                .addEnableRowMovementHandler(new EnableRowMovementHandler() {

                    @Override
                    public void onEnableRowMovementEvent(int rowIndex) {
                        handleEnableRowMovementEvent(rowIndex);
                    }
                }));
        registerHandler(display
                .addDisableRowMovementHandler(new DisableRowMovementHandler() {

                    @Override
                    public void onDisableRowMovementEvent() {
                        handleDisableRowMovementEvent();
                    }
                }));
      
    }

    protected abstract void handleStatementCompletedEvent(
            StatementMetadata metadata);
    
    protected abstract void handleCompleteMetadataEvent(StatementMetadata metadata);
    
    protected abstract void handleStatementBrokenEvent(
            StatementMetadata metadata);

    protected abstract void addStatement();

    protected abstract void handleDeleteRowEvent(int rowIndex);

    protected abstract void handleMoveRowEvent(Direction direction);//, CheckBox protectedCheckBox, CheckBox isPendingRuleCheckBox,Label statementInfo, TextArea inputValue);

    protected abstract void handleEnableRowMovementEvent(int rowIndex);

    protected abstract void handleDisableRowMovementEvent();
    
    private void updateIndexOfStatementPresenters() {
        for (int i = 0; i < statementPresenters.size(); i++)
            statementPresenters.get(i).getStatement().setIndex(i);
    }
    
    

    /*
     * Creates a new StatementPresenter sp, set the passed statement into sp,
     * adds sp to our list of StatementPresenters, add sp's display to our
     * display, and returns sp. Note: sp is IDLE: it doesn't listen to event,
     * because this method does not call sp.bind(): it's the caller
     * responsibility to decide whether to bind sp or not.
     */
    private StatementPresenter createIdleStatementPresenterFor(
            Statement statement) {
        // get a StatementPresenter
        final StatementPresenter sp = providerOfStatementPresenter.get();
        // set the statement into the StatementPresenter
        sp.setStatement(statement);
        // store the StatementPresenter into our list
        statementPresenters.add(sp);
        // add the StatementPresenter display to our display
        display.addStatementDisplay(sp.getDisplay());
        // return the StatementPresenter
        return sp;
    }

    /*
     * (non-Javadoc)
     * 
     * @see marco.client.presenter.statement.set.AbstractSspContext#
     * addStatementPresenterFor(marco.client.model.Statement)
     */
    @Override
    public void addStatementPresenterFor(Statement statement) {
        // get a StatementPresenter
        final StatementPresenter sp = createIdleStatementPresenterFor(statement);
        // set the statement into the StatementPresenter
        sp.setStatement(statement);
        // bind the StatementPresenter
        sp.bind();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * marco.client.presenter.statement.set.AbstractSspContext#getStatementPresenter
     * (int)
     */
    @Override
    public StatementPresenter getStatementPresenterAt(int index) {
        return statementPresenters.get(index);
    }

    /*
     * (non-Javadoc)
     * 
     * @see marco.client.presenter.statement.set.AbstractSspContext#
     * getStatementPresentersCount()
     */
    @Override
    public int getStatementPresentersCount() {
        return statementPresenters.size();
    }

    /*
     * (non-Javadoc)
     * 
     * @see marco.client.presenter.statement.set.AbstractSspContext#
     * moveStatementPresenter(int, int)
     */
    @Override
    public void moveStatementPresenter(int fromIndex, int toIndex) {
        StatementPresenter sp = statementPresenters.remove(fromIndex);
        statementPresenters.add(toIndex, sp);
        updateIndexOfStatementPresenters();
    }

    /*
     * (non-Javadoc)
     * 
     * @see marco.client.presenter.statement.set.AbstractSspContext#
     * removeStatementPresenterAt(int)
     */
    @Override
    public void removeStatementPresenterAt(int rowIndex) {
        // remove the StatementPresenter at rowIndex
        StatementPresenter sp = statementPresenters.remove(rowIndex);
        // unbind it
        sp.unbind();
        // tell everyone that the corresponding Statement has been deleted
        // (this is useful for updating VariableReferences, and statement
        // references)
        eventBus.fireEvent(new StatementDeletedEvent(sp.getStatement()));
        updateIndexOfStatementPresenters();
    }

    @Override
    public void setUserRole(String role) {
        this.userRole = role;
    }
    
    @Override
    public void setParamsTermList(HashMap<String,String> params){
        this.params=params;
    }
 
}
