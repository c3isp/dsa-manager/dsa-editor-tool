/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.view.parties;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.inject.Inject;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.parties.PartiesPresenter.PartiesDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.util.WidgetUtil;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Organization;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.OrganizationUtils;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.Issuer;

public class PartiesDisplayImpl extends AbstractOrganizationDisplayImpl
        implements PartiesDisplay {

    private final HTML selectedOrganizationsNames = new HTML("");
    private final VerticalPanel selectedOrganizations = new VerticalPanel();

    private List<Organization> chosenOrgs = new ArrayList();
    private String selectedOrgName;
    private int selectedPartyIndex;
    CellTable<Organization> table;

    @Inject
    public PartiesDisplayImpl() {
        super();

        final VerticalPanel mainvp = new VerticalPanel();
        initWidget(mainvp);

        // mainvp.add(selectedOrganizationsNames);
        mainvp.add(selectedOrganizations);
        // mainvp.add(dpChangeOrganizations);
    }

    @Override
    public void clearChosenOrganizationNames() {
        selectedOrganizationsNames.setHTML("");
        selectedOrganizations.clear();
    }

    @Override
    public List<Organization> getChosenOrganizations() {
        return this.chosenOrgs;
    }

    @Override
    public void setChosenOrganizationNames(List<String> organizationsNames) {
        // TODO Auto-generated method stub
    }

    @Override
    public void setChosenOrganization(List<Organization> selectedOrgs,
            String userRole, final List<Organization> availableOrgs, String name, String role, String responsibilities) {
        // reset parties panel
        selectedPartyIndex = -1;
        selectedOrganizations.clear();
        chosenOrgs.clear();
        chosenOrgs.addAll(selectedOrgs);
        StringBuffer buffer = new StringBuffer();
        VerticalPanel vPanel = new VerticalPanel();

        if ((role == null || role.isEmpty()) && (responsibilities == null || responsibilities.isEmpty())){
        	 @SuppressWarnings("unchecked")
        	 CellTable<Organization> partiesTable = (CellTable<Organization>) WidgetUtil.getPartiesTable(
                     selectedOrgs, userRole, name, role, responsibilities);
        	 table = partiesTable;
        }else{
        	 @SuppressWarnings("unchecked")
        	 CellTable<Organization> partiesTableForName = (CellTable<Organization>) WidgetUtil.getPartiesTableForName(selectedOrgs, userRole, name);
        	 table = partiesTableForName;
        }


        final SelectionModel<Organization> selectionModel = new SingleSelectionModel<Organization>();

        selectionModel
                .addSelectionChangeHandler(new SelectionChangeEvent.Handler() {

                    public void onSelectionChange(SelectionChangeEvent event) {
                        Organization selected = ((SingleSelectionModel<Organization>) selectionModel)
                                .getSelectedObject();
                        if (selected != null) {
                            selectedOrgName = selected.getName();

                        }
                    }
                });
        table.setSelectionModel(selectionModel);
        List<String> nameList = new ArrayList<String>();
        nameList.add("---");

        if (userRole.compareTo(Issuer.USER.getLabel()) != 0) {
            for (int i = 0; i < availableOrgs.size(); i++) {
                nameList.add(availableOrgs.get(i).getName());

            }

            SelectionCell changeNameCell = new SelectionCell(nameList);
            Column<Organization, String> changeNameCol = new Column<Organization, String>(
                    changeNameCell) {
                @Override
                public String getValue(Organization object) {
                    return object.getName();
                }
            };
            table.addColumn(changeNameCol, "Choose party name");

            changeNameCol.setFieldUpdater(new FieldUpdater<Organization, String>() {
                        @Override
                        public void update(int index, Organization object, String value) {
                            String uidForParty = getUidForParty(availableOrgs, value);
                            if (value.compareTo("---") == 0) {
                                Window.alert("Please, select a value in the menu");
                            }
                            object.setName(value);
                            object.setUid(uidForParty);
                            updateList(object);
                            table.redraw();
                        }

                        private void updateList(Organization object) {
                            for (int i = 0; i < chosenOrgs.size(); i++) {
                                Organization org = chosenOrgs.get(i);
                                if (org.equals(object)) {
                                    chosenOrgs.set(i, object);
                                }
                            }
                        }

                        private String getUidForParty(List<Organization> organizations, String value) {
                            for (Iterator<Organization> element = organizations
                                    .iterator(); element.hasNext();) {
                                Organization org = (Organization) element
                                        .next();
                                if (org.getName().compareTo(value) == 0)
                                    return org.getUid();

                            }
                            return null;
                        }
                    });

        }

        // add list data provider to dynamic table management
        final ListDataProvider<Organization> model = new ListDataProvider<Organization>(
                selectedOrgs);
        model.addDataDisplay(table);

        if (userRole.compareTo(Issuer.USER.getLabel()) != 0) {
            Column<Organization, String> deleteBtn = new Column<Organization, String>(
                    new ButtonCell()) {
                @Override
                public String getValue(Organization c) {
                    return "remove party";
                }
            };

            table.addColumn(deleteBtn, "");
            deleteBtn.setFieldUpdater(new FieldUpdater<Organization, String>() {
                @Override
                public void update(int index, Organization object, String value) {
                    model.getList().remove(object);
                    chosenOrgs.remove(object);
                    model.refresh();
                    table.redraw();
                }
            });
        }
        vPanel.add(table);
        if (userRole.compareTo(Issuer.USER.getLabel()) != 0) {
            Button addBtn = new Button("add party");
            addBtn.setStyleName("myButton");
            addBtn.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    Organization org = OrganizationUtils.getEmptyOrganization();
                    model.getList().add(org);
                    chosenOrgs.add(org);
                    model.refresh();
                    table.redraw();

                }
            });
            vPanel.add(addBtn);
        }

        selectedOrganizations.add(vPanel);
        selectedOrganizationsNames.setHTML(buffer.toString());

    }

}
