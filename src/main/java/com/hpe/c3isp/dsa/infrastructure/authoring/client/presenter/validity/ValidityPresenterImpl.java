/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.validity;

import java.util.Date;

import org.enunes.gwt.mvp.client.EventBus;

import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.inject.Inject;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.presenter.BusyDisplayEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.presenter.IdleDisplayEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.AbstractControllablePresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.validity.ValidityPresenter.ValidityDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Validity;

public class ValidityPresenterImpl extends
AbstractControllablePresenter<ValidityDisplay> implements
ValidityPresenter {

	private Validity validityBean;

	private String startDateLabel;

	private String endDateLabel;

	//    private String durationLabel;
	//
	//    private String unitLabel;

	@Inject
	public ValidityPresenterImpl(EventBus eventBus, ValidityDisplay display) {
		super(eventBus, display);
		validityBean = new Validity();
	}

	@Override
	public void bind() {
		super.bind();
		//        OpenHandler<DisclosurePanel> openHandler = new OpenHandler<DisclosurePanel>() {
		//            @Override
		//            public void onOpen(OpenEvent<DisclosurePanel> event) {
		//                eventBus.fireEvent(new BusyDisplayEvent(display));
		//            }
		//        };
		//        registerHandler(display.getDsaStartDatePanel().addOpenHandler(openHandler));
		//        registerHandler(display.getDsaEndDatePanel().addOpenHandler(openHandler));
		//




		registerHandler(display.getDsaStartDateBox().addValueChangeHandler(
				new ValueChangeHandler<Date>() {
					@Override
					public void onValueChange(ValueChangeEvent<Date> event) {
						Date startDate = event.getValue();
						validityBean.setStartDate(startDate);
						//                        display.setStartDate(startDate, startDateLabel);
						eventBus.fireEvent(new IdleDisplayEvent(display));
					}
				}));


		registerHandler(display.getDsaEndDateBox().addValueChangeHandler(
				new ValueChangeHandler<Date>() {
					@Override
					public void onValueChange(ValueChangeEvent<Date> event) {
						Date endDate = event.getValue();
						validityBean.setEndDate(endDate);
						//                        display.setEndDate(endDate, endDateLabel);
						eventBus.fireEvent(new IdleDisplayEvent(display));
					}
				}));








//		registerHandler(display.getDsaStartDatePicker().addValueChangeHandler(
//				new ValueChangeHandler<Date>() {
//					@Override
//					public void onValueChange(ValueChangeEvent<Date> event) {
//						Date startDate = event.getValue();
//						validityBean.setStartDate(startDate);
//						display.setStartDate(startDate, startDateLabel);
//						eventBus.fireEvent(new IdleDisplayEvent(display));
//					}
//				}));
//
//		registerHandler(display.getDsaEndDatePicker().addValueChangeHandler(
//				new ValueChangeHandler<Date>() {
//					@Override
//					public void onValueChange(ValueChangeEvent<Date> event) {
//						Date endDate = event.getValue();
//						validityBean.setEndDate(endDate);
//						display.setEndDate(endDate,endDateLabel);
//						eventBus.fireEvent(new IdleDisplayEvent(display));
//					}
//				}));
	}

	@Override
	public Validity getValidity() {
		return validityBean;
	}

	@Override
	public void setValidity(Validity validityBean) {
		this.validityBean = validityBean;
		display.setStartDate(validityBean.getStartDate(), startDateLabel);
		display.setEndDate(validityBean.getEndDate(), endDateLabel);




	}

	@Override
	public void setValidityLabels(String startDate, String endDate, String unit) {
		this.startDateLabel = startDate;
		this.endDateLabel = endDate;
		//       this.unitLabel = " ".concat(unit);


	}

}
