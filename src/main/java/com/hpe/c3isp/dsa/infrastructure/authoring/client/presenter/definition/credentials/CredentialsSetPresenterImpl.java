/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.credentials;

import java.util.ArrayList;
import java.util.List;

import org.enunes.gwt.mvp.client.EventBus;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.presenter.BusyDisplayHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.presenter.IdleDisplayHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.AbstractControllablePresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.credentials.CredentialsSetPresenter.CredentialsSetDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.ControllableDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Term;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Credentials;

public class CredentialsSetPresenterImpl extends
        AbstractControllablePresenter<CredentialsSetDisplay> implements
        CredentialsSetPresenter, BusyDisplayHandler, IdleDisplayHandler {

    private final List<CredentialPresenter> credentialPresenters = new ArrayList<CredentialPresenter>();

    private final Provider<CredentialPresenter> providerOfCredentialPresenter;

    @Inject
    public CredentialsSetPresenterImpl(EventBus eventBus,
            CredentialsSetDisplay display, Provider<CredentialPresenter> pcp) {
        super(eventBus, display);
        providerOfCredentialPresenter = pcp;
    }

    @Override
    public Credentials getCredentials() {
        Credentials credentials = new Credentials();
        for (CredentialPresenter cp : credentialPresenters)
            credentials.setTrustedContextProviders(cp.getCredential(),
                    cp.getTrustedContextProviders());
        return credentials;
    }

    @Override
    public void setCredentials(Credentials credentials) {
        credentialPresenters.clear();
        display.clear();
        for (Term credential : credentials.getCredentials()) {
            final CredentialPresenter cp = providerOfCredentialPresenter.get();
            cp.set(credential,
                    credentials.getTrustedContextProviders(credential));
            cp.addBusyDisplayHandler(this);
            cp.addIdleDisplayHandler(this);
            credentialPresenters.add(cp);
            cp.bind();
            display.addCredentialDisplay(cp.getDisplay());
        }
    }

    @Override
    public void onBusyDisplay(ControllableDisplay display) {
        // deactivate the displays of all my CredentialPresenters except the one
        // that possibly sent the BusyDisplayEvent
        for (CredentialPresenter cp : credentialPresenters) {
            ControllableDisplay cd = cp.getDisplay();
            if (!cd.equals(display))
                cd.deactivate();
        }
    }

    @Override
    public void onIdleDisplay(ControllableDisplay display) {
        // activate all the displays of my CredentialPresenters
        for (CredentialPresenter cp : credentialPresenters)
            cp.getDisplay().activate();
    }

}
