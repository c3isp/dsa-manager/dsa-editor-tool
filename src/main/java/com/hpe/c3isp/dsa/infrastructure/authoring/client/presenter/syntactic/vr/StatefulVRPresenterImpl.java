/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.syntactic.vr;

import org.enunes.gwt.mvp.client.EventBus;
import org.enunes.gwt.mvp.client.presenter.BasePresenter;
import org.enunes.gwt.mvp.client.presenter.Presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.inject.Inject;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.syntactic.HighlightShowableEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.syntactic.HighlightShowableHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.syntactic.SyntacticItemClickedEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.syntactic.SyntacticItemClickedHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.syntactic.UnhighlightShowableEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.syntactic.UnhighlightShowableHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.syntactic.vr.StatefulVRPresenter.VariableReferenceDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableReference;

/**
 * Implementation of a {@link StatefulVRPresenter}. It uses the <a
 * href="http://en.wikipedia.org/wiki/State_pattern">State Design Pattern</a>,
 * and, at the same time, act as a {@link Presenter} for the GWT
 * Model-View-Presenter (MVP) pattern.
 * 
 * @author Marco Luca Sbodio (mailto:marco.sbodio@hp.com)
 * 
 */
public class StatefulVRPresenterImpl extends
        BasePresenter<VariableReferenceDisplay> implements StatefulVRPresenter {

    private VariableReference variableReference;
    private SvrpState state;

    @Inject
    public StatefulVRPresenterImpl(EventBus eventBus,
            VariableReferenceDisplay display) {
        super(eventBus, display);
    }

    @Override
    public void bind() {
        super.bind();
        registerHandler(eventBus.addHandler(HighlightShowableEvent.TYPE,
                new HighlightShowableHandler() {

                    @Override
                    public void onHighlightShowable() {
                        handleHighlightShowableEvent();
                    }
                }));
        registerHandler(eventBus.addHandler(UnhighlightShowableEvent.TYPE,
                new UnhighlightShowableHandler() {

                    @Override
                    public void onUnhighlightShowable() {
                        handleUnhighlightShowableEvent();
                    }
                }));
        registerHandler(display.getLabel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                handleClickEvent();
            }
        }));
        registerHandler(eventBus.addHandler(SyntacticItemClickedEvent.TYPE,
                new SyntacticItemClickedHandler() {

                    @Override
                    public void onSyntacticItemClicked(
                            SyntacticItem syntacticItem) {
                        handleSyntacticItemClickedEvent(syntacticItem);
                    }
                }));
    }

    @Override
    public void show(VariableReference syntacticItem) {
        this.variableReference = syntacticItem;
        state = new SvrpStateNormal();
        String text = variableReference.getHumanForm();
        if (variableReference.getValue()!=null && variableReference.getValue()!="")
            text.concat("("+variableReference.getValue()+")");
        display.setText(text);
    }

    public VariableReference getVariableReference() {
        return variableReference;
    }

    /*
     * Sets the state. Note: we could move this method up to the {@link
     * StatefulVDPresenter} interface, however by doing so we would break the
     * State design pattern, because we would make this method public.
     */
    protected void setState(SvrpState state) {
        this.state = state;
    }

    /*
     * this method is protected essentially for the same reason that setState is
     * protected
     */
    protected <H extends EventHandler> void fireEvent(GwtEvent<H> event) {
        eventBus.fireEvent(event);
    }

    private void handleClickEvent() {
        state.handleClickEvent(this);
    }

    private void handleUnhighlightShowableEvent() {
        state.handleUnhighlightShowableEvent(this);
    }

    private void handleHighlightShowableEvent() {
        state.handleHighlightShowableEvent(this);
    }

    private void handleSyntacticItemClickedEvent(SyntacticItem syntacticItem) {
        state.handleSyntacticItemClickedEvent(this, syntacticItem);
    }

}
