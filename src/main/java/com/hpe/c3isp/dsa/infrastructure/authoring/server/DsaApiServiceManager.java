package com.hpe.c3isp.dsa.infrastructure.authoring.server;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xmlbeans.XmlException;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.DsaServiceManagerIF;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.RemoteRepository;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa.helper.DsaStorage;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.RemoteRepositoryContent;

import eu.cocoCloud.dsa.DsaDocument;

public class DsaApiServiceManager implements DsaServiceManagerIF{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private static String url;
	private static Log log = LogFactory.getLog(DsaApiServiceManager.class);

	private RemoteRepositoryContent documentContent;

	public DsaApiServiceManager() {
		url = DsaStorage.getInstance().getDsaApi();
		//only for test: remove proxy
		documentContent = new RemoteRepositoryContent();
		//        System.setProperty("proxyHost", "web-proxy.emea.hp.com");
		//        System.setProperty("proxyPort", "8088");
	}


	public RemoteRepositoryContent getDocumentContent() {
		return documentContent;
	}


	public void setDocumentContent(RemoteRepositoryContent documentContent) {
		this.documentContent = documentContent;
	}


	public boolean getDSA(String dsaUid) {
		log.info("Get DSA. Uuid="+dsaUid);

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add( new ByteArrayHttpMessageConverter());

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(RemoteRepository.getMediaType());

		HttpEntity<String> entity = new HttpEntity<String>(headers);

		ResponseEntity<byte[]> response = restTemplate.exchange(url+"/getDSA/{dsaid}", HttpMethod.GET, entity, byte[].class, dsaUid);

		if (response.getStatusCode() == HttpStatus.OK)
		{
			try {
				documentContent.setDocumentContent(IOUtils.toString(response.getBody(), "UTF-8"));
				String responseAsString = new String(response.getBody(), "UTF-8");
				documentContent.setDocumentContent(responseAsString);
				log.info("Success document="+documentContent.getDocumentContent());
				return true;

			} catch (IOException e) {
				// TODO Auto-generated catch block
				log.error(e.getMessage());
			}
		}
		else {
			// log error, retry or ?
			log.error("Error!"); //TODO manage error
			return false;
		}
		return true;
	}

	public DsaDocument loadDSA(String dsaUid) {
		log.info("Load DSA from remote repository. DSAAPI: "+url+" Uuid="+dsaUid);
		DsaDocument dsaDocument = null;
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add( new ByteArrayHttpMessageConverter());

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_XML));

		HttpEntity<String> entity = new HttpEntity<String>(headers);

		ResponseEntity<byte[]> response = restTemplate.exchange(url+"/getDSA/{dsaid}", HttpMethod.GET, entity, byte[].class, dsaUid);

		if (response.getStatusCode() == HttpStatus.OK)
		{
			try {
				String documentContent = IOUtils.toString(response.getBody(), "UTF-8");
				log.info("Success ");
				dsaDocument = DsaDocument.Factory.parse(documentContent);
			} catch (IOException e) {
				log.error("An IO error occurred. ");
				log.error(e.getMessage());
			} catch (XmlException e) {
				log.error("An XML exception occurred. ");
				log.error(e.getMessage());
			}
		}
		else {
			log.error("Error!");
		}
		return dsaDocument;
	}

	public boolean updateDSA(String dsaContent, String dsaID) {
		log.info("Update DSA. Uuid="+dsaID);
		RestTemplate template = new RestTemplate();
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();
		parts.add("dsaid", dsaID);


		//bugged (PATH_TRAVERSAL_IN)
		//        String filePath = DsaStorage.getInstance().getTempFilePath(dsaContent,dsaID);

		//FIXED
		String fixedDsaID = FilenameUtils.getName(dsaID); //fix
		String filePath = DsaStorage.getInstance().getTempFilePath(dsaContent,fixedDsaID);

		parts.add("file", new FileSystemResource(new File(filePath)));

		ResponseEntity<String>  response = template.postForEntity(url+"/updatedsa", parts, String.class);

		if (response.getStatusCode() == HttpStatus.OK) {
			log.info("Success "+ response.getBody());
			return true;
		}
		else {
			log.error("Error!");
			return false;
		}
		//TODO remember to delete temp file

	}
	public String createDSA(String dsaID, String userID, String dsaContent) {
		log.info("Create DSA. Uuid="+dsaID + " userId="+userID);
		RestTemplate template = new RestTemplate();
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();



		//bugged (PATH_TRAVERSAL_IN)
		//      String filePath = DsaStorage.getInstance().getTempFilePath(dsaContent,dsaID);

		//FIXED
		String fixedDsaID = FilenameUtils.getName(dsaID); //fix



		parts.add("dsaid", fixedDsaID);
		parts.add("userid", userID);

		//bugged (PATH_TRAVERSAL_IN)
		//      String filePath = DsaStorage.getInstance().getTempFilePath(dsaContent,dsaID);
		String filePath = DsaStorage.getInstance().getTempFilePath(dsaContent,fixedDsaID);//fixed




		parts.add("skelatonfile", new FileSystemResource(new File(filePath)));


		ResponseEntity<String> response = template.postForEntity(url+"/createdsa", parts, String.class);
		String dsaId = null;
		if (response.getStatusCode() == HttpStatus.OK) {
			dsaId = response.getBody();
			log.info("Success. Dsa Content "+ dsaId );
		}
		else{
			log.error("Error!");
		}
		return dsaId;
	}

}
