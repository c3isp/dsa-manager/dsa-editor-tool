/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.util;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.dom.builder.shared.ElementBuilderFactory;
import com.google.gwt.dom.builder.shared.PreBuilder;
import com.google.gwt.dom.client.Element;

public class HtmlDialog extends DialogBox {
    
    private String returnUrl;

    public HtmlDialog(String title, String content, String redirectUrl, String dialogStyle, String buttonLabel) {
        // Set the dialog box's caption.
        returnUrl = redirectUrl;
        setText(title);

        // Enable animation.
        setAnimationEnabled(false);

        // Enable glass background.
        setGlassEnabled(true);

        Button close = new Button(buttonLabel);
        close.setStyleName("myButton");
        close.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                HtmlDialog.this.hide();
                if (returnUrl!=null)
                    Window.Location.replace(returnUrl);
            }
        });

        // Build a <pre class="dialogContent"> [xml content] </pre>
        // Window.alert(content);
        PreBuilder preBuilder = ElementBuilderFactory.get().createPreBuilder();
        preBuilder.className(dialogStyle).text(content).endPre();
        Element pre = preBuilder.finish();
        HTML label = new HTML();
        label.setHTML(pre.getString());
        // label.setHTML("<pre class=\"dialogContent\">" + content + "</pre>");
        // label.setStyleName("dialogContent");

        VerticalPanel panel = new VerticalPanel();
        panel.setSpacing(10);
        panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        ScrollPanel scrollPane = new ScrollPanel();
        // scrollPane.setAlwaysShowScrollBars(true);
        scrollPane.add(label);
        scrollPane.setStyleName("scrollContent");
        panel.add(scrollPane);
        panel.add(close);

        setWidget(panel);
    }
}