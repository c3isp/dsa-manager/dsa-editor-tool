/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.fsm;

import java.util.ArrayList;
import java.util.List;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.model.VocabularyChoicesUpdater;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.helper.ReferenceableTermsKeeper;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Token;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Tokens;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.vocabulary.GetRootTerms;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.StatementType;

public class CnlPrompterState01 implements CnlPrompterState {

    private final List<Token> tokens;
    private final VocabularyChoicesUpdater vocabularyChoicesUpdater;

    public CnlPrompterState01() {
        tokens = new ArrayList<Token>();
        tokens.add(Tokens.TOKEN_IF);
        tokens.add(Tokens.FAKE_TOKEN_REFERENCE);
        tokens.add(Tokens.TOKEN_AFTER);
        vocabularyChoicesUpdater = new VocabularyChoicesUpdater(tokens,
                new GetRootTerms(ReferenceableTermsKeeper.getInstance()
                        .getVocabularyUri()));
    }

    @Override
    public void handleReference(CnlPrompterContext context,
            SyntacticItem syntacticItem) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleSelection(CnlPrompterContext context,
            SyntacticItem selectedItem) {
        if (selectedItem.equals(Tokens.TOKEN_IF)) {
            context.update(new CnlPrompterState06(), selectedItem);
        } else if (selectedItem.equals(Tokens.TOKEN_AFTER)) {
            context.update(new CnlPrompterState16(), selectedItem);
        } else if (selectedItem.equals(Tokens.FAKE_TOKEN_REFERENCE)) {
            context.updateAndEnableReferenceSelection(new CnlPrompterState12());
        } else {
            StatementType statementType = context.getStatementType();
            if (statementType.equals(StatementType.AUTHORIZATION)
                    || statementType
                            .equals(StatementType.DATA_SUBJECT_AUTHORIZATION)
                    || statementType
                            .equals(StatementType.DERIVED_OBJECTS_AUTHORIZATION))
                context.update(new CnlPrompterState02Auth(selectedItem),
                        selectedItem);
            else if (statementType.equals(StatementType.OBLIGATION)
                    || statementType
                            .equals(StatementType.DERIVED_OBJECTS_OBLIGATION))
                context.update(new CnlPrompterState02Obl(selectedItem),
                        selectedItem);
            else if (statementType.equals(StatementType.PROHIBITION)
                    || statementType
                            .equals(StatementType.DERIVED_OBJECTS_PROHIBITION))
                context.update(new CnlPrompterState02Pro(selectedItem),
                        selectedItem);
            else
                assert false : "unknown StatementType " + statementType;
        }
    }

    @Override
    public VocabularyChoicesUpdater getVocabularyChoicesUpdater() {
        return vocabularyChoicesUpdater;
    }

}
