package com.hpe.c3isp.dsa.infrastructure.authoring.client;

import eu.cocoCloud.dsa.DsaDocument;

public interface DsaServiceManagerIF{

    public boolean getDSA(String dsaUid);

    public DsaDocument loadDSA(String dsaUid);
    
    public boolean updateDSA(String dsaFilePath, String dsaID);
    
    public String createDSA(String dsaID, String userID, String dsaContent);

}