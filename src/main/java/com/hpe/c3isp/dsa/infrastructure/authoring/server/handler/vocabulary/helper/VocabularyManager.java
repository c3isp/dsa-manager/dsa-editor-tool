/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.helper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.hp.hpl.jena.ontology.OntDocumentManager;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.util.FileManager;
import com.hp.hpl.jena.vocabulary.RDFS;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.Config;

public class VocabularyManager {
    private static Log log = LogFactory.getLog(VocabularyManager.class);
    private static VocabularyManager THE_INSTANCE = null;
    private final OntModelSpec spec = new OntModelSpec(OntModelSpec.OWL_MEM); // definition
                                                                              // of
                                                                              // a
                                                                              // specific
                                                                              // reasoner
                                                                              // (TODO
                                                                              // pellet
                                                                              // reasoner
                                                                              // model
                                                                              // PelletReasonerFactory.THE_SPEC)

    private final OntDocumentManager odm = OntDocumentManager.getInstance();

//    private final String propertiesFile = "etc/config/vocabulary.properties";
    private String upperVocabularyUri = "";

    private VocabularyManager() {
//        Properties properties = new Properties();
//        InputStream is = getClass().getClassLoader().getResourceAsStream(propertiesFile);
//        if (is != null) {
//            try {
//                properties.load(is);
//                if (properties.containsKey("upperVocabularyUri")) {
//                    this.upperVocabularyUri = properties
//                            .getProperty("upperVocabularyUri");
//                    log.debug("upper-vocabulary URI: " + this.upperVocabularyUri);
//                } else {
//                    log.error("cannot find property upperVocabularyUri");
//                }
//            } catch (IOException e) {
//                log.error("cannot read etc/config/vocabulary.properties", e);
//            }
//            try {
//                is.close();
//            } catch (IOException e) {
//                log.error("cannot close InputStream attached to etc/config/vocabulary.properties");
//            }
//        } else {
//            log.error("cannot find etc/config/vocabulary.properties");
//        }

    	this.upperVocabularyUri = Config.getInstance().getUpperVocabularyUri();

        this.odm.setFileManager(FileManager.get());
        this.odm.setProcessImports(true);
        this.odm.setCacheModels(true);
        this.spec.setDocumentManager(this.odm);
        if (loadOntology(this.upperVocabularyUri) == null) {
            String message = "cannot load upper vocabulary from "
                    + this.upperVocabularyUri;
            log.fatal(message);
            throw new IllegalArgumentException(message);
        }
    }

    public static VocabularyManager getInstance() {
        if (THE_INSTANCE == null)
            THE_INSTANCE = new VocabularyManager();
        return THE_INSTANCE;
    }

    public VocabularyOntology getVocabularyOntology(String vocabularyUri) {
        VocabularyOntology vocont = null;
        OntModel om = loadOntology(vocabularyUri);
        if (om != null){

            vocont = new VocabularyOntology(vocabularyUri, om,
                    this.upperVocabularyUri,objectPropertiesHashForType(om, RDFS.isDefinedBy));
        }

        return vocont;
    }

    private OntModel loadOntology(String ontologyUri) {
        if (log.isDebugEnabled())
          log.info("loading ontology  " + ontologyUri);
        OntModel om = this.odm.getOntology(ontologyUri, this.spec);
        if (om.isEmpty()) {
            return null;
        }
        if (log.isDebugEnabled())
          log.info("preparing ontology " + ontologyUri);
        om.prepare();
        return om;
    }

    public List<String> objectPropertiesForType(Model m, final Property type) {

     ResIterator list = m.listResourcesWithProperty(type);

        List<String> propertyList = new ArrayList<String>();
        while (list.hasNext()) {
            Resource s = list.next();
           //only the term is extracted from the the URI
           propertyList.add(s.getURI().substring(s.getURI().indexOf("#")+1));

        }

        return propertyList;
    }


    public String objectPropertyValueForType(Model m, String property, final Property type) {
        ResIterator list = m.listResourcesWithProperty(type);
           while (list.hasNext()) {
               Resource s = list.next();
              //only the term is extracted from the the URI to get the property value
              String term = s.getURI().substring(s.getURI().indexOf("#")+1);
              if (term.compareTo(property)==0){
            	  return s.getProperty(type).getString();
              }
           }
           return null;
       }


    public HashMap<String,String> objectPropertiesHashForType(Model m, final Property type) {
        ResIterator list = m.listResourcesWithProperty(type);
        HashMap<String,String> propertyList = new HashMap<String,String>();
           while (list.hasNext()) {
               Resource s = list.next();
              //only the term is extracted from the the URI
               String term=s.getURI().substring(s.getURI().indexOf("#")+1);
               String params=s.getProperty(type).getString();
               String options = objectPropertyValueForType(m,term, RDFS.seeAlso);
               if (options!=null){
                   propertyList.put(term,params+"["+ options+"]");
               }else{
                   propertyList.put(term,params);
               }
           }
           return propertyList;
       }


}