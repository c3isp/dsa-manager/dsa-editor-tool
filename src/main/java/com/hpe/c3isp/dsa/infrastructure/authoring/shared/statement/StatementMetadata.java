/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement;

import com.google.gwt.user.client.rpc.IsSerializable;

public class StatementMetadata implements IsSerializable {

    /*
     * statement type: see enumeration {@link StatementType}
     */
    private StatementType type;

    /*
     * statement type: see enumeration {@link Issuer}
     */
    private Issuer issuer;

    /*
     * the unique identifier of this statement
     */
    private String uid;
    
    /*
     * layer specification 
     */
    private boolean isProtected;
    
    /*
     * is a pending rule
     */
    private boolean isPendingRule;   
    
    /*
     * statement info
     */
    private String statementInfo;  
    
    /*
     * value provided by user
     */
    private String inputValue;  
    
    /*
     * generate conflict 
     */
    private boolean conflict;

    /*
     * the index of this statement specifies its position in the ordered list of
     * statements for its type
     */
    private int index;

    /*
     * the uid of a statement that this statement refers to; by default it
     * refers to nothing (i.e. empty string); it cannot be null
     */
    private String referenceUid = "";

    // for serialization
    StatementMetadata() {
    }

    public StatementMetadata(Issuer issuer, StatementType type, String uid,
            int index, boolean isProtected, boolean conflict, boolean isPendingRule, String statementInfo, String inputValue) {
        this.issuer = issuer;
        this.type = type;
        this.uid = uid;
        this.index = index;
        this.isProtected = isProtected;
        this.conflict = conflict;
        this.isPendingRule = isPendingRule;
        this.inputValue = inputValue;
        this.statementInfo = statementInfo;
    }

    public StatementType getType() {
        return type;
    }

    public String getUid() {
        return uid;
    }

    public int getIndex() {
        return index;
    }

    protected void setIndex(int index) {
        this.index = index;
    }

    public String getReferenceUid() {
        return referenceUid;
    }

    public void setReferenceUid(String referenceUid) {
        // the referenceUid cannot be null
        if (referenceUid != null)
            this.referenceUid = referenceUid;
    }

    public Issuer getIssuer() {
        return issuer;
    }

    public void setIssuer(Issuer issuer) {
        this.issuer = issuer;
    }

    public boolean isProtected() {
        return isProtected;
    }

    public void setProtected(boolean isProtected) {
        this.isProtected = isProtected;
    }

    
    public boolean isConflict() {
        return conflict;
    }

    public void setConflict(boolean conflict) {
        this.conflict = conflict;
    }

    public boolean isPendingRule() {
        return isPendingRule;
    }

    public void setPendingRule(boolean isPendingRule) {
        this.isPendingRule = isPendingRule;
    }

    public String getStatementInfo() {
        return statementInfo;
    }

    public void setStatementInfo(String statementInfo) {
        this.statementInfo = statementInfo;
    }

    public String getInputValue() {
        return inputValue;
    }

    public void setInputValue(String inputValue) {
        this.inputValue = inputValue;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (conflict ? 1231 : 1237);
        result = prime * result + index;
        result = prime * result
                + ((inputValue == null) ? 0 : inputValue.hashCode());
        result = prime * result + (isPendingRule ? 1231 : 1237);
        result = prime * result + (isProtected ? 1231 : 1237);
        result = prime * result + ((issuer == null) ? 0 : issuer.hashCode());
        result = prime * result
                + ((referenceUid == null) ? 0 : referenceUid.hashCode());
        result = prime * result
                + ((statementInfo == null) ? 0 : statementInfo.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((uid == null) ? 0 : uid.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        StatementMetadata other = (StatementMetadata) obj;
        if (conflict != other.conflict)
            return false;
        if (index != other.index)
            return false;
        if (inputValue == null) {
            if (other.inputValue != null)
                return false;
        } else if (!inputValue.equals(other.inputValue))
            return false;
        if (isPendingRule != other.isPendingRule)
            return false;
        if (isProtected != other.isProtected)
            return false;
        if (issuer != other.issuer)
            return false;
        if (referenceUid == null) {
            if (other.referenceUid != null)
                return false;
        } else if (!referenceUid.equals(other.referenceUid))
            return false;
        if (statementInfo == null) {
            if (other.statementInfo != null)
                return false;
        } else if (!statementInfo.equals(other.statementInfo))
            return false;
        if (type != other.type)
            return false;
        if (uid == null) {
            if (other.uid != null)
                return false;
        } else if (!uid.equals(other.uid))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "StatementMetadata [type=" + type + ", issuer=" + issuer
                + ", uid=" + uid + ", isProtected=" + isProtected
                + ", isPendingRule=" + isPendingRule + ", statementInfo="
                + statementInfo + ", inputValue=" + inputValue + ", conflict="
                + conflict + ", index=" + index + ", referenceUid="
                + referenceUid + "]";
    }

   
}
