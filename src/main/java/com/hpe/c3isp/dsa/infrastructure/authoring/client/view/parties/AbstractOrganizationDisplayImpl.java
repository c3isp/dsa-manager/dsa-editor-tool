/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.view.parties;

import java.util.List;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.HasOpenHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.parties.OrganizationsDisplay;

public abstract class AbstractOrganizationDisplayImpl extends Composite
        implements OrganizationsDisplay {

    protected final ListBox allOrganizationsList = new ListBox(true);
    protected final Button okButton = new Button("OK");
    protected final Label messageLabel = new Label("");
    protected final DisclosurePanel dpChangeOrganizations = new DisclosurePanel(
            "Change");

    public AbstractOrganizationDisplayImpl() {
        final VerticalPanel dpvp = new VerticalPanel();
        dpvp.add(allOrganizationsList);
        dpvp.add(okButton);
        dpvp.add(messageLabel);

        dpChangeOrganizations.setAnimationEnabled(true);
        dpChangeOrganizations.setContent(dpvp);

    }

    @Override
    public void activate() {
        dpChangeOrganizations.setVisible(true);
    }

    @Override
    public void deactivate() {
        dpChangeOrganizations.setVisible(false);
    }

    @Override
    public Widget asWidget() {
        return this;
    }

    @Override
    public HasOpenHandlers<DisclosurePanel> getChangeOrganizationsPanel() {
        return dpChangeOrganizations;
    }

    @Override
    public void loadingOrganizations() {
        allOrganizationsList.clear();
        messageLabel.setText("Loading Organizations list from Trust "
                + "Manager, please wait ...");
    }

    @Override
    public void failure(String message) {
        allOrganizationsList.clear();
        messageLabel
                .setText("Error while contacting Trust Manager: " + message);
        dpChangeOrganizations.setOpen(false);
    }

    @Override
    public void showAvailableOrganizationNames(List<String> names) {
        for (String name : names)
            allOrganizationsList.addItem(name);
        allOrganizationsList.setSelectedIndex(0);
        messageLabel.setText("Choose the Organizations from the list");
    }

    @Override
    public void hideAvailableOrganizationNames() {
        dpChangeOrganizations.setOpen(false);
        messageLabel.setText("");
    }

    @Override
    public HasClickHandlers getOkButton() {
        return okButton;
    }

    @Override
    public int[] getSelectedOrganizations() {
        int n = 0;
        for (int i = 0; i < allOrganizationsList.getItemCount(); i++)
            if (allOrganizationsList.isItemSelected(i))
                n++;
        int[] selected = new int[n];
        for (int i = 0, j = 0; i < allOrganizationsList.getItemCount(); i++)
            if (allOrganizationsList.isItemSelected(i)) {
                selected[j] = i;
                j++;
            }
        return selected;
    }

}
