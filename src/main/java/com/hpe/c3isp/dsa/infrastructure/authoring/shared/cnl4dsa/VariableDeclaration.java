/**
//*  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa;

import com.google.gwt.core.client.GWT;

public class VariableDeclaration extends AbstractSyntacticItem {

	private String variable;
	private String syntacticForm;

	// for serialization
	VariableDeclaration() {}

	public VariableDeclaration(String uid, String humanForm, String variable, String value) {
		super(uid, humanForm, value);
		this.variable = variable;
		this.syntacticForm = variable + ":" + getFragmentOfUri(uid);
	}

	@Override
	public SyntacticItem copyOf() {
		return new VariableDeclaration(uid, humanForm, variable, value);
	}

	@Override
	public String toString() {
		String v = getValue();

		return "VariableDeclaration [syntacticForm=" + syntacticForm
				+ ", variable=" + variable + ", humanForm=" + humanForm
				+ ", uid=" + uid + ", value=" + value +"]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((syntacticForm == null) ? 0 : syntacticForm.hashCode());
		result = prime * result
				+ ((variable == null) ? 0 : variable.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		VariableDeclaration other = (VariableDeclaration) obj;
		if (syntacticForm == null) {
			if (other.syntacticForm != null)
				return false;
		} else if (!syntacticForm.equals(other.syntacticForm))
			return false;
		if (variable == null) {
			if (other.variable != null)
				return false;
		} else if (!variable.equals(other.variable))
			return false;
		return true;
	}

	@Override
	public String getSyntacticForm() {
		return syntacticForm;
	}

	public String getVariable() {
		return variable;
	}

	public String getType() {
		return uid;
	}

}
