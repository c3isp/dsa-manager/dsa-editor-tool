/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

import net.customware.gwt.dispatch.shared.ActionException;

import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.helper.VocabularyManager;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.helper.VocabularyOntology;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Functor;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItemTreeNode;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Term;

public abstract class AbstractVocabularyGetterHandler {

    private final SyntacticItemTreeNodeComparator comparator = new SyntacticItemTreeNodeComparator();

    protected List<SyntacticItemTreeNode> buildTermTreeNodes(
            Collection<String> syntacticItemUris, VocabularyOntology vocont) {
        List<SyntacticItemTreeNode> itemNodes = new ArrayList<SyntacticItemTreeNode>();
        for (String synitUri : syntacticItemUris) {
            String humanForm = vocont.getHumanFormOf(synitUri);
            boolean hasSubclasses = vocont.hasSubClasses(synitUri);           
            //TODO null?? value??
            Term term = new Term(synitUri, humanForm,null);
            itemNodes.add(new SyntacticItemTreeNode(term, hasSubclasses));
        }
        Collections.sort(itemNodes, comparator);
        return itemNodes;
    }

    protected List<SyntacticItemTreeNode> buildFunctorTreeNodes(
            Collection<String> syntacticItemUris, VocabularyOntology vocont) {
        List<SyntacticItemTreeNode> itemNodes = new ArrayList<SyntacticItemTreeNode>();
        for (String synitUri : syntacticItemUris) {
            String humanForm = vocont.getHumanFormOf(synitUri);
            boolean hasSubclasses = vocont.hasSubClasses(synitUri);
            Functor functor = new Functor(synitUri, humanForm);
            itemNodes.add(new SyntacticItemTreeNode(functor, hasSubclasses));
        }
        Collections.sort(itemNodes, comparator);
        return itemNodes;
    }

    protected VocabularyOntology getVocabularyOntology(String vocabularyUri)
            throws ActionException {
        VocabularyOntology vocont = VocabularyManager.getInstance()
                .getVocabularyOntology(vocabularyUri);
        if (vocont == null)
            throw new ActionException("cannot read vocabulary " + vocabularyUri);
        return vocont;
    }

    private final static class SyntacticItemTreeNodeComparator implements
            Comparator<SyntacticItemTreeNode> {

        @Override
        public int compare(SyntacticItemTreeNode o1, SyntacticItemTreeNode o2) {
            return o1.getSyntacticItem().getHumanForm()
                    .compareTo(o2.getSyntacticItem().getHumanForm());
        }

        public static <T, U extends Comparable<? super U>> Comparator<T> comparing(
                Function<? super T, ? extends U> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        public static <T, U> Comparator<T> comparing(
                Function<? super T, ? extends U> arg0,
                Comparator<? super U> arg1) {
            // TODO Auto-generated method stub
            return null;
        }

        public static <T> Comparator<T> comparingDouble(
                ToDoubleFunction<? super T> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        public static <T> Comparator<T> comparingInt(
                ToIntFunction<? super T> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        public static <T> Comparator<T> comparingLong(
                ToLongFunction<? super T> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        public static <T extends Comparable<? super T>> Comparator<T> naturalOrder() {
            // TODO Auto-generated method stub
            return null;
        }

        public static <T> Comparator<T> nullsFirst(Comparator<? super T> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        public static <T> Comparator<T> nullsLast(Comparator<? super T> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        public static <T extends Comparable<? super T>> Comparator<T> reverseOrder() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Comparator<SyntacticItemTreeNode> reversed() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Comparator<SyntacticItemTreeNode> thenComparing(
                Comparator<? super SyntacticItemTreeNode> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public <U extends Comparable<? super U>> Comparator<SyntacticItemTreeNode> thenComparing(
                Function<? super SyntacticItemTreeNode, ? extends U> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public <U> Comparator<SyntacticItemTreeNode> thenComparing(
                Function<? super SyntacticItemTreeNode, ? extends U> arg0,
                Comparator<? super U> arg1) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Comparator<SyntacticItemTreeNode> thenComparingDouble(
                ToDoubleFunction<? super SyntacticItemTreeNode> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Comparator<SyntacticItemTreeNode> thenComparingInt(
                ToIntFunction<? super SyntacticItemTreeNode> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Comparator<SyntacticItemTreeNode> thenComparingLong(
                ToLongFunction<? super SyntacticItemTreeNode> arg0) {
            // TODO Auto-generated method stub
            return null;
        }

    }

}
