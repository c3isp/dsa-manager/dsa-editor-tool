/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.parties;

import java.util.List;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.ControllablePresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Organization;

public interface PartiesPresenter extends
        ControllablePresenter<PartiesPresenter.PartiesDisplay> {

    public interface PartiesDisplay extends OrganizationsDisplay {
        // HasOpenHandlers<DisclosurePanel> getChangeOrganizationsPanel();
        //
        // void setOrganizationNames(List<String> organizationsNames);
        //
        // void clearOrganizationNames();
        //
        // void loadingOrganizations();
        //
        // void failure(String message);
        //
        // void showAvailableOrganizationNames(List<String> organizations);
        //
        // HasClickHandlers getOkButton();
        //
        // int[] getSelectedOrganizations();
        //
        // void hideAvailableOrganizationNames();
    }

    List<Organization> getParties();

    void setParties(List<Organization> organizations, String name, String role, String responsibilities);

    void setRole(String role);

    List<Organization> getAllAvailableOrganizations();

}
