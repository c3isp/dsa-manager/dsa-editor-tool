/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.fsm;

import java.util.ArrayList;
import java.util.List;

import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Token;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Tokens;

public abstract class AbstractCnlPrompterStateWithTermsAndTokenReference
        implements CnlPrompterState {

    protected final List<Token> tokens;

    public AbstractCnlPrompterStateWithTermsAndTokenReference() {
        tokens = new ArrayList<Token>();
        tokens.add(Tokens.FAKE_TOKEN_REFERENCE);
    }

    @Override
    public void handleReference(CnlPrompterContext context,
            SyntacticItem syntacticItem) {
        throw new UnsupportedOperationException();
    }

}
