/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.widget;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.roles.Operator;

public class RoleDefinitionEditor extends Composite {

    private final FlexTable table = new FlexTable();
    private final List<String> allCredentials = new ArrayList<String>();
    private final Operator[] operators = new Operator[] { Operator.AND,
            Operator.OR, Operator.END };

    private final OperatorChangeHandler operatorChangeHandler = new OperatorChangeHandler();

    public RoleDefinitionEditor() {
        initWidget(table);
    }

    public void setCredentials(List<String> allCredentials) {
        this.allCredentials.clear();
        this.allCredentials.addAll(allCredentials);
    }

    public void addCredentialExpression(int credentialIndex,
            String attributeExpression, Operator operator) {
        if (credentialIndex < allCredentials.size()) {
            final int row = table.getRowCount();
            final TextBox attributeExprTextBox = new TextBox();
            attributeExprTextBox.setTitle(".attribute operator value");
            attributeExprTextBox.addChangeHandler(new ChangeHandler() {

                @Override
                public void onChange(ChangeEvent event) {
                    String attributeExpr = attributeExprTextBox.getText();
                    if (!attributeExpr.startsWith("."))
                        attributeExprTextBox.setText("." + attributeExpr);
                }
            });
            attributeExprTextBox.setText(attributeExpression);
            table.setWidget(row, 0, buildCredentialsListBox(credentialIndex));
            table.setWidget(row, 1, attributeExprTextBox);
            table.setWidget(row, 2, buildOperatorsListBox(operator));
        }
    }

    public void addEmptyCredentialExpression() {
        addCredentialExpression(0, "", Operator.END);
    }

    public int getCredentialExpressionCount() {
        return table.getRowCount();
    }

    public int getCredentialIndexInExpression(int index) {
        Widget w = table.getWidget(index, 0);
        assert w instanceof ListBox;
        return ((ListBox) w).getSelectedIndex();
    }

    public String getAttributeInExpression(int index) {
        Widget w = table.getWidget(index, 1);
        assert w instanceof TextBox;
        return ((TextBox) w).getText();
    }

    public Operator getOperatorInExpression(int index) {
        Widget w = table.getWidget(index, 2);
        assert w instanceof ListBox;
        return operators[((ListBox) w).getSelectedIndex()];
    }

    public String getRoleDefinition() {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < table.getRowCount(); i++) {
            buffer.append(allCredentials.get(getCredentialIndexInExpression(i)));
            buffer.append(getAttributeInExpression(i));
            Operator op = getOperatorInExpression(i);
            if (!op.equals(Operator.END))
                buffer.append(" ").append(op.toString()).append(" ");
        }
        return buffer.toString();
    }

    public boolean isEmpty() {
        return table.getRowCount() == 0;
    }

    private ListBox buildCredentialsListBox(int selectedIndex) {
        final ListBox listBox = new ListBox(false);
        listBox.setVisibleItemCount(1);
        for (String credential : allCredentials)
            listBox.addItem(credential);
        listBox.setSelectedIndex(selectedIndex);
        return listBox;
    }

    private ListBox buildOperatorsListBox(Operator selectedOperator) {
        final ListBox listBox = new ListBox(false);
        listBox.setVisibleItemCount(1);
        for (int i = 0; i < operators.length; i++) {
            listBox.addItem(operators[i].toString());
            if (selectedOperator.equals(operators[i]))
                listBox.setSelectedIndex(i);
        }
        listBox.addChangeHandler(operatorChangeHandler);
        return listBox;
    }

    private final class OperatorChangeHandler implements ChangeHandler {

        @Override
        public void onChange(ChangeEvent event) {
            final Object s = event.getSource();
            assert s instanceof ListBox;
            final ListBox sourceListBox = (ListBox) s;
            final int selected = sourceListBox.getSelectedIndex();
            final String operator = sourceListBox.getItemText(selected);
            int row = 0;
            for (; row < table.getRowCount(); row++)
                if (table.getWidget(row, 2).equals(sourceListBox))
                    break;
            assert row != table.getRowCount();
            if (Operator.AND.toString().equals(operator)
                    || Operator.OR.toString().equals(operator)) {
                if (row == table.getRowCount() - 1)
                    addEmptyCredentialExpression();
            } else {
                row++;
                while (row < table.getRowCount())
                    table.removeRow(row);
            }
        }
    }

}
