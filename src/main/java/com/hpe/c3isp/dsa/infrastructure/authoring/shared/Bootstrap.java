/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.shared;

import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Organization;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Util;

public class Bootstrap extends Properties {
	private static final String propertiesFile = "etc/config/bootstrap.properties";
    private static Bootstrap THE_INSTANCE = null;
    private static final Log log = LogFactory.getLog(Bootstrap.class);

//    public String testProp = null;

    public static final String propSpring_application_name="spring.application.name"; //dsa-editor
    public static final String propSpring_cloud_config_uri="spring.cloud.config.uri";  //http://localhost:8888
    public static final String propSpring_cloud_config_username="spring.cloud.config.username";// configServer
    public static final String propSpring_cloud_config_password="spring.cloud.config.password"; //3fbc39fa-cc61-4f1b-a7a5-2b58f972fee7


    public String spring_application_name;
    public String spring_cloud_config_uri;
    public String spring_cloud_config_username;
    public String spring_cloud_config_password;


    private Bootstrap() {
    	log.info("---- Bootstrap Configuration initialization ----\n");
        InputStream is = null;
        try {
        	StringBuilder msg = new StringBuilder();
        	is = getClass().getClassLoader().getResourceAsStream(propertiesFile);
            this.load(is);

            spring_application_name = getProperty(propSpring_application_name);
            if(Util.isEmpty(spring_application_name)) {
            	msg.append(" " + propSpring_application_name + " not found\n");
            }

            spring_cloud_config_uri = getProperty(propSpring_cloud_config_uri);
            if(Util.isEmpty(spring_cloud_config_uri)) {
            	msg.append(" " + propSpring_cloud_config_uri + " not found\n");
            }


            spring_cloud_config_username = getProperty(propSpring_cloud_config_username);
            if(Util.isEmpty(spring_cloud_config_username)) {
            	msg.append(" " + propSpring_cloud_config_username + " not found\n");
            }

            spring_cloud_config_password = getProperty(propSpring_cloud_config_password);
            if(Util.isEmpty(spring_cloud_config_password)) {
            	msg.append(" " + propSpring_cloud_config_password + " not found\n");
            }


            if(msg.length() > 0) {
            	log.error("Configuration problems.\n" + msg.toString());
            }

            //debug
            msg.setLength(0);
            msg.append("Bootstrap Configuration loaded\n");

            msg.append(" spring_application_name:" + spring_application_name + "\n");
            msg.append(" spring_cloud_config_uri:" + spring_cloud_config_uri + "\n");
            msg.append(" spring_cloud_config_username:" + spring_cloud_config_username + "\n");
            msg.append(" spring_cloud_config_password: [XXXXXXXXXX]");

        	log.info(msg.toString());
        }catch (IOException e) {
            log.error("cannot read " + propertiesFile, e);
        }finally {
        	if(is != null) {
        		try {is.close();} catch (IOException e) {}
        	}
        }
    }

    public static synchronized Bootstrap getInstance() {
        if (THE_INSTANCE == null) {
            THE_INSTANCE = new Bootstrap();
        }
        return THE_INSTANCE;
    }

}
