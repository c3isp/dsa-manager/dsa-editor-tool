/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.click;

import com.google.gwt.event.shared.GwtEvent;

public class SelectedRowEvent extends GwtEvent<SelectedRowHandler> {

    public static Type<SelectedRowHandler> TYPE = new Type<SelectedRowHandler>();

    private final int row;

    public SelectedRowEvent(int row) {
        super();
        this.row = row;
    }

    public static void fire(HasSelectedRowHandlers source, int row) {
        SelectedRowEvent event = new SelectedRowEvent(row);
        source.fireEvent(event);
    }

    @Override
    protected void dispatch(SelectedRowHandler handler) {
        handler.onSelectedRow(row);
    }

    @Override
    public Type<SelectedRowHandler> getAssociatedType() {
        return TYPE;
    }

}
