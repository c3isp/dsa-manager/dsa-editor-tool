package com.hpe.c3isp.dsa.infrastructure.authoring.shared.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Util;

public class HumanFormHelper {
	//humanForm is something like:
	//IF a Subject hasId a Identifier OR that Subject hasId a Identifier THEN that Subject CAN Create a Data
	//output
	//IF a Subject hasId a Identifier_0 OR that Subject hasId a Identifier_1 THEN that Subject CAN Create a Data

	public static String prepareHumanForm(Set<String> params, String humanForm){
		GWT.log("prepareHumanForm() SRTART");
		GWT.log("   prepareHumanForm input, params=" + Util.setToString(params, "eMPTY") + " humanForm:" + humanForm);
		String ret = "";
		if(params == null || params.size() == 0 ||  Util.isEmpty(humanForm) ) {
			return ret;
		}
		ret = humanForm;
		for(String param:params) {
			String[] parsed = ret.split(param);
			if(parsed.length > 1) {
				StringBuffer sb = new StringBuffer();
				for (int i = 0; i < parsed.length; i++) {
					sb.append(parsed[i]);
					if(i < parsed.length - 1) {
						sb.append(param + "_" + i);
					}
				}
				ret = sb.toString();
			}
		}
		GWT.log("prepareHumanForm() END, return: " + ret);
		return ret;
	}

	//input value is of the form
	//"?X_29:PARAMNAME=VALUE|?X_30:PARAMNAME=VALUE|..."
	//separator is the | character
	//return a map PARAMNAME -> VALUE1, VALUE2.... keeping the order
	//example: inputvalue "?X_29:Identifier=iddd|?X_30:Identifier=iddd2|?X_31:Identifier=iddd3"
	//return Identifier -> iddd,iddd2
	public static Map<String, List<String>> parseInputValue(Set<String> params, String inputValue){
		Map<String, List<String>> ret = new HashMap<String, List<String>>();
		if(params == null || params.size() == 0 || Util.isEmpty(inputValue) ) {
			return ret;
		}
		final String sep = "\\" + InputValueHandler.INPUT_VAL_SEPARATOR;
		final String nameValueSep = "=";
		//first of all, tokenize input value string
		String[] parsed = inputValue.split(sep);
		final List<String> tokens = Arrays.asList(parsed);

		for(String param:params) {
			for(String token:tokens) {
				//token = "?X_29:Identifier=iddd"
				String[] nameVal = nameValueParse(token, nameValueSep);
				//nameVal[0] = "?X_29:Identifier", nameVal[1] = "iddd"
				if(nameVal != null) {
					final String paramName = nameVal[0];
					final String paramValue = nameVal[1];
					if(paramName.contains(param)) {
						List<String> values = ret.get(param);
						if(values == null) {
							//first value found
							values = new ArrayList<>();
							ret.put(param, values);
						}
						values.add(paramValue);
					}
				}
			}
		}

		return ret;
	}



	//input is: "panamname=value", return array [paramname][value]
	public static String[] nameValueParse(String input, String nameValueSeparator) {
		if(Util.isEmpty(input)||Util.isEmpty(nameValueSeparator)) {
			return null;
		}
		int i = input.indexOf(nameValueSeparator);
		if(i<=0) {
			return new String[] {input,""};
		}else {
			String val = input.substring(i+1);
			if(val == null) {
				val = "";
			}
			return new String[] {
				input.substring(0, i), //param name
				val	//param value
			};
		}

	}

}
