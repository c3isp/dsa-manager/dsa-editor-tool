/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.fsm;

import java.util.ArrayList;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.model.VocabularyChoicesUpdater;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Term;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Token;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableReference;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.vocabulary.GetLeafActionsWithTermAsSubject;

public class CnlPrompterState18 implements CnlPrompterState {

    private final Term subject;
    private final VocabularyChoicesUpdater vocabularyChoicesUpdater;

    public CnlPrompterState18(SyntacticItem previous) {
        assert previous != null;
        assert previous instanceof Term
                || previous instanceof VariableReference : "was expecting a Term or a VariableReference, got a "
                + previous.toString();
        subject = new Term(previous.getUid(), previous.getHumanForm(), previous.getValue());
        // Note: we're building a CNL AFTER-fragment here, and so we show to
        // the user only leaf-actions, that is those actions having no
        // sub-classes. If the user would select a non-leaf action, then, when
        // saving the DSA, we would have to expand such action using all its
        // subclasses, which, in turn, would yield a set
        // of CNL atomic-fragment linked with the PARALLEL operator. This is
        // impossible, because CNL grammar allows only an atomic fragment
        // after the token AFTER.
        vocabularyChoicesUpdater = new VocabularyChoicesUpdater(
                new ArrayList<Token>(), new GetLeafActionsWithTermAsSubject(
                        subject.getUid()));
    }

    @Override
    public VocabularyChoicesUpdater getVocabularyChoicesUpdater() {
        return vocabularyChoicesUpdater;
    }

    @Override
    public void handleReference(CnlPrompterContext context,
            SyntacticItem syntacticItem) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleSelection(CnlPrompterContext context,
            SyntacticItem selectedItem) {
        context.update(new CnlPrompterState19(selectedItem), selectedItem);
    }

}
