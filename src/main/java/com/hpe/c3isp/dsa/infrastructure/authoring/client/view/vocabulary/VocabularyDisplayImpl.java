///**
// *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
// *
// *  Licensed under the Apache License, Version 2.0 (the "License");
// *  you may not use this file except in compliance with the License.
// *  You may obtain a copy of the License at
// *
// *      http://www.apache.org/licenses/LICENSE-2.0
// *
// *  Unless required by applicable law or agreed to in writing, software
// *  distributed under the License is distributed on an "AS IS" BASIS,
// *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// *  See the License for the specific language governing permissions and
// *  limitations under the License.
// */
//package com.hpe.c3isp.dsa.infrastructure.authoring.client.view.vocabulary;
//
//import java.util.List;
//
//import com.google.gwt.dom.client.Style.Unit;
//import com.google.gwt.event.dom.client.ClickEvent;
//import com.google.gwt.event.dom.client.ClickHandler;
//import com.google.gwt.event.logical.shared.OpenEvent;
//import com.google.gwt.event.logical.shared.OpenHandler;
//import com.google.gwt.event.logical.shared.SelectionEvent;
//import com.google.gwt.event.logical.shared.SelectionHandler;
//import com.google.gwt.event.shared.HandlerRegistration;
//import com.google.gwt.user.client.ui.Button;
//import com.google.gwt.user.client.ui.DialogBox;
//import com.google.gwt.user.client.ui.DockLayoutPanel;
//import com.google.gwt.user.client.ui.HTML;
//import com.google.gwt.user.client.ui.HasHorizontalAlignment;
//import com.google.gwt.user.client.ui.HasVerticalAlignment;
//import com.google.gwt.user.client.ui.HorizontalPanel;
//import com.google.gwt.user.client.ui.Image;
//import com.google.gwt.user.client.ui.Label;
//import com.google.gwt.user.client.ui.ScrollPanel;
//import com.google.gwt.user.client.ui.Tree;
//import com.google.gwt.user.client.ui.TreeItem;
//import com.google.gwt.user.client.ui.VerticalPanel;
//import com.google.gwt.user.client.ui.Widget;
//import com.google.inject.Inject;
//import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.VocabularyPresenter.VocabularyDisplay;
//import com.hpe.c3isp.dsa.infrastructure.authoring.client.util.MyDialog;
//import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
//import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItemTreeNode;
//import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Tokens;
//
//public class VocabularyDisplayImpl extends DialogBox implements
//        VocabularyDisplay {
//
//	private static final String title = "Select one of the following choices";
//    private TreeItem root = null;
//
//	private VerticalPanel panelCenterWrapper = new VerticalPanel();
//	private DockLayoutPanel panelMainDock = new DockLayoutPanel(Unit.PX);
//	private Widget panelFooter;
//
//
//    @Inject
//    public VocabularyDisplayImpl() {
//        super(false, false);
////        super(true, true);
//        setAnimationEnabled(true);
//        center();
//        setText(title);
////      setText("Select one of the following choices");
//
//    	panelFooter = createFooterPanel();
//    	final ScrollPanel panelCenterScroll = new ScrollPanel();
//    	panelMainDock.addSouth(panelFooter, 60);
//    	panelCenterScroll.setSize("100%", "100%");
//    	panelCenterScroll.add(panelCenterWrapper);
////    	panelMainDock.add(panelCenterScroll);
//
//    	setWidget(panelMainDock);
//    }
//
//	private void updateCentralPanel(Widget w) {
//		panelCenterWrapper.clear();
//
//		panelCenterWrapper.setWidth("100%");
//		panelCenterWrapper.setHeight("100%");
//		panelCenterWrapper.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
//		panelCenterWrapper.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
//
//		if(w != null) {
//			panelCenterWrapper.add(w);
//		}
//		panelMainDock.getElement().getStyle().clearPosition();
//	}
//
//	private Widget createFooterPanel() {
//        Button close = new Button("Close");
//        close.addClickHandler(new ClickHandler() {
//            public void onClick(ClickEvent event) {
////                MyDialog.this.hide();
//            }
//        });
//		final HorizontalPanel hPanel = new HorizontalPanel();
////		hPanel.setStyleName("footer");
//		hPanel.add(close);
////		hPanel.setCellVerticalAlignment(htmlCp, VerticalPanel.ALIGN_MIDDLE);
////		hPanel.setCellVerticalAlignment(htmlVersion, VerticalPanel.ALIGN_BOTTOM);
//		return hPanel;
//	}
//
//
//    @Override
//    public Widget asWidget() {
//        return this;
//    }
//
//    @Override
//    public HandlerRegistration addSelectionHandler(
//            SelectionHandler<SyntacticItem> handler) {
//        return addHandler(handler, SelectionEvent.getType());
//    }
//
//    @Override
//    public HandlerRegistration addOpenHandler(OpenHandler<SyntacticItem> handler) {
//        return addHandler(handler, OpenEvent.getType());
//    }
//
//    @Override
//    public void show(List<SyntacticItemTreeNode> itemNodes) {
////        clear();
////        setWidget(buildTree(itemNodes));
//        updateCentralPanel(buildTree(itemNodes));
//        if (!isShowing()) {
//            show();
//        }
//    }
//
//    @Override
//    public void errorWhileFetchingData(String message) {
////        clear();
////        setWidget(new Label(message));
//    	updateCentralPanel(new Label(message));
//        if (!isShowing()) {
//            show();
//        }
//    }
//
//    @Override
//    public void errorWhileFetchingData(String message, SyntacticItem rootTerm) {
//        TreeItem rootTreeItem = searchTree(root, rootTerm);
//        assert rootTreeItem != null;
//        rootTreeItem.removeItems();
//        rootTreeItem.setText(message);
//        if (!isShowing()) {
//            show();
//        }
//    }
//
//    @Override
//    public void startFetchingData() {
////        clear();
////        setWidget(new Label("fetching data ..."));
//
//        updateCentralPanel(new Label("fetching data ..."));
//        if (!isShowing()) {
//            show();
//        }
//    }
//
//    @Override
//    public void startFetchingData(SyntacticItem rootTerm) {
//        TreeItem rootTreeItem = searchTree(root, rootTerm);
//        assert rootTreeItem != null;
//        rootTreeItem.setText("fetching data for " + rootTerm.getHumanForm());
//        if (!isShowing()) {
//            show();
//        }
//    }
//
//    @Override
//    public void showSubTerms(SyntacticItem rootTerm,
//            List<SyntacticItemTreeNode> itemNodes) {
//        TreeItem rootTreeItem = searchTree(root, rootTerm);
//        assert rootTreeItem != null;
//        rootTreeItem.setText(rootTerm.getHumanForm());
//        rootTreeItem.setUserObject(rootTerm);
//        updateTree(rootTreeItem, itemNodes);
//        rootTreeItem.setState(true);
//        if (!isShowing()) {
//            show();
//        }
//    }
//
//    private Tree buildTree(List<SyntacticItemTreeNode> itemNodes) {
//        Tree tree = new Tree();
//        tree.setAnimationEnabled(true);
//        root = new TreeItem();
//        root.setText("available choices ... ");
//        root.setUserObject(Tokens.FAKE_TOKEN_HAS_SUBTERMS);
//        tree.addItem(root);
//        updateTree(root, itemNodes);
//        // NOTE: we MUST call setState(true) for having the root node open, i.e.
//        // displaying its children. However, due to a bug, we must call it only
//        // after the tree is fully constructed, otherwise it does not work (see
//        // http://code.google.com/p/google-web-toolkit/issues/detail?id=1187
//        root.setState(true);
//        tree.addSelectionHandler(new SelectionHandler<TreeItem>() {
//
//            @Override
//            public void onSelection(SelectionEvent<TreeItem> event) {
//                // if the selected item has no parent, then the selected item
//                // is the root of our Tree, and we don't want to fire an
//                // event for it
//                if (event.getSelectedItem().getParentItem() != null) {
//                    Object o = event.getSelectedItem().getUserObject();
//                    assert o instanceof SyntacticItem;
//                    SelectionEvent.fire(VocabularyDisplayImpl.this,
//                            (SyntacticItem) o);
//                }
//            }
//        });
//        tree.addOpenHandler(new OpenHandler<TreeItem>() {
//
//            @Override
//            public void onOpen(OpenEvent<TreeItem> event) {
//                // if the node being opened is the root, then we don't want to
//                // fire an OpenEvent
//                if (event.getTarget().equals(root)) {
//                    return;
//                }
//                Object o = event.getTarget().getUserObject();
//                assert o instanceof SyntacticItem;
//                SyntacticItem openedSyntacticItem = (SyntacticItem) o;
//                TreeItem openedItem = event.getTarget();
//                assert openedItem.getChildCount() > 0;
//                TreeItem firstChild = openedItem.getChild(0);
//                assert firstChild.getUserObject() != null;
//                // if the first child of the opened node does not contain the
//                // fake-token-more, than we don't want to fire an OpenEvent
//                if (!firstChild.getUserObject().equals(
//                        Tokens.FAKE_TOKEN_HAS_SUBTERMS)) {
//                    return;
//                }
//                // if we get down here, than it means that the user has just
//                // opened a node whose sub-nodes have not been fetched yet, and
//                // so we fire an OpenEvent for our Presenter
//                OpenEvent.fire(VocabularyDisplayImpl.this, openedSyntacticItem);
//            }
//        });
//        return tree;
//    }
//
//    private void updateTree(TreeItem rootTreeItem,
//            List<SyntacticItemTreeNode> itemNodes) {
//        assert itemNodes.size() > 0;
//        rootTreeItem.removeItems();
//        for (SyntacticItemTreeNode sitn : itemNodes) {
//            SyntacticItem synit = sitn.getSyntacticItem();
//            TreeItem ti = new TreeItem();
//            ti.setText(synit.getHumanForm());
//            ti.setUserObject(synit);
//            rootTreeItem.addItem(ti);
//            if (sitn.hasChildren()) {
//                TreeItem more = new TreeItem();
//                more.setText(Tokens.FAKE_TOKEN_HAS_SUBTERMS.getHumanForm());
//                more.setUserObject(Tokens.FAKE_TOKEN_HAS_SUBTERMS);
//                ti.addItem(more);
//            }
//        }
//    }
//
//    private TreeItem searchTree(TreeItem root, SyntacticItem rootTerm) {
//        Object o = root.getUserObject();
//        assert o instanceof SyntacticItem;
//        SyntacticItem synit = (SyntacticItem) o;
//        if (synit.equals(rootTerm))
//            return root;
//        for (int i = 0; i < root.getChildCount(); i++) {
//            TreeItem ti = searchTree(root.getChild(i), rootTerm);
//            if (ti != null)
//                return ti;
//        }
//        return null;
//    }
//
//}
//







































/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.view.vocabulary;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.VocabularyPresenter.VocabularyDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItemTreeNode;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Tokens;

public class VocabularyDisplayImpl extends DialogBox implements
VocabularyDisplay {

	private static final String title = "Select one of the following choices";
	private TreeItem root = null;

	private VerticalPanel panelMain = null;
	private VerticalPanel panelCenterWrapper = null;

	//	private DockLayoutPanel panelMainDock = new DockLayoutPanel(Unit.PX);
	//	private Widget panelFooter;


	private void updateCentralPanel(Widget w) {
		GWT.log("VocabularyDisplayImpl updateCentralPanel");
		panelCenterWrapper.clear();

		panelCenterWrapper.setWidth("100%");
		panelCenterWrapper.setHeight("100%");
//		panelCenterWrapper.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
//		panelCenterWrapper.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);

		if(w != null) {
			panelCenterWrapper.add(w);
		}
		panelCenterWrapper.getElement().getStyle().clearPosition();
	}

	//	private Widget createFooterPanel() {
	//		GWT.log("VocabularyDisplayImpl createFooterPanel");
	//		Button close = new Button("Close");
	//		close.addClickHandler(new ClickHandler() {
	//			public void onClick(ClickEvent event) {
	//                fireCloseEvent();
	//			}
	//		});
	//		final HorizontalPanel hPanel = new HorizontalPanel();
	//		//	hPanel.setStyleName("footer");
	//		hPanel.add(close);
	//		//	hPanel.setCellVerticalAlignment(htmlCp, VerticalPanel.ALIGN_MIDDLE);
	//		//	hPanel.setCellVerticalAlignment(htmlVersion, VerticalPanel.ALIGN_BOTTOM);
	//		return hPanel;
	//	}


	private void customizeCaption() {

		// Create anchor we want to accept click events
		final Anchor myAnchor = new Anchor("X");

		// Add handler to anchor
		myAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				fireCloseEvent();
			}
		});
		myAnchor.setStyleName("dialog-close-button");
		myAnchor.setTitle("Close this dialog (remove the partial policy definition)");


		// Get caption element
		final HTML caption = ((HTML)this.getCaption());

		// Add anchor to caption
		caption.getElement().appendChild(myAnchor.getElement());

		// Add click handler to caption
		caption.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				// Get x,y caption click relative to the anchor
				final int x = event.getRelativeX(myAnchor.getElement());
				final int y = event.getRelativeY(myAnchor.getElement());

				// Check click was within bounds of anchor
				if(x >= 0 && y >= 0 &&
						x <= myAnchor.getOffsetWidth() &&
						y <= myAnchor.getOffsetHeight()) {
					// Raise event on anchor
					myAnchor.fireEvent(event);
				}
			}
		});
	}



	@Inject
	public VocabularyDisplayImpl() {
		super(false, false);
		//        super(true, true);
		setAnimationEnabled(true);
		center();
		setText(title);
		//      setText("Select one of the following choices");

		customizeCaption();


		if(panelMain == null) {
			GWT.log(" VocabularyDisplayImpl show first call...");
			panelMain = new VerticalPanel();
			panelCenterWrapper = new VerticalPanel();

			panelMain.add(panelCenterWrapper);

			setWidget(panelMain);
		}
	}

	@Override
	public Widget asWidget() {
		return this;
	}

	@Override
	public HandlerRegistration addSelectionHandler(
			SelectionHandler<SyntacticItem> handler) {
		return addHandler(handler, SelectionEvent.getType());
	}

	@Override
	public HandlerRegistration addOpenHandler(OpenHandler<SyntacticItem> handler) {
		return addHandler(handler, OpenEvent.getType());
	}


	//    //mr.
	//    private void updateMainContent(Widget w) {
	//        clear();
	//        setWidget(w);
	//    }

	//mr: codice realizzato in modo assolutamente sconclusionato e inestendibile, non c'e' un modo semplice per chiudere
	//il dialog prima della normale conclusione
	//se non implementando 12 interfacce ed estendendo 27 classi astratte, per un semplice bottone "cancel" da aggiungere a un dialog!!
	//unico modo per simulare un evento di chiusura e' il seguente: passo "null" nell'evento OpenEvent e chi riceve (classe VocabularyPresenterImpl)
	//l'evento controlla il valore, se null allora chiude il dialog, altrimenti continua cme prima
	private void fireCloseEvent() {
		GWT.log("VocabularyDisplayImpl.fireCloseEvent");
		OpenEvent.fire(VocabularyDisplayImpl.this, null);
	}


	@Override
	public void show(List<SyntacticItemTreeNode> itemNodes) {
		//    	clear();
		//      setWidget(buildTree(itemNodes));

		GWT.log("VocabularyDisplayImpl show(List<SyntacticItemTreeNode> itemNodes)");


		//    	if(panelCenterWrapper == null) {
		//	    	GWT.log(" VocabularyDisplayImpl show first call...");
		//	        panelCenterWrapper = new VerticalPanel();
		//	        panelFooter = createFooterPanel();
		//
		//	    	final ScrollPanel panelCenterScroll = new ScrollPanel();
		//	    	panelMainDock.addSouth(panelFooter, 60);
		//	    	panelCenterScroll.setSize("100%", "100%");
		//	    	panelCenterScroll.add(panelCenterWrapper);
		////
		//	        setWidget(panelMainDock);
		//    	}

		Tree t = buildTree(itemNodes);
//		final ScrollPanel panelCenterScroll = new ScrollPanel();
//		panelCenterScroll.setSize("100%", "100%");
//		panelCenterScroll.add(t);


		updateCentralPanel(t);


		if (!isShowing()) {
			show();
		}
	}

	@Override
	public void errorWhileFetchingData(String message) {
		GWT.log("VocabularyDisplayImpl errorWhileFetchingData");
		//    	clear();
		//        setWidget(new Label(message));
		updateCentralPanel(new Label(message));

		if (!isShowing()) {
			show();
		}
	}

	@Override
	public void errorWhileFetchingData(String message, SyntacticItem rootTerm) {
		TreeItem rootTreeItem = searchTree(root, rootTerm);
		assert rootTreeItem != null;
		rootTreeItem.removeItems();
		rootTreeItem.setText(message);
		if (!isShowing()) {
			show();
		}
	}

	@Override
	public void startFetchingData() {
		//        clear();
		//        setWidget(new Label("fetching data ..."));
		GWT.log("VocabularyDisplayImpl startFetchingData");


		updateCentralPanel(new Label("fetching data ..."));

		if (!isShowing()) {
			show();
		}
	}

	@Override
	public void startFetchingData(SyntacticItem rootTerm) {
		TreeItem rootTreeItem = searchTree(root, rootTerm);
		assert rootTreeItem != null;
		rootTreeItem.setText("fetching data for " + rootTerm.getHumanForm());
		if (!isShowing()) {
			show();
		}
	}

	@Override
	public void showSubTerms(SyntacticItem rootTerm, List<SyntacticItemTreeNode> itemNodes) {
		TreeItem rootTreeItem = searchTree(root, rootTerm);
		assert rootTreeItem != null;
		rootTreeItem.setText(rootTerm.getHumanForm());
		rootTreeItem.setUserObject(rootTerm);
		updateTree(rootTreeItem, itemNodes);
		rootTreeItem.setState(true);
		if (!isShowing()) {
			show();
		}
	}

	private Tree buildTree(List<SyntacticItemTreeNode> itemNodes) {
		Tree tree = new Tree();
		tree.setAnimationEnabled(true);
		root = new TreeItem();
		root.setText("available choices ... ");
		root.setUserObject(Tokens.FAKE_TOKEN_HAS_SUBTERMS);
		tree.addItem(root);
		updateTree(root, itemNodes);
		// NOTE: we MUST call setState(true) for having the root node open, i.e.
		// displaying its children. However, due to a bug, we must call it only
		// after the tree is fully constructed, otherwise it does not work (see
		// http://code.google.com/p/google-web-toolkit/issues/detail?id=1187
		root.setState(true);
		tree.addSelectionHandler(new SelectionHandler<TreeItem>() {

			@Override
			public void onSelection(SelectionEvent<TreeItem> event) {
				// if the selected item has no parent, then the selected item
				// is the root of our Tree, and we don't want to fire an
				// event for it
				if (event.getSelectedItem().getParentItem() != null) {
					Object o = event.getSelectedItem().getUserObject();
					assert o instanceof SyntacticItem;
					SelectionEvent.fire(VocabularyDisplayImpl.this,
							(SyntacticItem) o);
				}
			}
		});
		tree.addOpenHandler(new OpenHandler<TreeItem>() {

			@Override
			public void onOpen(OpenEvent<TreeItem> event) {
				// if the node being opened is the root, then we don't want to
				// fire an OpenEvent
				if (event.getTarget().equals(root)) {
					return;
				}
				Object o = event.getTarget().getUserObject();
				assert o instanceof SyntacticItem;
				SyntacticItem openedSyntacticItem = (SyntacticItem) o;
				TreeItem openedItem = event.getTarget();
				assert openedItem.getChildCount() > 0;
				TreeItem firstChild = openedItem.getChild(0);
				assert firstChild.getUserObject() != null;
				// if the first child of the opened node does not contain the
				// fake-token-more, than we don't want to fire an OpenEvent
				if (!firstChild.getUserObject().equals(
						Tokens.FAKE_TOKEN_HAS_SUBTERMS)) {
					return;
				}
				// if we get down here, than it means that the user has just
				// opened a node whose sub-nodes have not been fetched yet, and
				// so we fire an OpenEvent for our Presenter
				OpenEvent.fire(VocabularyDisplayImpl.this, openedSyntacticItem);
			}
		});
		return tree;
	}

	private void updateTree(TreeItem rootTreeItem, List<SyntacticItemTreeNode> itemNodes) {
		assert itemNodes.size() > 0;
		rootTreeItem.removeItems();
		for (SyntacticItemTreeNode sitn : itemNodes) {
			SyntacticItem synit = sitn.getSyntacticItem();
			TreeItem ti = new TreeItem();
			ti.setText(synit.getHumanForm());
			ti.setUserObject(synit);
			rootTreeItem.addItem(ti);
			if (sitn.hasChildren()) {
				TreeItem more = new TreeItem();
				more.setText(Tokens.FAKE_TOKEN_HAS_SUBTERMS.getHumanForm());
				more.setUserObject(Tokens.FAKE_TOKEN_HAS_SUBTERMS);
				ti.addItem(more);
			}
		}
	}

	private TreeItem searchTree(TreeItem root, SyntacticItem rootTerm) {
		Object o = root.getUserObject();
		assert o instanceof SyntacticItem;
		SyntacticItem synit = (SyntacticItem) o;
		if (synit.equals(rootTerm))
			return root;
		for (int i = 0; i < root.getChildCount(); i++) {
			TreeItem ti = searchTree(root.getChild(i), rootTerm);
			if (ti != null)
				return ti;
		}
		return null;
	}

}
