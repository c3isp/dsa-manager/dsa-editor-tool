/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.helper;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import java.io.ByteArrayOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class VocabularyUtilities {
    private static Log log = LogFactory.getLog(VocabularyUtilities.class);

    public static void executeSelect(Model model, String queryString,
            QuerySolutionProcessor querySolutionProcessor) {
        Query query = buildQuery(queryString);
        QueryExecution qe = QueryExecutionFactory.create(query, model);
        try {
            ResultSet rs = qe.execSelect();
            while (rs.hasNext()) {
                QuerySolution qs = rs.nextSolution();
                querySolutionProcessor.process(qs);
            }
        } finally {
            qe.close();
        }
    }

    public static boolean executeAsk(Model model, String queryString) {
        Query query = buildQuery(queryString);
        QueryExecution qe = QueryExecutionFactory.create(query, model);
        return qe.execAsk();
    }

    private static Query buildQuery(String queryString) {
        Query query = QueryFactory.create(queryString);

        if (query == null) {
            log.error("cannot build Query from \n[[\n" + queryString + "\n]]");
            return null;
        }

        if (log.isDebugEnabled()) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            query.serialize(baos);

            log.debug(baos.toString());
        }

        return query;
    }
}