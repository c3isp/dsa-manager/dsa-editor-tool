/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary;

import net.customware.gwt.dispatch.server.ActionHandler;
import net.customware.gwt.dispatch.server.ExecutionContext;
import net.customware.gwt.dispatch.shared.DispatchException;

import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.helper.VocabularyOntology;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.vocabulary.GetActionsWithTermAsSubject;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.vocabulary.VocabularyGetResult;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.vocabulary.VocabularyGetResultImpl;

public class GetActionsWithTermAsSubjectHandler extends
        AbstractVocabularyGetterHandler implements
        ActionHandler<GetActionsWithTermAsSubject, VocabularyGetResult> {

    @Override
    public VocabularyGetResult execute(GetActionsWithTermAsSubject action,
            ExecutionContext context) throws DispatchException {
        String termUri = action.getSyntacticItemUid();
        VocabularyOntology vocont = getVocabularyOntology(action
                .getSyntacticItemVocabularyUri());
        return new VocabularyGetResultImpl(buildFunctorTreeNodes(
                vocont.getActionsWithTermAsSubject(termUri), vocont));
    }

    @Override
    public Class<GetActionsWithTermAsSubject> getActionType() {
        return GetActionsWithTermAsSubject.class;
    }

    @Override
    public void rollback(GetActionsWithTermAsSubject action,
            VocabularyGetResult result, ExecutionContext context)
            throws DispatchException {
        // nothing to do: can't rollback a get-action
    }

}
