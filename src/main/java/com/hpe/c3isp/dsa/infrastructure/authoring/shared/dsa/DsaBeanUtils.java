/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableDeclaration;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.roles.Roles;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.Statement;

/**
 * Provides helper methods for {@link DsaBean}. We move this methods out of
 * {@link DsaBean} to make it smaller for serialization over the network.
 *
 * @author Marco Luca Sbodio (mailto:marco.sbodio@hp.com)
 *
 */
public class DsaBeanUtils {

    public static List<Statement> getAllStatements(DsaBean dsaBean) {
        List<Statement> statements = new ArrayList<Statement>();
        statements.addAll(dsaBean.getAuthorizations());
        statements.addAll(dsaBean.getDataSubjectAuthorizations());
        statements.addAll(dsaBean.getObligations());
        statements.addAll(dsaBean.getProhibitions());
        statements.addAll(dsaBean.getDerivedObjectsAuthorizations());
        statements.addAll(dsaBean.getDerivedObjectsObligations());
        statements.addAll(dsaBean.getDerivedObjectsProhibitions());
        return statements;
    }

    public static boolean containsEmptyTextFreeFields(DsaBean dsaBean){
        List<Statement> statementList = getAllStatements(dsaBean);
        for (Iterator<Statement> iterator = statementList.iterator(); iterator.hasNext();) {
            Statement statement = (Statement) iterator.next();
            if (statement.getStatementMetadata().isPendingRule() && statement.getStatementMetadata().getStatementInfo().isEmpty())
                return true;

        }
        return false;
    }

    public static long getMaxUidValue(DsaBean dsaBean) {
        long max = 0;
        List<Statement> statements = getAllStatements(dsaBean);
        for (Statement s : statements) {
            long i = getIndexOf(s.getUid());
            if (i > max)
                max = i;
            for (VariableDeclaration vd : s
                    .getSyntacticItems(VariableDeclaration.class)) {
                i = getIndexOf(vd.getVariable());
                if (i > max)
                    max = i;
            }
        }
        return max;
    }

    /*
     * Get the index part of a UID, that is the final integer attached to the
     * uid String. We might get a NumberFormatException here ... but we assume
     * that UID are well formed, so we silently ignore it.
     */
    private static long getIndexOf(String uid) {
        char[] cs = uid.toCharArray();
        int i = cs.length - 1;
        for (; i >= 0; i--)
            if (!Character.isDigit(cs[i]))
                break;
        return Long.parseLong(uid.substring(i + 1));
    }

    public static List<VariableDeclaration> getExplicitVariableDeclarations(
            DsaBean dsaBean) {
        List<VariableDeclaration> vds = new ArrayList<VariableDeclaration>();
        for (Statement statement : getAllStatements(dsaBean))
            vds.addAll(statement.getSyntacticItems(VariableDeclaration.class));
        return vds;
    }

    public static DsaBean createEmptyDsaBean(String dsaUid, String vocabularyUri, HashMap<String,String> paramTermList) {
    	final String defaultTitle = "new untitled DSA " + new Date().toString();
        DsaBean dsaBean = new DsaBean();
        dsaBean.setUid(dsaUid);
        dsaBean.setVocabularyUri(vocabularyUri);
        dsaBean.setTitle(defaultTitle);
        dsaBean.setValidity(getDefaultValidity());
        dsaBean.setCredentials(new Credentials());
        dsaBean.setRoles(new Roles());
        dsaBean.setParties(getDefaultParties());
        dsaBean.setAuthorizations(new ArrayList<Statement>());
        dsaBean.setDataSubjectAuthorizations(new ArrayList<Statement>());
        dsaBean.setObligations(new ArrayList<Statement>());
        dsaBean.setProhibitions(new ArrayList<Statement>());
        //ma che c... faiiiii
        //dsaBean.setDataClassification("Highly Confidential");//default is the first item in the list
        dsaBean.setGoverningLaw("");
        dsaBean.setIndemnities("");
        dsaBean.setUserConsent(false);
        dsaBean.setRequiredConsent(false);

        dsaBean.setPurpose("");
        dsaBean.setApplicationDomain("");
        dsaBean.setDataClassification("");


        dsaBean.setStatus("TEMPLATE");
        dsaBean.setExpirationPolicy(getDefaultComplexPolicy());
        dsaBean.setUpdatePolicy(getDefaultComplexPolicy());
        dsaBean.setRevocationPolicy(getDefaultComplexPolicy());
        dsaBean.setDescription("");
        dsaBean.setDerivedObjectsAuthorizations(new ArrayList<Statement>());
        dsaBean.setDerivedObjectsObligations(new ArrayList<Statement>());
        dsaBean.setDerivedObjectsProhibitions(new ArrayList<Statement>());
        dsaBean.setParamTermList(paramTermList);
        dsaBean.setVersion("0.0");
        return dsaBean;
    }

    public static List<Organization> getDefaultParties() {
        List<Organization> parties = new ArrayList<Organization>();
        parties.add(OrganizationUtils.getEmptyOrganization());
        return parties;
    }

    private static Validity getDefaultValidity() {
        Validity validity = new Validity();
        validity.setStartDate(new Date());
        validity.setEndDate(new Date());
        validity.setOfflineLicencesDurationInDays(1);
        return validity;
    }

    private static ComplexPolicy getDefaultComplexPolicy() {
        ComplexPolicy policy = new ComplexPolicy();
        policy.setPeriodInDays(0);
        policy.setPolicy("DenyAll");
        return policy;
    }

//    not used
//    public static List<String> getParamsTermFromVocabulary(String vocabularyUri){
//        VocabularyOntology vocont = VocabularyManager.getInstance()
//                .getVocabularyOntology(vocabularyUri);
//        return vocont.getParamTermList();
//    }
}
