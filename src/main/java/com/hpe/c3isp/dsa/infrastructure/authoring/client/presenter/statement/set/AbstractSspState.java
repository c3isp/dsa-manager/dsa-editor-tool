/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set;

import java.util.HashMap;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.move.MoveRowEvent.Direction;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.StatementSetDisplay;

public interface AbstractSspState<C extends AbstractSspContext<D>, D extends StatementSetDisplay> {

    void addStatement(C context);

    void handleStatementCompletedEvent(C context, int statementIndex,
            boolean isEnabled, boolean isEndUser, HashMap<String,String> params);

    void handleDeleteRowEvent(C context, int rowIndex);

    void handleMoveRowEvent(C context, Direction direction);

    void handleEnableRowMovementEvent(C context, int rowIndex);

    void handleDisableRowMovementEvent(C context);

    void handleStatementBrokenEvent(C context, int statementIndex);

    void handleCompleteMetadataEvent(C context, int rowIndex, boolean isEnabled, boolean isEndUser, HashMap<String,String> params);

 }
