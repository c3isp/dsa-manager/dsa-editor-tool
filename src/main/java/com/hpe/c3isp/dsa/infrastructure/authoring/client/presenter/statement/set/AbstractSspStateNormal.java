/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set;

import java.util.HashMap;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.Mode;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.move.MoveRowEvent.Direction;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.util.UIDGenerator;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.StatementSetDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.Statement;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.StatementMetadata;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.StatementType;

public abstract class AbstractSspStateNormal<C extends AbstractSspContext<D>, D extends StatementSetDisplay, S extends AbstractSspState<C, D>>
        implements AbstractSspState<C, D> {

    @Override
    public void addStatement(C context) {
        final int index = context.getStatementPresentersCount();
        final StatementType statementType = context.getStatementType();
        final String uidPrefix = statementType.toString() + "_";
        String uid = UIDGenerator.getInstance().getNextUid(uidPrefix);
        //default value for isProtected, conflict and isPendingRule attributes are false
        final Statement statement = new Statement(new StatementMetadata(null,
                statementType, uid, index,false, false,false,"",""), null);
        context.addStatementPresenterFor(statement);
    }

    @Override
    public void handleDeleteRowEvent(C context, int rowIndex) {
        assert rowIndex >= 0
                && rowIndex < context.getStatementPresentersCount() : "deleting unexpected row "
                + rowIndex + " of " + context.getStatementPresentersCount();
        context.removeStatementPresenterAt(rowIndex);
        context.getDisplay().deleteRow(rowIndex);
    }

    @Override
    public void handleDisableRowMovementEvent(C context) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleEnableRowMovementEvent(C context, int rowIndex) {
        context.getDisplay().highlightStatement(rowIndex, Mode.ON);
        context.getDisplay().showStatementMoveTools();
        makeTransitionToStateMoving(context, rowIndex);
    }

    protected abstract void makeTransitionToStateMoving(C context, int rowIndex);

    @Override
    public void handleMoveRowEvent(C context, Direction direction){
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleStatementBrokenEvent(C context, int statementIndex) {
        context.removeStatementPresenterAt(statementIndex);
        context.getDisplay().deleteRow(statementIndex);
    }

    @Override
    public void handleStatementCompletedEvent(C context, int statementIndex,
            boolean isEnabled, boolean isEndUser, HashMap<String,String> params){
        context.getDisplay().addStatementControlWidgets(statementIndex,
         isEnabled, context.getStatementPresenterAt(statementIndex).getStatement(),isEndUser,  params);
        }

    @Override
    public void handleCompleteMetadataEvent(C context, int statementIndex,
            boolean isEnabled, boolean isEndUser, HashMap<String,String> params){
        context.getDisplay().addStatementControlWidgets(statementIndex,
                isEnabled, context.getStatementPresenterAt(statementIndex).getStatement(), isEndUser, params);
        }

}
