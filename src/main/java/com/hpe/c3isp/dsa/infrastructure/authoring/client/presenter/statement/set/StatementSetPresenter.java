/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set;

import java.util.HashMap;
import java.util.List;

import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableDeclaration;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableReference;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.Statement;

public interface StatementSetPresenter {

    List<Statement> getStatements();

    void setStatements(List<Statement> statements, HashMap<String,String> list);

    /**
     * Updates every {@link Statement} in the set, by removing references to the
     * passed statement, and by turning the first {@link VariableReference
     * referring to a {@link VariableDeclaration} in the list toBeRemoved into a
     * corresponding {@link VariableDeclaration} (this eliminates references to
     * terms declared into the passed statement). Note: if a
     * {@link VariableReference} VR referring to a {@link VariableDeclaration}
     * VD in the list toBeRemoved is turned into a corresponding
     * {@link VariableDeclaration}, the VD is removed from the list toBeRemoved.
     * 
     * @param statement
     *            , a statement being deleted
     * @param toBeRemoved
     *            , a list of {@link VariableDeclaration} to be checked for
     *            {@link VariableReference}; this list is updated.
     */
    void removeReferencesTo(Statement statement,
            List<VariableDeclaration> toBeRemoved);

    void setUserRole(String role);
    
    void setParamsTermList(HashMap<String,String> params);

}
