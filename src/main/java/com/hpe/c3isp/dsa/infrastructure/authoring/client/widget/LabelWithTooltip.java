package com.hpe.c3isp.dsa.infrastructure.authoring.client.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.Resources;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Util;

public class LabelWithTooltip extends HorizontalPanel {

	private void _contructor(String txt, final String tooltip, String[] additionalStyles) {
		Resources resources = GWT.create(Resources.class);
		final Image qmark = new Image(resources.questionMark16());
		final Label label = new Label(txt);
//		qmark.addStyleName("tooltipIcon");
//		String definedStyles = qmark.getElement().getAttribute("style");
//		qmark.getElement().setAttribute("style", definedStyles + "; vertical-align:middle;");

		if(additionalStyles != null) {
			for(String s:additionalStyles) {
				label.addStyleName(s);
			}
		}
		if(!Util.isEmpty(tooltip)) {
			label.setTitle(tooltip);
		}
		qmark.addClickHandler(new ClickHandler() {
		      public void onClick(ClickEvent event) {
//		    	  final int X = event.getScreenX();
//		    	  final int Y = event.getScreenY();
		    	  // Instantiate the popup and show it.
		    	  final PopupPanel popup = new PopupPanel(true, true);
		    	  popup.setAnimationEnabled(true);
		    	  popup.setGlassEnabled(true);
		    	  popup.setWidget(new Label(tooltip));
//		    	  popup.setPopupPosition(X, Y);
//		    	  popup.setPopupPositionAndShow(new PopupPanel.PositionCallback() {
//		    		  public void setPosition(int offsetWidth, int offsetHeight) {
//		    			  int left = (Window.getClientWidth() - offsetWidth) / 3;
//		    			  int top = (Window.getClientHeight() - offsetHeight) / 3;
//		    			  //								popup.setPopupPosition(left, top);
//		    			  popup.setPopupPosition(X, Y);
//		    		  }
//		    	  }
//		    			  );
		    	  popup.setPopupPositionAndShow(new PopupPanel.PositionCallback() {
	                    public void setPosition(int offsetWidth, int offsetHeight) {
	                        popup.showRelativeTo(qmark);
	                    }
	                });


//				popup.show();
		      }
		    }
		);

		setVerticalAlignment(HorizontalPanel.ALIGN_MIDDLE);
		add(qmark);
		add(label);

	}

	public LabelWithTooltip(String txt, final String tooltip) {
		this._contructor(txt, tooltip, null);
	}

	public LabelWithTooltip(String txt, final String tooltip, String additionalStyle) {
		String[] styles = Util.isEmpty(additionalStyle)?null:new String[] {additionalStyle};
		this._contructor(txt, tooltip, styles);
	}

	public LabelWithTooltip(String txt, final String tooltip, String[] additionalStyles) {
		this._contructor(txt, tooltip, additionalStyles);
	}

}
