/**
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.enunes.gwt.mvp.client.EventBus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.SingleSelectionModel;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.presenter.BusyDisplayHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.presenter.IdleDisplayHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.statement.StatementDeletedEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.statement.StatementDeletedHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.gin.MyGinjector;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.crossref.CrossReferencePresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.parties.PartiesPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.StatementSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.authorizations.AuthorizationsSetPresenter;
//import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.datasubjectauthorizations.DataSubjectAuthorizationsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.obligations.ObligationsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.prohibitions.ProhibitionsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.thirdpartiesauthorizations.ThirdPartiesAuthorizationsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.thirdpartiesobligations.ThirdPartiesObligationsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.thirdpartiesprohibitions.ThirdPartiesProhibitionsSetPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.validity.ValidityPresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.helper.ReferenceableTermsKeeper;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.util.HtmlDialog;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.util.UIDGenerator;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.util.WidgetUtil;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.ControllableDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.widget.LabelWithTooltip;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.widget.MessagePanel;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.Config;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Functor;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableDeclaration;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.dsa.CreateNewDsaAction;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.dsa.FetchDsaAction;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.dsa.StoreDsaAction;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.vocabularylist.GetVocabulariesAction;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.dsa.CreateNewDsaResult;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.dsa.FetchDsaResult;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.dsa.StoreDsaResult;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.trustmanager.GetOrganizationsResult;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.vocabularylist.GetVocabulariesResult;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.DsaBean;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.DsaBeanUtils;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Organization;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.RepositoryContent;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Util;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Validity;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.Issuer;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.Statement;

import net.customware.gwt.dispatch.client.DispatchAsync;
import net.customware.gwt.dispatch.shared.Action;

//simulare chiamata redirect da DSAEditor
//http://127.0.0.1:8881/DSAAuthoringTool.html?issuer=LegalExpert&userID=f062e953a00b450b8799a4752aec22c6&dsaId=&action=Create&target=https://dsamgrc3isp.iit.cnr.it/DSAEditor
//simulare lettura da remoto
//http://127.0.0.1:8881/DSAAuthoringTool.html/?issuer=PolicyExpert&userID=59000000000000000000000000000001&dsaId=DSA-89480276-65bc-4504-a358-6bab604e882b.xml&action=Read&target=https://dsamgrc3isp.iit.cnr.it/DSAEditor


/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
@ComponentScan
public class DSAAuthoringTool implements EntryPoint, BusyDisplayHandler, IdleDisplayHandler {
	private DsaAuthoringToolMessages messages = GWT.create(DsaAuthoringToolMessages.class);
	private final MyGinjector ginjector = GWT.create(MyGinjector.class);
	private final List<ControllableDisplay> controllableDisplays = new ArrayList<ControllableDisplay>();
	private final List<StatementSetPresenter> statementSetPresenters = new ArrayList<StatementSetPresenter>();
	private AuthorizationsSetPresenter authorizationsSetPresenter = ginjector.getAuthorizationsPresenter();
	private ObligationsSetPresenter obligationsSetPresenter = ginjector.getObligationsPresenter();
	private ProhibitionsSetPresenter prohibitionsSetPresenter = ginjector.getProhibitionsSetPresenter();
	private final ValidityPresenter validityPresenter = ginjector.getValidityPresenter();

	//_MR
	// third parties policies
	private ThirdPartiesAuthorizationsSetPresenter thirdPartiesAuthorizationsSetPresenter = ginjector.getThirdPartiesAuthorizationsPresenter();
	private ThirdPartiesObligationsSetPresenter thirdPartiesObligationsSetPresenter = ginjector.getThirdPartiesObligationsPresenter();
	private ThirdPartiesProhibitionsSetPresenter thirdPartiesProhibitionsSetPresenter = ginjector.getThirdPartiesProhibitionsPresenter();


	private final PartiesPresenter partiesPresenter = ginjector.getPartiesPresenter();
	final CrossReferencePresenter crossReferencePresenter = ginjector.getCrossReferencePresenter();
	private final Button saveButton = new Button(messages.buttonSave());
	public String dsaUuid;
//	FileUpload vocabulary;
	private final Label dsaUid = new Label(messages.labelUID());
	private final Label dsaVocabulary = new Label(messages.labelVocabularyURI());

//	private final Label dsaDataClassification = new Label(messages.labelDataClassification());

	private final Label dsaTitle = new Label(messages.labelTitle());
	private final Label dsaStatus = new Label(messages.labelStatus());
	private final TextArea dsaDescription = new TextArea();
//	private final TextBox dsaPurpose = new TextBox();

//	//mr
//	private final TextBox dsaApplicationDomain = new TextBox();


	private final CheckBox consentCheckBox = new CheckBox();
	private final CheckBox askForConsentCheckBox = new CheckBox();
	private Label textToServerLabel = new Label();
	private String userRole;
	private String version;
//	private String selectedVocabularyUri;



	private int selectedPurpose;
	private ArrayList<String> purposeList = new ArrayList<String>();



	private int selectedDataClassification;
	private ArrayList<String> dataClassificationList = new ArrayList<String>();


	//mr
	private int selectedApplicationDomain;
	private ArrayList<String> applicationDomainList = new ArrayList<String>();

//	private String selectedApplicationDomainValue;




	private int selectedRevocationPolicy;
	private int selectedUpdatePolicy;
	private int selectedExpirationPolicy;
	private DsaBean dsaBean;
	private String returnUrl;
	private boolean isStandalone;
	private final TextBox revocationPolicy = new TextBox();
	private final TextBox revocationPolicyAttribute = new TextBox();
	private boolean revocationPeriodEnabled;
	private final TextBox updatePolicy = new TextBox();
	private final TextBox updatePolicyAttribute = new TextBox();
	private boolean updatePeriodEnabled;
	private final TextBox expirationPolicy = new TextBox();
	private final TextBox expirationPolicyAttribute = new TextBox();
	private boolean expirationPeriodEnabled;

	private ArrayList<String> policyList = new ArrayList<String>();
	private final DsaManagerServiceAsync dsaManagerService = GWT.create(DsaManagerService.class);
	private final Resources resources = GWT.create(Resources.class);

	//	/**
	//	 * This is the entry point method.
	//	 */
	//	public void onModuleLoad() {
	//		bind();
	//		// load default stylesheet
	//		resources.defaultCss().ensureInjected();
	//		dsaManagerService.getVersion(version, new AsyncCallback<String>() {
	//			public void onFailure(Throwable caught) {
	//				Window.alert(messages.messageLoadingApplicationVersion());
	//			}
	//
	//			public void onSuccess(String result) {
	//			}
	//		});
	//		// TODO install maven plugin for
	//		// Label pomVersion= new Label("Version: 1.0.0 "+ version );
	//		Label pomVersion = new Label("Version: 1.0 ");
	//		pomVersion.setStyleName("version");
	//		RootPanel.get("footer").add(pomVersion);
	//		HorizontalPanel hPanel = new HorizontalPanel();
	//		HorizontalPanel hLogoPanel = new HorizontalPanel();
	//		Image image = new Image(resources.logo());
	//		// image.setStyleName("logo");
	//		hLogoPanel.add(image);
	//		HTMLPanel header = new HTMLPanel("h1", messages.header());
	//		hPanel.add(header);
	//		Image userManual = new Image(resources.userManualDownload());
	//		userManual.setStyleName("download");
	//		userManual.setTitle(messages.tooltipHelpDownload());
	//		userManual.addClickHandler(new ClickHandler() {
	//			@Override
	//			public void onClick(ClickEvent event) {
	//				String path = GWT.getHostPageBaseURL() + "DSA Editor User Manual.pdf";
	//				Window.open(path, "User Manual", "");
	//			}
	//		});
	//		RootPanel.get("header").add(userManual);
	//		RootPanel.get("header").add(hLogoPanel);
	//		RootPanel.get("header").add(hPanel);
	//		String issuer = Window.Location.getParameter("issuer");
	//		String dsaId = Window.Location.getParameter("dsaId");
	//		String action = Window.Location.getParameter("action");
	//		String target = Window.Location.getParameter("target");
	//		String userID = Window.Location.getParameter("userID");
	//		String stylesheet = Window.Location.getParameter("stylesheet");
	//		initList();
	//
	//		if ((issuer == null) || (action == null) || (target == null) || (userID == null)) {
	//			// DSAAT in a standalone version: note that dsaId could be null in
	//			// case of create a dsa template
	//			loadRolePanel();
	//			isStandalone = true;
	//		} else {
	//			isStandalone = false;
	//			// load custom stylesheet
	//			if (stylesheet != null){
	//				if (stylesheet.equals(Resources.HEALTHCARE_CUSTOM_CSS))
	//					resources.customHealthcareCss().ensureInjected();
	//				if (stylesheet.equals(Resources.MOBILE_CUSTOM_CSS))
	//					resources.customMobileCss().ensureInjected();
	//				if (stylesheet.equals(Resources.PA_CUSTOM_CSS))
	//					resources.customPACss().ensureInjected();
	//			}
	//
	//			/**
	//			 * DSAAT as a service sample of url:
	//			 * https://<hostname:port>/DSAAuthoringTool
	//			 * /?issuer=<user_role>&userID=<user_id value>&dsaId=
	//			 * <dsa_file_name>& action=<action_string>&target=<url>&<stylesheet>
	//			 *
	//			 * where issuer = User | LegalExpert | PolicyExpert action = Read |
	//			 * Create | Update stylesheet TODO: define unique string for issuer
	//			 **/
	//			if (issuer.compareTo(Issuer.LEGAL_EXPERT.getRole()) == 0)
	//				userRole = Issuer.LEGAL_EXPERT.getLabel();
	//			else if (issuer.compareTo(Issuer.POLICY_EXPERT.getRole()) == 0)
	//				userRole = Issuer.POLICY_EXPERT.getLabel();
	//			else
	//				userRole = issuer;
	//
	//			returnUrl = target;
	//			redirect(issuer, dsaId, action, userID);
	//		}
	//	}



	private Widget panelHeader;
	//	private FlexTable panelCenterWrapper = new FlexTable();
	private VerticalPanel panelCenterWrapper = new VerticalPanel();



	private Widget panelFooter;
	private DockLayoutPanel panelMainDock = new DockLayoutPanel(Unit.PX);
	private Label labelUserInfo = new Label();




	@Value("${setting1}")
	private String setting1;

	@Value("${info.build.artifact}")
	private String infoBuildArtifact;


	@Value("${info.build.name}")
	private String infoBuildNAme;


	private String vocabularyValues;

	private void populateVocabularyValues() {
		final DispatchAsync dispatcher = ginjector.getDispatchAsync();
		dispatcher.execute(new GetVocabulariesAction(), new AsyncCallback<GetVocabulariesResult>() {

			@Override
			public void onFailure(Throwable caught) {
//				display.failure(caught.getMessage());
			}

			@Override
			public void onSuccess(GetVocabulariesResult result) {
				GWT.log("onSuccess: populateVocabularyValues ");
//				allOrganizations = result.getOrganizations();
//				display.showAvailableOrganizationNames(getOrganizationsNames(allOrganizations));
				vocabularyValues = result.getVocabularies();
			}
		});
	}






	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		bind();

		// load default stylesheet
		resources.defaultCss().ensureInjected();
		dsaManagerService.getVersion(version, new AsyncCallback<String>() {
			public void onFailure(Throwable caught) {
				Window.alert(messages.messageLoadingApplicationVersion());
			}

			public void onSuccess(String result) {
			}
		});

//		populateVocabularyValues();


		final DispatchAsync dispatcher = ginjector.getDispatchAsync();
		dispatcher.execute(new GetVocabulariesAction(), new AsyncCallback<GetVocabulariesResult>() {

			@Override
			public void onFailure(Throwable caught) {
//				display.failure(caught.getMessage());
			}

			@Override
			public void onSuccess(GetVocabulariesResult result) {
				GWT.log("onSuccess: populateVocabularyValues ");
//				allOrganizations = result.getOrganizations();
//				display.showAvailableOrganizationNames(getOrganizationsNames(allOrganizations));
				vocabularyValues = result.getVocabularies();
				initializeModule();
			}
		});




		//		updateUserInfo("isStandalone vale " + isStandalone);
	}

	private void initializeModule() {
		RootPanel root = RootPanel.get("maincontent");
		panelHeader = createHeaderPanel();
		panelFooter = createFooterPanel();

		final ScrollPanel panelCenterScroll = new ScrollPanel();
		panelMainDock.addNorth(panelHeader, 57);
		panelMainDock.addSouth(panelFooter, 60);
		//		panelMainDock.add(panelCenterWrapper);
		panelCenterScroll.setSize("100%", "100%");
		panelCenterScroll.add(panelCenterWrapper);
		panelMainDock.add(panelCenterScroll);

		root.add(panelMainDock);

		// DockLayoutPanel forcibly conflicts with sensible layout control, and
		// sticks inline styles on elements without permission. They must be
		// cleared.
		panelMainDock.getElement().getStyle().clearPosition();

		updateCentralPanel(null);

		String issuer = Window.Location.getParameter("issuer");
		String dsaId = Window.Location.getParameter("dsaId");
		String action = Window.Location.getParameter("action");
		String target = Window.Location.getParameter("target");
		String userID = Window.Location.getParameter("userID");
		String stylesheet = Window.Location.getParameter("stylesheet");

		final String dbg =
				"onModuleLoad, props read from Window.getParameter"
				+ "\n issuer=" + issuer == null?"NULL":issuer
				+ "\n dsaId=" + dsaId == null?"NULL":dsaId
				+ "\n action=" + action == null?"NULL":action
				+ "\n target=" + target == null?"NULL":target
				+ "\n userID=" + userID == null?"NULL":userID;
		GWT.log(dbg);
		initList();

		if ((issuer == null) || (action == null) || (target == null) || (userID == null)) {
			// DSAAT in a standalone version: note that dsaId could be null in
			// case of create a dsa template

			loadRolePanel();
			isStandalone = true;
		} else {
			isStandalone = false;
			// load custom stylesheet
			if (stylesheet != null){
				if (stylesheet.equals(Resources.HEALTHCARE_CUSTOM_CSS))
					resources.customHealthcareCss().ensureInjected();
				if (stylesheet.equals(Resources.MOBILE_CUSTOM_CSS))
					resources.customMobileCss().ensureInjected();
				if (stylesheet.equals(Resources.PA_CUSTOM_CSS))
					resources.customPACss().ensureInjected();
			}

			/**
			 * DSAAT as a service sample of url:
			 * https://<hostname:port>/DSAAuthoringTool
			 * /?issuer=<user_role>&userID=<user_id value>&dsaId=
			 * <dsa_file_name>& action=<action_string>&target=<url>&<stylesheet>
			 *
			 * where issuer = User | LegalExpert | PolicyExpert action = Read |
			 * Create | Update stylesheet TODO: define unique string for issuer
			 **/
			if (issuer.compareTo(Issuer.LEGAL_EXPERT.getRole()) == 0) {
				userRole = Issuer.LEGAL_EXPERT.getLabel();
			}
			else if (issuer.compareTo(Issuer.POLICY_EXPERT.getRole()) == 0) {
				userRole = Issuer.POLICY_EXPERT.getLabel();
			}
			else {
				userRole = issuer;
			}

			returnUrl = target;
			redirect(issuer, dsaId, action, userID);
		}
	}












//	/**
//	 * This is the entry point method.
//	 */
//	public void onModuleLoad() {
//		bind();
//
//		// load default stylesheet
//		resources.defaultCss().ensureInjected();
//		dsaManagerService.getVersion(version, new AsyncCallback<String>() {
//			public void onFailure(Throwable caught) {
//				Window.alert(messages.messageLoadingApplicationVersion());
//			}
//
//			public void onSuccess(String result) {
//			}
//		});
//
//		populateVocabularyValues();
//
////		// TODO install maven plugin for
////		GWT.log("DsaManagerServiceImpl getVersion called....................... "
////		 		+ " setting1=" + setting1
////		 		+ " info.build.artifact " + infoBuildArtifact
////		 		+ " info.build.name " + infoBuildNAme
////				);
//
//
//
//		RootPanel root = RootPanel.get("maincontent");
//		panelHeader = createHeaderPanel();
//		panelFooter = createFooterPanel();
//
//		final ScrollPanel panelCenterScroll = new ScrollPanel();
//		panelMainDock.addNorth(panelHeader, 57);
//		panelMainDock.addSouth(panelFooter, 60);
//		//		panelMainDock.add(panelCenterWrapper);
//		panelCenterScroll.setSize("100%", "100%");
//		panelCenterScroll.add(panelCenterWrapper);
//		panelMainDock.add(panelCenterScroll);
//
//		root.add(panelMainDock);
//
//		// DockLayoutPanel forcibly conflicts with sensible layout control, and
//		// sticks inline styles on elements without permission. They must be
//		// cleared.
//		panelMainDock.getElement().getStyle().clearPosition();
//
//		updateCentralPanel(null);
//
//		String issuer = Window.Location.getParameter("issuer");
//		String dsaId = Window.Location.getParameter("dsaId");
//		String action = Window.Location.getParameter("action");
//		String target = Window.Location.getParameter("target");
//		String userID = Window.Location.getParameter("userID");
//		String stylesheet = Window.Location.getParameter("stylesheet");
//
//		final String dbg =
//				"onModuleLoad, props read from Window.getParameter"
//				+ "\n issuer=" + issuer == null?"NULL":issuer
//				+ "\n dsaId=" + dsaId == null?"NULL":dsaId
//				+ "\n action=" + action == null?"NULL":action
//				+ "\n target=" + target == null?"NULL":target
//				+ "\n userID=" + userID == null?"NULL":userID;
//		GWT.log(dbg);
//		initList();
//
//		if ((issuer == null) || (action == null) || (target == null) || (userID == null)) {
//			// DSAAT in a standalone version: note that dsaId could be null in
//			// case of create a dsa template
//
//			loadRolePanel();
//			isStandalone = true;
//		} else {
//			isStandalone = false;
//			// load custom stylesheet
//			if (stylesheet != null){
//				if (stylesheet.equals(Resources.HEALTHCARE_CUSTOM_CSS))
//					resources.customHealthcareCss().ensureInjected();
//				if (stylesheet.equals(Resources.MOBILE_CUSTOM_CSS))
//					resources.customMobileCss().ensureInjected();
//				if (stylesheet.equals(Resources.PA_CUSTOM_CSS))
//					resources.customPACss().ensureInjected();
//			}
//
//			/**
//			 * DSAAT as a service sample of url:
//			 * https://<hostname:port>/DSAAuthoringTool
//			 * /?issuer=<user_role>&userID=<user_id value>&dsaId=
//			 * <dsa_file_name>& action=<action_string>&target=<url>&<stylesheet>
//			 *
//			 * where issuer = User | LegalExpert | PolicyExpert action = Read |
//			 * Create | Update stylesheet TODO: define unique string for issuer
//			 **/
//			if (issuer.compareTo(Issuer.LEGAL_EXPERT.getRole()) == 0) {
//				userRole = Issuer.LEGAL_EXPERT.getLabel();
//			}
//			else if (issuer.compareTo(Issuer.POLICY_EXPERT.getRole()) == 0) {
//				userRole = Issuer.POLICY_EXPERT.getLabel();
//			}
//			else {
//				userRole = issuer;
//			}
//
//			returnUrl = target;
//			redirect(issuer, dsaId, action, userID);
//		}
//
//		//		updateUserInfo("isStandalone vale " + isStandalone);
//	}


	private void updateCentralPanel(Widget w) {
		panelCenterWrapper.clear();

		panelCenterWrapper.setWidth("100%");
		panelCenterWrapper.setHeight("100%");
		panelCenterWrapper.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		panelCenterWrapper.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);

		if(w != null) {
			panelCenterWrapper.add(w);
		}
		panelMainDock.getElement().getStyle().clearPosition();
	}

	//	private void updateCentralPanel(Widget w) {
	//		panelCenterScroll.clear();
	//
	//		if(w != null) {
	//			panelCenterScroll.add(w);
	//		}
	//		panelMainDock.getElement().getStyle().clearPosition();
	//	}
	//
	//	private void updateCentralPanel(Widget w) {
	//		panelCenterWrapper.clear();
	//
	//		panelCenterWrapper.setWidth("100%");
	//		panelCenterWrapper.setHeight("100%");
	//		panelCenterWrapper.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
	//		panelCenterWrapper.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
	//
	//
	////		panelCenterWrapper.setSize("100%", "100%");
	////		panelCenterWrapper.getFlexCellFormatter().setVerticalAlignment(0, 0, HasVerticalAlignment.ALIGN_MIDDLE);
	////		panelCenterWrapper.getFlexCellFormatter().setHorizontalAlignment(0, 0, HasHorizontalAlignment.ALIGN_CENTER);
	//
	//		if(w != null) {
	//			ScrollPanel sp = new ScrollPanel();
	//			sp.setSize("100%", "100%");
	//			sp.add(w);
	//
	//			w.setHeight("100%");
	//
	//			panelCenterWrapper.add(sp);
	////			panelCenterWrapper.setWidget(0, 0, sp);
	//		}
	//		panelMainDock.getElement().getStyle().clearPosition();
	//	}

	private void updateUserInfo(String txt) {
		labelUserInfo.setText(txt);
	}

	private Widget createHeaderPanel() {
		final HTML htmlTitle = new HTML(messages.header(), false);
		htmlTitle.setStyleName("headerMainTitle");

		final Image c3logo = new Image(resources.c3ispLogo());
		c3logo.setStyleName("c3isplogo");

		final Image userManual = new Image(resources.userManualDownload());
		userManual.setStyleName("usermanual");

		labelUserInfo.setStyleName("userinfo");
		updateUserInfo("");//default empty

		userManual.setTitle(messages.tooltipHelpDownload());
		userManual.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				String path = GWT.getHostPageBaseURL() + "DSA Editor User Manual.pdf";
				Window.open(path, "User Manual", "");
			}
		});

		final HorizontalPanel hPanel = new HorizontalPanel();
		hPanel.setStyleName("topheader");
		hPanel.add(c3logo);
		hPanel.add(htmlTitle);
		hPanel.add(labelUserInfo);
		hPanel.add(userManual);
		hPanel.setCellWidth(htmlTitle, "50%");
		hPanel.setCellWidth(labelUserInfo, "50%");
		hPanel.setCellVerticalAlignment(htmlTitle, VerticalPanel.ALIGN_MIDDLE);
		hPanel.setCellVerticalAlignment(userManual, VerticalPanel.ALIGN_MIDDLE);
		hPanel.setCellVerticalAlignment(labelUserInfo, VerticalPanel.ALIGN_BOTTOM);
		hPanel.setCellVerticalAlignment(c3logo, VerticalPanel.ALIGN_MIDDLE);

		hPanel.setCellHorizontalAlignment(userManual, VerticalPanel.ALIGN_RIGHT);
		hPanel.setCellHorizontalAlignment(labelUserInfo, VerticalPanel.ALIGN_RIGHT);

		return hPanel;
	}

	private Widget createFooterPanel() {
		final HTML htmlVersion = new HTML("Version 1.0", false);
		htmlVersion.setStyleName("version");

		final HTML htmlCp = new HTML(
				"© 2015-2018 Hewlett Packard Enterprise Development Company, L.P.<br>" +
						"This project is funded under the European Union’s Horizon 2020 Programme (H2020) - Grant Agreement n. 700294", true);
		htmlCp.setStyleName("copyright");

		Image hpelogo = new Image(resources.hpeLogo());
		hpelogo.setStyleName("hpelogo");


		final HorizontalPanel hPanel = new HorizontalPanel();
		hPanel.setStyleName("footer");
		hPanel.add(hpelogo);
		hPanel.add(htmlCp);
		hPanel.add(htmlVersion);
		hPanel.setCellWidth(htmlCp, "100%");
		hPanel.setCellVerticalAlignment(htmlCp, VerticalPanel.ALIGN_MIDDLE);
		hPanel.setCellVerticalAlignment(htmlVersion, VerticalPanel.ALIGN_BOTTOM);

		return hPanel;
	}























	private void redirect(String issuer, final String dsaId, String action, String userID) {
		if (action.compareTo("Read") == 0) {
			//RootPanel.get("gwtContainer").clear();
			final MessagePanel messagePanel = new MessagePanel("Loading your DSA. Please, wait ...");
			messagePanel.show();
			if (dsaId == null) {
				String text = "Error while loading your DSA.";
				messagePanel.setAutoHideEnabled(true);
				messagePanel.setText(text);
				backHome(text);
			} else {
				messagePanel.hide();
				dsaUuid = dsaId;
				viewDsaContent(dsaId);
			}
		} else {
			if (action.compareTo("Create") == 0 && issuer.compareToIgnoreCase("LegalExpert") == 0) {
				createNewDsa(userID);
			} else {
				if (action.compareTo("Update") == 0) {
					loadDsaFromRemoteRepository(dsaId);
				} else {
					Window.alert(messages.messageActionError());
					// redirect to target
					Window.Location.replace(returnUrl);
				}
			}
		}
	}

	private void backHome(String message) {
		if (returnUrl != null) {
			HtmlDialog myDialog = new HtmlDialog("Info", message + messages.messageClose(), returnUrl,
					"backDialogContent", messages.buttonClose());
			myDialog.center();
			myDialog.show();
			myDialog.setSize("10em", "10em");
			myDialog.addStyleName("infoDialog");
		} else {
			dsaUuid = null;
			//RootPanel.get("gwtContainer").clear();
			RepoCallBack repoCallBack = new RepoCallBack();
			dsaManagerService.loadRepositoryContent(userRole, repoCallBack);
			//			RootPanel.get("gwtContainer");
			initializePresenters();
		}
	}

	private void initializePresenters() {
		statementSetPresenters.clear();
		authorizationsSetPresenter = ginjector.getAuthorizationsPresenter();
		prohibitionsSetPresenter = ginjector.getProhibitionsSetPresenter();
		obligationsSetPresenter = ginjector.getObligationsPresenter();

		//_MR
		thirdPartiesAuthorizationsSetPresenter = ginjector
				.getThirdPartiesAuthorizationsPresenter();
		thirdPartiesObligationsSetPresenter = ginjector
				.getThirdPartiesObligationsPresenter();
		thirdPartiesProhibitionsSetPresenter = ginjector
				.getThirdPartiesProhibitionsPresenter();
	}

	//	private void loadRolePanel() {
	//		// load checkbox panel to acquire user profile
	//		initWidgets();
	//		VerticalPanel vPanel = new VerticalPanel();
	//		vPanel.setStyleName("loginPanel");
	//		Button enterButton = new Button(messages.buttonEnter());
	//		Label intro = new Label(messages.messageRoleSelection());
	//		vPanel.add(intro);
	//		vPanel.add(createRadioButton());
	//		vPanel.add(enterButton);
	//		RootPanel.get("selectionContainer").add(vPanel);
	//		MyHandler handler = new MyHandler();
	//		enterButton.addClickHandler(handler);
	//		enterButton.setStyleName("myEnterButton");
	//	}



	private void loadRolePanel() {
		// load checkbox panel to acquire user profile
		//initWidgets();
		final VerticalPanel vPanel = new VerticalPanel();
		vPanel.setStyleName("loginPanel");
		vPanel.setSpacing(10);
		vPanel.setHeight("200px");

		Label intro = new Label(messages.messageRoleSelection());

		Button enterButton = new Button(messages.buttonEnter());
		enterButton.setStyleName("myEnterButton");

		MyHandler handler = new MyHandler();
		enterButton.addClickHandler(handler);
		//		enterButton.addClickHandler(new ClickHandler() {
		//			@Override
		//			public void onClick(ClickEvent event) {
		//				Window.alert("Enter button clicked");
		//			}
		//		});
		//

		Widget radioPanel = createRadioButton();

		vPanel.add(intro);
		vPanel.add(radioPanel);
		vPanel.add(enterButton);

		vPanel.setCellHeight(radioPanel, "100%");

		vPanel.setCellHorizontalAlignment(intro, VerticalPanel.ALIGN_RIGHT);
		vPanel.setCellHorizontalAlignment(radioPanel, VerticalPanel.ALIGN_CENTER);
		vPanel.setCellVerticalAlignment(radioPanel, VerticalPanel.ALIGN_MIDDLE);
		vPanel.setCellHorizontalAlignment(enterButton, VerticalPanel.ALIGN_RIGHT);

		updateCentralPanel(vPanel);
	}

	private static final int LIST_BOX_PURPOSE = 0;
	private static final int LIST_BOX_APPDOMAIN = 1;
	private static final int LIST_BOX_DATACLASSIFICATION = 2;

	private HorizontalPanel createPurposePanel(boolean isEditMode, boolean readOnly) {
		HorizontalPanel hPanel = new HorizontalPanel();

		VerticalPanel purposeVPanel = new VerticalPanel();


		// Purpose section


//		Label labelPurpose = new Label(messages.labelPurpose());
//		labelPurpose.setStyleName("required");
//		labelPurpose.setTitle(messages.tooltipPurpose());
//		purposeVPanel.add(labelPurpose);

		LabelWithTooltip labelPurpose= new LabelWithTooltip(messages.labelPurpose(), messages.tooltipPurpose(), "required");
		purposeVPanel.add(labelPurpose);

		if (readOnly || userRole.compareTo(Issuer.LEGAL_EXPERT.getLabel()) != 0) {
			Label value = new Label();
			value.setText(purposeList.get(selectedPurpose));
			value.setStyleName("normalFont");
			purposeVPanel.add(value);
		} else {
			ListBox lb = createPurposePanelListBox(purposeList, false, isEditMode, userRole, selectedPurpose, LIST_BOX_PURPOSE);
			purposeVPanel.add(lb);
		}

		final VerticalPanel applicationDomainVPanel = new VerticalPanel();

//		final Label labelApplicationDomain = new Label(messages.labelApplicationDomain());
//		labelApplicationDomain.setStyleName("required");
//		labelApplicationDomain.setTitle(messages.tooltipApplicationDomain());

//		applicationDomainVPanel.add(labelApplicationDomain);

		LabelWithTooltip lt = new LabelWithTooltip(messages.labelApplicationDomain(), messages.tooltipApplicationDomain(), "required");
		applicationDomainVPanel.add(lt);


		if (readOnly || userRole.compareTo(Issuer.LEGAL_EXPERT.getLabel()) != 0) {
			Label value = new Label();
			value.setText(applicationDomainList.get(selectedApplicationDomain));
			value.setStyleName("normalFont");
			applicationDomainVPanel.add(value);
		} else {
			ListBox lb = createPurposePanelListBox(applicationDomainList, false, isEditMode, userRole, selectedApplicationDomain, LIST_BOX_APPDOMAIN);
			applicationDomainVPanel.add(lb);
		}

		VerticalPanel dataClassificationVPanel = new VerticalPanel();

		//Data Classification section
//		Label labelDataClassification = new Label(messages.labelDataClassification());
//		labelDataClassification.setTitle(messages.tooltipDataClassification());
//		dataClassificationVPanel.add(labelDataClassification);

		LabelWithTooltip labelDataClassification= new LabelWithTooltip(messages.labelDataClassification(), messages.tooltipDataClassification());
		dataClassificationVPanel.add(labelDataClassification);


		if (readOnly || userRole.compareTo(Issuer.LEGAL_EXPERT.getLabel()) != 0) {
			Label dcValue = new Label();
			dcValue.setText(dataClassificationList.get(selectedDataClassification));
			dcValue.setStyleName("normalFont");
			dataClassificationVPanel.add(dcValue);
		} else {
			ListBox lb = createPurposePanelListBox(dataClassificationList, false, isEditMode, userRole, selectedDataClassification, LIST_BOX_DATACLASSIFICATION);
			dataClassificationVPanel.add(lb);
		}


		hPanel.add(purposeVPanel);
		hPanel.add(applicationDomainVPanel);
		hPanel.add(dataClassificationVPanel);

		return hPanel;
	}



	private void loadDSAFieldPanel(boolean isEditMode) {
		DecoratorPanel panel = new DecoratorPanel();
		final VerticalPanel vPanel = new VerticalPanel();
		vPanel.setSpacing(10);
		vPanel.add(buildActionPanel(isEditMode));
		if ((userRole.compareTo(Issuer.USER.getLabel()) == 0)
				&& (dsaBean.isRequiredConsent() || WidgetUtil.containsPendingRules(dsaBean))) {
			// user preferences
			vPanel.add(readOnlyDsa(false));
		} else {
			HorizontalPanel hPanel = new HorizontalPanel();
			HorizontalPanel innerHPanel = new HorizontalPanel();
			VerticalPanel leftVPanel = new VerticalPanel();
			VerticalPanel rightVPanel = new VerticalPanel();

			// title
			LabelWithTooltip lblName = new LabelWithTooltip(messages.labelTitle(), messages.tooltipTitle(), "required");
			innerHPanel.add(lblName);

//			Label lblName = new Label(messages.labelTitle());
//			lblName.setStyleName("required");
//			lblName.setTitle(messages.tooltipTitle());
//			innerHPanel.add(lblName);

			final TextBox textBox = new TextBox();
			textBox.setStyleName("myTextBox");
			textBox.setText(this.dsaTitle.getText());
			textBox.setVisibleLength(50);
			textBox.addValueChangeHandler(new ValueChangeHandler<String>() {
				@Override
				public void onValueChange(ValueChangeEvent<String> event) {
					textBox.setValue(((TextBox) event.getSource()).getText());
					dsaTitle.setText(textBox.getValue());
				}
			});

			innerHPanel.add(textBox);
			leftVPanel.add(innerHPanel);

			Label lblStatus = new Label(messages.labelStatus());
			innerHPanel = new HorizontalPanel();
			innerHPanel.add(lblStatus);

			this.dsaStatus.setStyleName("normalFont");
			innerHPanel.add(this.dsaStatus);
			rightVPanel.add(innerHPanel);

//			// Purpose section
//			lblName = new Label(messages.labelPurpose());
//			lblName.setStyleName("required");
//			lblName.setTitle(messages.tooltipPurpose());
//			leftVPanel.add(lblName);
//			if (userRole.compareTo(Issuer.LEGAL_EXPERT.getLabel()) != 0) {
//				Label purposeValue = new Label();
//				purposeValue.setText(purposeList.get(selectedPurpose));
//				purposeValue.setStyleName("normalFont");
//				leftVPanel.add(purposeValue);
//			} else {
//				leftVPanel.add(getListBox(purposeList, false, isEditMode, userRole, selectedPurpose, 0));
//			}



//			//Data Classification section
//			lblName = new Label(messages.labelDataClassification());
//			lblName.setTitle(messages.tooltipDataClassification());
//			rightVPanel.add(lblName);
//			if (userRole.compareTo(Issuer.LEGAL_EXPERT.getLabel()) != 0) {
//				Label dcValue = new Label();
//				dcValue.setText(dataClassificationList.get(selectedDataClassification));
//				dcValue.setStyleName("normalFont");
//				rightVPanel.add(dcValue);
//			} else {
//				rightVPanel.add(getListBox(dataClassificationList, false, isEditMode, userRole, selectedDataClassification, 4));
//			}


			//dio santo ma che i Panel e le label sono a pagamento? Ma perche' usare sempre gli stessi? E' una gara per scrivere il codice
			//nel modo piu' oscuro possibile?
			hPanel.add(leftVPanel);
			hPanel.add(rightVPanel);
			vPanel.add(hPanel);

			vPanel.add(createPurposePanel(isEditMode, false));

//////////////////////////////////////////////////////
			// description
			this.dsaDescription.setText(dsaBean.getDescription());
			this.dsaDescription.setStyleName("myTextArea");
			this.dsaDescription.addValueChangeHandler(new ValueChangeHandler<String>() {
				@Override
				public void onValueChange(ValueChangeEvent<String> event) {
					String val = dsaDescription.getValue();
					GWT.log("DESCRIPTION changed, VAL=" + val);
					dsaBean.setDescription(val);

//					dsaDescription.setValue(((TextArea) event.getSource()).getText());
//					dsaDescription.setText(dsaDescription.getValue());
				}

			});

			HorizontalPanel headerDesc = new HorizontalPanel();
//			LabelWithTooltip lDesc = new LabelWithTooltip(messages.labelDescription(), messages.tooltipDescription());
//			header.add(lDesc);
			headerDesc.add(new Image(resources.closedArrow()));
			headerDesc.add(new Label(messages.labelDescription()));


			DisclosurePanel pnlDisclosureDescription = new DisclosurePanel();
			pnlDisclosureDescription.setHeader(headerDesc);
			pnlDisclosureDescription.setTitle(messages.tooltipDescription());
			pnlDisclosureDescription.setContent(this.dsaDescription);
			pnlDisclosureDescription.setStyleName("my-gwt-DisclosurePanel");

			vPanel.add(pnlDisclosureDescription);



//////////////////////////////////////////////////////////////////
			// Additional Info (UID and Vocabulary URI)
			HorizontalPanel hPanelUID = new HorizontalPanel();
			//			lblName = new Label(messages.labelUID());
//			lblName.setTitle(messages.tooltipUUID());
//			lblName.setStyleName("required");
//			hPanel.add(lblName);
			LabelWithTooltip lblUID= new LabelWithTooltip(messages.labelUID(), messages.tooltipUUID(), "required");
			hPanelUID.add(lblUID);
			this.dsaUid.setStyleName("normalFont");
			hPanelUID.add(this.dsaUid);

			HorizontalPanel hPanelVoc = new HorizontalPanel();
//			lblName = new Label(messages.labelVocabularyURI());
//			lblName.setTitle(messages.tooltipVocabulary());
//			hPanel.add(lblName);
			LabelWithTooltip lblVocUri= new LabelWithTooltip(messages.labelVocabularyURI(), messages.tooltipVocabulary());
			hPanelVoc.add(lblVocUri);
			this.dsaVocabulary.setStyleName("normalFont");
			hPanelVoc.add(this.dsaVocabulary);
			VerticalPanel innerVPanelMetadata = new VerticalPanel();
			innerVPanelMetadata.add(hPanelUID);
			innerVPanelMetadata.add(hPanelVoc);

			HorizontalPanel headerMeta = new HorizontalPanel();
//			LabelWithTooltip lDesc = new LabelWithTooltip(messages.labelDescription(), messages.tooltipDescription());
//			header.add(lDesc);
			headerMeta.add(new Image(resources.closedArrow()));
			headerMeta.add(new Label(messages.labelMetadata()));


			DisclosurePanel pnlDisclosureMetadata = new DisclosurePanel();
			pnlDisclosureMetadata.setStyleName("my-gwt-DisclosurePanel");
			pnlDisclosureMetadata.setHeader(headerMeta);
			pnlDisclosureMetadata.setTitle(messages.tooltipMetadata());

//			LabelWithTooltip lbMetadata = new LabelWithTooltip(messages.labelMetadata(), messages.tooltipMetadata());
//			pnlDisclosure.setHeader(lbMetadata);
			pnlDisclosureMetadata.setContent(innerVPanelMetadata);

			vPanel.add(pnlDisclosureMetadata);



///////////////////////////////////////////////////////
			// validity
			VerticalPanel validityPanel = new VerticalPanel();
			LabelWithTooltip labelValidity= new LabelWithTooltip(messages.labelValidity(), messages.tooltipValidity(), "required");

//			Label label = new Label(messages.labelValidity());
//			label.setStyleName("required");
//			label.setTitle(messages.tooltipValidity());

			this.validityPresenter.setValidityLabels(messages.labelStartDate(), messages.labelEndDate(), messages.labelUnit());
			this.validityPresenter.bind();
			validityPanel.add(labelValidity);
			validityPanel.add(((ValidityPresenter.ValidityDisplay) this.validityPresenter.getDisplay()).asWidget());
			this.controllableDisplays.add(this.validityPresenter.getDisplay());
			this.validityPresenter.addBusyDisplayHandler(this);
			this.validityPresenter.addIdleDisplayHandler(this);

			vPanel.add(validityPanel);




			// add statements
			vPanel.add(setUpPresenters());
			vPanel.add(setUpGlobalPolicies(isEditMode));
		}



		// consent management
		//		HorizontalPanel hPanel = new HorizontalPanel();
		//		Label consentLabel = new Label();
		//		if ((userRole.compareTo(Issuer.USER.getLabel()) == 0)) {
		//			if (dsaBean.isRequiredConsent()) {
		//				// user preferences
		//				consentLabel = new Label();
		//				consentLabel.setText(messages.CONSENT_STATEMENT() + purposeList.get(selectedPurpose) + " *");
		//				hPanel.add(consentLabel);
		//				this.consentCheckBox.setValue(false);
		//				this.consentCheckBox.addClickHandler(new ClickHandler() {
		//					@Override
		//					public void onClick(ClickEvent event) {
		//						boolean checked = ((CheckBox) event.getSource()).getValue();
		//						consentCheckBox.setValue(checked);
		//						dsaBean.setUserConsent(checked);
		//					}
		//				});
		//				hPanel.add(this.consentCheckBox);
		//				vPanel.add(hPanel);
		//			}
		//		} else {
		//			consentLabel.setText(messages.ASK_FOR_CONSENT_STATEMENT() + " *");
		//			hPanel.add(consentLabel);
		//			if ((userRole.compareTo(Issuer.LEGAL_EXPERT.getLabel()) != 0) || !dsaBean.isRequiredConsent()) {
		//				// the consent has not already been required by the legal expert
		//				this.askForConsentCheckBox.setValue(dsaBean.isRequiredConsent());
		//				this.askForConsentCheckBox.addClickHandler(new ClickHandler() {
		//					@Override
		//					public void onClick(ClickEvent event) {
		//						boolean checked = ((CheckBox) event.getSource()).getValue();
		//						askForConsentCheckBox.setValue(checked);
		//						dsaBean.setRequiredConsent(checked);
		//					}
		//				});
		//			} else {
		//				// the consent is required by legal expert. The check box is not
		//				// editable
		//				this.askForConsentCheckBox.setValue(dsaBean.isRequiredConsent());
		//				this.askForConsentCheckBox.setEnabled(false);
		//			}
		//			hPanel.add(this.askForConsentCheckBox);
		//			vPanel.add(hPanel);
		//		}
		vPanel.add(buildActionPanel(isEditMode));
		panel.add(vPanel);

		//		RootPanel.get("gwtContainer").add(panel);
		updateCentralPanel(panel);

	}

	private Widget buildActionPanel(boolean isEditMode) {
		HorizontalPanel actionPanel = new HorizontalPanel();
		if (userRole.compareTo(Issuer.USER.getLabel()) != 0) {
			if (isEditMode) {
				Button updateButton = new Button(messages.buttonUpdateDSA());
				updateButton.setTitle(messages.tooltipUpdateButton());
				UpdateDsaHandler handler = new UpdateDsaHandler();
				updateButton.setStyleName("myButton");
				updateButton.addClickHandler(handler);
				actionPanel.add(updateButton);
			} else {
				Button saveButton = new Button(messages.buttonSaveDSATemplate());
				saveButton.setTitle(messages.tooltipSaveTemplateButton());
				SaveDsaHandler handler = new SaveDsaHandler();
				saveButton.setStyleName("myButton");
				saveButton.addClickHandler(handler);
				actionPanel.add(saveButton);
			}
		}
		if (userRole.compareTo(Issuer.LEGAL_EXPERT.getLabel()) != 0) {
			Button completeButton = new Button(messages.buttonCompleteDSA());
			completeButton.setTitle(messages.tooltipCompleteButton());
			completeButton.setStyleName("myButton");
			CompleteDsaHandler handler = new CompleteDsaHandler();
			completeButton.addClickHandler(handler);
			actionPanel.add(completeButton);
		}
		BackHomeHandler backHandler = new BackHomeHandler();
		Button homeButton = new Button(messages.buttonBack());
		homeButton.setTitle(messages.tooltipBackButton());
		homeButton.setStyleName("myButton");
		homeButton.addClickHandler(backHandler);
		actionPanel.add(homeButton);
		return actionPanel;
	}

	private VerticalPanel setUpPresenters() {
		VerticalPanel mainPanel = new VerticalPanel();
		final HorizontalPanel hPanel = new HorizontalPanel();

//		Label labelParties = new Label(messages.labelParties());
//		labelParties.setStyleName("required");
//		labelParties.setTitle(messages.tooltipParties());
//		labelParties.setStyleName("partiesLabel");

		LabelWithTooltip labelParties= new LabelWithTooltip(messages.labelParties(), messages.tooltipParties(), new String[] {"required", "partiesLabel"});
		hPanel.add(labelParties);

		mainPanel.add(hPanel);
		partiesPresenter.bind();
		mainPanel.add(((PartiesPresenter.PartiesDisplay) partiesPresenter.getDisplay()).asWidget());
		controllableDisplays.add(partiesPresenter.getDisplay());
		partiesPresenter.addBusyDisplayHandler(this);
		partiesPresenter.addIdleDisplayHandler(this);
		// cross ref
		this.crossReferencePresenter.bind();
		mainPanel.add(crossReferencePresenter.getDisplay().asWidget());
		controllableDisplays.add(this.crossReferencePresenter.getDisplay());
		crossReferencePresenter.addBusyDisplayHandler(this);
		crossReferencePresenter.addIdleDisplayHandler(this);
		// parties policies
		mainPanel.add(setUpPartiesPoliciesPresenters());

		//_MR
		// third parties policies
		mainPanel.add(setUpThirdPartiesPoliciesPresenters());

		return mainPanel;
	}




	private VerticalPanel setUpGlobalPolicies(boolean isEditMode) {
		this.revocationPolicyAttribute.setStyleName("num-TextBox");
		this.updatePolicyAttribute.setStyleName("num-TextBox");
		this.expirationPolicyAttribute.setStyleName("num-TextBox");
		VerticalPanel vPanel = new VerticalPanel();

		HorizontalPanel mainHPanel = new HorizontalPanel();
//		Label labelParties = new Label(messages.labelGlobalPoliciesParties());
//		labelParties.setTitle(messages.tooltipGlobalPoliciesParties());
//		labelParties.setStyleName("policyTitle");

		LabelWithTooltip labelParties= new LabelWithTooltip(messages.labelGlobalPoliciesParties(), messages.tooltipGlobalPoliciesParties(), "policyTitle");

		mainHPanel.add(labelParties);
		vPanel.add(mainHPanel);

		mainHPanel = new HorizontalPanel();
		VerticalPanel innerPanel = new VerticalPanel();

		LabelWithTooltip labelExpirationPolicy= new LabelWithTooltip(messages.labelExpirationPolicy(), messages.tooltipExpirationPolicy());
//		Label lblName = new Label(messages.labelExpirationPolicy());
//		lblName.setTitle(messages.tooltipExpirationPolicy());
		innerPanel.add(labelExpirationPolicy);
		LabelWithTooltip expirationPeriodLabel= new LabelWithTooltip(messages.labelPeriodInDays(), messages.tooltipPeriodInDays());

		if (userRole.compareTo(Issuer.USER.getLabel()) == 0) {
			Label expirationValue = new Label();
			expirationValue.setText(WidgetUtil.getPolicyTypeString(selectedExpirationPolicy));
			expirationValue.setText(dsaBean.getExpirationPolicy().getPolicy());
			innerPanel.add(expirationValue);

//			Label expirationPeriodLabel = new Label(messages.labelPeriodInDays());
//			expirationPeriodLabel.setTitle(messages.tooltipPeriodInDays());

			innerPanel.add(expirationPeriodLabel);
			Label expirationPeriod = new Label();
			expirationPeriod.setText(this.expirationPolicyAttribute.getValue() == null
					? this.expirationPolicyAttribute.getValue() : messages.messageNotAvailable());
			expirationPeriod.setStyleName("normalFont");
			innerPanel.add(expirationPolicyAttribute);
		} else {
			innerPanel.add(createGlobalPoliciesListBox(policyList, false, isEditMode, userRole,
					WidgetUtil.getPolicyTypeIndex(dsaBean.getExpirationPolicy().getPolicy()), 1));
			HorizontalPanel hPanel = new HorizontalPanel();

//			Label expirationPeriodLabel = new Label(messages.labelPeriodInDays());
//			expirationPeriodLabel.setTitle(messages.tooltipPeriodInDays());

			hPanel.add(expirationPeriodLabel);
			// check if a period is needed to be specified
			if (WidgetUtil.getPolicyTypeIndex(dsaBean.getExpirationPolicy().getPolicy()) == 2){
				expirationPeriodEnabled = true;
			}else{
				expirationPeriodEnabled = false;
			}
			expirationPolicyAttribute.setEnabled(expirationPeriodEnabled);
			expirationPolicyAttribute.setValue(String.valueOf(dsaBean.getExpirationPolicy().getPeriodInDays()));
			hPanel.add(expirationPolicyAttribute);
			innerPanel.add(hPanel);
		}
		mainHPanel.add(innerPanel);
		innerPanel = new VerticalPanel();

		LabelWithTooltip labelUpdatePolicy= new LabelWithTooltip(messages.labelUpdatePolicy(), messages.tooltipUpdatePolicy());

//		lblName = new Label(messages.labelUpdatePolicy());
//		lblName.setTitle(messages.tooltipUpdatePolicy());
		innerPanel.add(labelUpdatePolicy);

		LabelWithTooltip updatePeriodLabel= new LabelWithTooltip(messages.labelPeriodInDays(), messages.tooltipPeriodInDays());


		if (userRole.compareTo(Issuer.USER.getLabel()) == 0) {
			Label updateValue = new Label();
			updateValue.setText(WidgetUtil.getPolicyTypeString(selectedUpdatePolicy));
			updateValue.setStyleName("normalFont");
			innerPanel.add(updateValue);
//			Label updatePeriodLabel = new Label(messages.labelPeriodInDays());
//			updatePeriodLabel.setTitle(messages.tooltipPeriodInDays());
			innerPanel.add(updatePeriodLabel);
			Label updatePeriod = new Label();
			updatePeriod.setText(this.updatePolicyAttribute.getValue() == null ? this.updatePolicyAttribute.getValue()
					: messages.messageNotAvailable());
			updatePeriod.setStyleName("normalFont");
			innerPanel.add(updatePeriod);
		} else {
			innerPanel.add(createGlobalPoliciesListBox(policyList, false, isEditMode, userRole,
					WidgetUtil.getPolicyTypeIndex(dsaBean.getUpdatePolicy().getPolicy()), 2));
			HorizontalPanel hPanel = new HorizontalPanel();
//			Label updatePeriodLabel = new Label(messages.labelPeriodInDays());
//			updatePeriodLabel.setTitle(messages.tooltipPeriodInDays());
			hPanel.add(updatePeriodLabel);
			updatePolicyAttribute.setValue(String.valueOf(dsaBean.getUpdatePolicy().getPeriodInDays()));
			if (WidgetUtil.getPolicyTypeIndex(dsaBean.getUpdatePolicy().getPolicy()) == 2){
				updatePeriodEnabled = true;
			}else{
				updatePeriodEnabled = false;
			}
			updatePolicyAttribute.setEnabled(updatePeriodEnabled);
			hPanel.add(updatePolicyAttribute);
			innerPanel.add(hPanel);
		}
		mainHPanel.add(innerPanel);
		innerPanel = new VerticalPanel();

		LabelWithTooltip labelRevocationPolicy= new LabelWithTooltip(messages.labelRevocationPolicy(), messages.tooltipRevocationPolicy());
//		lblName = new Label(messages.labelRevocationPolicy());
//		lblName.setTitle(messages.tooltipRevocationPolicy());
		innerPanel.add(labelRevocationPolicy);

		LabelWithTooltip revocationPeriodLabel= new LabelWithTooltip(messages.labelPeriodInDays(), messages.tooltipPeriodInDays());


		if (userRole.compareTo(Issuer.USER.getLabel()) == 0) {
			Label revocationValue = new Label();
			revocationValue.setText(WidgetUtil.getPolicyTypeString(selectedRevocationPolicy));
			revocationValue.setStyleName("normalFont");
			innerPanel.add(revocationValue);
//			Label revocationPeriodLabel = new Label(messages.labelPeriodInDays());
//			revocationPeriodLabel.setTitle(messages.tooltipPeriodInDays());
			innerPanel.add(revocationPeriodLabel);
			Label revocationPeriod = new Label();
			revocationPeriod.setText(this.revocationPolicyAttribute.getValue() == null
					? this.revocationPolicyAttribute.getValue() : messages.messageNotAvailable());
			revocationPeriod.setStyleName("normalFont");
			innerPanel.add(revocationPeriod);
		} else {
			innerPanel.add(createGlobalPoliciesListBox(policyList, false, isEditMode, userRole,
					WidgetUtil.getPolicyTypeIndex(dsaBean.getRevocationPolicy().getPolicy()), 3));
			HorizontalPanel hPanel = new HorizontalPanel();
//			Label revocationPeriodLabel = new Label(messages.labelPeriodInDays());
//			revocationPeriodLabel.setTitle(messages.tooltipPeriodInDays());
			hPanel.add(revocationPeriodLabel);
			updatePolicyAttribute.setValue(String.valueOf(dsaBean.getUpdatePolicy().getPeriodInDays()));
			if (WidgetUtil.getPolicyTypeIndex(dsaBean.getRevocationPolicy().getPolicy()) == 2){
				revocationPeriodEnabled = true;
			}else{
				revocationPeriodEnabled = false;
			}
			revocationPolicyAttribute.setEnabled(revocationPeriodEnabled);
			revocationPolicyAttribute.setValue(String.valueOf(dsaBean.getRevocationPolicy().getPeriodInDays()));
			hPanel.add(revocationPolicyAttribute);
			innerPanel.add(hPanel);
		}
		mainHPanel.add(innerPanel);
		vPanel.add(mainHPanel);
		return vPanel;
	}

	private Widget setUpAllPolicies() {
		Label label = new Label(messages.labelPolicies());
		label.setStyleName("policyTitle");
		VerticalPanel policyPanel = new VerticalPanel();
		policyPanel.add(label);
		List<Statement> statementList = new ArrayList<Statement>();

		statementList.addAll(dsaBean.getAuthorizations());
		statementList.addAll(dsaBean.getObligations());
		statementList.addAll(dsaBean.getProhibitions());

		//_MR
		statementList.addAll(dsaBean.getDerivedObjectsAuthorizations());
		statementList.addAll(dsaBean.getDerivedObjectsObligations());
		statementList.addAll(dsaBean.getDerivedObjectsProhibitions());

		if (statementList.isEmpty()) {
			Label message = new Label("No policies defined for this DSA.");
			policyPanel.add(message);
		} else {
			CellTable<Statement> statementsTable = new CellTable<Statement>();
			statementsTable.setStyleName("myCellTable");
			
	         // Create isProtected column. --> USED to support Other Data
            TextColumn<Statement> isProtectedColumn = new TextColumn<Statement>() {
                @Override
                public String getValue(Statement s) {
                    boolean isProtected = s.getStatementMetadata().isProtected();
                    if (isProtected) {
                        return "(Applies to Other Data)"; //TODO: put label in I18N file
                    } else {
                        return ""; // return empty string if isProtected is not set
                    }
                }
            };
			
			// Create type column.
			TextColumn<Statement> typeColumn = new TextColumn<Statement>() {
				@Override
				public String getValue(Statement s) {
					return s.getType().name();
				}
			};
			// Create content column.
			TextColumn<Statement> humanFormColumn = new TextColumn<Statement>() {
				@Override
				public String getValue(Statement s) {
//					return s.getHumanForm();


					return generateHumanFormFromStatement(s);





//					String value = s.getHumanForm();
//					Set<String> params = dsaBean.getParamTermList().keySet();
//
//					GWT.log("[[[[[[[[[[[1 s.getHumanForm()=" + s.getHumanForm());
//
//					for (String p:params){
//						//						if (s.getStatementMetadata().isPendingRule()
//						//								&& s.getHumanForm().contains(p) ){
//						GWT.log("[[[[[[[[[[[2 param=" + p);
//
//						if (s.getHumanForm().contains(p) ){
//							String t = p;
//							String o = s.getStatementMetadata().getStatementInfo();
//							String v = s.getStatementMetadata().getInputValue();
//							GWT.log("[[[[[[[[[[[3 getStatementMetadata()=" + o + " getInputValue()="+v);
//
//
//							if (dsaBean.getParamTermList().get(p).contains("User")){ //free text field
//								GWT.log("[[[[[[[[[[[4 contains(User) YES");
//
//								value = value.substring(0, value.indexOf(t) + t.length()) + "(" + v.substring(v.indexOf("=") + 1)
//								+ ") " + value.substring(value.indexOf(t) + t.length()+1);
//
//							}else{
//								GWT.log("[[[[[[[[[[[4 contains(User) NO");
//
//								value = value.substring(0, value.indexOf(t) + t.length()) +
//										o.substring(o.indexOf("{"))
//								+ " " +value.substring(value.indexOf(t) + t.length() + 1);
//							}
//						}
//
//					}
//
//
//
//
//					//					for (String p:params){
//					////							if (s.getStatementMetadata().isPendingRule()
//					////									&& s.getHumanForm().contains(p) ){
//					//							if (s.getHumanForm().contains(p) ){
//					//								String t = p;
//					//								String o = s.getStatementMetadata().getStatementInfo();
//					//								String v = s.getStatementMetadata().getInputValue();
//					//								if (dsaBean.getParamTermList().get(p).contains("User")){ //free text field
//					//									value = value.substring(0, value.indexOf(t) + t.length()) + "(" + v.substring(v.indexOf("=") + 1)
//					//									+ ") " + value.substring(value.indexOf(t) + t.length()+1);
//					//
//					//								}else{
//					//
//					//									value = value.substring(0, value.indexOf(t) + t.length()) +
//					//									o.substring(o.indexOf("{"))
//					//									+ " " +value.substring(value.indexOf(t) + t.length() + 1);
//					//								}
//					//							}
//					//
//					//					}
//
//					return value;
				}
			};
			// Add the columns.
			statementsTable.addColumn(typeColumn, messages.labelType());
			statementsTable.addColumn(humanFormColumn, messages.labelPolicies());
			statementsTable.addColumn(isProtectedColumn, ""); // isProtected column does not have any label
			statementsTable.setRowData(0, (List<? extends Statement>) statementList);
			policyPanel.add(statementsTable);
		}
		return policyPanel;
	}



	private String generateHumanFormFromStatement(Statement s) {
		final String humanForm = s.getHumanForm();
		final String statementInfo = s.getStatementMetadata().getStatementInfo();
		final String inputValue = s.getStatementMetadata().getInputValue();

		GWT.log("**** generateHumanFormFromStatement START");
		GWT.log("statement getHumanForm()=" + humanForm);
		GWT.log("statement Metadata().getStatementInfo()=" + statementInfo);
		GWT.log("statement Metadata().getInputValue()=" + inputValue);

		final StringBuffer buffer = new StringBuffer();

		List<SyntacticItem> syntacticItems = s.getSyntacticItems();
		GWT.log("statement getSyntacticItems() size=" + syntacticItems.size());
		for(SyntacticItem sint:syntacticItems) {
			String itemHumanForm = sint.getHumanForm();
			if(itemHumanForm == null) {
				itemHumanForm = "";
			}
			GWT.log(" SyntacticItem found=" + sint);
//			buffer.append(sint.getHumanForm());
			if (sint instanceof VariableDeclaration) {
				GWT.log("      it is a VariableDeclaration");
				buffer.append(itemHumanForm);
				VariableDeclaration vd = (VariableDeclaration)sint;
				String v = vd.getValue();
				if(!Util.isEmpty(v)) {
					buffer.append("(" + v + ")");
				}
			}
			else if (sint instanceof Functor) {
				GWT.log("      it is a Functor");
				if(!Util.isEmpty(statementInfo) && statementInfo.startsWith(itemHumanForm)) {
					//exmaple:
					//statementInfo = "AnonymiseBySuppression{param=request option=}"
					//itemhumanForm = AnonymiseBySuppression
					buffer.append(statementInfo);
				}else {
					buffer.append(itemHumanForm);
				}
			} else {
				buffer.append(itemHumanForm);
			}
			buffer.append(" ");
		}
		if(buffer.length() > 0) {
			return buffer.toString().trim();
		}else {
			return "";
		}

////
////		//"?X_29:Identifier=iddd|?X_30:Identifier=iddd2|?X_31:Identifier=iddd3"
////		//following tranform in a map
////		//?X_29:Identifier---->iddd , ?X_30:Identifier---->iddd2 , ?X_31:Identifier---->iddd3
////		Map<String, String> mapInputValue = InputValueHandler.stringToMap(inputValue);
////		GWT.log("   mapInputValue=" + Util.MapToString(mapInputValue, ""));
////
////		int syntacticItemCount = 0;
////		for (SyntacticItem si : syntacticItems) {
////			syntacticItemCount++;
////			System.out.println("    -----> syntacticItemCount = "+ syntacticItemCount);
////			if (si instanceof VariableDeclaration) {
////				VariableDeclaration vd = (VariableDeclaration) si;
////				System.out.println("        -----> VariableDeclaration found: "+ vd);
////				String value = InputValueHandler.findRightInputValue(vd, mapInputValue);
////
//////				vd.setValue(value);
////
////				//questo fa si che la parte del XML "UserText" contanga gia' al proprio interno il replace delle
////				//variabili con l'ID selezionato. Purtroppo la stringa in "UserText" non viene mai utilizzat in lettura
////				//quindi e' pressoche' inutile. In particolare, per costruire la stringa visualizzata a video
////				//in "View DSA" non si utlizza "UserText" m si cstruisce una nuova stringa da zero a partire dalle varibili dello
////				//statement
////				String newHumanForm = null;
////				//modify human form adding the found value
////				if(!Util.isEmpty(value)) {
////					newHumanForm = vd.getHumanForm();
////					if(!Util.isEmpty(newHumanForm)) {
////						buffer.append(newHumanForm+"("+value+") ");
////					}
////				}
////			}
////		}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//		final Map<String, String> mapParams = dsaBean.getParamTermList();
//		final Set<String> allParams = mapParams.keySet();
//
//		GWT.log("checking all possible params, dsaBean.getParamTermList() retruned params NO:" + allParams.size());
//
//		final Set<String> inputValueParams = new HashSet<String>();
//		final Set<String> statementInfoParams = new HashSet<String>();
//
//		for (String param:allParams){
//			String term = mapParams.get(param);
//			if(term.contains("User")) {
//				inputValueParams.add(param);
//			}else {
//				statementInfoParams.add(param);
//			}
//		}
//		//input value is of the form
//		//"?X_29:PARAMNAME=VALUE|?X_30:PARAMNAME=VALUE|..."
//		//separator is the | character
//		//I want a map PARAMNAME -> VALUE1, VALUE2.... keeping the order
//		//example: inputvalue "?X_29:Identifier=iddd|?X_30:Identifier=iddd2|?X_31:Identifier=iddd3"
//		//return Identifier -> iddd,iddd2
//		Map<String, List<String>> inputValueMap = HumanFormHelper.parseInputValue(inputValueParams, inputValue);
//
//		//humanForm is something like:
//		//IF a Subject hasId a Identifier OR that Subject hasId a Identifier THEN that Subject CAN Create a Data
//		//I want:
//		//IF a Subject hasId a Identifier_0 OR that Subject hasId a Identifier_1 THEN that Subject CAN Create a Data
//		String preparedHumanForm = HumanFormHelper.prepareHumanForm(inputValueParams, humanForm);
//		GWT.log("preparedHumanForm:" + preparedHumanForm);
//
//		//now simple substitution
//		for (String param:inputValueParams){
//			List<String> values = inputValueMap.get(param);
//
//			GWT.log(" inputValueMap: " + param + " -> " + Util.listToString(values, "empty!!"));
//
//			if(values != null && values.size() > 0) {
//				for(int i=0; i<values.size(); i++) {
//					String search = param + "_" + i; //Identifier_0, Identifier_1, ...
//					String replace = param + "(" + values.get(i) + ")";
//					preparedHumanForm = preparedHumanForm.replace(search, replace);
//				}
//			}
//		}
//
//
//		//now go for stementInfo replacement. assumption: only single param per statement
//
////	    <obligation id="OBLIGATION_91" index="0" isProtected="false" isPendingRule="true" statementInfo="AnonymizeDelimitedStringsBySuppression{param=dst option=LOWER_SUBSTRING_HIGH_PRIVACY}" inputValue="" conflict="false">
////	      <expression language="CNL_4_DSA_E" issuer="Legal Expert">if ?X_92:Data hasType ?X_93:Email then ?X_94:System must AnonymizeDelimitedStringsBySuppression ?X_92</expression>
////	      <expression language="UPOL" issuer="Legal Expert"/>
////	      <expression language="UserText" issuer="Legal Expert">IF a Data hasType Email THEN a System MUST AnonymizeDelimitedStringsBySuppression that Data</expression>
////	      <expression language="CNL4DSA" issuer="Legal Expert">if  hasType(?X_92,?X_93)  then  must [?X_94, AnonymizeDelimitedStringsBySuppression, ?X_92]</expression>
////	    </obligation>
//
//		//IF a Data hasType DatabaseRecord THEN a System MUST AnonymiseByGeoIndistinguishability{param=DestinationAddress option=GEO_LOW} that Data
//
//		for (String param:statementInfoParams){
//			if(preparedHumanForm.contains(param)) {
//				preparedHumanForm = preparedHumanForm.replace(param, statementInfo);
//				break; //assume statement info contains single param assignemnt
//			}
//		}
//
//
//
//
//
//
//
//
//
////		for (String param:allParams){
////			GWT.log(" check param:" + param);
////			if (humanForm.contains(param) ){
////				GWT.log("  humanForm contains this param, continue");
////				String term = mapParams.get(param);
////				GWT.log("  term from map corresponding to this param is " + term);
////
////				if(term.contains("User")) {
////					GWT.log("    given term contains 'User', it is a free text field");
////
////				}else {
////					GWT.log("    given term DOE NOT contains 'User', it is NOT a free text field");
////
////				}
////			} else {
////				GWT.log("humanForm DOES NOT contains this param, skip to next");
////			}
////		}
//
//
//		GWT.log("****** generateHumanFormFromStatement END ");
//		return preparedHumanForm;
//
//
//
//
////		String value = s.getHumanForm();
////		Set<String> params = dsaBean.getParamTermList().keySet();
////
////		GWT.log("[[[[[[[[[[[1 s.getHumanForm()=" + s.getHumanForm());
////
////		for (String p:params){
////			//						if (s.getStatementMetadata().isPendingRule()
////			//								&& s.getHumanForm().contains(p) ){
////			GWT.log("[[[[[[[[[[[2 param=" + p);
////
////			if (s.getHumanForm().contains(p) ){
////				String t = p;
////				String o = s.getStatementMetadata().getStatementInfo();
////				String v = s.getStatementMetadata().getInputValue();
////				GWT.log("[[[[[[[[[[[3 getStatementMetadata()=" + o + " getInputValue()="+v);
////
////
////				if (dsaBean.getParamTermList().get(p).contains("User")){ //free text field
////					GWT.log("[[[[[[[[[[[4 contains(User) YES");
////
////					value = value.substring(0, value.indexOf(t) + t.length()) + "(" + v.substring(v.indexOf("=") + 1)
////					+ ") " + value.substring(value.indexOf(t) + t.length()+1);
////
////				}else{
////					GWT.log("[[[[[[[[[[[4 contains(User) NO");
////
////					value = value.substring(0, value.indexOf(t) + t.length()) +
////							o.substring(o.indexOf("{"))
////					+ " " +value.substring(value.indexOf(t) + t.length() + 1);
////				}
////			}
////
////		}
////		return value;




	}


	private VerticalPanel setUpPartiesPoliciesPresenters() {
		Label label = new Label(messages.labelPartiesPolicies());
		label.setStyleName("policyTitle");
		VerticalPanel policyPanel = new VerticalPanel();
		policyPanel.add(label);
		label = new Label(messages.labelAuthorisations());
		policyPanel.add(label);
		this.statementSetPresenters.add(this.authorizationsSetPresenter);
		this.authorizationsSetPresenter.bind();
		policyPanel.add(authorizationsSetPresenter.getDisplay().asWidget());
		this.controllableDisplays.add(this.authorizationsSetPresenter.getDisplay());
		this.authorizationsSetPresenter.addBusyDisplayHandler(this);
		this.authorizationsSetPresenter.addIdleDisplayHandler(this);
		label = new Label(messages.labelObligations());
		policyPanel.add(label);
		this.statementSetPresenters.add(this.obligationsSetPresenter);
		this.obligationsSetPresenter.bind();
		policyPanel.add(obligationsSetPresenter.getDisplay().asWidget());
		this.controllableDisplays.add(this.obligationsSetPresenter.getDisplay());
		this.obligationsSetPresenter.addBusyDisplayHandler(this);
		this.obligationsSetPresenter.addIdleDisplayHandler(this);
		label = new Label(messages.labelProhibitions());
		policyPanel.add(label);
		this.statementSetPresenters.add(this.prohibitionsSetPresenter);
		this.prohibitionsSetPresenter.bind();
		policyPanel.add(prohibitionsSetPresenter.getDisplay().asWidget());
		this.controllableDisplays.add(this.prohibitionsSetPresenter.getDisplay());
		this.prohibitionsSetPresenter.addBusyDisplayHandler(this);
		this.prohibitionsSetPresenter.addIdleDisplayHandler(this);
		return policyPanel;
	}

	//_MR
	private VerticalPanel setUpThirdPartiesPoliciesPresenters() {
		Label label = new Label(messages.labelAnalyticsResultPolicies());
		label.setStyleName("policyTitle");
		VerticalPanel policyPanel = new VerticalPanel();
		policyPanel.add(label);
		label = new Label(messages.labelAuthorisations());
		policyPanel.add(label);

		//authorizations
		this.statementSetPresenters.add(this.thirdPartiesAuthorizationsSetPresenter);
		this.thirdPartiesAuthorizationsSetPresenter.bind();
		policyPanel.add(thirdPartiesAuthorizationsSetPresenter.getDisplay().asWidget());
		this.controllableDisplays.add(this.thirdPartiesAuthorizationsSetPresenter.getDisplay());
		this.thirdPartiesAuthorizationsSetPresenter.addBusyDisplayHandler(this);
		this.thirdPartiesAuthorizationsSetPresenter.addIdleDisplayHandler(this);

		//obligations
		label = new Label(messages.labelObligations());
		policyPanel.add(label);
		this.statementSetPresenters.add(this.thirdPartiesObligationsSetPresenter);
		this.thirdPartiesObligationsSetPresenter.bind();
		policyPanel.add(thirdPartiesObligationsSetPresenter.getDisplay().asWidget());
		this.controllableDisplays.add(this.thirdPartiesObligationsSetPresenter.getDisplay());
		this.thirdPartiesObligationsSetPresenter.addBusyDisplayHandler(this);
		this.thirdPartiesObligationsSetPresenter.addIdleDisplayHandler(this);

		//prohibitions
		label = new Label(messages.labelProhibitions());
		policyPanel.add(label);
		this.statementSetPresenters.add(this.thirdPartiesProhibitionsSetPresenter);
		this.thirdPartiesProhibitionsSetPresenter.bind();
		policyPanel.add(thirdPartiesProhibitionsSetPresenter.getDisplay().asWidget());
		this.controllableDisplays.add(this.thirdPartiesProhibitionsSetPresenter.getDisplay());
		this.thirdPartiesProhibitionsSetPresenter.addBusyDisplayHandler(this);
		this.thirdPartiesProhibitionsSetPresenter.addIdleDisplayHandler(this);

		return policyPanel;
	}


	//	//_MR
	//    private DisclosurePanel setUpThirdPartiesPoliciesPresenters() {
	//        DisclosurePanel pnlDisclosure = new DisclosurePanel();
	//        Label label = new Label(messages.labelAnalyticsResultPolicies());
	//        label.setTitle(messages.tooltipThirdPartiesPolicies());
	//        pnlDisclosure.setHeader(label);
	//        pnlDisclosure.setStyleName("internal-gwt-DisclosurePanel");
	//
	//        VerticalPanel mainPanel = new VerticalPanel();
	//
	//        label = new Label(messages.labelAuthorisations());
	//        mainPanel.add(label);
	//        this.statementSetPresenters.add(this.thirdPartiesAuthorizationsSetPresenter);
	//        this.thirdPartiesAuthorizationsSetPresenter.bind();
	//        mainPanel.add(thirdPartiesAuthorizationsSetPresenter.getDisplay().asWidget());
	//        this.controllableDisplays.add(this.thirdPartiesAuthorizationsSetPresenter.getDisplay());
	//        this.thirdPartiesAuthorizationsSetPresenter.addBusyDisplayHandler(this);
	//        this.thirdPartiesAuthorizationsSetPresenter.addIdleDisplayHandler(this);
	//
	//        label = new Label(messages.labelObligations());
	//        mainPanel.add(label);
	//        this.statementSetPresenters.add(this.thirdPartiesObligationsSetPresenter);
	//        this.thirdPartiesObligationsSetPresenter.bind();
	//        mainPanel.add(thirdPartiesObligationsSetPresenter.getDisplay().asWidget());
	//        this.controllableDisplays.add(this.thirdPartiesObligationsSetPresenter.getDisplay());
	//        this.thirdPartiesObligationsSetPresenter.addBusyDisplayHandler(this);
	//        this.thirdPartiesObligationsSetPresenter.addIdleDisplayHandler(this);
	//
	//        label = new Label(messages.labelProhibitions());
	//        mainPanel.add(label);
	//        this.statementSetPresenters.add(this.thirdPartiesProhibitionsSetPresenter);
	//        this.thirdPartiesProhibitionsSetPresenter.bind();
	//        mainPanel.add(thirdPartiesProhibitionsSetPresenter.getDisplay().asWidget());
	//        this.controllableDisplays.add(this.thirdPartiesProhibitionsSetPresenter.getDisplay());
	//        this.thirdPartiesProhibitionsSetPresenter.addBusyDisplayHandler(this);
	//        this.thirdPartiesProhibitionsSetPresenter.addIdleDisplayHandler(this);
	//
	//
	//
	//        pnlDisclosure.add(mainPanel);
	//        return pnlDisclosure;
	//    }

	private void bind() {
		EventBus eventBus = ginjector.getEventBus();
		eventBus.addHandler(StatementDeletedEvent.TYPE, new StatementDeletedHandler() {
			@Override
			public void onStatementDeleted(Statement statement) {
				List<VariableDeclaration> vds = statement.getSyntacticItems(VariableDeclaration.class);
				for (int i = 0; i < statementSetPresenters.size() && !vds.isEmpty(); i++)
					statementSetPresenters.get(i).removeReferencesTo(statement, vds);
				// if vds is not yet empty, then we must update our
				// ReferenceableTermsKeeper removing vds' items
				for (VariableDeclaration vd : vds)
					ReferenceableTermsKeeper.getInstance().remove(vd);
			}
		});
	}

	//	private void initWidgets() {
	////		RootPanel.get("userInfo").clear();
	//		//RootPanel.get("gwtContainer").clear();
	////		RootPanel.get("selectionContainer").clear();
	//
	//	}

	/*
	 * build a DsaBean
	 */
	private DsaBean buildDsaBean(DsaBean updatedDsa, String issuer, Boolean isCompleted) {
		if (updatedDsa == null) {
			// create new dsa
			updatedDsa = DsaBeanUtils.createEmptyDsaBean(dsaUid.getText(), dsaVocabulary.getText(), null);
		}
		if (isCompleted) {
			if (issuer.compareTo(Issuer.LEGAL_EXPERT.getLabel()) != 0) {
				//				if ((updatedDsa.isRequiredConsent() && !updatedDsa.isUserConsent())
				//						|| (WidgetUtil.containsPendingRules(updatedDsa))) {
				//					updatedDsa.setStatus("PREPARED");
				//				} else {
				updatedDsa.setStatus("COMPLETED");
				//				}
			}
		} else {
			if (issuer.compareTo(Issuer.POLICY_EXPERT.getLabel()) == 0) {
				updatedDsa.setStatus("CUSTOMISED");
			}
			if (issuer.compareTo(Issuer.LEGAL_EXPERT.getLabel()) == 0) {
				updatedDsa.setStatus("TEMPLATE");
			}
		}

		if (!updatedDsa.getStatus().equals("TEMPLATE")) {
			// no versioning for dsa template
			updatedDsa.setVersion(String.valueOf(Float.valueOf(updatedDsa.getVersion()).floatValue() + 1.0));
		}
		updatedDsa.setTitle(dsaTitle.getText());
		updatedDsa.setParties(partiesPresenter.getDisplay().getChosenOrganizations());


//		GWT.log( "dataClassificationList=" + Util.listToString(dataClassificationList, "") + " selectedDataClassification=" + selectedDataClassification
//				+ "dataClassificationList.get(selectedDataClassification)=" + dataClassificationList.get(selectedDataClassification)
//				);
		updatedDsa.setPurpose(purposeList.get(selectedPurpose));
		updatedDsa.setDataClassification(dataClassificationList.get(selectedDataClassification));
		updatedDsa.setApplicationDomain(applicationDomainList.get(selectedApplicationDomain));

//		final String desc = dsaDescription.getValue();
//		GWT.log( "buildDsaBean: desc = " + desc);

		//description
		updatedDsa.setDescription(dsaDescription.getValue());


		// general policies
		updatedDsa.getUpdatePolicy().setPolicy(WidgetUtil.getPolicyTypeString(selectedUpdatePolicy));
		updatedDsa.getUpdatePolicy().setPeriodInDays(Integer.valueOf(updatePolicyAttribute.getValue()));
		updatedDsa.getRevocationPolicy().setPolicy(WidgetUtil.getPolicyTypeString(selectedRevocationPolicy));
		updatedDsa.getRevocationPolicy().setPeriodInDays(Integer.valueOf(revocationPolicyAttribute.getValue()));
		updatedDsa.getExpirationPolicy().setPolicy(WidgetUtil.getPolicyTypeString(selectedExpirationPolicy));
		updatedDsa.getExpirationPolicy().setPeriodInDays(Integer.valueOf(expirationPolicyAttribute.getValue()));


		updatedDsa.setValidity(validityPresenter.getValidity());
		updatedDsa.setUserConsent(consentCheckBox.getValue());
		updatedDsa.setRequiredConsent(askForConsentCheckBox.getValue());
		List<Statement> auths = setIssuer(authorizationsSetPresenter.getStatements(), issuer);
		updatedDsa.setAuthorizations(auths);
		List<Statement> obls = setIssuer(obligationsSetPresenter.getStatements(), issuer);
		updatedDsa.setObligations(obls);
		List<Statement> pros = setIssuer(prohibitionsSetPresenter.getStatements(), issuer);
		updatedDsa.setProhibitions(pros);

		//_MR
		// add third parties
		List<Statement> tpAuths = setIssuer(
				thirdPartiesAuthorizationsSetPresenter.getStatements(), issuer);
		updatedDsa.setDerivedObjectsAuthorizations(tpAuths);
		List<Statement> tpObls = setIssuer(
				thirdPartiesObligationsSetPresenter.getStatements(), issuer);
		updatedDsa.setDerivedObjectsObligations(tpObls);
		List<Statement> tpPros = setIssuer(
				thirdPartiesProhibitionsSetPresenter.getStatements(), issuer);
		updatedDsa.setDerivedObjectsProhibitions(tpPros);

		return updatedDsa;
	}

	/**
	 * Fill the fields using the DSA
	 *
	 * @param dsa
	 */
	protected void initFromDsaBean(DsaBean dsa, String issuer) {
		dsaUid.setText(dsa.getUid());
		dsaDescription.setText(dsa.getDescription());
		dsaVocabulary.setText(dsa.getVocabularyUri());
		dsaTitle.setText(dsa.getTitle());
		dsaStatus.setText(dsa.getStatus());

//		dsaDataClassification.setText(dsa.getDataClassification());

		askForConsentCheckBox.setValue(dsa.isRequiredConsent());
		askForConsentCheckBox.setStyleName("customCheckBox");
		consentCheckBox.setValue(dsa.isUserConsent());
		consentCheckBox.setStyleName("customCheckBox");
//		dsaPurpose.setText(dsa.getPurpose());
//
//
//		dsaApplicationDomain.setText(dsa.getApplicationDomain());


		revocationPolicy.setText(dsa.getRevocationPolicy().getPolicy());
		revocationPolicyAttribute.setText(String.valueOf(dsa.getRevocationPolicy().getPeriodInDays()));
		expirationPolicy.setText(dsa.getExpirationPolicy().getPolicy());
		expirationPolicyAttribute.setText(String.valueOf(dsa.getExpirationPolicy().getPeriodInDays()));
		updatePolicy.setText(dsa.getUpdatePolicy().getPolicy());
		updatePolicyAttribute.setText(String.valueOf(dsa.getUpdatePolicy().getPeriodInDays()));

//		GWT.log( "dataClassificationList=" + Util.listToString(dataClassificationList, "") + " selectedDataClassification=" + selectedDataClassification );

		if (dsa.getDataClassification() != null) {
			selectedDataClassification = dataClassificationList.indexOf(dsa.getDataClassification());
		} else {
			selectedDataClassification = 0;
		}
		if(selectedDataClassification < 0 || selectedDataClassification > dataClassificationList.size()) {
			selectedDataClassification = 0;
		}





		if (dsa.getPurpose() != null) {
			selectedPurpose = purposeList.indexOf(dsa.getPurpose());
		} else {
			selectedPurpose = 0;
		}
		//MRusso: bug, in alcuni casi il purpose e' stringa vuota quindi l'index vale -1 e all'apertura scoppia...
		if(selectedPurpose < 0 || selectedPurpose > purposeList.size()) {
			selectedPurpose = 0;
		}




		if (dsa.getApplicationDomain() != null) {
			selectedApplicationDomain = applicationDomainList.indexOf(dsa.getApplicationDomain());
		} else {
			selectedApplicationDomain = 0;
		}
		if(selectedApplicationDomain < 0 || selectedApplicationDomain > applicationDomainList.size()) {
			selectedApplicationDomain = 0;
		}

		partiesPresenter.setRole(issuer);
		partiesPresenter.setParties(dsa.getParties(), messages.labelPartyName(), messages.labelPartyRole(),
				messages.labelPartyResponsibilities());
		validityPresenter.setValidityLabels(messages.labelStartDate(), messages.labelEndDate(),
				messages.labelUnit());
		validityPresenter.setValidity(dsa.getValidity());
		authorizationsSetPresenter.setUserRole(issuer);
		authorizationsSetPresenter.setParamsTermList(dsa.getParamTermList());
		authorizationsSetPresenter.setStatements(dsa.getAuthorizations(), dsa.getParamTermList());
		obligationsSetPresenter.setUserRole(issuer);
		obligationsSetPresenter.setParamsTermList(dsa.getParamTermList());
		obligationsSetPresenter.setStatements(dsaBean.getObligations(), dsa.getParamTermList());
		prohibitionsSetPresenter.setUserRole(issuer);
		prohibitionsSetPresenter.setParamsTermList(dsa.getParamTermList());
		prohibitionsSetPresenter.setStatements(dsaBean.getProhibitions(), dsa.getParamTermList());

		//_MR
		// Third parties statements
		thirdPartiesAuthorizationsSetPresenter.setUserRole(issuer);
		thirdPartiesAuthorizationsSetPresenter.setParamsTermList(dsa
				.getParamTermList());
		thirdPartiesAuthorizationsSetPresenter
		.setStatements(dsaBean.getDerivedObjectsAuthorizations(),
				dsa.getParamTermList());

		thirdPartiesObligationsSetPresenter.setUserRole(issuer);
		thirdPartiesObligationsSetPresenter.setParamsTermList(dsa
				.getParamTermList());
		thirdPartiesObligationsSetPresenter.setStatements(
				dsaBean.getDerivedObjectsObligations(), dsa.getParamTermList());

		thirdPartiesProhibitionsSetPresenter.setUserRole(issuer);
		thirdPartiesProhibitionsSetPresenter.setParamsTermList(dsa
				.getParamTermList());
		thirdPartiesProhibitionsSetPresenter.setStatements(
				dsaBean.getDerivedObjectsProhibitions(), dsa.getParamTermList());
	}

	private List<Statement> setIssuer(List<Statement> statements, String issuer) {
		List<Statement> statementList = new ArrayList<Statement>();
		for (Iterator<Statement> iterator = statements.iterator(); iterator.hasNext();) {
			Statement statement = (Statement) iterator.next();
			if (statement.getStatementMetadata().getIssuer() != Issuer.LEGAL_EXPERT) {
				if (issuer.compareTo(Issuer.POLICY_EXPERT.getLabel()) == 0) {
					statement.getStatementMetadata().setIssuer(Issuer.POLICY_EXPERT);
				}
				if (issuer.compareTo(Issuer.LEGAL_EXPERT.getLabel()) == 0) {
					statement.getStatementMetadata().setIssuer(Issuer.LEGAL_EXPERT);
				}
				if (issuer.compareTo(Issuer.USER.getLabel()) == 0 && statement.getStatementMetadata().isPendingRule()) {
					statement.getStatementMetadata().setIssuer(Issuer.USER);
				}
			}
			statementList.add(statement);
		}
		return statementList;
	}





	/*
	 * create a new DSA: asks the user to choose the vocabulary URI, and creates
	 * a new DSA UID.
	 */
	private void createNewDsa(final String userId) {
		//RootPanel.get("gwtContainer").clear();
		final PopupPanel createDsaPanel = new PopupPanel(false, true);
		createDsaPanel.setAnimationEnabled(true);
		createDsaPanel.setGlassEnabled(true);
		final VerticalPanel vp = new VerticalPanel();
		createDsaPanel.setWidget(vp);
		vp.add(new Label(messages.messageVocabularyURI()));
		GWT.log("vocabularyValues: " + vocabularyValues);

//		final ListBox vocabularyListBox = WidgetUtil.getVocabularyListBox(messages.availableVocabularyList());
		final ListBox vocabularyListBox = WidgetUtil.getVocabularyListBox(vocabularyValues);

//		vocabularyListBox.addClickHandler(new ClickHandler() {
//			@Override
//			public void onClick(ClickEvent event) {
//				ListBox source = (ListBox) event.getSource();
//				selectedVocabularyUri= source.getSelectedValue();
//			}});

		vocabularyListBox.setStyleName("customListBox");
		vp.add(vocabularyListBox);
		HorizontalPanel hPanel = new HorizontalPanel();
		final Button okButton = new Button(messages.buttonOK());
		okButton.setStyleName("myButton");
		hPanel.add(okButton);
		Button cancelButton = new Button(messages.buttonCancel());
		cancelButton.setStyleName("myButton");
		hPanel.add(cancelButton);
		vp.add(hPanel);
		final Label message = new Label("");
		vp.add(message);
		createDsaPanel.show();
		okButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int selIndex = vocabularyListBox.getSelectedIndex();
				String selValue = vocabularyListBox.getSelectedValue();
				//				String vocUri = selectedVocabularyUri;//uriTextBox.getText().trim();
				String vocUri = selValue;
				GWT.log("CreateNewDsa: index " + selIndex + " - " + vocUri);

				if (selIndex == 0 || vocUri == null || "***".equals(vocUri)) {
					message.setText(messages.messageVocabularyURIShort());
					return;
				}
				// add '#' at the end if needed
				final String vocabularyUri = (vocUri.endsWith("#")) ? vocUri : vocUri + "#";
				// I cannot verify URI on the client side, because GWT does not
				// give support for java.net.URL
				message.setText(messages.messageVocabularyLoading());
				final DispatchAsync dispatcher = ginjector.getDispatchAsync();
				dispatcher.execute(new CreateNewDsaAction(vocabularyUri, isStandalone, userId), new AsyncCallback<CreateNewDsaResult>() {
					@Override
					public void onFailure(Throwable caught) {
						GWT.log("CreateNewDsaAction-CreateNewDsaAction - 1");
						String text = messages.messageErrorGettingDSA();
						if (!"".equals(caught.getMessage()))
							text += ": " + caught.getMessage();
						text += messages.messageErrorEditingDSA();
						message.setText(text);
						backHome(text);
					}
					@Override
					public void onSuccess(CreateNewDsaResult result) {
						GWT.log("CreateNewDsaAction-onSuccess - 1");

						//RootPanel.get("gwtContainer").clear();
						dsaBean = result.getDsaBean();

						if (dsaBean == null || dsaBean.getUid() == null) {
							GWT.log("CreateNewDsaAction-onSuccess - 2");
							message.setText(messages.messageErrorGettingNewDSA());
						} else {
							GWT.log("CreateNewDsaAction-onSuccess - 3");
							ReferenceableTermsKeeper.getInstance().initWith(vocabularyUri);
							GWT.log("CreateNewDsaAction-onSuccess - 4");
							initFromDsaBean(dsaBean, userRole);
							GWT.log("CreateNewDsaAction-onSuccess - 5");
							createDsaPanel.hide();
							GWT.log("CreateNewDsaAction-onSuccess - 6");
							loadDSAFieldPanel(false);
							GWT.log("CreateNewDsaAction-onSuccess - 7");
						}
					}
				});

			}
		});
		cancelButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dsaUuid = null;
				createDsaPanel.hide();
				//RootPanel.get("gwtContainer").clear();
				RepoCallBack repoCallBack = new RepoCallBack();
				dsaManagerService.loadRepositoryContent(userRole, repoCallBack);
				//				RootPanel.get("gwtContainer");
				if (returnUrl != null) {
					Window.Location.replace(returnUrl);
				}
			}
		});
	}










	//	/*
	//	 * create a new DSA: asks the user to choose the vocabulary URI, and creates
	//	 * a new DSA UID.
	//	 */
	//	private void createNewDsa(final String userId) {
	//		//RootPanel.get("gwtContainer").clear();
	//		final PopupPanel createDsaPanel = new PopupPanel(false, true);
	//		createDsaPanel.setAnimationEnabled(true);
	//		createDsaPanel.setGlassEnabled(true);
	//		final VerticalPanel vp = new VerticalPanel();
	//		createDsaPanel.setWidget(vp);
	//		vp.add(new Label(messages.messageVocabularyURI()));
	//		ListBox vocabularyListBox=WidgetUtil.getVocabularyListBox(messages.availableVocabularyList());
	//		vocabularyListBox.addClickHandler(new ClickHandler() {
	//			@Override
	//			public void onClick(ClickEvent event) {
	//				ListBox source = (ListBox) event.getSource();
	//				selectedVocabularyUri= source.getSelectedValue();
	//			}});
	//		vocabularyListBox.setStyleName("customListBox");
	//		vp.add(vocabularyListBox);
	//		HorizontalPanel hPanel = new HorizontalPanel();
	//		final Button okButton = new Button(messages.buttonOK());
	//		okButton.setStyleName("myButton");
	//		hPanel.add(okButton);
	//		Button cancelButton = new Button(messages.buttonCancel());
	//		cancelButton.setStyleName("myButton");
	//		hPanel.add(cancelButton);
	//		vp.add(hPanel);
	//		final Label message = new Label("");
	//		vp.add(message);
	//		createDsaPanel.show();
	//		okButton.addClickHandler(new ClickHandler() {
	//			@Override
	//			public void onClick(ClickEvent event) {
	//				String vocUri = selectedVocabularyUri;//uriTextBox.getText().trim();
	//				if (vocUri == null || "***".equals(vocUri))
	//
	//					message.setText(messages.messageVocabularyURIShort());
	//				// add '#' at the end if needed
	//				final String vocabularyUri = (vocUri.endsWith("#")) ? vocUri : vocUri + "#";
	//				// I cannot verify URI on the client side, because GWT does not
	//				// give support for java.net.URL
	//				message.setText(messages.messageVocabularyLoading());
	//				final DispatchAsync dispatcher = ginjector.getDispatchAsync();
	//				dispatcher.execute(new CreateNewDsaAction(vocabularyUri, isStandalone, userId),
	//						new AsyncCallback<CreateNewDsaResult>() {
	//							@Override
	//							public void onFailure(Throwable caught) {
	//								String text = messages.messageErrorGettingDSA();
	//								if (!"".equals(caught.getMessage()))
	//									text += ": " + caught.getMessage();
	//								text += messages.messageErrorEditingDSA();
	//								message.setText(text);
	//								backHome(text);
	//							}
	//							@Override
	//							public void onSuccess(CreateNewDsaResult result) {
	//								//RootPanel.get("gwtContainer").clear();
	//								dsaBean = result.getDsaBean();
	//								if (dsaBean == null || dsaBean.getUid() == null) {
	//									message.setText(messages.messageErrorGettingNewDSA());
	//								} else {
	//
	//									ReferenceableTermsKeeper.getInstance().initWith(vocabularyUri);
	//
	//									initFromDsaBean(dsaBean, userRole);
	//									createDsaPanel.hide();
	//									loadDSAFieldPanel(false);
	//								}
	//							}
	//						});
	//			}
	//		});
	//		cancelButton.addClickHandler(new ClickHandler() {
	//			@Override
	//			public void onClick(ClickEvent event) {
	//				dsaUuid = null;
	//				createDsaPanel.hide();
	//				//RootPanel.get("gwtContainer").clear();
	//				RepoCallBack repoCallBack = new RepoCallBack();
	//				dsaManagerService.loadRepositoryContent(userRole, repoCallBack);
	////				RootPanel.get("gwtContainer");
	//				if (returnUrl != null)
	//					Window.Location.replace(returnUrl);
	//			}
	//		});
	//	}

	@Override
	public void onBusyDisplay(ControllableDisplay display) {
		saveButton.setEnabled(false);
		for (ControllableDisplay cd : controllableDisplays) {
			if (!cd.equals(display)) {
				cd.deactivate();
			}
		}
	}

	@Override
	public void onIdleDisplay(ControllableDisplay display) {
		GWT.log("DSAAuthoringTool onIdleDisplay");
		if(!saveButton.isEnabled()) {
			saveButton.setEnabled(true);
			for (ControllableDisplay cd : controllableDisplays) {
				cd.activate();
			}
		}
	}

	/**
	 * Create a radio button for the initial selection
	 *
	 * @return
	 */
	private Widget createRadioButton() {
		// Create some radio buttons, all in one group 'radioGroup'.
		final RadioButton radioButton1 = new RadioButton("radioGroup", Issuer.LEGAL_EXPERT.getLabel());
		final RadioButton radioButton2 = new RadioButton("radioGroup", Issuer.POLICY_EXPERT.getLabel());
		final RadioButton radioButton3 = new RadioButton("radioGroup", Issuer.USER.getLabel());
		// Check 'First' by default.
		radioButton1.setValue(true);
		userRole = radioButton1.getText();
		radioButton1.setEnabled(true);
		radioButton1.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				userRole = radioButton1.getText();
			}
		});
		radioButton2.setValue(false);
		radioButton2.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				userRole = radioButton2.getText();
			}
		});
		radioButton3.setValue(false);
		radioButton3.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				userRole = radioButton3.getText();
			}
		});
		// Add toggle button to the root panel.
		HorizontalPanel panel = new HorizontalPanel();
		panel.setSpacing(10);
		radioButton1.setStyleName("myRadioButton");
		radioButton2.setStyleName("myRadioButton");
		radioButton3.setStyleName("myRadioButton");
		panel.add(radioButton1);
		panel.add(radioButton2);
		panel.add(radioButton3);
		return panel;
	}



	private ListBox createGlobalPoliciesListBox(ArrayList<String> itemValue, boolean dropdown, boolean isEditMode, String userRole,
			int selected, final int index) {
		ListBox widget = new ListBox();
		widget.addStyleName("listBox");
		if (selected == 2) { // need to specify expiration period
			switch (index) { // evaluate for which policy enable the expiration period
			case 1:
				expirationPeriodEnabled = true;
			case 2:
				updatePeriodEnabled = true;
			case 3:
				revocationPeriodEnabled = true;
			default:
				revocationPeriodEnabled = false;
				updatePeriodEnabled = false;
				expirationPeriodEnabled = false;
			}
		}
		selectedExpirationPolicy = WidgetUtil.getPolicyTypeIndex(expirationPolicy.getValue());
		selectedUpdatePolicy = WidgetUtil.getPolicyTypeIndex(updatePolicy.getValue());
		selectedRevocationPolicy = WidgetUtil.getPolicyTypeIndex(revocationPolicy.getValue());

		for (int i = 0; i < itemValue.size(); i++) {
			widget.addItem(itemValue.get(i));
		}

		widget.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				ListBox source = (ListBox) event.getSource();
				int item = source.getSelectedIndex();
				if (index == 1) {
					selectedExpirationPolicy = item;
					expirationPolicy.setValue(WidgetUtil.getPolicyTypeString(selectedExpirationPolicy));
					if (item == 2) {
						expirationPeriodEnabled = true;
						expirationPolicyAttribute.setEnabled(expirationPeriodEnabled);
					} else {
						expirationPeriodEnabled = false;
						expirationPolicyAttribute.setEnabled(expirationPeriodEnabled);
						expirationPolicyAttribute.setValue(String.valueOf(0));
					}
				}
				if (index == 2) {
					selectedUpdatePolicy = item;
					updatePolicy.setValue(WidgetUtil.getPolicyTypeString(selectedUpdatePolicy));
					if (item == 2) {
						updatePeriodEnabled = true;
						updatePolicyAttribute.setEnabled(updatePeriodEnabled);
					} else {
						updatePeriodEnabled = false;
						updatePolicyAttribute.setEnabled(updatePeriodEnabled);
						updatePolicyAttribute.setValue(String.valueOf(0));
					}
				}
				if (index == 3) {
					selectedRevocationPolicy = item;
					revocationPolicy.setValue(WidgetUtil.getPolicyTypeString(selectedRevocationPolicy));
					if (item == 2) {
						revocationPeriodEnabled = true;
						revocationPolicyAttribute.setEnabled(revocationPeriodEnabled);
					} else {
						revocationPeriodEnabled = false;
						revocationPolicyAttribute.setEnabled(revocationPeriodEnabled);
						revocationPolicyAttribute.setValue(String.valueOf(0));
					}
				}
			}
		});

		if (isEditMode) {
			widget.setSelectedIndex(selected);
		} else {
//			selectedPurpose = 1;
//			selectedDataClassification = 1;
		}
		if (!dropdown) {
			widget.setVisibleItemCount(3);
		}
		return widget;
	}









	private ListBox createPurposePanelListBox(ArrayList<String> itemValue, boolean dropdown, boolean isEditMode, String userRole,
			int selected, final int listType) {
		ListBox widget = new ListBox();
		widget.addStyleName("listBox");
		for (int i = 0; i < itemValue.size(); i++) {
			widget.addItem(itemValue.get(i));
		}

		widget.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				ListBox source = (ListBox) event.getSource();
				int item = source.getSelectedIndex();
				if (listType == LIST_BOX_PURPOSE) {
					selectedPurpose = item;
				}
				if (listType == LIST_BOX_APPDOMAIN) {
					selectedApplicationDomain = item;
				}
				if (listType == LIST_BOX_DATACLASSIFICATION) {
					selectedDataClassification = item;
				}
			}
		});

		if (isEditMode) {
			widget.setSelectedIndex(selected);
		}
//		else {
//			selectedPurpose = 1;
//			selectedDataClassification = 1;
//			selectedApplicationDomain = 1;
//		}
		if (!dropdown) {
			widget.setVisibleItemCount(5);
		}
		return widget;
	}

	/**
	 * Manage the server interaction about repository content
	 *
	 * @author gambardc
	 *
	 */
	private class RepoCallBack implements AsyncCallback<ArrayList<RepositoryContent>> {
		public void onFailure(Throwable caught) {
			/* server side error occured */
			Window.alert("Unable to obtain server response: " + caught.getMessage());
		}

		@Override
		public void onSuccess(ArrayList<RepositoryContent> result) {

			final ArrayList<RepositoryContent> repoContent = result;
			final CellTable<RepositoryContent> table = new CellTable<RepositoryContent>();
			table.setStyleName("myCellTable");
			TextColumn<RepositoryContent> uidColumn = new TextColumn<RepositoryContent>() {
				@Override
				public String getValue(RepositoryContent row) {
					return row.getItem().get("uuid");
				}
			};
			TextColumn<RepositoryContent> sizeColumn = new TextColumn<RepositoryContent>() {
				@Override
				public String getValue(RepositoryContent row) {
					return row.getItem().get("size");
				}
			};
			TextColumn<RepositoryContent> titleColumn = new TextColumn<RepositoryContent>() {
				@Override
				public String getValue(RepositoryContent row) {
					return row.getItem().get("title");
				}
			};
			TextColumn<RepositoryContent> statusColumn = new TextColumn<RepositoryContent>() {
				@Override
				public String getValue(RepositoryContent row) {
					return row.getItem().get("status");
				}
			};
			// Add the columns.
			table.addColumn(titleColumn, messages.labelTitle());
			table.addColumn(uidColumn, messages.labelUID());
			table.addColumn(sizeColumn, messages.labelSize());
			table.addColumn(statusColumn, messages.labelStatus());
			final SelectionModel<? super RepositoryContent> selectionModel = new SingleSelectionModel<RepositoryContent>();
			selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
				public void onSelectionChange(SelectionChangeEvent event) {
					@SuppressWarnings("unchecked")
					RepositoryContent selected = ((SingleSelectionModel<RepositoryContent>) selectionModel)
					.getSelectedObject();
					if (selected != null) {
						dsaUuid = selected.getItem().get("uuid");
					}
				}
			});
			table.setSelectionModel(selectionModel);
			table.setRowData(0, (List<? extends RepositoryContent>) repoContent);
			final ListDataProvider<RepositoryContent> model = new ListDataProvider<RepositoryContent>(repoContent);
			model.addDataDisplay(table);
			Column<RepositoryContent, String> deleteBtn = new Column<RepositoryContent, String>(new ButtonCell()) {
				@Override
				public String getValue(RepositoryContent c) {
					return "x";
				}
			};
			table.addColumn(deleteBtn, "");
			deleteBtn.setFieldUpdater(new FieldUpdater<RepositoryContent, String>() {
				@Override
				public void update(int index, RepositoryContent object, String value) {
					model.getList().remove(object);
					repoContent.remove(object);
					dsaManagerService.deleteFileFromRepository(object.getItem().get("uuid"), new AsyncCallback<Void>() {

						@Override
						public void onSuccess(Void result) {
							Window.alert(messages.messageFileRemoved());

						}

						@Override
						public void onFailure(Throwable caught) {
							Window.alert(messages.messageDeleteOperationFailed());
						}
					});
					model.refresh();
					table.redraw();
				}
			});
			HorizontalPanel hPanel = new HorizontalPanel();
			hPanel.add(table);
			VerticalPanel vPanel = new VerticalPanel();
			vPanel.setSpacing(10);
			vPanel.add(hPanel);
			HorizontalPanel hButtonPanel = new HorizontalPanel();
			vPanel.setSpacing(5);
			Button buttonMessage = null;
			for (int i = 0; i < 5; i++) {
				if ((i == 0) && (WidgetUtil.setCustomViewButtonPanel(userRole, messages.buttonViewDSA(),
						messages.buttonViewDSATemplate()) != null)) {
					buttonMessage = new Button(WidgetUtil.setCustomViewButtonPanel(userRole, messages.buttonViewDSA(),
							messages.buttonViewDSATemplate()));

					ViewHandler viewHandler = new ViewHandler();
					buttonMessage.addClickHandler(viewHandler);
				} else if ((i == 1) && (WidgetUtil.setCustomViewRawDataButtonPanel(userRole, messages.buttonRawDSA(),
						messages.buttonRawDSATemplate()) != null)) {
					buttonMessage = new Button(WidgetUtil.setCustomViewRawDataButtonPanel(userRole,
							messages.buttonRawDSA(), messages.buttonRawDSATemplate()));
					ViewRawDataHandler viewHandler = new ViewRawDataHandler();
					buttonMessage.addClickHandler(viewHandler);
				} else if ((i == 2) && (WidgetUtil.setCustomCreateButtonPanel(userRole,
						messages.buttonCreateDSATemplate()) != null)) {
					buttonMessage = new Button(
							WidgetUtil.setCustomCreateButtonPanel(userRole, messages.buttonCreateDSATemplate()));
					CreateHandler createHandler = new CreateHandler();
					buttonMessage.addClickHandler(createHandler);
				} else if ((i == 3) && (WidgetUtil.setCustomEditButtonPanel(userRole, messages.buttonEditDSA(),
						messages.buttonEditDSATemplate()) != null)) {
					buttonMessage = new Button(WidgetUtil.setCustomEditButtonPanel(userRole, messages.buttonEditDSA(),
							messages.buttonEditDSATemplate()));
					EditDSAHandler editHandler = new EditDSAHandler();
					buttonMessage.addClickHandler(editHandler);
				} else if (i == 4) {
					buttonMessage = new Button(messages.buttonLogout());
					buttonMessage.addClickHandler(new ClickHandler() {
						@Override
						public void onClick(ClickEvent event) {
							loadRolePanel();
						}
					});
				}
				buttonMessage.setStyleName("myButton");
				hButtonPanel.add(buttonMessage);
				vPanel.setCellHorizontalAlignment(buttonMessage, HasHorizontalAlignment.ALIGN_CENTER);
			}
			vPanel.add(hButtonPanel);

			DecoratorPanel panel = new DecoratorPanel();

			panel.add(vPanel);

			//			// Add table widget to the root panel.
			//			RootPanel.get("gwtContainer").add(panel);

			updateCentralPanel(panel);
		}
	}

	// Create a handler for the view button
	//	class ViewHandler implements ClickHandler {
	//		public void onClick(ClickEvent event) {
	//			//RootPanel.get("gwtContainer").clear();
	//			final MessagePanel messagePanel = new MessagePanel("Loading your DSA. Please, wait ...");//
	//			messagePanel.show();
	//			if (dsaUuid == null) {
	//				String text = messages.messageDSASelection();
	//				messagePanel.setAutoHideEnabled(true);
	//				messagePanel.setText(text);
	//				backHome(text);
	//			} else {
	//				messagePanel.hide();
	//				viewDsaContent(dsaUuid);
	//			}
	//		}
	//	}

	class ViewHandler implements ClickHandler {
		public void onClick(ClickEvent event) {
			//RootPanel.get("gwtContainer").clear();

			if (dsaUuid == null) {
				String text = messages.messageDSASelection();
				final MessagePanel messagePanel = new MessagePanel("Loading your DSA. Please, wait ...");
				messagePanel.setAutoHideEnabled(true);
				messagePanel.setText(text);
				messagePanel.show();
				backHome(text);
			} else {
				//				messagePanel.hide();
				viewDsaContent(dsaUuid);
			}
		}
	}


	/*
	 * view the content of the DSA
	 */
	private void viewDsaContent(String uid) {
		final MessagePanel messagePanel = new MessagePanel(messages.messageDSALoading());
		//		final MessagePanel messagePanel = new MessagePanel("XXXXX");

		messagePanel.show();
		final DispatchAsync dispatcher = ginjector.getDispatchAsync();
		dispatcher.execute(new FetchDsaAction(uid, userRole, isStandalone), new AsyncCallback<FetchDsaResult>() {
			@Override
			public void onFailure(Throwable caught) {
				String text = messages.messageDSASelection();
				if (!"".equals(caught.getMessage()))
					text += ": " + caught.getMessage();
				messagePanel.setAutoHideEnabled(true);
				messagePanel.setText(text);
				backHome(text);
			}
			@Override
			public void onSuccess(FetchDsaResult result) {
				dsaBean = result.getDsaBean();
				if (dsaBean == null) {
					messagePanel.setAutoHideEnabled(false);
					messagePanel.setText(messages.messageDSAErrorLoading());
				} else {
					long maxIndex = DsaBeanUtils.getMaxUidValue(dsaBean);
					UIDGenerator.getInstance().setIndex(maxIndex);
					ReferenceableTermsKeeper.getInstance().initWith(dsaBean);
					initFromDsaBean(dsaBean, userRole);
					messagePanel.hide();
					view();
				}
			}
		});
	}

	// Create a handler for the raw data button
	class ViewRawDataHandler implements ClickHandler {
		/**
		 * Fired when the user clicks on the sendButton.
		 */
		public void onClick(ClickEvent event) {
			dsaManagerService.viewRawData(dsaUuid, true, new AsyncCallback<String>() {
				public void onFailure(Throwable caught) {
					Window.alert(messages.messageDSAErrorOpening());
				}
				public void onSuccess(String result) {
					HtmlDialog myDialog = new HtmlDialog(dsaUuid, result, null, "dialogContent",
							messages.buttonCancel());
					myDialog.center();
					myDialog.show();
				}
			});
		}
	}

	class CreateHandler implements ClickHandler {
		public void onClick(ClickEvent event) {
			GWT.runAsync(new RunAsyncCallback() {
				public void onFailure(Throwable caught) {
					String text = messages.messageGenericError();
					Window.alert(text);
					backHome(text);
				}
				public void onSuccess() {
					// load form to create a dsa template
					//RootPanel.get("gwtContainer").clear();
					// if the DSA is invoked as standalone tool, the userId is
					// not used
					createNewDsa(null);
				}
			});
		}
	}

	// Create a handler for the enter button
	class MyHandler implements ClickHandler {
		public void onClick(ClickEvent event) {
			//RootPanel.get("gwtContainer").clear();
			//			RootPanel.get("selectionContainer").clear();
			sendNameToServer();
			backHome(messages.messageOperationCompleted());
		}
		private void sendNameToServer() {
			String textToServer = userRole;
			textToServerLabel.setText(textToServer);
			// serverResponseLabel.setText("");
			dsaManagerService.retrieveUser(textToServer, new AsyncCallback<String>() {
				public void onFailure(Throwable caught) {
					/* server side error occured */
					Window.alert(messages.messageConnectionError() + caught.getMessage());
				}
				public void onSuccess(String result) {
					updateUserInfo(messages.messageLoggedAs() + userRole);
					//Label userInfo = new Label(messages.messageLoggedAs() + userRole);
					//					userInfo.setStyleName("userInfo");
					//					RootPanel.get("userInfo").add(userInfo);
				}
			});
		}
	}

	// Create a handler for the save button
	class SaveDsaHandler implements ClickHandler {
		public void onClick(ClickEvent event) {
			//RootPanel.get("gwtContainer").clear();
			saveDsa();
		}

		/*
		 * save the DSA
		 */
		private void saveDsa() {
			final MessagePanel messagePanel = new MessagePanel(messages.messageSaving());
			messagePanel.show();
			DsaBean updatedBean = buildDsaBean(null, userRole, false);
			final DispatchAsync dispatcher = ginjector.getDispatchAsync();
			dispatcher.execute(new StoreDsaAction(updatedBean, isStandalone), new AsyncCallback<StoreDsaResult>() {
				@Override
				public void onFailure(Throwable caught) {
					String text = messages.messageErrorSaving();
					if (!"".equals(caught.getMessage()))
						text += ": " + caught.getMessage();
					text += messages.messageContinue();
					messagePanel.setAutoHideEnabled(true);
					messagePanel.setText(text);
					backHome(text);
				}

				@Override
				public void onSuccess(StoreDsaResult result) {
					messagePanel.setAutoHideEnabled(true);
					String text = null;
					if (result.isStored()) {
						if (result.isStored()) {
							text = messages.messageDoneAndContinue();
							messagePanel.setText(text);
						} else {
							text = messages.messageDSASavedWithWarning();
							messagePanel.setText(text);
						}

					} else {
						text = messages.messageDSASavedWithError();
						messagePanel.setText(text);
					}
					backHome(text);
				}
			});
		}
	}

	// Create a handler for the update button
	class UpdateDsaHandler implements ClickHandler {
		public void onClick(ClickEvent event) {
			//RootPanel.get("gwtContainer").clear();
			updateDsa();
		}
	}

	private void updateDsa() {
		final MessagePanel messagePanel = new MessagePanel(messages.messageUpdating());
		messagePanel.show();
		DsaBean updatedDsa = buildDsaBean(dsaBean, userRole, false);
		final DispatchAsync dispatcher = ginjector.getDispatchAsync();
		dispatcher.execute(new StoreDsaAction(updatedDsa, isStandalone), new AsyncCallback<StoreDsaResult>() {
			@Override
			public void onFailure(Throwable caught) {
				String text = messages.messafeErrorUpdatingDSA();
				if (!"".equals(caught.getMessage()))
					text += ": " + caught.getMessage();
				text += messages.messageContinue();
				messagePanel.setAutoHideEnabled(true);
				messagePanel.setText(text);
				backHome(text);
			}
			@Override
			public void onSuccess(StoreDsaResult result) {
				messagePanel.setAutoHideEnabled(true);
				String text = null;
				if (result.isStored()) {
					if (result.isStored()) {
						text = messages.messageDoneAndContinue();
						messagePanel.setText(text);
					} else {
						text = messages.messageDSASavedWithWarning();
						messagePanel.setText(text);
					}
				} else {
					text = messages.messafeErrorUpdatingDSA();
					messagePanel.setText(text);
				}
				backHome(text);
			}
		});
	}

	// Create a handler for the complete button
	class CompleteDsaHandler implements ClickHandler {
		public void onClick(ClickEvent event) {
			if (dsaBean.isRequiredConsent() && (userRole.compareTo(Issuer.USER.getLabel()) == 0)
					&& (!consentCheckBox.getValue())) {
				Window.alert(messages.messageInfoMissingConsent());
			} else {
				if (completeDsa()) {
					//RootPanel.get("gwtContainer").clear();
					updateCentralPanel(null);
				}
				else {
					final MessagePanel errorPanel = new MessagePanel(messages.messageInfoMissingUserPreferences());
					errorPanel.show();
					errorPanel.setAutoHideEnabled(true);
				}
			}
		}
		private boolean completeDsa() {
			DsaBean updatedDsa = buildDsaBean(dsaBean, userRole, true);
			if ((userRole.compareTo(Issuer.USER.getLabel()) == 0)
					&& DsaBeanUtils.containsEmptyTextFreeFields(updatedDsa)) {
				return false;
			} else {
				final MessagePanel messagePanel = new MessagePanel(messages.messageFinalizing());
				messagePanel.show();
				final DispatchAsync dispatcher = ginjector.getDispatchAsync();
				dispatcher.execute(new StoreDsaAction(updatedDsa, isStandalone), new AsyncCallback<StoreDsaResult>() {

					@Override
					public void onFailure(Throwable caught) {
						String text = messages.messageDSACompletingWithError();
						if (!"".equals(caught.getMessage()))
							text += ": " + caught.getMessage();
						text += caught.getCause() + messages.messageContinue();
						messagePanel.setAutoHideEnabled(true);
						messagePanel.setText(text);
						backHome(text);
					}
					@Override
					public void onSuccess(StoreDsaResult result) {
						messagePanel.setAutoHideEnabled(true);
						String text = null;
						if (result.isStored()) {
							text = messages.messageDoneAndContinue();
							messagePanel.setText(text);
						} else {
							text = messages.messageDSACompleteWithWarning();
							messagePanel.setText(text);
						}
						backHome(text);
					}
				});
			}
			return true;
		}
	}

	private class EditDSAHandler implements ClickHandler {
		public void onClick(ClickEvent event) {
			//RootPanel.get("gwtContainer").clear();
			loadDsa(dsaUuid);
		}
	}

	/*
	 * load the DSA from remote repository
	 */
	private void loadDsaFromRemoteRepository(String uid) {
		final MessagePanel messagePanel = new MessagePanel(messages.messageLoadingFromRemoteRepository());
		messagePanel.show();
		final DispatchAsync dispatcher = ginjector.getDispatchAsync();
		dispatcher.execute(new FetchDsaAction(uid, userRole, isStandalone), new AsyncCallback<FetchDsaResult>() {
			@Override
			public void onFailure(Throwable caught) {
				String text = messages.messagesLoadingAndRetry();
				if (!"".equals(caught.getMessage()))
					text += ": " + caught.getMessage();
				messagePanel.setAutoHideEnabled(true);
				messagePanel.setText(text);
				backHome(text);
			}
			@Override
			public void onSuccess(FetchDsaResult result) {
				dsaBean = result.getDsaBean();
				String text = null;
				if ((dsaBean.getStatus().compareTo("COMPLETED") == 0)
						|| (dsaBean.getStatus().compareTo("AVAILABLE") == 0)) {
					text = messages.messageErrorEditDSA();
					messagePanel.setAutoHideEnabled(true);
					messagePanel.setText(text);
					backHome(text);
				} else {
					if (dsaBean == null) {
						text = messages.messageDSAErrorLoading();
						messagePanel.setAutoHideEnabled(false);
						messagePanel.setText(text);
					} else {
						long maxIndex = DsaBeanUtils.getMaxUidValue(dsaBean);
						UIDGenerator.getInstance().setIndex(maxIndex);
						ReferenceableTermsKeeper.getInstance().initWith(dsaBean);
						initFromDsaBean(dsaBean, userRole);
						messagePanel.hide();
						loadDSAFieldPanel(true);
					}
				}
			}
		});
	}

	/*
	 * load the DSA
	 */
	private void loadDsa(String uid) {
		final MessagePanel messagePanel = new MessagePanel(messages.messageDSALoading());
		messagePanel.show();
		final DispatchAsync dispatcher = ginjector.getDispatchAsync();
		dispatcher.execute(new FetchDsaAction(uid, userRole, isStandalone), new AsyncCallback<FetchDsaResult>() {
			@Override
			public void onFailure(Throwable caught) {
				String text = messages.messageDSASelection();
				if (!"".equals(caught.getMessage()))
					text += ": " + caught.getMessage();
				messagePanel.setAutoHideEnabled(true);
				messagePanel.setText(text);
				backHome(text);
			}
			@Override
			public void onSuccess(FetchDsaResult result) {
				dsaBean = result.getDsaBean();
				String text = null;
				if ((dsaBean.getStatus().compareTo("COMPLETED") == 0)
						|| (dsaBean.getStatus().compareTo("AVAILABLE") == 0)) {
					text = messages.messageErrorEditDSA();
					messagePanel.setAutoHideEnabled(true);
					messagePanel.setText(text);
					backHome(text);
				} else {
					if (dsaBean == null) {
						text = messages.messageDSAErrorLoading();
						messagePanel.setAutoHideEnabled(false);
						messagePanel.setText(text);
					} else {
						long maxIndex = DsaBeanUtils.getMaxUidValue(dsaBean);
						UIDGenerator.getInstance().setIndex(maxIndex);
						ReferenceableTermsKeeper.getInstance().initWith(dsaBean);
						initFromDsaBean(dsaBean, userRole);
						messagePanel.hide();
						loadDSAFieldPanel(true);
					}
				}

			}
		});
	}

	private void view() {
		VerticalPanel vPanel = new VerticalPanel();
		vPanel.setSpacing(10);
		Button homeButton = new Button(messages.buttonBack());
		homeButton.setStyleName("myButton");
		BackHomeHandler backHandler = new BackHomeHandler();
		homeButton.addClickHandler(backHandler);
		vPanel.add(homeButton);
		vPanel.add(readOnlyDsa(true));
		//		HorizontalPanel hPanel = new HorizontalPanel();
		//		if (dsaBean.isRequiredConsent()) {
		//			Label consentLabel = new Label();
		//			if (dsaBean.isUserConsent()) {
		//				consentLabel.setText(messages.CONSENT_STATEMENT_OBTAINED() + purposeList.get(selectedPurpose));
		//			} else {
		//				consentLabel.setText(messages.CONSENT_STATEMENT_NOT_OBTAINED() + purposeList.get(selectedPurpose));
		//			}
		//			hPanel.add(consentLabel);
		//			vPanel.add(hPanel);
		//		}
		DecoratorPanel panel = new DecoratorPanel();
		panel.add(vPanel);
		//		RootPanel.get("gwtContainer").add(panel);
		updateCentralPanel(panel);

	}

	private Widget readOnlyDsa(boolean viewMode) {
		VerticalPanel vPanel = new VerticalPanel();
		VerticalPanel innerLVPanel = new VerticalPanel();
		VerticalPanel innerRVPanel = new VerticalPanel();
		HorizontalPanel hPanel = new HorizontalPanel();
		HorizontalPanel subHPanel = new HorizontalPanel();
		//		if (dsaBean.isRequiredConsent()) {
		//			Label lblName = new Label(messages.CONSENT_WARNING_MESSAGE());
		//			lblName.setStyleName("italicFont");
		//			hPanel.add(lblName);
		//		} else {
		//			Label lblName = new Label(messages.CONSENT_INFO_MESSAGE());
		//			lblName.setStyleName("italicFont");
		//			hPanel.add(lblName);
		//		}
		vPanel.add(hPanel);
		// title
		hPanel = new HorizontalPanel();
		Label lblName = new Label(messages.labelTitle());
		lblName.setTitle(messages.tooltipTitle());
		hPanel.add(lblName);
		Label title = new Label();
		title.setText(this.dsaTitle.getText());
		title.setStyleName("normalFont");
		hPanel.add(title);
		innerLVPanel.add(hPanel);
		// status
		hPanel = new HorizontalPanel();
		lblName = new Label(messages.labelStatus());
		hPanel.add(lblName);
		Label status = new Label();
		status.setText(this.dsaStatus.getText());
		status.setStyleName("normalFont");
		hPanel.add(status);
		innerRVPanel.add(hPanel);

//		// purpose
//		hPanel = new HorizontalPanel();
//		lblName = new Label(messages.labelPurpose());
//		lblName.setTitle(messages.tooltipPurpose());
//		hPanel.add(lblName);
//		Label purposeValue = new Label();
//		purposeValue.setText(purposeList.get(selectedPurpose));
//		purposeValue.setStyleName("normalFont");
//		hPanel.add(purposeValue);
//		innerLVPanel.add(hPanel);
//
//
//
//		//data classification
//		hPanel = new HorizontalPanel();
//		lblName = new Label(messages.labelDataClassification());
//		lblName.setTitle(messages.tooltipDataClassification());
//		hPanel.add(lblName);
//		Label dcValue = new Label();
//		dcValue.setText(dataClassificationList.get(selectedDataClassification));
//		dcValue.setStyleName("normalFont");
//		hPanel.add(dcValue);
//		innerRVPanel.add(hPanel);




		subHPanel.add(innerLVPanel);
		subHPanel.add(innerRVPanel);
		vPanel.add(subHPanel);

		vPanel.add(createPurposePanel(false, true));



		HorizontalPanel headerDesc = new HorizontalPanel();
//		LabelWithTooltip lDesc = new LabelWithTooltip(messages.labelDescription(), messages.tooltipDescription());
//		header.add(lDesc);
		headerDesc.add(new Image(resources.closedArrow()));
		headerDesc.add(new Label(messages.labelDescription()));
		String desc = dsaBean.getDescription();
		this.dsaDescription.setText(desc);
		TextArea tmpTextArea = new TextArea();
		tmpTextArea.setText(!Util.isEmpty(desc) ? desc : messages.messageNotAvailable());
		tmpTextArea.setReadOnly(true);
		tmpTextArea.setStyleName("normalFont");
		DisclosurePanel pnlDisclosureDescription = new DisclosurePanel();
		pnlDisclosureDescription.setHeader(headerDesc);
		pnlDisclosureDescription.setTitle(messages.tooltipDescription());
		pnlDisclosureDescription.setContent(tmpTextArea);
		pnlDisclosureDescription.setStyleName("my-gwt-DisclosurePanel");
		vPanel.add(pnlDisclosureDescription);







//		hPanel = new HorizontalPanel();
//		DisclosurePanel pnlDisclosure = new DisclosurePanel();
////		LabelWithTooltip lDesc = new LabelWithTooltip(messages.labelDescription(), messages.tooltipDescription());
////		pnlDisclosure.setHeader(lDesc);
//		pnlDisclosure.setHeader(new Label(messages.labelDescription()));
//		pnlDisclosure.setTitle(messages.tooltipDescription());
//		pnlDisclosure.setStyleName("my-gwt-DisclosurePanel");
//		this.dsaDescription.setText(dsaBean.getDescription());
//		this.dsaDescription.setStyleName("myTextArea");
//		TextArea description = new TextArea();
//		description.setText(dsaDescription.getValue().compareTo("") != 0 ? dsaDescription.getValue() : messages.messageNotAvailable());
//		description.setReadOnly(true);
//		description.setStyleName("normalFont");
//		pnlDisclosure.setContent(description);
//		hPanel.add(pnlDisclosure);
//		vPanel.add(hPanel);






//		// additional info
//		HorizontalPanel headerMeta = new HorizontalPanel();
////		LabelWithTooltip lDesc = new LabelWithTooltip(messages.labelDescription(), messages.tooltipDescription());
////		header.add(lDesc);
//		headerMeta.add(new Image(resources.closedArrow()));
//		headerMeta.add(new Label(messages.labelMetadata()));
//
//
//		hPanel = new HorizontalPanel();
//		lblName = new Label(messages.labelUID());
//		lblName.setTitle(messages.tooltipUUID());
//		hPanel.add(lblName);
//		this.dsaUid.setStyleName("normalFont");
//		hPanel.add(this.dsaUid);
//		VerticalPanel innerVPanel = new VerticalPanel();
//		innerVPanel.add(hPanel);
//
//
//		hPanel = new HorizontalPanel();
//
//		lblName = new Label(messages.labelVocabularyURI());
//		lblName.setTitle(messages.tooltipVocabulary());
//
//
//
//		hPanel = new HorizontalPanel();
//		hPanel.add(lblName);
//		this.dsaVocabulary.setStyleName("normalFont");
//		hPanel.add(this.dsaVocabulary);
//		innerVPanel.add(hPanel);
//
//
//		DisclosurePanel pnlDisclosureInfo = new DisclosurePanel();
//		pnlDisclosureInfo.setStyleName("my-gwt-DisclosurePanel");
//		pnlDisclosureInfo.setHeader(headerMeta);
//		pnlDisclosureInfo.setTitle(messages.tooltipMetadata());
//		pnlDisclosureInfo.setContent(innerVPanel);
//
//
//
//		vPanel.add(pnlDisclosureInfo);




		// Additional Info (UID and Vocabulary URI)
		HorizontalPanel hPanelUID = new HorizontalPanel();
		//			lblName = new Label(messages.labelUID());
//		lblName.setTitle(messages.tooltipUUID());
//		lblName.setStyleName("required");
//		hPanel.add(lblName);
		LabelWithTooltip lblUID= new LabelWithTooltip(messages.labelUID(), messages.tooltipUUID());
		hPanelUID.add(lblUID);
		this.dsaUid.setStyleName("normalFont");
		hPanelUID.add(this.dsaUid);

		HorizontalPanel hPanelVoc = new HorizontalPanel();
//		lblName = new Label(messages.labelVocabularyURI());
//		lblName.setTitle(messages.tooltipVocabulary());
//		hPanel.add(lblName);
		LabelWithTooltip lblVocUri= new LabelWithTooltip(messages.labelVocabularyURI(), messages.tooltipVocabulary());
		hPanelVoc.add(lblVocUri);
		this.dsaVocabulary.setStyleName("normalFont");
		hPanelVoc.add(this.dsaVocabulary);
		VerticalPanel innerVPanelMetadata = new VerticalPanel();
		innerVPanelMetadata.add(hPanelUID);
		innerVPanelMetadata.add(hPanelVoc);

		HorizontalPanel headerMeta = new HorizontalPanel();
//		LabelWithTooltip lDesc = new LabelWithTooltip(messages.labelDescription(), messages.tooltipDescription());
//		header.add(lDesc);
		headerMeta.add(new Image(resources.closedArrow()));
		headerMeta.add(new Label(messages.labelMetadata()));

		DisclosurePanel pnlDisclosureMetadata = new DisclosurePanel();
		pnlDisclosureMetadata.setStyleName("my-gwt-DisclosurePanel");
		pnlDisclosureMetadata.setHeader(headerMeta);
		pnlDisclosureMetadata.setTitle(messages.tooltipMetadata());

//		LabelWithTooltip lbMetadata = new LabelWithTooltip(messages.labelMetadata(), messages.tooltipMetadata());
//		pnlDisclosure.setHeader(lbMetadata);
		pnlDisclosureMetadata.setContent(innerVPanelMetadata);

		vPanel.add(pnlDisclosureMetadata);










//		// validity
//		DisclosurePanel pnlDisclosureValidity = new DisclosurePanel();
//		LabelWithTooltip labelValidity= new LabelWithTooltip(messages.labelValidity(), messages.tooltipValidity());
//		pnlDisclosureValidity.setHeader(labelValidity);
//		pnlDisclosureValidity.setTitle(messages.tooltipValidity());
//		pnlDisclosureValidity.setStyleName("my-gwt-DisclosurePanel");
//		VerticalPanel validityPanel = new VerticalPanel();
//		HorizontalPanel validityHPanel = new HorizontalPanel();
//		Label validityStartLabel = new Label();
//		validityStartLabel.setText(messages.labelStartDate());
//		validityHPanel.add(validityStartLabel);
//		Label validityStartValue = new Label(dsaBean.getValidity().getStartDate().toString());
//		validityStartValue.setStyleName("normalFont");
//		validityHPanel.add(validityStartValue);
//		validityPanel.add(validityHPanel);
//		validityHPanel = new HorizontalPanel();
//		Label validityEndLabel = new Label();
//		validityEndLabel.setText(messages.labelEndDate());
//		validityHPanel.add(validityEndLabel);
//		Label validityEndValue = new Label(dsaBean.getValidity().getEndDate().toString());
//		validityEndValue.setStyleName("normalFont");
//		validityHPanel.add(validityEndValue);
//		validityPanel.add(validityHPanel);
//		pnlDisclosureValidity.setContent(validityPanel);
//		vPanel.add(pnlDisclosureValidity);



		// validity
        final HorizontalPanel hpDates = new HorizontalPanel();
        hpDates.setSpacing(5);
        hpDates.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);

        DateBox dateBoxStart = new DateBox();
        DateBox dateBoxEnd = new DateBox();
        dateBoxStart.setEnabled(false);
        dateBoxEnd.setEnabled(false);
        dateBoxStart.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat(ValidityPresenter.DATEFORMAT)));
        dateBoxEnd.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat(ValidityPresenter.DATEFORMAT)));

        hpDates.add(new Label("From"));
        hpDates.add(dateBoxStart);
        hpDates.add(new Label("To"));
        hpDates.add(dateBoxEnd);
		Validity validity = dsaBean.getValidity();
		dateBoxStart.setValue(validity.getStartDate());
		dateBoxEnd.setValue(validity.getEndDate());

		VerticalPanel validityPanel = new VerticalPanel();
		LabelWithTooltip labelValidity= new LabelWithTooltip(messages.labelValidity(), messages.tooltipValidity());
		validityPanel.add(labelValidity);
		validityPanel.add(hpDates);
		vPanel.add(validityPanel);









		// add statements
		vPanel.add(addStatements(viewMode));

		// add general policies
		hPanel = new HorizontalPanel();

//		Label labelParties = new Label(messages.labelGlobalPoliciesParties());
//		labelParties.setTitle(messages.tooltipGlobalPoliciesParties());
//		labelParties.setStyleName("policyTitle");

		LabelWithTooltip labelParties= new LabelWithTooltip(messages.labelGlobalPoliciesParties(), messages.tooltipGlobalPoliciesParties(), "policyTitle");


		hPanel.add(labelParties);
		vPanel.add(hPanel);
		hPanel = new HorizontalPanel();
		VerticalPanel verticalPanel = new VerticalPanel();


		LabelWithTooltip labelExpirationPolicy= new LabelWithTooltip(messages.labelExpirationPolicy(), messages.tooltipExpirationPolicy());
//
//		lblName = new Label(messages.labelExpirationPolicy());
//		lblName.setTitle(messages.tooltipExpirationPolicy());
		verticalPanel.add(labelExpirationPolicy);


		Label expValue = new Label();
		expValue.setText(dsaBean.getExpirationPolicy().getPolicy());
		expValue.setStyleName("normalFont");
		verticalPanel.add(expValue);
		HorizontalPanel innerHPanel = new HorizontalPanel();
		Label period = new Label(messages.messageNotAvailable());
		period.setStyleName("normalFont");
		if (WidgetUtil.getPolicyTypeIndex(dsaBean.getExpirationPolicy().getPolicy()) == 2) {
			period.setText(expirationPolicyAttribute.getValue());
		}
		innerHPanel.add(new Label(messages.labelPeriodInDays()));
		innerHPanel.add(period);
		verticalPanel.add(innerHPanel);
		hPanel.add(verticalPanel);
		verticalPanel = new VerticalPanel();


		LabelWithTooltip labelUpdatePolicy= new LabelWithTooltip(messages.labelUpdatePolicy(), messages.tooltipUpdatePolicy());
//		lblName = new Label(messages.labelUpdatePolicy());
//		lblName.setTitle(messages.tooltipUpdatePolicy());

		verticalPanel.add(labelUpdatePolicy);


		Label updValue = new Label();
		updValue.setText(dsaBean.getUpdatePolicy().getPolicy());
		updValue.setStyleName("normalFont");
		verticalPanel.add(updValue);
		innerHPanel = new HorizontalPanel();
		innerHPanel.add(new Label(messages.labelPeriodInDays()));
		period = new Label(messages.messageNotAvailable());
		period.setStyleName("normalFont");
		if (WidgetUtil.getPolicyTypeIndex(dsaBean.getUpdatePolicy().getPolicy()) == 2) {
			period.setText(updatePolicyAttribute.getValue());
		}
		innerHPanel.add(period);
		verticalPanel.add(innerHPanel);
		hPanel.add(verticalPanel);
		verticalPanel = new VerticalPanel();

		LabelWithTooltip labelRevocationPolicy= new LabelWithTooltip(messages.labelRevocationPolicy(), messages.tooltipRevocationPolicy());
//		lblName = new Label(messages.labelRevocationPolicy());
//		lblName.setTitle(messages.tooltipRevocationPolicy());
		verticalPanel.add(labelRevocationPolicy);

		Label revValue = new Label();
		revValue.setText(dsaBean.getRevocationPolicy().getPolicy());
		revValue.setStyleName("normalFont");
		verticalPanel.add(revValue);
		innerHPanel = new HorizontalPanel();
		innerHPanel.add(new Label(messages.labelPeriodInDays()));
		period = new Label(messages.messageNotAvailable());
		period.setStyleName("normalFont");
		if (WidgetUtil.getPolicyTypeIndex(dsaBean.getRevocationPolicy().getPolicy()) == 2) {
			period.setText(revocationPolicyAttribute.getValue());
		}
		innerHPanel.add(period);
		verticalPanel.add(innerHPanel);
		hPanel.add(verticalPanel);
		vPanel.add(hPanel);
		return vPanel;
	}

	private Widget addStatements(boolean readOnly) {
		VerticalPanel mainPanel = new VerticalPanel();
		final HorizontalPanel hPanel = new HorizontalPanel();
		Label labelParties = new Label(messages.labelParties());
		labelParties.setTitle(messages.tooltipParties());
		labelParties.setStyleName("partiesLabel");
		hPanel.add(labelParties);
		mainPanel.add(hPanel);
		List<Organization> partiesList = dsaBean.getParties();
		CellTable<Organization> partiesTable = new CellTable<Organization>();
		partiesTable.setStyleName("myCellTable");
		// Create name column.
		TextColumn<Organization> nameColumn = new TextColumn<Organization>() {
			@Override
			public String getValue(Organization p) {
				return p.getName();
			}
		};
		// Add the columns.
		partiesTable.addColumn(nameColumn, messages.labelPartyName());
		partiesTable.setRowData(0, (List<? extends Organization>) partiesList);
		mainPanel.add(partiesTable);

		VerticalPanel vp = setUpPartiesPoliciesPresenters();
		//_MR
		vp.add(setUpThirdPartiesPoliciesPresenters());
		Widget wid = setUpAllPolicies();

		if (WidgetUtil.containsPendingRules(dsaBean) && !readOnly) {
			mainPanel.add(vp);
		} else {
			mainPanel.add(wid);
		}
		return mainPanel;
	}

	class BackHomeHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent arg0) {
			//RootPanel.get("gwtContainer").clear();
			backHome(messages.messageOperationCompleted());
		}
	}

	private void initList() {
		String purposeValues = messages.purposeValuesAsString();
		purposeList.addAll(Arrays.asList(purposeValues.split(",")));

		String applicationdomainValues = messages.applicationDomainValuesAsString();
		applicationDomainList.addAll(Arrays.asList(applicationdomainValues.split(",")));

		String dataClassificationValues = messages.dataClassificationLValuesAsString();
		dataClassificationList.addAll(Arrays.asList(dataClassificationValues.split(",")));

		policyList.add(messages.labelPolicy1());
		policyList.add(messages.labelPolicy2());
		policyList.add(messages.labelPolicy3());
		updatePeriodEnabled = false;
		revocationPeriodEnabled = false;
		expirationPeriodEnabled = false;
	}

}
