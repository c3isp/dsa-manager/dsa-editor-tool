/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Util;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.util.HumanFormHelper;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.util.InputValueHandler;

//import java.io.File;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//import javax.xml.parsers.ParserConfigurationException;
//
//import org.w3c.dom.Attr;
//import org.w3c.dom.Document;
//import org.w3c.dom.Node;
//import org.w3c.dom.NodeList;
//import org.xml.sax.SAXException;
//
//import com.hp.cococloud.dsa.infrastructure.authoring.server.handler.dsa.helper.DsaStorage;
//import com.hp.cococloud.dsa.infrastructure.authoring.shared.dsa.RepositoryContent;
//
public class Test {
    //
    // public static void main(String[] args) {
    // //File directory = new
    // File("C:\\apache-tomcat-7.0.55\\webapps\\DSAAuthoringTool\\WEB-INF\\dsaRepository");
    // File directory = new File(DsaStorage.getInstance().getBasePath());
    //
    // List<File> fileList = new ArrayList<File>();
    // ArrayList<RepositoryContent> resultList = new
    // ArrayList<RepositoryContent>();
    //
    // // get all the files from a directory
    // File[] fList = directory.listFiles();
    // fileList.addAll(Arrays.asList(fList));
    // int i = 0;
    // for (File file : fList) {
    // i ++;
    // if (file.isFile() && (file.getName().startsWith("DSA"))) {
    // System.out.println(file.getAbsolutePath());
    // String title = getTitleFromXMLFile(file);
    // System.out.println("Title="+title);
    // }
    // }
    // }
    //
    //
    // private static String getTitleFromXMLFile(File file) {
    // DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    // DocumentBuilder builder = null;
    // String title = null;
    // try {
    // builder = factory.newDocumentBuilder();
    // Document doc = builder.parse(file);
    //
    //
    //
    // NodeList nodeList = doc.getChildNodes();
    //
    // for (int i = 0; i < nodeList.getLength(); i++) {
    // Node node = nodeList.item(i);
    //
    // if (node.hasAttributes()) {
    // Attr attr = (Attr) node.getAttributes().getNamedItem("title");
    // if (attr != null) {
    // title= attr.getValue();
    //
    //
    // }
    // }
    // }
    //
    //
    // } catch (ParserConfigurationException e1) {
    // // TODO Auto-generated catch block
    // e1.printStackTrace();
    // } catch (SAXException | IOException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    // return title;
    // }

	public static void main(String[] args) {

		String inputValue = "?X_29:Identifier=iddd|?X_30:Identifier=iddd2|?X_31:Identifier=iddd3";
		Map<String, String> map = InputValueHandler.stringToMap(inputValue);
		System.out.println(Util.MapToString(map, ""));




////		String statementInfo = "AnonymizeByDelimiter{param=DestinationAddress option=SUBSTRING_LOW}";
////		System.out.println(statementInfo.substring(statementInfo.indexOf("param=")+ "param=".length(), statementInfo.indexOf(" ")));;////		System.out.println(statementInfo.substring(statementInfo.indexOf("option=")+ "option=".length(), statementInfo.indexOf("}")));;
//
//		String nameValueSep = "=";
//		String input = "name1=value1";
//		String[] parsed = HumanFormHelper.nameValueParse(input, nameValueSep);
//		System.out.println("input:" + input + " --- parsed: " + ((parsed == null)? "null":" param name is (" + parsed[0] + ") value is (" + parsed[1] + ")"));
//
//		input = "";
//		parsed = HumanFormHelper.nameValueParse(input, nameValueSep);
//		System.out.println("input:" + input + " --- parsed: " + ((parsed == null)? "null":" param name is (" + parsed[0] + ") value is (" + parsed[1] + ")"));
//
//		input = "ccaac=";
//		parsed = HumanFormHelper.nameValueParse(input, nameValueSep);
//		System.out.println("input:" + input + " --- parsed: " + ((parsed == null)? "null":" param name is (" + parsed[0] + ") value is (" + parsed[1] + ")"));
//
//		input = "prova prova spazi=valore con spazi";
//		parsed = HumanFormHelper.nameValueParse(input, nameValueSep);
//		System.out.println("input:" + input + " --- parsed: " + ((parsed == null)? "null":" param name is (" + parsed[0] + ") value is (" + parsed[1] + ")"));
//
//
//		Set<String> params = new HashSet<String>(
//				Arrays.asList(new String[] {
//						"AnonymiseByGeneralization",
//						"AnonymiseLocationsWithinRadius",
//						"Name",
//						"Group",
//						"PseudonymiseCTIfieldMalware",
//						"AnonymiseBySuppression",
//						"Identifier",
//						"AnonymizeDelimitedStringsBySuppression",
//						"AnonymiseCountsForMeanByPerturbation"
//				})
//		);
//
//
//
//
//		String inputValue = "?X_29:Identifier=iddd|?X_30:Identifier=iddd2|?X_31:Identifier=iddd3";
//		Map<String, List<String>> mapValues = HumanFormHelper.parseInputValue(params, inputValue);
//		debugInfo(inputValue, mapValues);
//
//		inputValue = "?X_29:Identifier=iddd|?X_30:AAA=iiiii|?X_30:Identifier=iddd2|?X_31:Identifier=iddd3";
//		mapValues = HumanFormHelper.parseInputValue(params, inputValue);
//		debugInfo(inputValue, mapValues);
//
//		inputValue = "";
//		mapValues = HumanFormHelper.parseInputValue(params, inputValue);
//		debugInfo(inputValue, mapValues);
//
//		inputValue = "?X_29:Identifier=iddd";
//		mapValues = HumanFormHelper.parseInputValue(params, inputValue);
//		debugInfo(inputValue, mapValues);
//
//		inputValue = "?X_29:Identifier=iddd|?X_30:Name=nnnnn|?X_30:Identifier=iddd2|?X_31:Identifier=iddd3";
//		mapValues = HumanFormHelper.parseInputValue(params, inputValue);
//		debugInfo(inputValue, mapValues);
//
//		inputValue = "IF a Subject hasId a Identifier OR that Subject hasId a Identifier THEN that Subject CAN Create a Data";
//		System.out.println("\r\n" + inputValue + "\r\nprepareHumanForm=" + HumanFormHelper.prepareHumanForm(params, inputValue));
//
//		inputValue = "IF a Subject hasId a Identifier OR that Subject isMemberOf a Group THEN that Subject CAN Create a Data";
//		System.out.println("\r\n" + inputValue + "\r\nprepareHumanForm=" + HumanFormHelper.prepareHumanForm(params, inputValue));
//
//		inputValue = "IF a Subject hasId a Identifier OR that Subject hasId a Identifier OR that Subject isMemberOf a Group OR that Subject isMemberOf a Group THEN that Subject CAN Create a Data";
//		System.out.println("\r\n" + inputValue + "\r\nprepareHumanForm=" + HumanFormHelper.prepareHumanForm(params, inputValue));


	}

	private static void debugInfo(String inputValue, Map<String, List<String>> mapValues) {
		System.out.println("\r\n\r\nINPUT:" + inputValue);
		for(String s:mapValues.keySet()) {
			System.out.print("\r\nPARAM:" + s + " -> ");

			List<String> l = mapValues.get(s);
			for(String v:l) {
				System.out.print(v + ",");
			}
		}
	}



}
