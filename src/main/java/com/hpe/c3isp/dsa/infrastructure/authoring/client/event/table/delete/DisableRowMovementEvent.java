/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.delete;

import com.google.gwt.event.shared.GwtEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.move.HasDisableRowMovementHandlers;

public class DisableRowMovementEvent extends
        GwtEvent<DisableRowMovementHandler> {

    public static Type<DisableRowMovementHandler> TYPE = new Type<DisableRowMovementHandler>();

    public static void fire(HasDisableRowMovementHandlers source) {
        DisableRowMovementEvent event = new DisableRowMovementEvent();
        source.fireEvent(event);
    }

    @Override
    protected void dispatch(DisableRowMovementHandler handler) {
        handler.onDisableRowMovementEvent();
    }

    @Override
    public Type<DisableRowMovementHandler> getAssociatedType() {
        return TYPE;
    }

}
