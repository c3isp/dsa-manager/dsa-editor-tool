/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.datasubjectauthorizations;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.AbstractSspState;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.DataSubjectAuthorizationsSetDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.StatementMetadata;

public interface DataSubjectAspState
        extends
        AbstractSspState<DataSubjectAspContext, DataSubjectAuthorizationsSetDisplay> {

    void handleSelectedRowEvent(DataSubjectAspContext context, int row);

    void handleLinkFromSourceStatementEvent(DataSubjectAspContext context,
            StatementMetadata source);

}
