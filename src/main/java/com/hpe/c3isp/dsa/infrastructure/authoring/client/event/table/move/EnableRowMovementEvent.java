/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.move;

import com.google.gwt.event.shared.GwtEvent;

public class EnableRowMovementEvent extends GwtEvent<EnableRowMovementHandler> {

    public static Type<EnableRowMovementHandler> TYPE = new Type<EnableRowMovementHandler>();

    private final int rowIndex;

    public EnableRowMovementEvent(int rowIndex) {
        super();
        this.rowIndex = rowIndex;
    }

    public static void fire(HasMoveRowHandlers source, int rowIndex) {
        EnableRowMovementEvent event = new EnableRowMovementEvent(rowIndex);
        source.fireEvent(event);
    }

    @Override
    protected void dispatch(EnableRowMovementHandler handler) {
        handler.onEnableRowMovementEvent(rowIndex);
    }

    @Override
    public Type<EnableRowMovementHandler> getAssociatedType() {
        return TYPE;
    }

}
