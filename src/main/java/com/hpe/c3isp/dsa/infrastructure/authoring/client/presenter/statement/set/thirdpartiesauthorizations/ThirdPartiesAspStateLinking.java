/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.thirdpartiesauthorizations;

import java.util.HashMap;

import com.google.gwt.user.client.Timer;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.Mode;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.move.MoveRowEvent.Direction;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.StatementMetadata;

public class ThirdPartiesAspStateLinking implements ThirdPartiesAspState {

    private final StatementMetadata source;

    public ThirdPartiesAspStateLinking(StatementMetadata source) {
        this.source = source;
    }

    @Override
    public void handleLinkFromSourceStatementEvent(
            ThirdPartiesAspContext context, StatementMetadata source) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleSelectedRowEvent(final ThirdPartiesAspContext context,
            final int row) {
        // if the source is already linked to a statement, then we have
        // highlighted it, and now we must turn highlight off
        if (!"".equals(source.getReferenceUid())) {
            int targetIndex = context.getIndexOfStatement(source
                    .getReferenceUid());
            assert targetIndex != -1;
            context.getDisplay().highlightStatement(targetIndex, Mode.OFF);
        }
        StatementMetadata target = null;
        // check if the user has selected a row, or if she/he has clicked
        // the no-reference button
        if (row >= 0) {
            // the user has selected a row: we flash the selected row
            context.getDisplay().highlightStatement(row, Mode.ON);
            Timer timer = new Timer() {

                @Override
                public void run() {
                    context.getDisplay().highlightStatement(row, Mode.OFF);
                }
            };
            timer.schedule(500);
            // get target metadata
            target = context.getStatementPresenterAt(row).getStatement()
                    .getStatementMetadata();
        }
        // hide link-tools
        context.getDisplay().hideStatementLinkTools();
        // set linking
        context.setStatementLink(source, target);
        // change state
        context.setState(new ThirdPartiesAspStateNormal());
    }

    @Override
    public void addStatement(ThirdPartiesAspContext context) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleDeleteRowEvent(ThirdPartiesAspContext context,
            int rowIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleDisableRowMovementEvent(ThirdPartiesAspContext context) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleEnableRowMovementEvent(ThirdPartiesAspContext context,
            int rowIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleMoveRowEvent(ThirdPartiesAspContext context,
            Direction direction){//, CheckBox protectedCheckBox, CheckBox isPendingRuleCheckBox, Label statementInfo, TextArea inputValue) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleStatementBrokenEvent(ThirdPartiesAspContext context,
            int statementIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleStatementCompletedEvent(ThirdPartiesAspContext context,
            int statementIndex, boolean isEnabled, boolean isEndUser, HashMap<String,String> params){//CheckBox protectedCheckBox,CheckBox isPendingRuleCheckBox, Label statementInfo, TextArea inputValue) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleCompleteMetadataEvent(ThirdPartiesAspContext context,
            int rowIndex, boolean isEnabled, boolean isEndUser, HashMap<String,String> params) {
        throw new UnsupportedOperationException();
    }



}
