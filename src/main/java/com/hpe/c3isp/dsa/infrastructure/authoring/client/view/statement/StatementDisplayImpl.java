/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.StatementPresenter.StatementDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.syntactic.SyntacticItemDisplay;

public class StatementDisplayImpl extends Composite implements StatementDisplay {

    protected final FlowPanel flowPanel = new FlowPanel();

    @Inject
    public StatementDisplayImpl() {
        super();
        initWidget(flowPanel);
    }

    @Override
    public void addSyntacticItemDisplay(SyntacticItemDisplay display) {
        flowPanel.add(display.asWidget());
    }

    @Override
    public void clear() {
        flowPanel.clear();
    }

    @Override
    public Widget asWidget() {
        return this;
    }

}
