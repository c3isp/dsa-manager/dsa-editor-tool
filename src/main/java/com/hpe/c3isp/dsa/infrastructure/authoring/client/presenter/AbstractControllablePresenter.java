/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter;

import org.enunes.gwt.mvp.client.EventBus;
import org.enunes.gwt.mvp.client.presenter.BasePresenter;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.presenter.BusyDisplayEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.presenter.BusyDisplayHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.presenter.IdleDisplayEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.presenter.IdleDisplayHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.ControllableDisplay;

public abstract class AbstractControllablePresenter<D extends ControllableDisplay>
        extends BasePresenter<D> implements ControllablePresenter<D> {

    public AbstractControllablePresenter(EventBus eventBus, D display) {
        super(eventBus, display);
    }

    @Override
    public HandlerRegistration addBusyDisplayHandler(BusyDisplayHandler handler) {
        return eventBus.addHandler(BusyDisplayEvent.TYPE, handler);
    }

    @Override
    public HandlerRegistration addIdleDisplayHandler(IdleDisplayHandler handler) {
        return eventBus.addHandler(IdleDisplayEvent.TYPE, handler);
    }

    @Override
    public void fireEvent(GwtEvent<?> event) {
        eventBus.fireEvent(event);
    }

}
