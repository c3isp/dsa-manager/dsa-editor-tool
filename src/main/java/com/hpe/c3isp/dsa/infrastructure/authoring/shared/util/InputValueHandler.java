package com.hpe.c3isp.dsa.infrastructure.authoring.shared.util;

import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gwt.core.shared.GWT;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableDeclaration;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Util;

//helper class to handle multiple values stored in inputValue metadata property
public class InputValueHandler{
	public static final String INPUT_VAL_SEPARATOR = "|";

	//existingInputValueString forma is param1=val2|param2=val2...
	public static String updateValue(String existingInputValueString, String paramName, String paramValue) {
		if(Util.isEmpty(paramName)) {
			return existingInputValueString;
		}
		LinkedHashMap<String, String> map =  InputValueHandler.stringToMap(existingInputValueString);
		String val = Util.isEmpty(paramValue)?"":paramValue;
		map.put(paramName, val);
		return mapToString(map);
	}

	//existingInputValueString forma is param1=val2|param2=val2...
	public static String gatValue(String existingInputValueString, String paramName) {
		if(Util.isEmpty(paramName)) {
			return "";
		}
		LinkedHashMap<String, String> map =  InputValueHandler.stringToMap(existingInputValueString);
		return map.get(paramName);
	}

	//inpust string = param1=val2|param2=val2...
	public static LinkedHashMap<String, String> stringToMap(String existingInputValueString){
		GWT.log("Called stringToMap with input:" + existingInputValueString);
		final LinkedHashMap<String, String> ret = new LinkedHashMap<String, String>();
		if(!Util.isEmpty(existingInputValueString)) {
			final String reg = "\\" + INPUT_VAL_SEPARATOR;
			String[] tokens = existingInputValueString.split(reg);
			if(tokens.length > 0) {
				for(String t:tokens) {
					if(Util.isEmpty(t)) {
						continue;
					}
					int eq = t.indexOf("=");
					String pname, pvalue = "";
					if(eq <= 0) {
						//malformed name=value string, take all string as paramName
						pname = t;
					}else {
						pname = t.substring(0, eq);
						pvalue = t.substring(eq+1);
					}
					ret.put(pname,  pvalue);
				}
			}
		}
		return ret;
	}

	//return param1=val2|param2=val2...
	public static String mapToString(LinkedHashMap<String, String> map){
		if(map == null) {
			return "";
		}
		StringBuffer ret = new StringBuffer();
		for(String k:map.keySet()) {
			String v = map.get(k);
			if(Util.isEmpty(v)) {
				v = "";
			}
			String s = k+"="+v;
			if(ret.length() > 0) {
				ret.append(INPUT_VAL_SEPARATOR);
			}
			ret.append(s);
		}
		return ret.toString();
	}










	public static String findRightInputValue(VariableDeclaration vd, Map<String, String> mapInputValues) {
		if(vd == null || mapInputValues == null) {
			System.out.println(" findRightInputValue: NULL VALUESSSSS");
			return null;
		}
		//VariableDeclaration [syntacticForm=?X_3:Identifier, variable=?X_3, humanForm=a Identifier, uid=http://localhost:7080/vocabularies/c3isp_vocabulary_3.3.owl#Identifier, value=id1|?X_4:Identifier=id2]
		String syntacticForm = vd.getSyntacticForm();
		//Map is
			//?X_29:Identifier---->iddd
			//?X_30:Identifier---->iddd2
			//?X_31:Identifier---->iddd3
		String val = mapInputValues.get(syntacticForm);
		if(val == null) {
			val = "";
		}
		System.out.println(" findRightInputValue: (syntacticForm, value) in map: (" + syntacticForm + ", " + val + ")");
		return val;
	}








}
