/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.crossref;

import org.enunes.gwt.mvp.client.EventBus;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.inject.Inject;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.syntactic.SyntacticItemClickedEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.syntactic.SyntacticItemClickedHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.AbstractControllablePresenter;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;

public class CrossReferencePresenterImpl
        extends
        AbstractControllablePresenter<CrossReferencePresenter.CrossReferenceDisplay>
        implements CrossReferencePresenter {

    private CrpState state;

    @Inject
    public CrossReferencePresenterImpl(EventBus eventBus,
            CrossReferenceDisplay display) {
        super(eventBus, display);
        state = new CrpStateCrossRefsOff();
    }

    @Override
    public void bind() {
        super.bind();
        registerHandler(eventBus.addHandler(SyntacticItemClickedEvent.TYPE,
                new SyntacticItemClickedHandler() {

                    @Override
                    public void onSyntacticItemClicked(
                            SyntacticItem syntacticItem) {
                        handleClickOnSyntacticItem(syntacticItem);
                    }
                }));
        registerHandler(display.getToggleReferencesButton().addClickHandler(
                new ClickHandler() {

                    @Override
                    public void onClick(ClickEvent event) {
                        doToggleReferences();
                    }
                }));
    }

    protected void handleClickOnSyntacticItem(SyntacticItem syntacticItem) {
        state.handleClickOnSyntacticItem(this, syntacticItem);
    }

    protected void doToggleReferences() {
        state.doToggleReferences(this);
    }

    /*
     * Sets the state. Note: we could move this method up to the
     * CrossReferencePresenter interface, however by doing so we would break the
     * State design pattern, because we would make this method public.
     */
    protected void setState(CrpState state) {
        this.state = state;
    }

}
