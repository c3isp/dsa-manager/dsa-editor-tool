/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class VersionManager {

    private static Log log = LogFactory.getLog(VersionManager.class);

    private final String propertiesFile = "version.properties";
    private static String version;

    private VersionManager() {
        Properties properties = new Properties();
        InputStream is = getClass().getResourceAsStream(propertiesFile);
        // getClass().getClassLoader().getResourceAsStream(propertiesFile);
        if (is != null) {
            try {
                properties.load(is);
                if (properties.containsKey("version")) {
                    version = properties.getProperty("version");
                } else {
                    version = "N/A";
                }
                log.info("Pom Version:" + version);
            } catch (IOException e) {
                log.error("cannot read version.properties", e);
            }
        }
    }

    public static String getVersion() {
        return version;
    }

}
