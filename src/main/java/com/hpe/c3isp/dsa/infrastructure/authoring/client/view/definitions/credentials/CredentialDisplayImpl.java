/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.view.definitions.credentials;

import java.util.Iterator;
import java.util.List;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.inject.Inject;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.definition.credentials.CredentialPresenter.CredentialDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.parties.AbstractOrganizationDisplayImpl;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Organization;

public class CredentialDisplayImpl extends AbstractOrganizationDisplayImpl
        implements CredentialDisplay {

    private final InlineLabel termLabel = new InlineLabel("?");
    private final InlineLabel sentenceLabel = new InlineLabel(" ");
    private final InlineLabel chosenOrganizationsLabel = new InlineLabel();

    @Inject
    public CredentialDisplayImpl() {
        super();
        final HorizontalPanel hp = new HorizontalPanel();
        initWidget(hp);

        final FlowPanel fp = new FlowPanel();
        fp.add(chosenOrganizationsLabel);
        fp.add(sentenceLabel);
        fp.add(termLabel);

        hp.add(fp);
        hp.add(dpChangeOrganizations);

        termLabel.setStylePrimaryName("syntacticItemLabel");
        sentenceLabel.setStylePrimaryName("syntacticItemLabel");
        chosenOrganizationsLabel.setStylePrimaryName("syntacticItemLabel");
    }

    @Override
    public void clearChosenOrganizationNames() {
        chosenOrganizationsLabel.setText("");
    }

    @Override
    public void setChosenOrganizationNames(List<String> organizationsNames) {
        if (organizationsNames.size() == 0)
            sentenceLabel.setText("There are no trusted providers for ");
        else if (organizationsNames.size() == 1)
            sentenceLabel.setText(" is a trusted provider for ");
        else
            sentenceLabel.setText(" are trusted providers for ");
        final StringBuffer buffer = new StringBuffer();
        final Iterator<String> orgsIterator = organizationsNames.iterator();
        if (orgsIterator.hasNext()) {
            buffer.append(orgsIterator.next());
            while (orgsIterator.hasNext()) {
                String orgName = orgsIterator.next();
                if (orgsIterator.hasNext())
                    buffer.append(", ");
                else
                    buffer.append(" and ");
                buffer.append(orgName);
            }
        }
        chosenOrganizationsLabel.setText(buffer.toString());
    }

    @Override
    public void setTerm(String humanForm) {
        termLabel.setText(humanForm);
    }

    @Override
    public List<Organization> getChosenOrganizations() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setChosenOrganization(List<Organization> organizations,
            String userRole, List<Organization> allOrgs, String name, String role, String responsibilities) {
        // TODO Auto-generated method stub

    }

}
