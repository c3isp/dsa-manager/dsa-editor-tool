/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.fsm;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.model.VocabularyChoicesUpdater;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableDeclaration;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableReference;

public abstract class AbstractCnlPrompterStateWithTheSelectedReference
        implements CnlPrompterState {

    @Override
    public void handleSelection(CnlPrompterContext context,
            SyntacticItem selectedItem) {
        throw new UnsupportedOperationException();
    }

    @Override
    public VocabularyChoicesUpdater getVocabularyChoicesUpdater() {
        throw new UnsupportedOperationException();
    }

    protected VariableReference buildVariableReference(
            SyntacticItem syntacticItem) {
        // ensure that we have a VariableDeclaration
        assert syntacticItem instanceof VariableDeclaration : "a reference should refer to a VariableDeclaration and not "
                + "to a " + syntacticItem;
        VariableDeclaration vd = (VariableDeclaration) syntacticItem;
        // build a VariableReference
        return new VariableReference(vd.getUid(), vd.getHumanForm(),
                vd.getVariable(), vd.getValue());
    }

}
