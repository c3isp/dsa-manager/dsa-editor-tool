/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa;

import java.util.HashMap;

import com.google.gwt.user.client.rpc.IsSerializable;

public class RepositoryContent implements IsSerializable {

    private HashMap<String, String> item;

    public RepositoryContent() {
        item = new HashMap<String, String>();
        item.put("uuid", "");
        item.put("size", "");
        item.put("title", "");
        item.put("status", "");

    }

    public HashMap<String, String> getItem() {
        return item;
    }

    public void setItem(HashMap<String, String> item) {
        this.item = item;
    }

    public void setUuid(String uuid) {
        item.put("uuid", uuid);
    }

    public void setSize(String size) {
        item.put("size", size);
    }

    public void setTitle(String title) {
        item.put("title", title);
    }

    public void setStatus(String status) {
        item.put("status", status);
    }

}
