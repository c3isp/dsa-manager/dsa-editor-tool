/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.helper;

import java.util.ArrayList;
import java.util.List;

import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Term;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.VariableDeclaration;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.DsaBean;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.DsaBeanUtils;

public class ReferenceableTermsKeeper {

    private static ReferenceableTermsKeeper THE_INSTANCE = null;

    private String vocabularyUri = "";

    // We have a List, and not a Set, here, because we may have several
    // instances of the same Term, each of which is referenceable.
    private final List<String> referenceableTermUris = new ArrayList<String>();

    private ReferenceableTermsKeeper() {
    }

    public static ReferenceableTermsKeeper getInstance() {
        if (THE_INSTANCE == null)
            THE_INSTANCE = new ReferenceableTermsKeeper();
        return THE_INSTANCE;
    }

    public boolean canReferTo(String uri) {
        return referenceableTermUris.contains(uri);
    }

    public void initWith(String vocabularyUri) {
        this.vocabularyUri = vocabularyUri;
        referenceableTermUris.clear();
    }

    public void initWith(DsaBean dsaBean) {
        vocabularyUri = dsaBean.getVocabularyUri();
        referenceableTermUris.clear();
        for (VariableDeclaration vd : DsaBeanUtils
                .getExplicitVariableDeclarations(dsaBean)) {
            referenceableTermUris.add(vd.getUid());
        }
    }

    public void add(SyntacticItem synit) {
        if (synit instanceof Term && synit.getUid().startsWith(vocabularyUri))
            referenceableTermUris.add(synit.getUid());
    }

    public String getVocabularyUri() {
        return vocabularyUri;
    }

    public void remove(SyntacticItem synit) {
        referenceableTermUris.remove(synit.getUid());
    }

}
