/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabularylist;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa.CreateNewDsaActionHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.Config;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.vocabularylist.GetVocabulariesAction;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.vocabularylist.GetVocabulariesResult;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.vocabularylist.GetVocabulariesResultImpl;

import net.customware.gwt.dispatch.server.ActionHandler;
import net.customware.gwt.dispatch.server.ExecutionContext;
import net.customware.gwt.dispatch.shared.DispatchException;

public class GetVocabulariesActionHandler implements
        ActionHandler<GetVocabulariesAction, GetVocabulariesResult> {

    private static Log log = LogFactory.getLog(GetVocabulariesActionHandler.class);

    @Override
    public GetVocabulariesResult execute(GetVocabulariesAction action, ExecutionContext context) throws DispatchException {
//    	log.debug("    :::::::::::: GetVocabulariesActionHandler    execute");
    	String voc = Config.getInstance().getAvailableVocabularyList();
    	GetVocabulariesResultImpl ret = new GetVocabulariesResultImpl(voc);
        return ret;
    }

    @Override
    public Class<GetVocabulariesAction> getActionType() {
        return GetVocabulariesAction.class;
    }

    @Override
    public void rollback(GetVocabulariesAction action, GetVocabulariesResult result, ExecutionContext context) throws DispatchException {
        throw new UnsupportedOperationException();
    }

}
