/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.syntactic;

import org.enunes.gwt.mvp.client.EventBus;
import org.enunes.gwt.mvp.client.presenter.BasePresenter;

import com.google.inject.Inject;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.syntactic.SyntacticItemDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Token;

public class TokenPresenterImpl extends BasePresenter<SyntacticItemDisplay>
        implements TokenPresenter {

    private Token token = null;

    @Inject
    public TokenPresenterImpl(EventBus eventBus, SyntacticItemDisplay display) {
        super(eventBus, display);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void show(Token syntacticItem) {
        token = syntacticItem;
        String text = token.getHumanForm();
        if (token.getValue()!=null && token.getValue()!="")
            text.concat("("+token.getValue()+")");
        display.setText(text);
    }

}
