/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set;

import java.util.HashMap;

import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.Mode;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.move.MoveRowEvent.Direction;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set.StatementSetDisplay;

public abstract class AbstractSspStateMoving<C extends AbstractSspContext<D>, D extends StatementSetDisplay, S extends AbstractSspState<C, D>>
        implements AbstractSspState<C, D> {

    private int rowBeingMoved = -1;

    public AbstractSspStateMoving(int rowBeingMoved) {
        this.rowBeingMoved = rowBeingMoved;
    }

    @Override
    public void addStatement(C context) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleDeleteRowEvent(C context, int rowIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleDisableRowMovementEvent(C context) {
        context.getDisplay().highlightStatement(rowBeingMoved, Mode.OFF);
        context.getDisplay().hideStatementMoveTools();
        makeTransitionToStateNormal(context);
    }

    protected abstract void makeTransitionToStateNormal(C context);

    @Override
    public void handleEnableRowMovementEvent(C context, int rowIndex) {
        throw new UnsupportedOperationException();
    }


    @Override
    public void handleMoveRowEvent(C context, Direction direction){
        assert rowBeingMoved != -1 : "bad row index while moving";
        int newIndex;
        if (direction.equals(Direction.UP)) {
            newIndex = rowBeingMoved - 1;
            if (newIndex < 0)
                newIndex = context.getStatementPresentersCount() - 1;
        } else {
            newIndex = rowBeingMoved + 1;
            if (newIndex == context.getStatementPresentersCount())
                newIndex = 0;
        }
        context.moveStatementPresenter(rowBeingMoved, newIndex);
        StatementSetDisplay display = context.getDisplay();
        display.deleteRow(rowBeingMoved);
        display.insertRow(context.getStatementPresenterAt(newIndex)
                .getDisplay(), newIndex, context.getStatementPresenterAt(newIndex).getStatement());
        display.highlightStatement(newIndex, Mode.ON);
        rowBeingMoved = newIndex;
    }

    @Override
    public void handleStatementBrokenEvent(C context, int statementIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleStatementCompletedEvent(C context, int statementIndex,
            boolean isEnabled, boolean isEndUser, HashMap<String,String> params){
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleCompleteMetadataEvent(C context, int statementIndex,
            boolean isEnabled, boolean isEndUser, HashMap<String,String> params){
        throw new UnsupportedOperationException();
    }


}
