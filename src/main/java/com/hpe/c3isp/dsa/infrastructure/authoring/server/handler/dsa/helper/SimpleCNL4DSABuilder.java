/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa.helper;

import java.util.Arrays;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.helper.VocabularyManager;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.helper.VocabularyOntology;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Token;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Tokens;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.DsaBean;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.StatementType;

public class SimpleCNL4DSABuilder {

    protected static Log log = LogFactory.getLog(SimpleCNL4DSABuilder.class);

    protected final VocabularyOntology vocont;

    public SimpleCNL4DSABuilder(DsaBean dsaBean) {
        vocont = VocabularyManager.getInstance().getVocabularyOntology(
                dsaBean.getVocabularyUri());
    }

    public String buildStatement(StatementType statementType, String cnl4dsaeStr) {
        final StringBuffer buffer = new StringBuffer();
        String[] ps = transform(statementType, cnl4dsaeStr);
        int i = 0;
        while (i < ps.length) {
            Token t = Tokens.getTokenHavingSyntacticForm(ps[i], statementType);
            if (t != null) {
                // ps[i] is a Token
                if (t.equals(Tokens.TOKEN_CAN) || t.equals(Tokens.TOKEN_MUST)
                        || t.equals(Tokens.TOKEN_CANNOT)
                        || t.equals(Tokens.TOKEN_AFTER)) {
                    // here we consume a modal operator (CAN, MUST, CANNOT) or
                    // an AFTER operator and its corresponding CNL4DSA atomic
                    // fragment
                    assert i + 3 < ps.length : "Cannot find "
                            + "subject-action-object after " + ps[i] + " in "
                            + Arrays.toString(ps);
                    buffer.append(consumeAtomicFragment(ps[i], ps[i + 1],
                            ps[i + 2], ps[i + 3]));
                    // here we simply move the index because we have consumed
                    // the previous token and the atomic fragment
                    i += 4;
                } else {
                    // here we consume all other CNL4DSA tokens
                    buffer.append(ps[i]).append(" ");
                    // here we simply move the index because we have consumed
                    // the previous token
                    i++;
                }
            } else {
                // since we have previously transformed our cnl4dsaeStr String,
                // and since we consume CNL4DSA Tokens, variables, and
                // vocabulary Actions, then we can assume that if ps[i] is not
                // a Token, then it must be a vocabulary Predicate
                String uri = vocont.getFullUriOfFragment(ps[i]);
                assert uri != null && !"".equals(uri) : "unknown vocabulary item "
                        + ps[i] + " in " + Arrays.toString(ps);
                assert vocont.isPredicate(uri);
                // here we consume the predicate and all its parameters
                // we known that all Predicates are binary (because they are
                // OWL ObjectProperties)
                assert i + 2 < ps.length : "unexpeceted end of statement "
                        + "after " + ps[i] + " in " + Arrays.toString(ps);
                buffer.append(consumePredicate(ps[i], ps[i + 1], ps[i + 2]));
                // here we simply move the index because we have consumed
                // the predicate and its 2 arguments
                i += 3;
                // check that ps[i] is not another variable
                if (i < ps.length)
                    assert !ps[i].startsWith("?") : "too many variables for "
                            + "predicate <" + uri + "> : " + " in "
                            + Arrays.toString(ps);
            }
        }
        return buffer.toString();
    }

    protected String consumeAtomicFragment(String token, String subject,
            String action, String object) {
        return token + " [" + subject + ", " + action + ", " + object + "] ";
    }

    protected String consumePredicate(String predicate, String arg1, String arg2) {
        assert arg1.startsWith("?") : "was expecting a variable, but "
                + "found " + arg1 + " as argument of predicate " + predicate;
        assert arg2.startsWith("?") : "was expecting a variable, but "
                + "found " + arg2 + " as argument of predicate " + predicate;
        return predicate + "(" + arg1 + "," + arg2 + ") ";
    }

    /*
     * Transform a CNL4DSA-E statement: - swap positions for Tokens CAN, MUST,
     * CANNOT, NOT - removes URI fragments after VariableDeclarations - swap
     * positions for Predicates
     */
    private String[] transform(StatementType statementType, String cnl4dsaeStr) {
        log.info("SimpleCNL4DSABuilder: transform - statement=" + cnl4dsaeStr);
        final String[] ps = cnl4dsaeStr.split(" ");
        String[] rs = new String[ps.length];
        for (int i = 0; i < ps.length; i++) {
            com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Token t = Tokens
                    .getTokenHavingSyntacticForm(ps[i], statementType);
            log.debug("SimpleCNL4DSABuilder: transform - token=" + cnl4dsaeStr);
            if (t != null)
                // ps[i] is a Token

                if (t.equals(Tokens.TOKEN_CAN) || t.equals(Tokens.TOKEN_MUST)
                        || t.equals(Tokens.TOKEN_CANNOT)
                        || t.equals(Tokens.TOKEN_NOT)) {
                    // swap with ps[i-1]
                    int j = i - 1;
                    assert j >= 0 : "unexpected Token " + t + " at position "
                            + i + " in " + Arrays.toString(ps);

                    String tmp = rs[j];
                    rs[j] = ps[i];
                    rs[i] = tmp;
                } else {
                    // just copy
                    rs[i] = ps[i];
                }
            else if (ps[i].startsWith("?")) {
                // ps[i] is a variable
                int c = ps[i].indexOf(":");
                if (c > 0) {
                    // ps[i] is a VariableDeclaration: we drop its URI fragment
                    rs[i] = ps[i].substring(0, c);
                } else {
                    // ps[i] is a VariableReference
                    rs[i] = ps[i];
                }
            } else {
                // ps[i] it's neither a CNL4DSA Token nor a variable,
                // so it's necessarily a vocabulary item (either an Action, or
                // a Predicate)
                log.debug("vocont.getUri()=" + vocont.getUri());

                log.debug("Vocabulary ="
                        + (new StringBuilder(String.valueOf(vocont.getUri())))
                                .append(ps[i]).toString());
                String uri = vocont.getFullUriOfFragment(ps[i]);
                log.debug("Fragment =" + ps[i]);

                assert uri != null && !"".equals(uri) : "unknown vocabulary item "
                        + ps[i] + " in " + Arrays.toString(ps);
                log.debug("Fragment uri=" + uri);
                if (vocont.isPredicate(uri)) {
                    // ps[i] is a Predicate: swap it with ps[i-1]
                    int j = i - 1;
                    assert j >= 0 : "unexpected Predicate <" + uri
                            + "> at position " + i + " in "
                            + Arrays.toString(ps);
                    String tmp = rs[j];
                    rs[j] = ps[i];
                    rs[i] = tmp;
                } else if (vocont.isAction(uri)) {
                    // ps[i] is an Action: just copy it
                    rs[i] = ps[i];
                } else
                    assert false : "unexpected vocabulary item <" + uri + ">"
                            + " in " + Arrays.toString(ps);
            }
        }
        return rs;
    }

    // public static void main(String[] args) {
    // SimpleCNL4DSABuilder builder = new SimpleCNL4DSABuilder();
    // String cnl4dsae =
    // "after ?X_10:User Read ?X_11:NexusFile then ?X_12:System must Log ?X_13:Event";
    // System.out.println(builder.buildStatement(StatementType.OBLIGATION,
    // cnl4dsae));
    // cnl4dsae =
    // "if ?X_6:User hasRole ?X_7:BeamlineScientist then ?X_6 can Read ?X_8:NexusFile";
    // System.out.println(builder.buildStatement(StatementType.AUTHORIZATION,
    // cnl4dsae));
    // }

}
