package com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;


public class Util {
    public static String MapToString(Map<?,?> map, String alt){
    	String ret = alt;
    	if(map != null && map.size() > 0){
    		ret = "";
    		final Set keys = map.keySet();
    		if(keys != null && keys.size()>0){
        		for(Object key:keys){
        			ret += "'" +
        				safeToStringNull(key) + "->" + safeToStringNull(map.get(key))
        			+ "' ";
        		}
    		}
    	}
        return ret;
    }

    public static String listToString(List<?> list, String alt){
    	String ret = alt;
    	if(list != null && list.size() > 0){
    		ret = "";
    		for(Object s:list){
    			ret += "'" + safeToStringNull(s) + "' ";
    		}
    	}
        return ret;
    }

    public static String setToString(Set<?> set, String alt){
    	String ret = alt;
    	if(set != null && set.size() > 0){
    		ret = "";
    		for(Object s:set){
    			ret += "'" + safeToStringNull(s) + "' ";
    		}
    	}
        return ret;
    }



    public static final String safeToString(Object o, String alt) {
    	String ret = alt;
    	if(o != null && o.toString() != null){
    		ret = o.toString();
    	}
        return ret;
    }

    public static final String safeToStringNull(Object o) {
    	return Util.safeToString(o, "<null>");
    }

    public static boolean isEmpty(String buf) {
        if (buf == null || buf.length() == 0 || buf.trim().length() == 0){
    		return true;
    	}

    	return false;
    }
}
