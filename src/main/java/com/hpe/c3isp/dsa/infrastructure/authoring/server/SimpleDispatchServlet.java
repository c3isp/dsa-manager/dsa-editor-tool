/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa.CreateNewDsaActionHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa.FetchDsaActionHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.dsa.StoreDsaActionHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.trustmanager.GetContextProvidersActionHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.trustmanager.GetPartiesActionHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.GetActionsWithTermAsSubjectHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.GetAllObjectsForActionHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.GetAllTermsInRangeForPredicateHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.GetLeafActionsWithTermAsSubjectHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.GetLeafObjectsForActionHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.GetLeafTermsHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.GetNothingHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.GetObjectsForActionHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.GetPredicatesWithTermInDomainHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.GetSubTermsHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.GetTermsHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabulary.GetTermsInRangeForPredicateHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.vocabularylist.GetVocabulariesActionHandler;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.Config;

import net.customware.gwt.dispatch.client.standard.StandardDispatchService;
import net.customware.gwt.dispatch.server.DefaultActionHandlerRegistry;
import net.customware.gwt.dispatch.server.Dispatch;
import net.customware.gwt.dispatch.server.InstanceActionHandlerRegistry;
import net.customware.gwt.dispatch.server.SimpleDispatch;
import net.customware.gwt.dispatch.shared.Action;
import net.customware.gwt.dispatch.shared.DispatchException;
import net.customware.gwt.dispatch.shared.Result;


public class SimpleDispatchServlet extends RemoteServiceServlet implements
        StandardDispatchService {

    private static final long serialVersionUID = 1906522046823747705L;

    private Log logger = LogFactory.getLog(SimpleDispatchServlet.class);

    private Dispatch dispatch;

    public SimpleDispatchServlet() {
        super();

        //load property file


        InstanceActionHandlerRegistry registry = new DefaultActionHandlerRegistry();

        registry.addHandler(new GetNothingHandler());

        registry.addHandler(new GetActionsWithTermAsSubjectHandler());
        registry.addHandler(new GetLeafActionsWithTermAsSubjectHandler());

        registry.addHandler(new GetObjectsForActionHandler());
        registry.addHandler(new GetLeafObjectsForActionHandler());
        registry.addHandler(new GetAllObjectsForActionHandler());

        registry.addHandler(new GetPredicatesWithTermInDomainHandler());

        registry.addHandler(new GetSubTermsHandler());

        registry.addHandler(new GetTermsHandler());
        registry.addHandler(new GetLeafTermsHandler());

        registry.addHandler(new GetTermsInRangeForPredicateHandler());
        registry.addHandler(new GetAllTermsInRangeForPredicateHandler());

        registry.addHandler(new StoreDsaActionHandler());
        registry.addHandler(new CreateNewDsaActionHandler());
        registry.addHandler(new FetchDsaActionHandler());

        registry.addHandler(new GetPartiesActionHandler());
        registry.addHandler(new GetContextProvidersActionHandler());


        registry.addHandler(new GetVocabulariesActionHandler());


        dispatch = new SimpleDispatch(registry);


//        logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>.................... SimpleDispatchServlet"
//        		);
//
//        loadProperties();
    }



//    private void loadProperties() {
//    	Config.getInstance();
//    }


    @Override
    public Result execute(Action<?> action) throws DispatchException {
        try {
            return dispatch.execute(action);
        } catch (DispatchException e) {
            log("Exception while executing " + action.getClass().getName()
                    + ": " + e.getMessage(), e);
            throw e;
        }
    }


//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp)  throws ServletException, IOException {
//    	PrintWriter out = null;
//
//    	try {
//        	String prop = req.getParameter("q");
//        	out = resp.getWriter();
//        	out.print("req = " + prop);
//        	out.flush();
//        	out.close();
//    	} finally {
//    		if(out != null) {
//    			out.close();
//    		}
//    	}
//
////    	Random rnd = new Random();
////    	PrintWriter out = resp.getWriter();
////    	out.println('[');
////    	String[] stockSymbols = req.getParameter("q").split(" ");
////    	boolean firstSymbol = true;
////    	for (String stockSymbol : stockSymbols) {
////    		double price = rnd.nextDouble() * MAX_PRICE;
////    		double change = price * MAX_PRICE_CHANGE * (rnd.nextDouble() * 2f - 1f);
////    		if (firstSymbol) {
////    			firstSymbol = false;
////    		} else {
////    			out.println("  ,");
////    		}
////    		out.println("  {");
////    		out.print("    \"symbol\": \"");
////    		out.print(stockSymbol);
////    		out.println("\",");
////    		out.print("    \"price\": ");
////    		out.print(price);
////    		out.println(',');
////    		out.print("    \"change\": ");
////    		out.println(change);
////    		out.println("  }");
////    	}
////    	out.println(']');
////    	out.flush();
//    }


}
