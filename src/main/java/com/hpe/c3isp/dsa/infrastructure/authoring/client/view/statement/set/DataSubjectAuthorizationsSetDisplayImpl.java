/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.view.statement.set;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTMLTable.Cell;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.click.SelectedRowEvent;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.click.SelectedRowHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;

public class DataSubjectAuthorizationsSetDisplayImpl extends
        StatementSetDisplayImpl implements DataSubjectAuthorizationsSetDisplay {

    // we reuse the same ClickHandler for the link-target button of every
    // row, thus saving some memory
    private final ClickHandler targetLinktButtonClickHandler;

    private final DialogBox linkTools = new DialogBox(false, false);

    public DataSubjectAuthorizationsSetDisplayImpl() {
        super();
        initLinkTools();
        targetLinktButtonClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                final Cell clickedCell = theFlexTable.getCellForEvent(event);
                if (clickedCell != null) {
                    final int selected = clickedCell.getRowIndex();
                    SelectedRowEvent.fire(
                            DataSubjectAuthorizationsSetDisplayImpl.this,
                            selected);
                }
            }
        };
    }

    private void initLinkTools() {
        linkTools.setText("Link Tools");
        linkTools.setAnimationEnabled(true);
        linkTools.center();
        linkTools.hide();
        HorizontalPanel hp = new HorizontalPanel();
        linkTools.setWidget(hp);

        final Button noLinkButton = new Button("No Reference");
        hp.add(noLinkButton);
        noLinkButton.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                SelectedRowEvent.fire(
                        DataSubjectAuthorizationsSetDisplayImpl.this, -1);
            }
        });
    }

    @Override
    public HandlerRegistration addSelectedRowHandler(SelectedRowHandler handler) {
        return addHandler(handler, SelectedRowEvent.TYPE);
    }

    @Override
    protected void addLinkingButtons(int row) {
        final Button targetLinkButton = new Button("&lArr;");
        targetLinkButton.setTitle("Choose this statement as reference for "
                + "another statement");
        targetLinkButton.setVisible(false);
        targetLinkButton.addClickHandler(targetLinktButtonClickHandler);
        theFlexTable.setWidget(row, 3, targetLinkButton);
    }

    @Override
    public void showStatementLinkTools() {
        linkTools.show();
        setStatementLinkToolsVisible(true);
    }

    @Override
    public void hideStatementLinkTools() {
        linkTools.hide();
        setStatementLinkToolsVisible(false);
    }

    private void setStatementLinkToolsVisible(boolean visible) {
        for (int i = 0; i < theFlexTable.getRowCount(); i++) {
            Widget w = theFlexTable.getWidget(i, 3);
            assert w instanceof Button;
            ((Button) w).setVisible(visible);
        }
    }

    @Override
    protected void setSpecializedWidgetsEnabled(boolean enabled) {
        // we don't need to disable our target-link buttons, because they're
        // not visible, and if they're visible, then the user needs them!
    }

}
