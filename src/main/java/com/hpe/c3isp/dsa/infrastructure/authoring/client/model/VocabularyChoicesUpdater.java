/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.model;

import java.util.List;

import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Token;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.action.vocabulary.VocabularyGetter;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dispatch.result.vocabulary.VocabularyGetResult;

public class VocabularyChoicesUpdater {

    private final List<Token> tokens;
    private final VocabularyGetter<VocabularyGetResult> action;
    private VocabularyGetter<VocabularyGetResult> actionConstrainingReferenceToken = null;

    public VocabularyChoicesUpdater(List<Token> tokens,
            VocabularyGetter<VocabularyGetResult> action) {
        this.tokens = tokens;
        this.action = action;
    }

    public VocabularyChoicesUpdater(List<Token> tokens,
            VocabularyGetter<VocabularyGetResult> action,
            VocabularyGetter<VocabularyGetResult> constrainReferenceAction) {
        super();
        this.tokens = tokens;
        this.action = action;
        this.actionConstrainingReferenceToken = constrainReferenceAction;
    }

    public List<Token> getTokens() {
        return tokens;
    }

    public VocabularyGetter<VocabularyGetResult> getAction() {
        return action;
    }

    public VocabularyGetter<VocabularyGetResult> getActionConstrainingReferenceToken() {
        return actionConstrainingReferenceToken;
    }

    public boolean hasActionConstrainingReferenceToken() {
        return actionConstrainingReferenceToken != null;
    }
}
