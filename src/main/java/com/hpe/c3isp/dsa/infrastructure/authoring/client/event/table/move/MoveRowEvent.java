/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.move;

import com.google.gwt.event.shared.GwtEvent;

public class MoveRowEvent extends GwtEvent<MoveRowHandler> {

    public enum Direction {
        UP, DOWN
    };

    public static Type<MoveRowHandler> TYPE = new Type<MoveRowHandler>();

    private final Direction direction;

//    private final CheckBox protectedCheckBox;
//
//    private final CheckBox isPendingRuleCheckBox;
//
//    private final Label statementInfo;
//
//    private final TextArea inputValue;

    public MoveRowEvent(Direction direction){//, CheckBox protectedCheckBox, CheckBox isPendingRuleCheckBox, Label statementInfo, TextArea inputValue) {
        super();
        this.direction = direction;
//        this.protectedCheckBox = protectedCheckBox;
//        this.isPendingRuleCheckBox = isPendingRuleCheckBox;
//        this.statementInfo = statementInfo;
//        this.inputValue = inputValue;
    }

    public static void fire(HasMoveRowHandlers source, Direction direction){//, CheckBox protectedCheckBox, CheckBox isPendingRuleCheckBox, Label statementInfo, TextArea inputValue) {
        MoveRowEvent event = new MoveRowEvent(direction);//, protectedCheckBox, isPendingRuleCheckBox, statementInfo, inputValue);
        source.fireEvent(event);
    }

    @Override
    protected void dispatch(MoveRowHandler handler) {
        handler.onMoveRow(direction);// protectedCheckBox, isPendingRuleCheckBox, statementInfo, inputValue);
    }

    @Override
    public Type<MoveRowHandler> getAssociatedType() {
        return TYPE;
    }

}
