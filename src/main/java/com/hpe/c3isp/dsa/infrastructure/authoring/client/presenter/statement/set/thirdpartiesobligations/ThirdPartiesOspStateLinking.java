/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.statement.set.thirdpartiesobligations;

import java.util.HashMap;

import com.google.gwt.user.client.Timer;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.Mode;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.event.table.move.MoveRowEvent.Direction;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.StatementMetadata;

public class ThirdPartiesOspStateLinking implements ThirdPartiesOspState {

    @Override
    public void handleSelectedRowEvent(ThirdPartiesOspContext context, int row) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void addStatement(ThirdPartiesOspContext context) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleDeleteRowEvent(ThirdPartiesOspContext context,
            int rowIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleDisableRowMovementEvent(ThirdPartiesOspContext context) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleEnableRowMovementEvent(ThirdPartiesOspContext context,
            int rowIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleMoveRowEvent(ThirdPartiesOspContext context,
            Direction direction){//, CheckBox protectedCheckBox, CheckBox isPendingRuleCheckBox, Label statementInfo, TextArea inputValue) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleStatementBrokenEvent(ThirdPartiesOspContext context,
            int statementIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleStatementCompletedEvent(ThirdPartiesOspContext context,
            int statementIndex, boolean isEnabled, boolean isEndUser, HashMap<String,String> params){
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleLinkToTargetStatementEvent(
            final ThirdPartiesOspContext context,
            final StatementMetadata source, StatementMetadata target) {
        Timer timer = new Timer() {

            @Override
            public void run() {
                context.getDisplay().highlightStatement(source.getIndex(),
                        Mode.OFF);
                context.setState(new ThirdPartiesOspStateNormal());
            }
        };
        timer.schedule(500);
    }

    @Override
    public void handleCompleteMetadataEvent(ThirdPartiesOspContext context,
            int rowIndex, boolean isEnabled, boolean isEndUser, HashMap<String,String> params) {
        throw new UnsupportedOperationException();

    }

}
