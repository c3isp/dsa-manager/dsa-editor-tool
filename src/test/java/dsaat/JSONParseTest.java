package dsaat;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Base64;
import java.util.Map;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

import org.apache.commons.io.IOUtils;
import org.apache.xmlbeans.XmlException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.hpe.c3isp.dsa.infrastructure.authoring.shared.Config;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.Config.JConf;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.Config.Props;

import eu.cocoCloud.dsa.DsaDocument;

public class JSONParseTest {
//	config server
//	dsamgrc3isp.iit.cnr.it
//	ssh user dsamgrc3isp/dsac3is17
//	curl -u configServer:password http://localhost:8888/dsa-editor/testbed,rest
//		// per vedere i log delle chiamate
//		//dsamgrc3isp@dsamgrc3isp:~$ journalctl -fu ConfigServer.service
//	 -Dspring.profiles.active (il valore dovrebbe essere -Dspring.profiles.active=testbed,rest)



//
    public static void main(String[] args) throws IOException {

//    	parseJsonConf();

    	Config.getRemoteConf(url, username, password);
//    	readRemoteConf(url, username, password);
    }


    public static void parseJsonConf() throws IOException {
    	String json = new String(Files.readAllBytes(Paths.get("backup/resp.json")));

    	System.out.println(json);
    	System.out.println("--------------------");

    	JConf jc = JConf.fromJsonString(json);

    	if(jc != null && jc.propertySources != null && jc.propertySources.size() > 0) {
    		Props props = jc.propertySources.get(0);
    		if(props != null && props.source != null && props.source.size() > 0) {
    			Map<String, String> m = props.source;
    			System.out.println("Parsed properties");
    			for(String k:m.keySet()) {
    				String v = m.get(k);
    				System.out.println(String.format("%s ---> %s",
    						k, v == null?"no value found ":v));
    			}
    		}
    	}
    }


    final static String url = "http://localhost:8888/dsa-editor/testbed,rest";
    final static String username = "configServer";
    final static String password = "3fbc39fa-cc61-4f1b-a7a5-2b58f972fee7";

    public static void readRemoteConf(String url, String username, String password) {
    	 String auth = username + ":" + password;
         byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(Charset.forName("US-ASCII")) );
         String authHeader = "Basic " + new String( encodedAuth );

//        DsaDocument dsaDocument = null;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add( new ByteArrayHttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
//        headers.setAccept(Arrays.asList(MediaType.APPLICATION_XML));
        headers.set( "Authorization", authHeader );

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        ResponseEntity<byte[]> response = restTemplate.exchange(url, HttpMethod.GET, entity, byte[].class);
        HttpStatus status = response.getStatusCode();

        if (status == HttpStatus.OK){
            try {
               String documentContent = IOUtils.toString(response.getBody(), "UTF-8");
               System.out.println(documentContent);
            } catch (Exception e) {
                System.out.println("Error occurred. ");
                e.printStackTrace();
            }
        }
        else {
            System.out.println("Error, http response code = " + status);
        }
    }



}
