/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.client.view.vocabulary;

import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.presenter.vocabulary.VocabularyPresenter.VocabularyDisplay;
import com.hpe.c3isp.dsa.infrastructure.authoring.client.util.MyDialog;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItem;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.SyntacticItemTreeNode;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.cnl4dsa.Tokens;

public class VocabularyDisplayImpl extends DialogBox implements
        VocabularyDisplay {

	private static final String title = "Select one of the following choices";
    private TreeItem root = null;

    @Inject
    public VocabularyDisplayImpl() {
        super(false, false);
//        super(true, true);
        setAnimationEnabled(true);
        center();
        setText(title);
//      setText("Select one of the following choices");

//        Button close = new Button("Close");
//        close.addClickHandler(new ClickHandler() {
//            public void onClick(ClickEvent event) {
////                MyDialog.this.hide();
//            }
//        });
//        close.setStyleName("TopRight");


//        Image closeButton = new Image("");
//        closeButton.addClickHandler(new ClickHandler() {
//           public void onClick(ClickEvent event) {
////              registerBox.hide();
//           }
//        });
//        closeButton.setStyleName("TopRight");

    }

    @Override
    public Widget asWidget() {
        return this;
    }

    @Override
    public HandlerRegistration addSelectionHandler(
            SelectionHandler<SyntacticItem> handler) {
        return addHandler(handler, SelectionEvent.getType());
    }

    @Override
    public HandlerRegistration addOpenHandler(OpenHandler<SyntacticItem> handler) {
        return addHandler(handler, OpenEvent.getType());
    }

    private void updateMaincontent(Widget w) {
        clear();
        setWidget(w);
    }

    @Override
    public void show(List<SyntacticItemTreeNode> itemNodes) {
        clear();
        setWidget(buildTree(itemNodes));
        if (!isShowing()) {
            show();
        }
    }

    @Override
    public void errorWhileFetchingData(String message) {
        clear();
        setWidget(new Label(message));
        if (!isShowing()) {
            show();
        }
    }

    @Override
    public void errorWhileFetchingData(String message, SyntacticItem rootTerm) {
        TreeItem rootTreeItem = searchTree(root, rootTerm);
        assert rootTreeItem != null;
        rootTreeItem.removeItems();
        rootTreeItem.setText(message);
        if (!isShowing()) {
            show();
        }
    }

    @Override
    public void startFetchingData() {
        clear();
        setWidget(new Label("fetching data ..."));
        if (!isShowing()) {
            show();
        }
    }

    @Override
    public void startFetchingData(SyntacticItem rootTerm) {
        TreeItem rootTreeItem = searchTree(root, rootTerm);
        assert rootTreeItem != null;
        rootTreeItem.setText("fetching data for " + rootTerm.getHumanForm());
        if (!isShowing()) {
            show();
        }
    }

    @Override
    public void showSubTerms(SyntacticItem rootTerm,
            List<SyntacticItemTreeNode> itemNodes) {
        TreeItem rootTreeItem = searchTree(root, rootTerm);
        assert rootTreeItem != null;
        rootTreeItem.setText(rootTerm.getHumanForm());
        rootTreeItem.setUserObject(rootTerm);
        updateTree(rootTreeItem, itemNodes);
        rootTreeItem.setState(true);
        if (!isShowing()) {
            show();
        }
    }

    private Tree buildTree(List<SyntacticItemTreeNode> itemNodes) {
        Tree tree = new Tree();
        tree.setAnimationEnabled(true);
        root = new TreeItem();
        root.setText("available choices ... ");
        root.setUserObject(Tokens.FAKE_TOKEN_HAS_SUBTERMS);
        tree.addItem(root);
        updateTree(root, itemNodes);
        // NOTE: we MUST call setState(true) for having the root node open, i.e.
        // displaying its children. However, due to a bug, we must call it only
        // after the tree is fully constructed, otherwise it does not work (see
        // http://code.google.com/p/google-web-toolkit/issues/detail?id=1187
        root.setState(true);
        tree.addSelectionHandler(new SelectionHandler<TreeItem>() {

            @Override
            public void onSelection(SelectionEvent<TreeItem> event) {
                // if the selected item has no parent, then the selected item
                // is the root of our Tree, and we don't want to fire an
                // event for it
                if (event.getSelectedItem().getParentItem() != null) {
                    Object o = event.getSelectedItem().getUserObject();
                    assert o instanceof SyntacticItem;
                    SelectionEvent.fire(VocabularyDisplayImpl.this,
                            (SyntacticItem) o);
                }
            }
        });
        tree.addOpenHandler(new OpenHandler<TreeItem>() {

            @Override
            public void onOpen(OpenEvent<TreeItem> event) {
                // if the node being opened is the root, then we don't want to
                // fire an OpenEvent
                if (event.getTarget().equals(root)) {
                    return;
                }
                Object o = event.getTarget().getUserObject();
                assert o instanceof SyntacticItem;
                SyntacticItem openedSyntacticItem = (SyntacticItem) o;
                TreeItem openedItem = event.getTarget();
                assert openedItem.getChildCount() > 0;
                TreeItem firstChild = openedItem.getChild(0);
                assert firstChild.getUserObject() != null;
                // if the first child of the opened node does not contain the
                // fake-token-more, than we don't want to fire an OpenEvent
                if (!firstChild.getUserObject().equals(
                        Tokens.FAKE_TOKEN_HAS_SUBTERMS)) {
                    return;
                }
                // if we get down here, than it means that the user has just
                // opened a node whose sub-nodes have not been fetched yet, and
                // so we fire an OpenEvent for our Presenter
                OpenEvent.fire(VocabularyDisplayImpl.this, openedSyntacticItem);
            }
        });
        return tree;
    }

    private void updateTree(TreeItem rootTreeItem,
            List<SyntacticItemTreeNode> itemNodes) {
        assert itemNodes.size() > 0;
        rootTreeItem.removeItems();
        for (SyntacticItemTreeNode sitn : itemNodes) {
            SyntacticItem synit = sitn.getSyntacticItem();
            TreeItem ti = new TreeItem();
            ti.setText(synit.getHumanForm());
            ti.setUserObject(synit);
            rootTreeItem.addItem(ti);
            if (sitn.hasChildren()) {
                TreeItem more = new TreeItem();
                more.setText(Tokens.FAKE_TOKEN_HAS_SUBTERMS.getHumanForm());
                more.setUserObject(Tokens.FAKE_TOKEN_HAS_SUBTERMS);
                ti.addItem(more);
            }
        }
    }

    private TreeItem searchTree(TreeItem root, SyntacticItem rootTerm) {
        Object o = root.getUserObject();
        assert o instanceof SyntacticItem;
        SyntacticItem synit = (SyntacticItem) o;
        if (synit.equals(rootTerm))
            return root;
        for (int i = 0; i < root.getChildCount(); i++) {
            TreeItem ti = searchTree(root.getChild(i), rootTerm);
            if (ti != null)
                return ti;
        }
        return null;
    }

}
