/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa;

import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.roles.Roles;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.statement.Statement;

public class DsaBean implements IsSerializable {

    private String uid;
    public String version;
    private String vocabularyUri;
    private String title;
    private String status;
    private String dataClassification;
    private Validity validity;
    private Credentials credentials;
    private Roles roles;
    private List<Organization> parties;
    private List<Statement> authorizations;
    private List<Statement> dataSubjectAuthorizations;
    private List<Statement> obligations;
    private List<Statement> prohibitions;
    // Third parties
    private List<Statement> thirdPartiesAuthorizations;
    private List<Statement> thirdPartiesObligations;
    private List<Statement> thirdPartiesProhibitions;
    private String governingLaw;
    private String indemnities;
    private String purpose;
    private String keyEncryptionSchema;
    private boolean userConsent;
    private boolean requiredConsent;
    private String description;
    private ComplexPolicy expirationPolicy;
    private ComplexPolicy updatePolicy;
    private ComplexPolicy revocationPolicy;
    
    private HashMap<String,String> paramTermList;
    

    // for serialization
    protected DsaBean() {
    }

    
    public String getVersion() {
        return version;
    }


    public void setVersion(String version) {
        this.version = version;
    }


    public List<Statement> getAuthorizations() {
        return authorizations;
    }

    public void setAuthorizations(List<Statement> authorizations) {
        this.authorizations = authorizations;
    }

    public List<Statement> getObligations() {
        return obligations;
    }

    public void setObligations(List<Statement> obligations) {
        this.obligations = obligations;
    }

    public List<Statement> getProhibitions() {
        return prohibitions;
    }

    public void setProhibitions(List<Statement> prohibitions) {
        this.prohibitions = prohibitions;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Validity getValidity() {
        return validity;
    }

    public void setValidity(Validity validityBean) {
        this.validity = validityBean;
    }

    public String getVocabularyUri() {
        return vocabularyUri;
    }

    public void setVocabularyUri(String vocabularyUri) {
        this.vocabularyUri = vocabularyUri;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Organization> getParties() {
        return parties;
    }

    public void setParties(List<Organization> parties) {
        this.parties = parties;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public Roles getRoles() {
        return roles;
    }

    public void setRoles(Roles roles) {
        this.roles = roles;
    }

    public String getDataClassification() {
        return dataClassification;
    }

    public void setDataClassification(String dataClassification) {
        this.dataClassification = dataClassification;
    }

    public String getGoverningLaw() {
        return governingLaw;
    }

    public void setGoverningLaw(String governingLaw) {
        this.governingLaw = governingLaw;
    }

    public String getIndemnities() {
        return indemnities;
    }

    public void setIndemnities(String indemnities) {
        this.indemnities = indemnities;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public List<Statement> getDataSubjectAuthorizations() {
        return dataSubjectAuthorizations;
    }

    public void setDataSubjectAuthorizations(
            List<Statement> dataSubjectAuthorizations) {
        this.dataSubjectAuthorizations = dataSubjectAuthorizations;
    }

    public List<Statement> getThirdPartiesAuthorizations() {
        return thirdPartiesAuthorizations;
    }

    public void setThirdPartiesAuthorizations(
            List<Statement> thirdPartiesAuthorizations) {
        this.thirdPartiesAuthorizations = thirdPartiesAuthorizations;
    }

    public List<Statement> getThirdPartiesObligations() {
        return thirdPartiesObligations;
    }

    public void setThirdPartiesObligations(
            List<Statement> thirdPartiesObligations) {
        this.thirdPartiesObligations = thirdPartiesObligations;
    }

    public List<Statement> getThirdPartiesProhibitions() {
        return thirdPartiesProhibitions;
    }

    public void setThirdPartiesProhibitions(
            List<Statement> thirdPartiesProhibitions) {
        this.thirdPartiesProhibitions = thirdPartiesProhibitions;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
   public String getKeyEncryptionSchema() {
        return keyEncryptionSchema;
    }

    public void setKeyEncryptionSchema(String keyEncryptionSchema) {
        this.keyEncryptionSchema = keyEncryptionSchema;
    }

    public boolean isUserConsent() {
        return userConsent;
    }

    public void setUserConsent(boolean userConsent) {
        this.userConsent = userConsent;
    }

    public boolean isRequiredConsent() {
        return requiredConsent;
    }

    public void setRequiredConsent(boolean requiredConsent) {
        this.requiredConsent = requiredConsent;
    }

    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ComplexPolicy getExpirationPolicy() {
        return expirationPolicy;
    }

    public void setExpirationPolicy(ComplexPolicy expirationPolicy) {
        this.expirationPolicy = expirationPolicy;
    }

    public ComplexPolicy getUpdatePolicy() {
        return updatePolicy;
    }

    public void setUpdatePolicy(ComplexPolicy updatePolicy) {
        this.updatePolicy = updatePolicy;
    }

    public ComplexPolicy getRevocationPolicy() {
        return revocationPolicy;
    }

    public void setRevocationPolicy(ComplexPolicy revocationPolicy) {
        this.revocationPolicy = revocationPolicy;
    }
    
    public HashMap<String,String> getParamTermList() {
        return paramTermList;
    }

    public void setParamTermList(HashMap<String,String> paramTermList) {
        this.paramTermList = paramTermList;
    }


    @Override
    public String toString() {
        return "DsaBean [uid=" + uid + ", version=" + version
                + ", vocabularyUri=" + vocabularyUri + ", title=" + title
                + ", status=" + status + ", dataClassification="
                + dataClassification + ", validity=" + validity
                + ", credentials=" + credentials + ", roles=" + roles
                + ", parties=" + parties + ", authorizations=" + authorizations
                + ", dataSubjectAuthorizations=" + dataSubjectAuthorizations
                + ", obligations=" + obligations + ", prohibitions="
                + prohibitions + ", thirdPartiesAuthorizations="
                + thirdPartiesAuthorizations + ", thirdPartiesObligations="
                + thirdPartiesObligations + ", thirdPartiesProhibitions="
                + thirdPartiesProhibitions + ", governingLaw=" + governingLaw
                + ", indemnities=" + indemnities + ", purpose=" + purpose
                + ", keyEncryptionSchema=" + keyEncryptionSchema
                + ", userConsent=" + userConsent + ", requiredConsent="
                + requiredConsent + ", description=" + description
                + ", expirationPolicy=" + expirationPolicy + ", updatePolicy="
                + updatePolicy + ", revocationPolicy=" + revocationPolicy
                + ", paramTermList=" + paramTermList + "]";
    }
   
}
