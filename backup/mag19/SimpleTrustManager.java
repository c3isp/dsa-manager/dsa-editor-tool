/**
 *  Copyright 2007-2015 Hewlett-Packard Development Company, L.P.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hpe.c3isp.dsa.infrastructure.authoring.server.handler.trustmanager.helper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;

import com.hpe.c3isp.dsa.infrastructure.authoring.shared.Config;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Organization;
import com.hpe.c3isp.dsa.infrastructure.authoring.shared.dsa.Util;

public class SimpleTrustManager {

//	final String propertiesFile = "etc/config/trust-manager.properties";
    private static SimpleTrustManager THE_INSTANCE = null;
    private static final Log log = LogFactory.getLog(SimpleTrustManager.class);

    private List<Organization> parties = new ArrayList<Organization>();
    private List<Organization> trustedProviders = new ArrayList<Organization>();
    private String encryptionKeySchema = "";


    public static SimpleTrustManager getInstance() {
        if (THE_INSTANCE == null)
            THE_INSTANCE = new SimpleTrustManager();
        return THE_INSTANCE;
    }

//    private SimpleTrustManager() {
//        Properties properties = new Properties();
//        InputStream is = getClass().getClassLoader().getResourceAsStream(
//                propertiesFile);
//        if (is != null) {
//            try {
//
//                properties.load(is);
//                if (properties.containsKey("encryption-key-schema")) {
//                    encryptionKeySchema = properties.getProperty("encryption-key-schema");
//                    log.info("using encryption-key-schema: " + encryptionKeySchema);
//                } else{
//                    log.error("cannot find property encryption-key-schema");
//                }
//                loadList(properties, "parties", parties);
//                loadList(properties, "context-providers", trustedProviders);
//
//            } catch (IOException e) {
//                log.error("cannot read " + propertiesFile, e);
//            }
//            try {
//                is.close();
//            } catch (IOException e) {
//                log.error("cannot close InputStream attached to "
//                        + propertiesFile);
//            }
//        } else {
//            log.error("cannot find " + propertiesFile);
//        }
//
//    }


    private SimpleTrustManager() {
    	this.parties = loadOrganizations(Config.parties);
    	this.trustedProviders = loadOrganizations(Config.context_providers);
    	this.encryptionKeySchema = Config.getInstance().getProperty(Config.encryptionKeySchema);

    	if(this.parties.size() == 0) {
    		log.error("Configuration problem, property not loaded: " + Config.parties);
    	}else {
    		log.info("Configuration, parties available: " + parties.size());
    	}

    	if(this.trustedProviders.size() == 0) {
    		log.error("Configuration problem, property not loaded: " + Config.context_providers);
    	}else {
    		log.info("Configuration, trustedProviders available: " + this.trustedProviders.size());
    	}
    	log.info("Configuration, encryptionKeySchema available: " + this.encryptionKeySchema);


//        Properties properties = new Properties();
//        InputStream is = getClass().getClassLoader().getResourceAsStream(
//                propertiesFile);
//        if (is != null) {
//            try {
//
//                properties.load(is);
//                if (properties.containsKey("encryption-key-schema")) {
//                    encryptionKeySchema = properties.getProperty("encryption-key-schema");
//                    log.info("using encryption-key-schema: " + encryptionKeySchema);
//                } else{
//                    log.error("cannot find property encryption-key-schema");
//                }
//                loadList(properties, "parties", parties);
//                loadList(properties, "context-providers", trustedProviders);
//
//            } catch (IOException e) {
//                log.error("cannot read " + propertiesFile, e);
//            }
//            try {
//                is.close();
//            } catch (IOException e) {
//                log.error("cannot close InputStream attached to "
//                        + propertiesFile);
//            }
//        } else {
//            log.error("cannot find " + propertiesFile);
//        }

    }




    private List<Organization> loadOrganizations(String key) {
    	List<Organization> ret = new ArrayList<>();
    	String orgsStr = Config.getInstance().getProperty(key);
    	if(Util.isEmpty(orgsStr)) {
    		log.error("Configuration problem, property not found: " + key);
    	}else {
    		String[] orgs = orgsStr.split(";");
    		if(orgs == null || orgs.length == 0) {
    			log.error("Configuration problem, property malformed: " + key);
    		}else {
                for (String orgStr : orgs) {
                    String orgData[] = orgStr.split(",");
                    if(orgData == null || orgData.length != 2) {
                    	log.error("Configuration problem, bad organization data: " + key);
                    }else {
                    	final String uid = orgData[0];
                    	final String name = orgData[1];
                    	ret.add(new Organization(uid, name, "", ""));
                    }
                }
    		}

    	}
    	return ret;
    }


    private void loadList(Properties properties, String key, List<Organization> list) {

    	if (properties.containsKey(key)) {
            String orgsStr = properties.getProperty(key);
            String[] orgs = orgsStr.split(";");
            assert orgs != null && orgs.length > 0 : "missing parties";
            for (String orgStr : orgs) {
                String orgData[] = orgStr.split(",");
                assert orgData != null && orgData.length == 2 : "bad organization data";
                list.add(new Organization(orgData[0], orgData[1], "", ""));
            }
        } else {
            log.error("cannot find property " + key);
        }
    }



    public List<Organization> getParties() {
        List<Organization> result = new ArrayList<Organization>(parties.size());
        result.addAll(parties);
        return result;
    }

    public List<Organization> getTrustedProviders() {
        List<Organization> result = new ArrayList<Organization>(
                trustedProviders.size());
        result.addAll(trustedProviders);
        return result;
    }

    public Organization getTrustedProvider(String uid) {
        for (Organization org : trustedProviders)
            if (uid.equals(org.getUid()))
                return org;
        return null;
    }

    public Organization getTrustedProviderFromName(String name) {
        for (Organization org : trustedProviders)
            if (name.equals(org.getName()))
                return org;
        return null;
    }

    public String getEncryptionKeySchema() {
        return encryptionKeySchema;
    }


}
