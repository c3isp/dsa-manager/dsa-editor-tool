@echo off

set TOMCAT_HOME=C:\marco\dev\apache-tomcat-8.5.29
set LOCAL_WAR=C:\marco\dev\eclipse-ws\dsaat\target\DSAAuthoringTool.war
set LOCAL_VOCABULARIES=C:\marco\dev\eclipse-ws\dsaat\src\main\resources\vocabularies_local

CALL "%TOMCAT_HOME%\bin\shutdown.bat"
DEL /Q "%TOMCAT_HOME%\logs\*"
rem if exist "%TOMCAT_HOME%\webapps\DSAAuthoringTool.war" DEL "%TOMCAT_HOME%\webapps\DSAAuthoringTool.war"
rem if exist "%TOMCAT_HOME%\webapps\DSAAuthoringTool" RMDIR /S /Q "%TOMCAT_HOME%\webapps\DSAAuthoringTool"
call mvn clean package -P local
rem XCOPY "%LOCAL_WAR%" "%TOMCAT_HOME%\webapps\"

rem copy vocabularies
if exist "%TOMCAT_HOME%\webapps\vocabularies" RMDIR /S /Q "%TOMCAT_HOME%\webapps\vocabularies"
MKDIR "%TOMCAT_HOME%\webapps\vocabularies"
XCOPY "%LOCAL_VOCABULARIES%\upper_vocabulary.owl" "%TOMCAT_HOME%\webapps\vocabularies"
XCOPY "%LOCAL_VOCABULARIES%\c3isp_vocabulary_3.0.owl" "%TOMCAT_HOME%\webapps\vocabularies"

rem create dsaRepository folder
rem if exist "%TOMCAT_HOME%\webapps\dsaRepository" RMDIR /S /Q "%TOMCAT_HOME%\webapps\dsaRepository"
if NOT exist "%TOMCAT_HOME%\webapps\dsaRepository" MKDIR "%TOMCAT_HOME%\webapps\dsaRepository"


rem call "%TOMCAT_HOME%\bin\startup.bat"

@pause








rem call "C:\apache-tomcat-8.0.50\bin\shutdown.bat"
rem DEL /Q "c:\apache-tomcat-8.0.50\logs\*"
rem DEL "C:\apache-tomcat-8.0.50\webapps\DSAAuthoringTool.war"
rem RMDIR /S /Q "C:\apache-tomcat-8.0.50\webapps\DSAAuthoringTool"
rem call mvn -Dmaven.wagon.http.ssl.insecure=true -Dmaven.wagon.http.ssl.allowall=true clean install -P local
rem XCOPY "C:\Users\gambardc\git\dsaat\target\DSAAuthoringTool.war" "C:\apache-tomcat-8.0.50\webapps\"
call "C:\apache-tomcat-8.0.50\bin\startup.bat"
