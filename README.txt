/**
*  Copyright 2007-2017 Hewlett Packard Enterprise Development Company, L.P.
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/

Data Sharing Agreement Manager
=============================================

Changelog

- v1.0.0 - alfa release.


Vocabularies
============
There is an assembly goal that creates a vocabularies.war which is installed in /vocabularies at deployment time.
The vocabularies.war contains the owl files, filtered as per the maven profile used, i.e.:
- mvn assembly:single -P test
  This generates the vocabularies for the test environment, e.g. it writes the testcocodsa.iit.cnr.it as the URL used in the OWL files
- mvn assembly:single -P local
  This generates the vocabularies for the local development, e.g. it writes the localhost:8080 as the URL used in the OWL files
Check pom.xml, <profiles> section, for further details.







Local build
==================
pom > Run as Maven build; goal = package; Profile=local
war in /target/

Local deploy
==================
deploy.bat (edit customize for local environment)
ensure in local tomcat exist folders:
 - webapps/dsaRepository
 - webapps/vocabularies
               /c3isp_vocabulary_3.0.owl
               /upper_vocabulary.owl

Local debug via GWT Dev Mode
============================
Run as > GWT Dev Mode with jetty.
App search for local vocabulary files via HTTP Url, so run
an apache or tomcat is locally serving the
local vocabularies in vocabularies/ folder at the urls specified
in property "availableVocabularyList" in file
src\main\java\com\hpe\c3isp\dsa\infrastructure\authoring\client\DsaAuthoringToolMessages.properties

Production build
================
remeber to edit DSAAuthoringTool.gwt.xml and modify for production deploy
enabling compile of different localizations 



